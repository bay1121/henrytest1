# Arrow #

## Overview ##
ARROW helps Campaign Managers understand and target market segments most relevant to their advertising campaigns minimizing cost and maximizing impact.

The arrow project is separated into two parts: Data Processing and Syndication. Data processing code is in `prod/arrow_analytics` and Syndication code is in `prod/syndication`. _Please see the `README.md` in these projects to get specific information regarding them._

### Data Processing ###
* Takes care of ingesting data set from S3, Redshift, etc. to an HDFS cache, calculating enrichment sets, enriching events, and egressing to ElasticSearch 
* At this time these analytics are batch and built with maven
* Analytic scheduling is handled by oozie
### Syndication ###
* Handles export of data from Arrow/ElasticSearch by campaign and audience to Bluekai and S3 
* These are _streaming_ analytics
* Analytics are run manually via spark submit

In addition to scala analytics, there are a series of watchers in ElasticSearch which support dataset and analytic monitoring   

## Useful Links ##
 
* High Level Architecture: https://wbdatasquad.atlassian.net/wiki/spaces/CAMP/pages/2293769/ARROW+High+Level+Architecture
* Jira: https://wbdatasquad.atlassian.net/secure/RapidBoard.jspa?rapidView=154&view=planning
* Confluence: https://wbdatasquad.atlassian.net/wiki/spaces/CAMP/overview
* Bitbucket: https://bitbucket.org/account/user/wbcidata/projects/ARROW
* Analytics: https://bitbucket.org/wbcidata/arrow-analytics
* Application: https://bitbucket.org/wbcidata/arrow-application
* Orchestration: https://bitbucket.org/wbcidata/arrow-orchestration


## BUILD RELEASE PACKAGE ##
```
mvn --batch-mode release:prepare release:perform
```

## Environments

### Production ###
 
* Ambari - http://ambari_bastion.bash.bane.cmdt.wb.com:8080/#/login (kerberos cred)
* Spark - http://nn1.bash.bane.cmdt.wb.com:8088/cluster/apps
* Arrow - https://arrow.wb.com/
 
### Development ###
 
* Ambari - http://ambari_bastion.bade.bane.cmdt.wb.com:8080/#/login (kerberos cred)
* Spark - http://nn1.bash.bane.cmdt.wb.com:8088/cluster/apps
* Arrow - https://10.176.29.5:9002/

## S3 ##
We use S3 as a cloud object store for reading and writing data 

### Reading 
An easy way to interact with it for testing purposes is via:
```
aws s3 ls s3://bucket
```

### Reading S3 Alternative
You can go through hadoop to read S3 like this:
```
hadoop fs -D fs.s3a.enableServerSideEncryption=true -D fs.s3a.server-side-encryption-algorithm=AES256 -D fs.s3a.security.credential.provider.path=jceks://hdfs/arrow/hadoop/resources/arrow.jceks -ls s3a://cmdt-application/arrow/export-list/bluekai-mobile-ids/RPO-LATAM/wbCampaign/email/1p-StanleyKubrick-Interest-FLX/20180701
```

## Redshift ##
We load data from Redshift every night. You can go through the redshift data using a command like this:
```
psql -h bane-red-cidw.ca6ovdu82zsz.us-west-2.redshift.amazonaws.com -U svc_bane_hadoop -d cidw -p 5439
```
After providing the password for the service account listed after the `-U`, you will be able to explore the redshift tables. Our current jdbc urls are in properties under the key `jdbc.url`. Some useful commands are below:
```
// Show table
cidw=> \dt ods_madw.ma_user

// Describe table
cidw=> \d+ ods_madw.ma_user

// Any psql query
select count(*) from ods_madw.ma_user where dw_current_flag = 'Y';
```

## Kafka ##

### Blessed Commands For Kafka ### 
```
kinit arrow-analytic -k -t /home/arrow-analytic/auth/arrow-analytics.keytab
cat > key.conf <<EOL
KafkaClient {
 com.sun.security.auth.module.Krb5LoginModule required
 useTicketCache=true
 serviceName="kafka"
 client=true
 doNotPrompt=true;
};
Client {
 com.sun.security.auth.module.Krb5LoginModule required
 useTicketCache=true
 serviceName="zookeeper"
 client=true
 doNotPrompt=true;
};
EOL
export KAFKA_OPTS=-Djava.security.auth.login.config=/home/arrow-analytic/key.conf
export zookeeper=nn1.bade.bane.cmdt.wb.com:2181,nn2.bade.bane.cmdt.wb.com:2181,dn101.bade.bane.cmdt.wb.com:2181
/usr/hdp/2.6.1.0-129/kafka/bin/kafka-topics.sh --zookeeper $zookeeper --list
/usr/hdp/current/kafka-broker/bin/kafka-console-consumer.sh --topic wb-arrow-aws-2 --from-beginning --zookeeper $zookeeper --security-protocol PLAINTEXTSASL
```
An awesome resource: https://community.hortonworks.com/questions/31182/how-to-run-kafka-with-kerberos.html

### Reading Kafka for Debugging ###
In order to read Kafka, you can use the console consumer. I like to redirect this to a file, by tacking on the redirect at end. _Please note that you have to manually ctrl-c for the consumer to stop. I like to wait a minute or so before doing that._
```
/usr/hdp/current/kafka-broker/bin/kafka-console-consumer.sh --topic wb-arrow-aws-2 --from-beginning --zookeeper nn1.bash.bane.cmdt.wb.com:2181,nn2.bash.bane.cmdt.wb.com:2181,dn101.bash.bane.cmdt.wb.com:2181 --security-protocol PLAINTEXTSASL > messages.jsonl
```

At this point, I like to throw the file into spark so I can sort it by date.
```
SPARK_MAJOR_VERSION=2 spark-shell
val messages = spark.read.json("file:///home/arrow-analytic/wb-arrow-aws.jsonl")
messages.withColumn("timestamp", from_unixtime(col("timestamp")/1000)).sort(col("timestamp")).show
```

### Helpful Kafka Commands ###
```
export KAFKA_OPTS="-Djava.security.auth.login.config=/etc/kafka/conf/kafka_client_jaas.conf"
./kafka-console-consumer.sh --topic TOPIC_NAME --from-beginning --zookeeper ZOOKEEPER_HOST:ZOOKEEPER_PORT --security-protocol PLAINTEXTSASL
./kafka-console-producer.sh --topic TOPIC_NAME --broker-list BROKER_HOST:BROKER_PORT --security-protocol PLAINTEXTSASL
```

## Airflow ##
### Helpful Airflow Commands ###
```
# The arrow airflow dags are arrow_bluekai, arrow_metadata, arrow_historical
airflow list_dags

# To run a dag
airflow trigger_dag <dag_name>

```

## Contribution Guidelines ##
### Git Process
All tasks should be completed in a branch off of dev and merged in to the dev branch after testing is complete. Before each release dev will be merged in to master to support the new deploy.

### Writing tests ###
All functions which can be supported by a unit test should have one integrated via junit. Examples are available in the test directory for each reprository. 
A set of integration tests based off of static data has been developed to test the end to end processing of Bluekai data.
All tests should successfully complete prior to integration of new code in the baseline.

### Code review ###
* Code reviews should be managed via bitbucket pull requests

### Resources ###

* [spark best practices](https://umbertogriffo.gitbooks.io/apache-spark-best-practices-and-tuning/content/)
* [scala style guide](https://github.com/databricks/scala-style-guide) 
