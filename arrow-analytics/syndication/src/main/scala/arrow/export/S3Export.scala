package arrow.export

import java.net.URI

import arrow.IDExport
import arrow.config.ArrowLogger
import arrow.constants.Constants
import arrow.constants.Constants._
import arrow.util.ESUtil.updateCampaign
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{col, md5, sha2}
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.json4s.JsonDSL._
import org.json4s._

import scala.collection.Map
import scala.collection.mutable.ArrayBuffer

// case class for holding hash format along with dataframe
case class MaybeHashedDataFrame(hashFormat: Option[String], dataFrame: DataFrame)

object S3Export {

  val EXPORT_HEADER_MAPPING: Map[String, String] = Map(
    "maid" -> "MAID",
    "maid_hash" -> "maid_hash",
    "email_address" -> "Email",
    "client_ip" -> "IP",
    Constants.WBCID_KEY -> "WBCID",
    Constants.UID_KEY -> "UID"
  )

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  /**
    * Name normalization enforcing S3 bucket restrictions
    *
    * @param name
    * @return
    */
  def normalizeBucketName(name: String): String = {
    name.replaceAll("[^0-9a-zA-Z!\\-\\_\\*\\'\\(\\)]", "-")
  }

  /**
    * Extract names and configure initial bucket name
    *
    * @param syn
    * @param audience
    * @param campaign
    * @return
    */
  def getBaseNames(syn: IDExport, audience: JValue, campaign: JValue): (String, String, String) = {
    var bucketName = syn.outputPattern
    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis
    val dirDateString = DateTimeFormat.forPattern("YYYYMMdd").withZone(DateTimeZone.UTC).print(now)
    bucketName = bucketName.replace("{{TIMESTAMP}}", dirDateString)

    val audienceName = (audience \ AUDIENCE_NAME_KEY).extract[String]
    val campaignName = (campaign \ CAMPAIGN_NAME_KEY).extract[String]
    val audienceNameNormalized = normalizeBucketName(audienceName)
    //.replaceAll("[^0-9a-zA-Z!\\-\\_\\*\\'\\(\\)]", "-")
    val campaignNameNormalized = normalizeBucketName(campaignName) //.replaceAll("[^0-9a-zA-Z!\\-\\_\\*\\'\\(\\)]", "-")
    bucketName = bucketName.replace("{{AUDIENCE_NAME}}", audienceNameNormalized)
    bucketName = bucketName.replace("{{CAMPAIGN_NAME}}", campaignNameNormalized)

    (bucketName, audienceNameNormalized, campaignNameNormalized)
  }

  private def handleHashing(encryptionType: String, df: DataFrame, syn: IDExport): MaybeHashedDataFrame = {
    if (encryptionType.equals(MD5_KEY)) {
      val hashedDF = df.withColumn("hashed", md5(col("ids"))).
        drop("ids").
        withColumnRenamed("hashed", "ids")
      MaybeHashedDataFrame(Some(MD5_KEY), hashedDF)
    } else if (encryptionType.equals(SHA_KEY)) {
      val hashedDF = df.withColumn("hashed", sha2(col("ids"), 256)).
        drop("ids").
        withColumnRenamed("hashed", "ids")
      MaybeHashedDataFrame(Some(SHA_KEY), hashedDF)
    } else {
      val msg = s"Unknown encryption type [$encryptionType]"
      ArrowLogger.log.error(msg)
      throw new IllegalArgumentException(msg)
    }
  }

  /**
    * Method to perform data write to properly formatted s3 bucket
    *
    * @param syn
    * @param hadoopConf
    * @param bucketName
    * @param data
    * @param exportFormats
    * @param partnerKeys
    * @param fieldName
    * @param typeName
    * @param audienceNameNormalized
    * @param jsonStub
    * @return
    */
  def performS3Export(
                       syn: IDExport,
                       spark: SparkSession,
                       bucketName: String,
                       data: DataFrame,
                       exportFormats: Seq[String],
                       partnerKeys: Seq[String],
                       fieldName: String,
                       typeName: String,
                       audienceNameNormalized: String,
                       jsonStub: JValue

                     ): (JValue,String) = {
    // TODO: This method and performBulkS3Export look nearly identical... Needs refactoring


    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration

    var json = jsonStub
    var newFileName:String = ""
    var outputs: ArrayBuffer[MaybeHashedDataFrame] = ArrayBuffer[MaybeHashedDataFrame]()
    if (exportFormats == null || exportFormats.isEmpty) {
      outputs.append(MaybeHashedDataFrame(None, data))
    } else {
      for (exportFormat <- exportFormats) {
        // TODO: A for loop needs to be added to handle hashing multiple fields
        outputs.append(handleHashing(exportFormat, data, syn))
      }
    }

    for (data <- outputs) {
      var dataOut = data.dataFrame
      val hashFormat = data.hashFormat

      // TODO: Rename the col to the export string
      dataOut = dataOut.withColumnRenamed("ids", fieldName)

      var parts = 1
      if (syn.outputMaxFileLength > 0) {
        val dfSize = dataOut.count //audienceSize
        parts = math.ceil(dfSize / syn.outputMaxFileLength).toInt
        if (parts == 0) {
          parts = 1
        }
      }

      for (p <- partnerKeys) {
        val outputRoot = syn.configs.getString("aws.location.root.idexport")
        val partnerBucket = bucketName.replace("{{PARTNER_NAME}}", p)
        val outputLoc = partnerBucket.replace("{{TYPE}}", typeName)
        val outputBucket = s"$outputRoot$outputLoc"

        newFileName = s"$audienceNameNormalized"

        if (hashFormat.nonEmpty) {
          newFileName = s"$newFileName.${hashFormat.get}"
        }

        //Remove old part files
        setupS3Bucket(outputBucket, hadoopConf, newFileName)

        //Append new files to directory
        dataOut.repartition(parts).write.mode(SaveMode.Append).option("header", "true").option("compression", "gzip").csv(outputBucket)

        //Rename new part files
        renameS3Files(outputBucket, hadoopConf, newFileName)

        //Update Output Locations in json
        if (syn.configs.hasPath("aws.path.root.idexport")) {
          val outputPathRoot = syn.configs.getString("aws.path.root.idexport")
          val locationJson: JValue = (EXPORT_BUCKETS_KEY, (p, (typeName, s"$outputPathRoot$outputLoc")))
          json = json merge locationJson
        }
      }
    }

    (json,newFileName)
  }

  /**
    * Exports data to multiple locations in S3 with desired format
    *
    * @param sc
    * @param syn
    * @param hadoopConf
    * @param bucketMaps
    * @param data
    * @param encryptionType
    * @param exporFieldName
    * @param typeName
    * @param audienceNameNormalized
    * @param jsonStub
    * @param shouldUpdateCampaign
    * @return
    */
  def performBulkS3Export(
                           sc: SparkContext,
                           syn: IDExport,
                           hadoopConf: Configuration,
                           bucketMaps: Iterable[Map[String, String]],
                           data: DataFrame,
                           encryptionType: String,
                           exporFieldName: String,
                           typeName: String,
                           audienceNameNormalized: String,
                           jsonStub: JValue,
                           shouldUpdateCampaign: Boolean
                         ): Iterable[Map[String, Any]] = {
    // TODO: This method and performS3Export look nearly identical... Needs refactoring

    implicit val formats: org.json4s.DefaultFormats = DefaultFormats
    var json = jsonStub
    var outputData: Iterable[Map[String, Any]] = bucketMaps

    // TODO: A for loop needs to be added to handle hashing multiple fields
    var hashedData = handleHashing(encryptionType, data, syn)
    var dataOut = hashedData.dataFrame

    // TODO: Rename the col to the export string
    dataOut = dataOut.withColumnRenamed("ids", exporFieldName)

    var parts = 1
    if (syn.outputMaxFileLength > 0) {
      val dfSize = dataOut.count //audienceSize
      parts = math.ceil(dfSize / syn.outputMaxFileLength).toInt
      if (parts == 0) {
        parts = 1
      }
    }

    println("Repartitioning Data to " + s"$parts")
    dataOut = dataOut.repartition(parts)

    for (bn <- outputData) {
      var bucketInfo = bn
      val outputRoot = syn.configs.getString("aws.location.root.idexport")
      val campaignID = bucketInfo.getOrElse("campaignID", null).asInstanceOf[String]
      val campaignPartner = bucketInfo.getOrElse("campaignPartner", null).asInstanceOf[String]
      val campaignName = bucketInfo.getOrElse("campaignName", null).asInstanceOf[String]
      val bucketName = bucketInfo.getOrElse("bucketName", null).asInstanceOf[String]
      val bucketLocation = s"$outputRoot$bucketName"
      val newFileName = s"$audienceNameNormalized.${hashedData.hashFormat}"

      ArrowLogger.log.info("campaignID: " + campaignID)
      ArrowLogger.log.info("campaignPartner: " + campaignPartner)
      ArrowLogger.log.info("campaignName: " + campaignName)
      ArrowLogger.log.info("bucketName: " + bucketName)
      ArrowLogger.log.info("bucketLocation: " + bucketLocation)
      ArrowLogger.log.info("newFileName: " + newFileName)

      //Remove old part files
      setupS3Bucket(bucketLocation, hadoopConf, newFileName)

      //Append new files to directory
      dataOut.write.mode(SaveMode.Append).option("header", "true").option("compression", "gzip").csv(bucketLocation)

      //Rename new part files
      renameS3Files(bucketLocation, hadoopConf, newFileName)

      //Update Output Locations in json
      if (syn.configs.hasPath("aws.path.root.idexport")) {
        val dateSyndicatedJSON: JValue = (DATE_SYNDICATED_KEY, System.currentTimeMillis())
        val outputPathRoot = syn.configs.getString("aws.path.root.idexport")
        val locationJson: JValue = (EXPORT_BUCKETS_KEY, (campaignPartner, (typeName, s"$outputPathRoot$bucketName")))

        json = json merge locationJson
        json = json merge dateSyndicatedJSON

        updateCampaign(campaignID,
          json,
          syn.esResourceCampaign,
          syn.esWriteConf,
          syn.esReadConf,
          sc)

        bucketInfo += "json" -> json
      }
    }

    outputData
  }

  /**
    * Function to create buckets and perform cleanup on existing buckets to avoid name collision
    *
    * @param outputBucket
    * @param hadoopConf
    * @param newFileName
    */
  def setupS3Bucket(outputBucket: String, hadoopConf: Configuration, newFileName: String): Unit = {
    val bucketPath = new Path(outputBucket)
    val fs = FileSystem.get(new URI(outputBucket), hadoopConf)

    if (fs.exists(bucketPath)) {
      cleanupS3Bucket(bucketPath, fs, newFileName)
    } else {
      fs.create(bucketPath)
    }
  }

  /**
    * Clean up old part files, _success files, and files to be replaced
    *
    * @param bucketPath
    * @param fs
    * @param newFileName
    */
  def cleanupS3Bucket(bucketPath: Path, fs: FileSystem, newFileName: String): Unit = {
    val fileItr = fs.listFiles(bucketPath, false)
    while (fileItr.hasNext) {
      val file = fileItr.next
      val filePath = file.getPath
      val fileName = filePath.getName

      if (fileName.toLowerCase == "_success" ||
        fileName.matches("^part-\\d{5}-.+\\.gz") ||
        fileName.matches(s"^${newFileName}.+")) {
        fs.delete(filePath, false)
      }
    }
  }

  /**
    * Rename files in s3 from parts to audience name with file count
    *
    * @param outputBucket
    * @param hadoopConf
    * @param filename
    */
  def renameS3Files(outputBucket: String, hadoopConf: Configuration, filename: String): Unit = {
    val fs = FileSystem.get(new URI(outputBucket), hadoopConf)
    val fileItr = fs.listFiles(new Path(outputBucket), false)
    var count = 0
    var newFileName = filename

    //Cleanup and Rename part files
    while (fileItr.hasNext) {
      val file = fileItr.next
      val filePath = file.getPath
      val fileName = filePath.getName

      if (fileName.toLowerCase == "_success") {
        fs.delete(filePath, false)
      } else if (fileName.matches("^part-\\d{5}-.+\\.gz")) {
        val fileSuffix = fileName.substring(fileName.indexOf("."))

        if (count > 0) {
          newFileName = newFileName + "-" + count.toString
        }
        newFileName = newFileName + fileSuffix

        count += 1
        val newFilePath = filePath.toString.replace(fileName, newFileName)
        fs.rename(filePath, new Path(newFilePath))
      }
    }
  }
}
