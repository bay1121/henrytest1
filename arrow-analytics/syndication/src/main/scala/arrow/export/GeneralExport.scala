package arrow.export

import arrow.IDExport
import arrow.config.ArrowLogger
import arrow.constants.Constants
import arrow.constants.Constants._
import arrow.export.S3Export._
import arrow.util.ESUtil._
import arrow.util.JsonUtil._
import arrow.util.{ArrowQueryBuilder, MoviesAnywhereFilter}
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import arrow.config.Util._

import scala.util.{Failure, Success, Try}

object GeneralExport {

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  def run(syn: IDExport, spark: SparkSession, messageJson: JValue) {
    implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }

    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration


    //Get values from kafka message
    val syndicate = (messageJson \ IS_SYNDICATION_KEY).extract[Boolean]
    val audienceID = (messageJson \ AUDIENCE_ID_KEY).extract[String]
    val campaignID = (messageJson \ CAMPAIGN_ID_KEY).extract[String]

    //Get Audience and Campaign definitions from ElasticSearch
    val audience_tupled = getAudienceByID(audienceID, syn.esResourceAudience, syn.esReadConf, sc)
    val campaign_tupled = getCampaignByID(campaignID, syn.esResourceCampaign, syn.esReadConf, sc)

    var json: JValue = (IS_SYNDICATION_ACTIVE_KEY, syndicate) ~ (IS_SYNDICATION_QUEUED_KEY, false)

    val triedJson: Try[JValue] = Try {

      if (audience_tupled != null && syndicate) {
        val syndicationJSON: JValue = (DATE_LAST_EXPORTED_KEY, System.currentTimeMillis)

        //Get audience from ES
        val audience = parse(audience_tupled.first()._2)
        val campaign = parse(campaign_tupled.first()._2)

        ArrowLogger.log.info("Audience: " + audience)
        ArrowLogger.log.info("Campaign: " + campaign)
        val audienceSize = (audience \ "count" \ "bkids").extract[Double]

        val partnerKeys = getPartners(campaign)

        if (partnerKeys.nonEmpty) {
          ArrowLogger.log.info("partnerKeys: " + partnerKeys)
          val baseName = getBaseNames(syn, audience, campaign)
          val bucketName = baseName._1
          val audienceName = baseName._2
          val campaignName = baseName._3

          ArrowLogger.log.info("bucketname: " + bucketName)
          ArrowLogger.log.info("audienceName: " + audienceName)
          ArrowLogger.log.info("campaignName: " + campaignName)

          val exportFormats = getHashFormats(campaign)
          ArrowLogger.log.info("exportFormats: " + exportFormats)

          if (!audience.has("query")) {
            val msg = "The audience is missing a query!"
            ArrowLogger.log.error(msg)
            throw new RuntimeException(msg)
          }

          val originalQuery: String = (audience \ "query").extract[String]
          val queryBuilder = new ArrowQueryBuilder(originalQuery, childType = "arrow_child")
          var fileData: Map[String, String] = null
          val destinations = getSyndicationDestinations(campaign).filter(i =>  i==S3_KEY||i==LIVERAMP_KEY)
          ArrowLogger.log.info("destinations : " + destinations)
          if (campaign.has(Constants.CAMPAIGN_BUSINESS_UNIT_KEY)) {
            val businessUnit = (campaign \ Constants.CAMPAIGN_BUSINESS_UNIT_KEY).extract[String]
            MoviesAnywhereFilter.applyFilterToQueryBuilder(businessUnit, queryBuilder)
          } else {
            val msg = s"Missing mandatory field (${Constants.CAMPAIGN_BUSINESS_UNIT_KEY}) in campaign."
            ArrowLogger.log.error(msg)
            throw new RuntimeException(msg)
          }
          var inputFile: String = ""
          for (dest <- destinations) {

            var exportTypes = getExportTypes(campaign, dest)

            if(dest==LIVERAMP_KEY) exportTypes=exportTypes.filter(i=> i==EMAIL_KEY||i==MAID_KEY)
            ArrowLogger.log.info("exportTypes for " + dest + " are: " + exportTypes)

            val exportFields = getExportFields(exportTypes)
            ArrowLogger.log.info("exportFields: " + exportFields.mkString(","))

            val ids = getAudienceIDs(queryBuilder.buildAsJson(), exportFields, syn.esResourceEvent, syn.esReadConf, spark)

            for (t <- exportTypes) {

              var typeName = t
              if ((dest == S3_KEY) || (dest == LIVERAMP_KEY && (!exportFormats.isEmpty || (json \ EXPORT_BUCKETS_KEY \ partnerKeys.head \ t) == JNothing))) {
                ArrowLogger.log.info("dest : " + dest)
                ArrowLogger.log.info("exportFormats : " + exportFormats)
                ArrowLogger.log.info("type : " + json \ EXPORT_BUCKETS_KEY \ partnerKeys.head \ t)
                ArrowLogger.log.info("exporttype : " + t)
                if (dest == LIVERAMP_KEY) typeName = LIVERAMP_KEY + "_" + t
                val fieldNames = EXPORT_TYPES.getOrElse(t, null)

                val schema = StructType(Array(
                  StructField("ids", StringType, nullable = true)
                ))
                var dataOut = spark.createDataFrame(sc.emptyRDD[Row], schema)

                // TODO: This block will need to be rewritten to allow for more fields in a future ticket
                if (fieldNames != null && fieldNames.length == 1) {
                  // TODO: These variables assume only 1 fieldName
                  val colName = fieldNames(0)._1
                  val fieldName = EXPORT_HEADER_MAPPING.getOrElse(colName, colName)

                  if (ids.columns.contains(colName)) {
                    // Have to check for ArrayType before we explode the IDs because WBCID is a StringType which breaks on explode()
                    ids.dtypes.find(tuple => tuple._1 == colName) match {
                      case Some(dtypesTuple) =>
                        val dataSelect: DataFrame = if (dtypesTuple._2.startsWith("ArrayType")) {
                          ids.select(explode(col(s"`$colName`")).alias("ids"))
                        } else {
                          ids.select(col(s"`$colName`").alias("ids"))
                        }

                        val data = dataSelect.filter(col("ids").isNotNull).distinct

                        dataOut = dataOut.union(data)
                      case _ =>
                        ArrowLogger.log.warn(s"Didn't find colName ($colName) in ids.dtypes -- Skipping this export due to unknown type.")
                    }
                  }
                  ArrowLogger.log.info("Running S3 export for destination: " + dest)

                  val synOut = performS3Export(
                    syn,
                    spark,
                    bucketName,
                    dataOut,
                   if (dest == LIVERAMP_KEY || fieldName == MAID_HASH_KEY) Seq.empty  else exportFormats,
                    partnerKeys,
                    fieldName,
                    typeName,
                    audienceName,
                    json
                  )
                  json = synOut._1
                  inputFile = synOut._2+".csv.gz"
                  ArrowLogger.log.info(pretty(render(json)))

                } else {
                  ArrowLogger.log.warn(s"Given export type [$t], could not find mapping!")
                }
              }
              if (dest == LIVERAMP_KEY) {
                ArrowLogger.log.info("running SNS publish: ")
                val s3ReplacedPath = (json \ EXPORT_BUCKETS_KEY \ partnerKeys.head \ typeName).extract[String].replace("https://console.aws.amazon.com/s3/buckets/cmdt-application/","")
                val gpgRecipient = syn.configs.getString("aws.liveramp.gpg.recipient")
                val gpgKeyLoc = syn.configs.getString("aws.liveramp.gpg.public.key")
                val inputBucket = syn.configs.getString("aws.liveramp.bucket")
                ArrowLogger.log.info("inputFile:"+inputFile+ " "+"gpgRecipient:"+gpgRecipient+" "+"gpgKeyLoc:"+ gpgKeyLoc+" "+"s3ReplacedPath:"+ s3ReplacedPath+" ")

                publishSNSMessage(syn.snsTopic,s"""{\"default\":\"GPG encryption of ARROW LiveRamp extract\",\"product\":\"${LIVERAMP_KEY}\",\"gpg_recipient\":\"${gpgRecipient}\",\"input_file\":\"${inputFile}\",\"input_file_path\":\"${s3ReplacedPath}\",\"output_file\":\"${inputFile}\",\"gpg_public_key\":\"${gpgKeyLoc}\",\"output_file_path\": \"${s3ReplacedPath}/encrypted\",\"input_bucket\":\"${inputBucket}\"}""")
              }
            }
          }
        } else {
          ArrowLogger.log.warn("Could not find any partner keys with the campaign!")
        }

        updateAudience(audience_tupled,
          audienceID,
          syndicationJSON,
          syn.esResourceAudience,
          syn.esWriteConf
        )
      } else {
        ArrowLogger.log.info("Skipping syndication.")
      }

      json
    }

    triedJson match {
      case Success(jvalue) =>
        val dateSyndicated: JValue = (DATE_SYNDICATED_KEY, System.currentTimeMillis())
        val jsonWithDate = jvalue merge dateSyndicated
        ArrowLogger.log.info(jsonWithDate)
        updateCampaign(campaign_tupled, campaignID, jsonWithDate, syn.esResourceCampaign, syn.esWriteConf)
      case Failure(error) =>
        ArrowLogger.log.warn("Encountered an error during syndication. Updating ES anyway.")
        ArrowLogger.log.info(json)
        updateCampaign(campaign_tupled, campaignID, json, syn.esResourceCampaign, syn.esWriteConf)
        throw error
    }
  }
}