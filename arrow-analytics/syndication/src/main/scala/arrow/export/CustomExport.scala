package arrow.export

import arrow.IDExport
import arrow.config.ArrowLogger
import arrow.constants.Constants._
import arrow.export.S3Export._
import arrow.util.ESUtil._
import arrow.util.JsonUtil._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection._
import scala.util.{Failure, Success, Try}

object CustomExport {

  implicit val formats : org.json4s.DefaultFormats = DefaultFormats

  def run(syn: IDExport, spark: SparkSession, messageJson: JValue) {
    implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }

    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration

    //Get values from kafka message
    val syndicate = (messageJson \ IS_SYNDICATION_KEY).extract[Boolean]
    val audienceID = (messageJson \ AUDIENCE_ID_KEY).extract[String]
    val campaignID = (messageJson \ CAMPAIGN_ID_KEY).extract[String]

    //Get Audience and Campaign definitions from ElasticSearch
    val audience_tupled = getAudienceByID(audienceID, syn.esResourceSegment, syn.esReadConf, sc)
    val campaign_tupled = getCampaignByID(campaignID, syn.esResourceCampaign, syn.esReadConf, sc)

    var json : JValue = (IS_SYNDICATION_ACTIVE_KEY, syndicate) ~ (IS_SYNDICATION_QUEUED_KEY, false)

    val triedJson:Try[JValue] = Try {

      if (audience_tupled != null && syndicate) {
        val syndicationJSON: JValue = (DATE_LAST_EXPORTED_KEY, System.currentTimeMillis)

        //Get audience from ES
        val audience = parse(audience_tupled.first()._2)
        val campaign = parse(campaign_tupled.first()._2)

        val partnerKeys = getPartners(campaign)

        if (partnerKeys.nonEmpty) {
          val baseName = getBaseNames(syn, audience, campaign)
          val bucketName = baseName._1
          val audienceName = baseName._2
          val campaignName = baseName._3

          ArrowLogger.log.info("bucketname: " + bucketName)
          //get fields for export from

          val exportFormats = getHashFormats(campaign)
          ArrowLogger.log.info("exportFormats: " + exportFormats)

          val exportTypes = getExportTypes(campaign,S3_KEY)
          ArrowLogger.log.info("exportTypes: " + exportFormats)

          val exportFields = getExportFields(exportTypes)
          ArrowLogger.log.info("exportFields: " + exportFields.mkString(","))

          /**
            * For custom audience segments, the ids are stored in the audience document
            * "ids": [
            * {
            * "wbcid": "WBCID-thing@me.com",
            * "email_address": "thing@me.com",
            * "last_seen": 1513707858303,
            * "source": "Fandango"
            * }
            * ]
            */
          val index = (audience \ INDEX).extract[String]
          val ids = esQuery(index, syn.esReadConf, sc).map(esTuple => parse(esTuple._2).extract[Map[String, String]])

          for (t <- exportTypes) {
            val typeName = t
            val fieldNames = EXPORT_TYPES.getOrElse(t, null)

            val schema = StructType(Array(
              StructField("ids", StringType, true)
            ))

            // TODO: This block will need to be rewritten to allow for more fields in a future ticket
            if (fieldNames != null && fieldNames.length == 1) {
              // TODO: These variables assume only 1 fieldName
              val colName = fieldNames(0)._1
              val fieldName = EXPORT_HEADER_MAPPING.getOrElse(colName, null)

              val data = ids.map(eventMap => Row(eventMap.get(colName).orNull)).distinct
              val dataOut = spark.createDataFrame(data, schema)

              val synOut = performS3Export(
                syn,
                spark,
                bucketName,
                dataOut,
                exportFormats,
                partnerKeys,
                fieldName,
                typeName,
                audienceName,
                json
              )
              json=synOut._1
            }
          }
        } else {
          ArrowLogger.log.warn("Could not find any partner keys with the campaign!")
        }

        updateAudience(audience_tupled,
          audienceID,
          syndicationJSON,
          syn.esResourceSegment,
          syn.esWriteConf
        )
      } else {
        ArrowLogger.log.info("Skipping S3 Export")
      }

      json
    }

    triedJson match {
      case Success(jvalue) =>
        val dateSyndicated:JValue = (DATE_SYNDICATED_KEY, System.currentTimeMillis())
        val jsonWithDate = jvalue merge dateSyndicated
        ArrowLogger.log.info(jsonWithDate)
        updateCampaign(campaign_tupled, campaignID, jsonWithDate, syn.esResourceCampaign, syn.esWriteConf)
      case Failure(error) =>
        ArrowLogger.log.warn("Encountered an error during syndication. Updating ES anyway.")
        ArrowLogger.log.info(json)
        updateCampaign(campaign_tupled, campaignID, json, syn.esResourceCampaign, syn.esWriteConf)
        throw error
    }
  }
}

