package arrow.export

import arrow.IDExport
import arrow.config.ArrowLogger
import arrow.constants.Constants._
import arrow.export.S3Export.EXPORT_HEADER_MAPPING
import arrow.util.{ESUtil, JsonUtil}
import arrow.util.ESUtil.updateAudience
import org.apache.hadoop.conf.Configuration
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection._

object NightlySegmentExport {

  def run(syn: IDExport, spark: SparkSession, hadoopConf: Configuration) {
    val sc = spark.sparkContext
    //Get All Audiences
    val segmentMetas = ESUtil.getAllAudienceMetaData(syn.esResourceSegment, syn.esReadConf, sc)
    val json: JValue = (IS_SYNDICATION_ACTIVE_KEY, true) ~ (IS_SYNDICATION_QUEUED_KEY, false)
    val syndicationJSON: JValue = (DATE_LAST_EXPORTED_KEY, System.currentTimeMillis)

    //Iterate over segments
    for (segmentMeta <- segmentMetas) {
      implicit val formats: org.json4s.DefaultFormats = DefaultFormats

      //For Each Audience load segment data
      val segmentID = segmentMeta.get("id").orNull
      val segmentName = segmentMeta.get("name").orNull

      if (segmentID != null && segmentName != null) {
        try {
          ArrowLogger.log.info("Working on Audience: " + segmentID + " aka " + segmentName)

          val segmentNameNormalized = S3Export.normalizeBucketName(segmentName)
          val segmentRDD = ESUtil.getAudienceByID(segmentID, syn.esResourceSegment, syn.esReadConf, sc)
          val segment = parse(segmentRDD.first()._2)

          //Get segment data
          val ids = (segment \ "ids").extract[Array[Map[String, String]]]

          val segmentQueryParams: scala.Predef.Map[String, Array[String]] = scala.Predef.Map[String, Array[String]](
            "externalSegments.id" -> Array[String](segmentID),
            "target.s3" -> Array[String]("true")
          )

          //Get campaigns that reference this segment
          val campaigns = ESUtil.esQueryWithKeyValueMap(segmentQueryParams, syn.esResourceCampaign, syn.esReadConf, sc)

          val schema = StructType(Array(
            StructField("campaignID", StringType, true),
            StructField("campaignName", StringType, true),
            StructField("campaignExportTypes", ArrayType(StringType, true), true),
            StructField("campaignPartners", ArrayType(StringType, true), true),
            StructField("campaignEncryption", ArrayType(StringType, true), true)
          ))

          //Normalize destinations
          val campaignDestinations = campaigns.map(campaign => {
            implicit val formats: org.json4s.DefaultFormats = DefaultFormats
            //Get name, id, exportId, partners, encryption
            val campaignID = campaign._1
            val campaignObj = parse(campaign._2)
            val campaignName = (campaignObj \ "productName").extract[String]
            val campaignExportTypes = JsonUtil.getExportTypes(campaignObj,"")
            val campaignPartners = JsonUtil.getPartners(campaignObj)
            val campaignEncryption = JsonUtil.getHashFormats(campaignObj)

            Row(campaignID, campaignName, campaignExportTypes, campaignPartners, campaignEncryption)
          })

          val now = new java.sql.Timestamp(new org.joda.time.DateTime().withZone(DateTimeZone.UTC).withTime(23, 59, 59, 0).getMillis)
          val exportsAll = spark.createDataFrame(campaignDestinations, schema)
          val exportsSyndicate = exportsAll.filter(col("campaignInSyndication") && col("campaignSyndicationEndDate").cast(TimestampType) >= now)

          //Generate export combinations
          val exportCombos = exportsSyndicate.withColumn("campaignExportType", explode(col("campaignExportTypes"))).
            withColumn("campaignPartner", explode(col("campaignPartners"))).
            withColumn("campaignEncryption", explode(col("campaignEncryption"))).
            rdd.
            keyBy(x => (x.getAs[String]("campaignEncryption"), x.getAs[String]("campaignExportType"))).
            mapValues(y => Map(
              "campaignID" -> y.getAs[String]("campaignID"),
              "campaignPartner" -> y.getAs[String]("campaignPartner"),
              "campaignName" -> y.getAs[String]("campaignName"))).
            groupByKey()

          exportCombos.collect.map(combo => {
            implicit val formats = DefaultFormats

            val campaignEncryption = combo._1._1
            val campaignExportType = combo._1._2
            val exportLocs = combo._2
            val fieldNames = EXPORT_TYPES.getOrElse(campaignExportType, null)

            if (fieldNames != null) {
              val colName = fieldNames(0)._1
              val exportFieldName = EXPORT_HEADER_MAPPING.getOrElse(colName, null)
              val schema = StructType(Array(
                StructField("ids", StringType, true)
              ))

              val data = ids.map(ids => Row(ids.get(colName).orNull)).distinct.toSeq
              val dataOut = spark.createDataFrame(sc.parallelize(data), schema)

              val bucketMap = exportLocs.map(pair => {
                var out = pair
                val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis
                val dirDateString = DateTimeFormat.forPattern("YYYYMMdd").withZone(DateTimeZone.UTC).print(now)
                var bucketPattern = syn.outputPattern
                bucketPattern = bucketPattern.replace("{{TIMESTAMP}}", dirDateString)
                bucketPattern = bucketPattern.replace("{{AUDIENCE_NAME}}", segmentNameNormalized)
                bucketPattern = bucketPattern.replace("{{TYPE}}", campaignExportType)
                bucketPattern = bucketPattern.replace("{{PARTNER_NAME}}", pair.get("campaignPartner").orNull)
                bucketPattern = bucketPattern.replace("{{CAMPAIGN_NAME}}", S3Export.normalizeBucketName(pair.get("campaignName").orNull))
                out += "bucketName" -> bucketPattern
                out
              })

              S3Export.performBulkS3Export(
                sc,
                syn,
                hadoopConf,
                bucketMap,
                dataOut,
                campaignEncryption,
                exportFieldName,
                campaignExportType,
                segmentNameNormalized,
                json,
                true: Boolean
              )
            }
          })

          //update segment
          updateAudience(segmentRDD,
            segmentID,
            syndicationJSON,
            syn.esResourceSegment,
            syn.esWriteConf)
        } catch {
          case e: Exception =>
            ArrowLogger.log.error("Unable to complete NightlySegmentExport: " + segmentID + " aka " + segmentName)
            ArrowLogger.log.error(e.printStackTrace())
        }
      } else {
        ArrowLogger.log.info("Unable to export segment, missing required information. segmentID: " + segmentID + ", segmentName: " + segmentName)
      }
    }
  }
}
