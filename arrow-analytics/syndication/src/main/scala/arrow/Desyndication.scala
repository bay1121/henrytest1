package arrow

import arrow.config.Util
import arrow.syn.NightlyDesyndication
import com.typesafe.config.Config
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import com.typesafe.config.{Config, ConfigFactory}
import scala.collection._
import org.apache.hadoop.conf.Configuration

class Desyndication extends Serializable {

  val configs : Config = ConfigFactory.load()

  //Secure Credentials
  val esPasswd: String = Util.getSecureCredential(Util.ES_SYND_USER)

  val esNodes: String = configs.getString("es.nodes")
  val esPort: String = configs.getString("es.port")
  val esUser: String = configs.getString("es.user")
  val esResourceAudience: String = configs.getString("es.resource.audience")
  val esResourceCampaign: String = configs.getString("es.resource.campaign")
  val esResourceEvent: String = configs.getString("es.resource.event")
  val esResourceSegment: String = configs.getString("es.resource.segment")

  val esReadConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true"
  )

  val esWriteConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.batch.size.bytes" -> "24mb",
    "es.batch.size.entries" -> "24000",
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.write.operation" -> "update",
    "es.index.auto.create" -> "true"
  )
}

object DesyndicationNightly {
  def main(args: Array[String]) {

    val spark = SparkSession.builder()
      .appName("DesyndicationNightly")
      .getOrCreate()
    val sc = spark.sparkContext
    val syn = new Desyndication

    //Desyndicate all audiences on a nightly interval
    NightlyDesyndication.run(syn, spark)
  }
}