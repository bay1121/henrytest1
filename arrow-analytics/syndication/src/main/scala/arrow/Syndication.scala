package arrow

import java.net.{ URI, URL }
import java.util.concurrent.Executors

import arrow.bluekai.Util._
import arrow.config.{ ArrowLogger, Util }
import arrow.constants.Constants
import arrow.util.{ EmailUtil, SlackAysncMessagePoster, YarnFailedAppSlackMessageBuilder }
import com.typesafe.config.Config
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.{ CanCommitOffsets, HasOffsetRanges, KafkaUtils, OffsetRange }
import org.apache.spark.streaming.{ Seconds, StreamingContext }
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods.{ parse, render }
import org.json4s.native.Serialization.write
import org.spark_project.guava.io.BaseEncoding

import scala.collection._
import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal
import scala.util.{ Failure, Success }
import com.typesafe.config.{ Config, ConfigFactory }
import org.apache.hadoop.conf.Configuration

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest

// passing all params is UGLY but this is a dirty quick fix to work around a serialization issue - HAVE to change later
class Syndication(configs: Config) extends Serializable {

  val esPasswd: String = Util.getSecureCredential(Util.ES_SYND_USER)
  val bkuid: String = Util.getSecureCredential(Util.BK_UID)
  val bksecretkey: String = Util.getSecureCredential(Util.BK_SECRET_KEY)
  val apikey: String = Util.getSecureCredential(Util.BK_API_KEY)
  val awsSmtpKey: String = Util.getSecureCredential(Util.AWS_SMTP_KEY)

  //Constants
  val BULK_BLUEKAI_RETRY: Int = configs.getInt("bluekai.bulk.retry")
  val HTTP_TIMEOUT: Int = configs.getInt("http.timeout")
  val HEADERS: scala.Predef.Map[String, String] = scala.Predef.Map[String, String](
    "User_Agent" -> "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
    "Content-Type" -> "application/json")
  val BULK_HEADERS: scala.Predef.Map[String, String] = scala.Predef.Map[String, String]("ApiKey" -> apikey, "Accept" -> "application/json", "Content-Type" -> "application/json")

  //App Properties
  val audienceURL: String = configs.getString("bluekai.audience.url")
  val bulkmax: Int = configs.getInt("bluekai.bulk.max")
  val bulkURL: String = configs.getString("bluekai.bulk.url")
  val callbackURL: String = configs.getString("bluekai.callback.url")
  val retirecategoryid: Int = configs.getInt("bluekai.category_id.retire")
  val parentCategoryId: Int = configs.getInt("bluekai.category_id.parent")
  val classificationURL: String = configs.getString("bluekai.classification.url")
  val classificationRuleURL: String = configs.getString("bluekai.classification_rule.url")
  val partnerID: String = configs.getString("bluekai.partner_id")
  val siteID: String = configs.getString("bluekai.site_id")
  val userURLPrefix: String = configs.getString("bluekai.user.url.prefix")
  val userURLSuffix: String = configs.getString("bluekai.user.url.suffix")
  val kfQuorum: String = configs.getString("kafka.quorum")
  val kafka_topic: String = configs.getString("kafka.topic.syndication")
  val kafkaGroupID: String = configs.getString("kafka.group.id.syndication")
  val kafkaAutoOffsetReset: String = configs.getString("kafka.auto.offset.reset")

  val esNodes: String = configs.getString("es.nodes")
  val esPort: String = configs.getString("es.port")
  val esUser: String = configs.getString("es.user")
  val esResourceCampaign: String = configs.getString("es.resource.campaign")
  val esResourceAudience: String = configs.getString("es.resource.audience")
  val esResourceEvent: String = configs.getString("es.resource.event")

  //Email Params
  val awsSmtpUser: String = Util.getSecureCredential(Util.AWS_SMTP_USER)
  val awsSmtpHost: String = configs.getString("aws.smpt.host")
  val alertId: String = configs.getString("alert.email.id")
  val defaultEmail: String = configs.getString("support.email.id")

  // used for slack messages
  val slackWebhookEndpoint: String = configs.getString("slack.webhook.endpoint")

  val userURL = s"$userURLPrefix$siteID$userURLSuffix"
  val esReadConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.index.auto.create" -> "true")

  val esWriteConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.batch.size.bytes" -> "24mb",
    "es.batch.size.entries" -> "24000",
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.index.auto.create" -> "true",
    "es.write.operation" -> "update",
    "es.index.auto.create" -> "true")

  val topics: Array[String] = Array(kafka_topic)
  val kafkaParams: Map[String, Object] = Map[String, Object](
    // the usual params, make sure to change the port in bootstrap.servers if 9092 is not TLS
    "bootstrap.servers" -> kfQuorum,
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> kafkaGroupID,
    "auto.offset.reset" -> kafkaAutoOffsetReset,
    "enable.auto.commit" -> (false: java.lang.Boolean))

  // Returns the BlueKai JSON body for a user subrequest
  def buildUser(audienceID: String, uid: String): JValue = {
    ("Method", "POST") ~
      ("URIPath", userURL + "?bkuid=" + bkuid + "&userid=" + uid + "&phint=audience=" + audienceID) ~
      ("RequestID", audienceID + '|' + uid)
  }

  def buildBulkUser(): JValue = {
    ("ResponseType", "Summary") ~ ("Method", "POST") ~ ("ResponseCallbackUrl", callbackURL) ~ ("Scatter", List[JValue]())
  }

  def signatureBulkBuilder(data: String): String = {
    signatureBulkBuilder(data, bksecretkey)
  }

  // Create the method signature for bulk
  def signatureBulkBuilder(data: String, bksk: String): String = {

    val algorithm = "SHA256"

    def hmac(key: Array[Byte], message: Array[Byte]): Array[Byte] = {
      val secret = new SecretKeySpec(key, algorithm)
      val mac = Mac.getInstance("HMACSHA256")

      mac.init(secret)
      mac.doFinal(message)
    }

    val h = hmac(bksk.getBytes(), data.getBytes())
    val s = BaseEncoding.base64().encode(h)

    bulkURL.concat("?bksig=").concat(s)
  }

  def signatureInputBuilder(url: String, method: String, data: String): String = {
    signatureInputBuilder(url, method, data, bkuid, bksecretkey)
  }

  //Creating the method signature
  def signatureInputBuilder(url: String, method: String, data: String, bku: String, bksk: String): String = {

    val algorithm = "SHA256"

    def hmac(key: Array[Byte], message: Array[Byte]): Array[Byte] = {
      val secret = new SecretKeySpec(key, algorithm)
      val mac = Mac.getInstance("HMACSHA256")

      mac.init(secret)
      mac.doFinal(message)
    }

    val path = new URL(url).getPath

    var stringToSign = method + path
    var u = ""
    if (data != null) {
      stringToSign += data
      val h = hmac(bksk.getBytes, stringToSign.getBytes)
      val s = BaseEncoding.base64().encode(h)
      u = java.net.URLEncoder.encode(s, "UTF-8")
    }

    var newUrl: String = url
    if (!url.endsWith("?")) {
      newUrl += '?'
    } else {
      newUrl += '&'
    }
    newUrl.concat("bkuid=").concat(bku).concat("&bksig=").concat(u)
  }
}

// TODO: We don't have any way of dealing with good messages that failed due to reasons
// outside of this job's control. For example, bluekai, elasticsearch, etc could be down.
// If we implemented the concept of Recoverable and NotRecoverable states, then we could
// publish the message back to Kafka to try it again later.
object Syndication {

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  def main(args: Array[String]) {

    implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }

    val spark = SparkSession.builder()
      .appName("SyndicationSession")
      .getOrCreate()
    val sc = spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(10))

    val syn = new Syndication(ConfigFactory.load())

    val executorService = Executors.newSingleThreadExecutor()
    implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(executorService)

    val messages = KafkaUtils.createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](syn.topics, syn.kafkaParams))

    // Hold a reference to the current offset ranges, so it can be used downstream
    var offsetRanges = Array.empty[OffsetRange]

    messages.transform(rdd => {
      offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd

      /**
       * Kafka Message Format
       *
       * {"timestamp":1515463679424,"id":"AWC9-ZyXqJS2n9bMc_UH","syndicate":true,"campaign":"Hy8v4EEKb","custom":true}
       * {"timestamp":1515463679422,"id":"HJRVE44tZ","syndicate":true,"campaign":"Hy8v4EEKb","custom":false}
       * {"timestamp":1515463780386,"id":"HJRVE44tZ","syndicate":false,"campaign":"Hy8v4EEKb","custom":false}
       * {"timestamp":1515463780387,"id":"AWC9-ZyXqJS2n9bMc_UH","syndicate":false,"campaign":"Hy8v4EEKb","custom":true}
       */
    }).foreachRDD { rdd =>
      val rdd2 = rdd.map(x => x.value())
      val sc = spark.sparkContext

      val it = rdd2.collect().iterator

      it.foreach { message =>
        try {
          val messageJson = parse(message)

          if ((messageJson \ "syndicate").extract[Boolean]) {
            syndicate(syn, spark, messageJson)
          } else if (!(messageJson \ "syndicate").extract[Boolean]) {
            endSyndication(syn, sc, messageJson)
          } else {
            throw new IllegalArgumentException("I don't know how to process this message. " +
              write(render(messageJson)))
          }
        } catch {
          case NonFatal(e) =>
            ArrowLogger.log.error("Failed to process syndication batch successfully!")
            ArrowLogger.log.error(s"$e\n\n${e.getStackTrace.mkString("\n")}")

            val subject = "Error while running Syndication.scala"
            val body = s"Error:\n${e.getMessage}\n Exception:\n$e\n\n\nStack Trace:\n${e.getStackTrace.mkString("\n")}"
            EmailUtil.send(syn.alertId, syn.defaultEmail, subject, body, syn.awsSmtpHost, syn.awsSmtpUser, syn.awsSmtpKey)

            var audienceID = ""
            var campaignID = ""

            try {
              val messageJson = parse(message)
              audienceID = (messageJson \ Constants.AUDIENCE_ID_KEY).extract[String]
              campaignID = (messageJson \ Constants.CAMPAIGN_ID_KEY).extract[String]
            } catch {
              case t: Throwable => ArrowLogger.log.error("Error parsing json message for alert!")
            }

            val yarnJobUri = URI.create(s"${sc.applicationId}")
            val slackMessage = new YarnFailedAppSlackMessageBuilder(yarnJobUri, e)
              .addJsonMessage(message)
              .addCampaignId(campaignID)
              .addAudienceId(audienceID)
              .addOriginatingClass(getClass.getName)
              .build()

            val slackEndpoint = URI.create(syn.slackWebhookEndpoint)
            val future = SlackAysncMessagePoster.send(slackEndpoint, slackMessage)

            future.onComplete {
              case Success(r) =>
                ArrowLogger.log.info(s"Successfully posted to slack with response $r")
              case Failure(error) =>
                ArrowLogger.log.warn(s"Failed to post to slack with error $error")
            }
        }
      }

      // After batch completes successfully, write offsets to HDFS
      ArrowLogger.log.info("Committing offsets to Kafka")
      messages.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)
    }

    ArrowLogger.log.info("Starting Syndication App")
    ssc.start()
    ssc.awaitTermination()
  }
}

//
//object BluekaiNightlySyndication {
//
//  def main(args: Array[String]) {
//
//    val spark = SparkSession.builder()
//      .appName("SyndicationNightly")
//      .getOrCreate()
//    val sc = spark.sparkContext
//    val syn = new Syndication(sc,args)
//
//    val hadoopConf = sc.hadoopConfiguration
//    hadoopConf.set("fs.s3a.enableServerSideEncryption", "true")
//    hadoopConf.set("fs.s3a.server-side-encryption-algorithm", "AES256")
//
//    //Handle syndication status to bluekai
//    NightlySyndication.run(syn, spark)
//  }
//}
