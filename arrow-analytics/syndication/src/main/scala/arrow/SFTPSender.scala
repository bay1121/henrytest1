package arrow


import java.net.URI

import arrow.config.{ArrowLogger, Util}
import arrow.util.ESUtil.{getAudienceByID, getAudiencesByIDs, getCampaignByID, updateAudience, updateCampaign}
import arrow.util.JsonUtil.{getCampaignAudienceIDs, getExportFields, getExportTypes, parseCampaign}
import arrow.util.PGPHadoop
import com.typesafe.config.Config
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, SaveMode, SparkSession}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}
import org.apache.hadoop.conf.{Configuration, Configured}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.kafka.common.serialization.StringDeserializer
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.native.Serialization.write

import scala.collection.JavaConversions._
import scala.collection.{Map, mutable}
import org.apache.hadoop.conf.Configuration
import com.typesafe.config.{Config, ConfigFactory}

class SFTPSender extends Serializable {
  val configs: Config = ConfigFactory.load()
  //Secure Credentials
  val esPasswd: String = Util.getSecureCredential(Util.ES_SYND_USER)
  val nsPasswd: String = Util.getSecureCredential(Util.NS_USER_KEY)


  val esNodes: String = configs.getString("es.nodes")
  val esPort: String = configs.getString("es.port")
  val esUser: String = configs.getString("es.user")
  val esResourceAudience: String = configs.getString("es.resource.audience")
  val esResourceCampaign: String = configs.getString("es.resource.campaign")
  val esResourceEvent: String = configs.getString("es.resource.event")
  val esResourceSegment: String = configs.getString("es.resource.segment")
  val esReadConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true"
  )

  val esWriteConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.batch.size.bytes" -> "24mb",
    "es.batch.size.entries" -> "24000",
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.write.operation" -> "update",
    "es.index.auto.create" -> "true"
  )

  val awsLocationRoot: String = configs.getString("aws.location.root")
  val encryptedDir = configs.getString("sftp.sender.encryptedDir")
  val keyPath = configs.getString("sftp.sender.keyPath")
  
  var inputDir = configs.getString("sftp.sender.inputDir")
  var outputDir = configs.getString("sftp.sender.outputDir")
  var toEncrypt = configs.getBoolean("sftp.sender.toEncrypt")
}


object SFTPSender {

  def main(args: Array[String]): Unit = {

    implicit val formats: org.json4s.DefaultFormats = DefaultFormats

    // Start spark session
    val spark = SparkSession.
      builder().
      appName("SFTPSender").
      getOrCreate()

    // Initialize Configuration
    val conf = spark.sparkContext.hadoopConfiguration
    val sender = new SFTPSender

    val configs = ConfigFactory.load()

    // Get SFTP Configs
    val sftpHost = configs.getString("sftp.host")
    val sftpUser = configs.getString("sftp.user")
    val sftpPasswd = Util.getSecureCredential(Util.NS_USER_KEY)

    // Get the root dir and generate the file system
    val rootDir = sender.awsLocationRoot
    val hadoopFS = FileSystem.get(new URI(rootDir), conf)

    // Get the input/output dirs
    val inputDir = sender.inputDir
    val inputPath = new Path(inputDir)
    val outputDir = sender.outputDir

    // Get the failure path
    val failureDir = sender.awsLocationRoot + "failure/"
    val failurePath = new Path(failureDir)
    val successDir = sender.awsLocationRoot + "success/"
    val successPath = new Path(successDir)

    val toEncrypt = sender.toEncrypt
    var encryptedDir = ""
    var key = ""

    // Find all CSV files to encrypt and send
    val ls = hadoopFS.listStatus(inputPath).map(status => status.getPath())
    val statusesToSend = ls.map(path => hadoopFS.listStatus(path))
    val pathsToSend = statusesToSend.map(statuses => statuses.map(status => status.getPath()))

    // Flatten the arrays of files and make sure they are CSV files in the root dir
    // As an extra protection for deleting
    var toSend = pathsToSend.flatMap(path => path).filter(path => path.getName().contains(".csv") && path.toString.contains(rootDir))

    ArrowLogger.log.info("rootDir: " + rootDir)
    ArrowLogger.log.info("inputDir: " + inputDir)
    ArrowLogger.log.info("outputDir: " + outputDir)
    ArrowLogger.log.info("failureDir: " + failureDir)
    ArrowLogger.log.info("successDir: " + successDir)

    var results: Array[(String, Boolean)] = null

    if (toEncrypt) {
      encryptedDir = sender.encryptedDir
      key = sender.keyPath

      ArrowLogger.log.info("To Encrypt: " + toEncrypt)
      ArrowLogger.log.info("Encryption Dir: " + encryptedDir)
      ArrowLogger.log.info("Key Dir: " + key)

      // Encrypt all CSV files
      results = toSend.map(path => encryptAndSend(spark, hadoopFS, path.toString(), encryptedDir, key,
        outputDir, sftpHost, sftpUser, sftpPasswd))
    } else {
      results = toSend.map(path => sendSFTP(spark, hadoopFS, path.toString, outputDir + "/" + path.getName,
        sftpHost, sftpUser, sftpPasswd))

    }

    val success = results.filter(result => result._2)

    // Create success dir
    if (!hadoopFS.exists(successPath)) {
      hadoopFS.create(successPath)
    }

    // Send success to success folder
    success.foreach(result => moveFile(result._1, successDir + result._1.split("/").last, hadoopFS))

    // Create failure dir
    if (!hadoopFS.exists(failurePath)) {
      hadoopFS.create(failurePath)
    }

    // Send failures to failed folder
    results.filter(result => !result._2).foreach(result => moveFile(result._1, failureDir + result._1.split("/").last, hadoopFS))

    // Delete the left over parent if configured to do so
    val toDelete = results.map(result => (new Path(result._1)).getParent.toString)
    toDelete.filter(path => (path.contains(rootDir) && !path.equals(rootDir))).foreach(deleteFile(_, true, hadoopFS))

    // Update Campaign
    val campaignsToUpdate = results.filter(result => result._2).map(tuple => (new Path(tuple._1)).getName.split('-')(0))
    ArrowLogger.log.info("Campaigns To Update: ")
    campaignsToUpdate.foreach(campaign => ArrowLogger.log.info("To Update: " + campaign))
    campaignsToUpdate.foreach(campaignId => updateCampaignAndAudiences(campaignId, sender.esResourceCampaign,
      sender.esResourceAudience, sender.esResourceEvent, sender.esReadConf, sender.esWriteConf, spark))
  }

  /**
    *
    * @param src
    * @param dest
    * @param fs
    * @return
    */
  def moveFile(src: String, dest: String, fs: FileSystem): (String, Boolean) = {
    var suc = true

    ArrowLogger.log.info("Moving: " + src + " to " + dest)

    try {
      val srcPath = new Path(src)
      val destPath = new Path(dest)
      fs.rename(srcPath, destPath)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to move file: " + src + " to " + dest)
        ArrowLogger.log.error(e)
        suc = false
    }
    (src, suc)
  }

  /**
    *
    * @param campaignID
    * @param esResourceCampaign
    * @param esResourceAudience
    * @param esResourceEvent
    * @param esReadConf
    * @param esWriteConf
    * @param spark
    */
  def updateCampaignAndAudiences(campaignID: String, esResourceCampaign: String, esResourceAudience: String,
                                 esResourceEvent: String, esReadConf: mutable.Map[String, String],
                                 esWriteConf: mutable.Map[String, String], spark: SparkSession): Unit = {
    val audienceExported: JValue = ("dateLastExported", System.currentTimeMillis)
    val campaignTupled = getCampaignByID(campaignID, esResourceCampaign, esReadConf, spark.sparkContext)

    // Obtain a list of audiences within a campaign
    val parsedCampaigns = parseCampaign(campaignTupled).collect()(0)
    val audienceIDs = getCampaignAudienceIDs(parsedCampaigns)
    val audiences = audienceIDs.map(audienceid => (audienceid, getAudienceByID(audienceid, esResourceAudience, esReadConf, spark.sparkContext)))

    // Update Audiences
    audiences.foreach(audience => updateAudience(audience._2,
      audience._1,
      audienceExported,
      esResourceAudience,
      esWriteConf))

    // Update Campaign
    var campaignSyndicated: JValue = ("dateSyndicated", System.currentTimeMillis()) ~ ("isSyndicationQueued", false)
    updateCampaign(campaignTupled,
      campaignID,
      campaignSyndicated,
      esResourceCampaign,
      esWriteConf)
  }

  /**
    *
    * @param spark
    * @param fs
    * @param input
    * @param encryptedDir
    * @param key
    * @param output
    * @param host
    * @param username
    * @param pass
    * @return
    */
  def encryptAndSend(spark: SparkSession, fs: FileSystem, input: String, encryptedDir: String, key: String, output: String,
                     host: String, username: String, pass: String): (String, Boolean) = {
    val inputPath = new Path(input)
    val encryptedFile = encryptedDir + inputPath.getParent().getName() + inputPath.getName().split("-")(1) + ".encrypted"

    // Encrypt all CSV files
    val encryptResult = encrypt(input, encryptedFile, key, fs)

    ArrowLogger.log.info("Encrypted File" + encryptedFile)

    if (encryptResult._2) {
      val sendResult = sendSFTP(spark, fs, encryptedFile, output, host, username, pass)

      if (sendResult._2) {
        deleteFile(sendResult._1, false, fs)
      }
      return (input, sendResult._2)
    } else {
      return encryptResult
    }
  }

  /**
    *
    * @param filePath
    * @param encryptedFilePath
    * @param keyPath
    * @param fs
    * @return
    */
  def encrypt(filePath: String, encryptedFilePath: String, keyPath: String, fs: FileSystem): (String, Boolean) = {
    var suc = true
    try {
      PGPHadoop.encrypt(filePath, encryptedFilePath, keyPath, fs)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to encrypt file: " + filePath)
        ArrowLogger.log.error(e)
        e.printStackTrace()

        suc = false
    }

    (filePath, suc)
  }

  /**
    *
    * @param spark
    * @param fs
    * @param input
    * @param output
    * @param host
    * @param username
    * @param pass
    * @return
    */
  def sendSFTP(spark: SparkSession, fs: FileSystem, input: String, output: String,
               host: String, username: String, pass: String): (String, Boolean) = {
    var suc = true
    try {
      // Read file with spark
      val df = spark.read.textFile(input)
      val inputPath = new Path(input)

      ArrowLogger.log.info("Sending File: " + input)

      // Write to SFTP
      df.write.format("com.springml.spark.sftp").
        option("host", host).
        option("username", username).
        option("password", pass).
        option("fileType", "csv").
        option("delimiter", "\n").
        option("header", "false").
        option("tempLocation", "/tmp/").
        save(output + "/" + inputPath.getName)

    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to send file: " + input)
        ArrowLogger.log.error(e)
        e.printStackTrace()
        suc = false
    }

    (input, suc)
  }

  /**
    *
    * @param input
    * @param fs
    * @return
    */
  def deleteFile(input: String, recursive: Boolean, fs: FileSystem): (String, Boolean) = {
    var suc = true
    try {
      val path = new Path(input)
      fs.delete(path, recursive)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to delete file: " + input)
        ArrowLogger.log.error(e)
        e.printStackTrace()
        suc = false
    }
    (input, suc)
  }
}