package arrow.constants

/**
  * Created by sphifer on 6/27/17.
  */
object Constants {
  //Kafka Message Constants
  val IS_SYNDICATION_KEY : String = "syndicate"
  val AUDIENCE_ID_KEY : String = "id"
  val CAMPAIGN_ID_KEY : String = "campaign"
  val AD_ACCOUNT_ID: String = "adAccountId"

  //ElasticSearch Key Constants
  val PARTNER_KEY : String = "partner"
  val HASH_FORMAT_KEY : String = "hashFormat"
  val EXPORT_ID_KEY : String = "exportId"
  val DESTINATION_KEY: String = "destination"
  val EXPORT_BUCKETS_KEY : String = "exportBuckets"
  val AUDIENCE_NAME_KEY : String = "name"
  val CAMPAIGN_NAME_KEY : String = "productName"
  val CAMPAIGN_BUSINESS_UNIT_KEY : String = "businessUnit"
  val NEUSTAR_KEY = "neustar"
  val CAMPAIGN_AUDIENCE_KEY: String = "audiences"
  val CAMPAIGN_SEGMENTS_KEY: String = "externalSegments"
  val IS_SYNDICATION_ACTIVE_KEY : String = "isSyndicationActive"
  val IS_SYNDICATION_QUEUED_KEY : String = "isSyndicationQueued"
  val DATE_LAST_EXPORTED_KEY : String = "dateLastExported"
  val DATE_LAST_EXPORTED_ORACLE_KEY : String = "dateLastExportedOracle"
  val DATE_SYNDICATED_KEY : String = "dateSyndicated"
  val DATE_SYNDICATION_ENDS_KEY : String = "dateSyndicationEnds"
  val DATE_USER_MODIFIED_KEY: String = "dateUserModified"
  val LIVERAMP_KEY: String = "liveramp"
  val S3_KEY: String = "s3"
val FACEBOOK_KEY:String ="facebook"
  val GOOGLE_KEY:String ="google"
  //  ElasticSearch Date Patterns
  val DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

  //Bucket Name Keys
  val ORACLE_PARTNER_KEY = "oracle"
  val AWS_PARTNER_KEY = "aws"
  val MAID_KEY = "maid"
  val UID_KEY = "uid"
  val EMAIL_KEY = "email"
  val IP_KEY = "ip"
  val MAID_HASH_KEY = "maid_hash"

  val ADID_KEY = "adid"
  val IDFA_KEY = "idfa"
  val EMAIL_ADDRESS_KEY = "email_address"
  val CLIENT_IP_KEY = "client_ip"

  //Custom Segment Constants
  val WBCID_KEY = "wbcid"
  val NEUSTAR_HH_ID = "neustar_hhid"
  val ZIP_CODE = "zip_code"

  val INDEX = "index"

  //Hash Formats
  val SHA_KEY = "sha256"
  val MD5_KEY = "md5"

  // Export type mapping {key = (column_name, is_array)}
  var EXPORT_TYPES : Map[String,Array[(String, Boolean)]] = Map(
    WBCID_KEY -> Array[(String, Boolean)]((WBCID_KEY, false)),
    UID_KEY -> Array[(String, Boolean)]((UID_KEY, true)),
    MAID_KEY -> Array[(String, Boolean)]((MAID_KEY, true)),
    MAID_HASH_KEY -> Array[(String, Boolean)]((MAID_HASH_KEY, true)),
    EMAIL_KEY -> Array[(String, Boolean)]((EMAIL_ADDRESS_KEY, true)),
    IP_KEY -> Array[(String, Boolean)]((CLIENT_IP_KEY, true)),
    NEUSTAR_KEY -> Array[(String, Boolean)]((WBCID_KEY, false), (MAID_KEY, true), (EMAIL_ADDRESS_KEY, true), (CLIENT_IP_KEY, true))
  )
}
