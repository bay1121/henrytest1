package arrow.facebook
import scala.util.control.NonFatal
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.explode
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.sha2
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.json4s.DefaultFormats
import org.json4s.JNothing
import org.json4s.JValue
import org.json4s.JsonDSL.boolean2jvalue
import org.json4s.JsonDSL.long2jvalue
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.JsonDSL.string2jvalue
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.json4s.string2JsonInput
import arrow.FacebookSyndication
import arrow.config.ArrowLogger
import arrow.constants.Constants
import arrow.constants.Constants.AUDIENCE_ID_KEY
import arrow.constants.Constants.CAMPAIGN_ID_KEY
import arrow.constants.Constants.DATE_SYNDICATED_KEY
import arrow.constants.Constants.EXPORT_TYPES
import arrow.constants.Constants.IS_SYNDICATION_ACTIVE_KEY
import arrow.constants.Constants.IS_SYNDICATION_KEY
import arrow.constants.Constants.IS_SYNDICATION_QUEUED_KEY
import arrow.constants.Constants.FACEBOOK_KEY
import arrow.constants.Constants.AD_ACCOUNT_ID
import arrow.util.ArrowQueryBuilder
import arrow.util.ESUtil.getAudienceByID
import arrow.util.ESUtil.getAudienceIDs
import arrow.util.ESUtil.getCampaignByID
import arrow.util.JsonUtil.getExportFields
import arrow.util.JsonUtil.getExportTypes
import arrow.util.JsonUtil.getPartners
import arrow.util.MoviesAnywhereFilter
import arrow.util.ESUtil.updateAudience
import arrow.util.ESUtil.updateCampaign
import org.apache.spark.rdd.RDD

object SyndicateToFacebook {
  
  implicit val formats: org.json4s.DefaultFormats = DefaultFormats
  
 implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }
  def run(syn: FacebookSyndication, spark: SparkSession, messageJson: JValue) {
   
    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration

    //Get values from kafka message
    val syndicate = (messageJson \ IS_SYNDICATION_KEY).extract[Boolean]
    val audienceID = (messageJson \ AUDIENCE_ID_KEY).extract[String]
    val campaignID = (messageJson \ CAMPAIGN_ID_KEY).extract[String]
    
    //Allowed Facebook Syndication Types
    
    var allowedFacebookSyndicationTypes:Map[String,String] = Map(
        "email"->"EMAIL_SHA256",
        "maid" ->"MOBILE_ADVERTISER_ID"
        )

    //Get Audience and Campaign definitions from ElasticSearch
    val audience_tupled = getAudienceByID(audienceID, syn.esResourceAudience, syn.esReadConf, sc)
    val campaign_tupled = getCampaignByID(campaignID, syn.esResourceCampaign, syn.esReadConf, sc)

     try{
      var minDateLastExported : Long = 2147483647000L
      if (audience_tupled != null && syndicate) {
        //Get audience from ES
        val audience = parse(audience_tupled.first()._2)
        val campaign = parse(campaign_tupled.first()._2)
        ArrowLogger.log.info("Audience: " + audience)
        ArrowLogger.log.info("Campaign: " + campaign)
        if (isAudienceValid(audience)) {
        val queryBuilder = getQueryBuilder(audience,campaign)
        /*Valid Export Types that Facebook can take are Mobile Advertising ID and Email Address */
        val exportTypes = getExportTypes(campaign,FACEBOOK_KEY)
        ArrowLogger.log.info("exportTypes: " + exportTypes)

        val partnerKeys = getPartners(campaign)
        val exportFields = getExportFields(exportTypes)
        ArrowLogger.log.info("exportFields: " + exportFields.mkString(","))
        val ids = getAudienceIDs(queryBuilder.buildAsJson(), exportFields, syn.esResourceEvent, syn.esReadConf, spark)   
        var json: JValue = JNothing
        for (t <- exportTypes) {
          if (allowedFacebookSyndicationTypes.contains(t)){
            ArrowLogger.log.info ("Export Type being processed is " + t)
            val typeName = t
            val customAudienceId =getCustomAudienceId(syn,audience,t,audience_tupled,audienceID)
            val customAudienceIdJson: JValue = ("customAudienceId_"+t, customAudienceId)
            json =  customAudienceIdJson merge json
            val fieldNames = EXPORT_TYPES.getOrElse(t, null)
            val schema = StructType(Array(
              StructField("ids", StringType, nullable = true)
            ))
            var dataOut = spark.createDataFrame(sc.emptyRDD[Row], schema)
            if (fieldNames != null && fieldNames.length == 1) {
             val colName = fieldNames(0)._1
             if (ids.columns.contains(colName)) {
                // Have to check for ArrayType before we explode the IDs because WBCID is a StringType which breaks on explode()
                ids.dtypes.find(tuple => tuple._1 == colName) match {
                  case Some(dtypesTuple) =>
                    val dataSelect: DataFrame = if (dtypesTuple._2.startsWith("ArrayType")) {
                      ids.select(explode(col(s"`$colName`")).alias("ids"))
                    } else {
                      ids.select(col(s"`$colName`").alias("ids"))
                    }

                    val data = dataSelect.filter(col("ids").isNotNull).distinct
                    dataOut = dataOut.union(data)
                  case _ =>
                    ArrowLogger.log.warn(s"Didn't find colName ($colName) in ids.dtypes -- Skipping this")
                }
              }
              json = performFacebookSyndication(
                syn,
                spark,
                dataOut,
                typeName,
                customAudienceId,
                allowedFacebookSyndicationTypes,
                partnerKeys,
                json
              )
              val hasLastExportedForType = json.has(Constants.DATE_LAST_EXPORTED_KEY)
              if(hasLastExportedForType){
                  minDateLastExported = Math.min(minDateLastExported,(json \ (Constants.DATE_LAST_EXPORTED_KEY)).extract[Long])
              }
              val minminDateLastExportedJson:JValue = (Constants.DATE_LAST_EXPORTED_KEY,minDateLastExported)
              json = json merge minminDateLastExportedJson
            }
          }
          else
          {
            ArrowLogger.log.warn("Export Type is " + t + " and is not valid Facebook Syndication Target")
          }
        }

        updateAudience(audience_tupled,
          audienceID,
          json,
          syn.esResourceAudience,
          syn.esWriteConf
        )
      }
      }else {
        ArrowLogger.log.info("Skipping syndication.")
      }
      
      val dateSyndicated: JValue = (DATE_SYNDICATED_KEY, minDateLastExported)
      val syndicationMonitoringJson = (IS_SYNDICATION_ACTIVE_KEY, syndicate) ~ (IS_SYNDICATION_QUEUED_KEY, false) 
      val jsonWithDate = syndicationMonitoringJson merge dateSyndicated
      ArrowLogger.log.info(jsonWithDate)
      updateCampaign(campaign_tupled, campaignID, jsonWithDate, syn.esResourceCampaign, syn.esWriteConf)
    }
    catch{
      case NonFatal(e) =>
        val syndicationMonitoringJson = (IS_SYNDICATION_ACTIVE_KEY, syndicate) ~ (IS_SYNDICATION_QUEUED_KEY, false) 
        ArrowLogger.log.warn("Encountered an error during syndication. Updating ES anyway.")
        ArrowLogger.log.info(syndicationMonitoringJson)
        updateCampaign(campaign_tupled, campaignID, syndicationMonitoringJson, syn.esResourceCampaign, syn.esWriteConf)
        throw e
    }
  }
  
    /**
    * Method for returning the Custom Audience ID if it is already available as part of Audience Information in Arrow. If not, it will generate
    * new custom audience id
    * @param audience, adAccountID
    * @return audienceIdFromFB
    */

  def getCustomAudienceId(syn:FacebookSyndication,audience:JValue,exportType:String,audience_tupled: RDD[(String, String)]=null,audienceID: String=null) = {
       
        val audienceName:String = (audience \ "name").extract[String]
        val audienceDescription:String = exportType match {
          case "email" => "Email IDs"
          case  "maid" =>"Mobile IDs"
          case  "_" => "Not applicable"
        }
        val endPointBase=syn.endPointBase
        val videoBase=syn.videoBase
        val apiVersion=syn.apiVersion

        audience.has("customAudienceId_"+exportType) match {
          case true => {
            val audienceIdFromFB = (audience \ ("customAudienceId_"+exportType)).extract[Long]
            ArrowLogger.log.info("Facebook Audience ID is found and skipping Facebook Lookup: " + audienceIdFromFB)
            audienceIdFromFB
          }
          case _ => {
            val msg = "Missing customer audience id customAudienceId_"+exportType + "in campaign."
            ArrowLogger.log.info(msg)
            val audienceIdFromFB=FacebookUtils.generateAudienceId(audienceName ,audienceDescription,endPointBase,videoBase,apiVersion)
            ArrowLogger.log.info("Audience ID: " + audienceIdFromFB)
            ArrowLogger.log.info("Updating Audience ID in ElasticSearch")
            val customAudienceIdJson: JValue = ("customAudienceId_"+exportType, audienceIdFromFB)

            updateAudience(audience_tupled,
              audienceID,
              customAudienceIdJson,
              syn.esResourceAudience,
              syn.esWriteConf
            )
            audienceIdFromFB
          }
        }
  }

  def syndicateFromS3(spark:SparkSession,typeName: String, filePath: String, audienceName: String): Unit = {
    val sc = spark.sparkContext
    val sourceDF = spark.read.csv(filePath)
    val sourceFilter = handleHashing(sourceDF.filter(col("_c0").isNotNull).withColumnRenamed("_c0","ids"))

    ArrowLogger.log.info("Input TypeName: "+typeName+" , SourceFilepath: "+filePath +" ,audienceName: "+audienceName)
    val totalAudienceUIDs = sourceFilter.count
    var numberOfGroups = math.round(totalAudienceUIDs.toDouble / 9500).toInt
    var audienceId: Long = 0


    val allowedFacebookSyndicationTypes: Map[String, String] = Map(
      "email" -> "EMAIL_SHA256",
      "maid" -> "MOBILE_ADVERTISER_ID"
    )
    if (allowedFacebookSyndicationTypes.contains(typeName)) {
      val audienceDescription: String = typeName match {
        case "email" => "Email IDs"
        case "maid" => "Mobile IDs"
        case "_" => "Not applicable"
      }
      ArrowLogger.log.info("Export Type being processed is " + typeName)

      ArrowLogger.log.info("Total " + typeName + "'s from dataframe: " + totalAudienceUIDs)
      ArrowLogger.log.info("Number of groups to make: " + numberOfGroups)

      val checkID = FacebookUtils.checkAudienceExistence(audienceName,typeName)
      if (checkID == 0L) {
        ArrowLogger.log.info("Audience Id is not present in Facebook account. Hence creating a new audience list")
        audienceId=FacebookUtils.generateAudienceId(audienceName,audienceDescription)
      }
      else {
        ArrowLogger.log.info("Audience Id is already present in Facebook account. Hence audience list from S3 file will be uploaded to existing audience_id: "+checkID)
        audienceId = checkID
      }
      if (numberOfGroups > 0) {
        val countAccum = sc.longAccumulator
        val chunk_rdd = sourceFilter.rdd.map(x => {
          countAccum.add(1L)
          (countAccum.value % numberOfGroups, List(x.getAs[String](0)))
        }
        ).reduceByKey(_ ++ _)
        val metrics = chunk_rdd.map(input => {

          FacebookUtils.addUsersToCustomAudience(audienceId, allowedFacebookSyndicationTypes.getOrElse(typeName,""), input._2)
        }).collect()
      }
      else {
        ArrowLogger.log.info("No groups to syndicate to Facebook for Facebook audience: " + audienceId)
      }
    } else {
      ArrowLogger.log.warn("Export Type is " + typeName + " and is not valid Facebook Syndication Target. Valid Target types: email,maid")
    }
  }
  /**
    * Method to perform SHA256 Hashing.Facebook expects the following rules to be applied while hashing 
    * 1. Email addresses - Use key EMAIL. Trimming leading and trailing whitespace and convert all characters to lowercase.
    * 2. Mobile advertiser id - MADID, all lower case. Keep hyphens.
    * 3. Expectation is that fields are formatted already applying above rules and the below method handles only Hashing
    * 4. Facebook recognizes only SHA256 hashing
    *
    * @param df
    * @return
    */

  def handleHashing(df: DataFrame):DataFrame = {
      df.withColumn("hashed", sha2(lower(trim(col("ids"))), 256)).
        drop("ids").
        withColumnRenamed("hashed", "ids") 
  }
  
  /**
    * Method for doing any validation on audience. All validations on Audience to be included in this method
    *
    * @param audience
    * @return
    */
  
  def isAudienceValid(audience:JValue):Boolean = {    
     if (!audience.has("query")) {
            val msg = "The audience is missing a query!"
            ArrowLogger.log.error(msg)
            throw new RuntimeException(msg)
      }
      true
  }
  
   /**
    * Method responsible for building query extracting information from both campaign and audience as appropriate
    * @param audience, campaign
    * @return
    */
  
  def getQueryBuilder(audience:JValue, campaign:JValue) = {
        val originalQuery: String = (audience \ "query").extract[String]
        val queryBuilder = new ArrowQueryBuilder(originalQuery, childType = "arrow_child")
        if (campaign.has(Constants.CAMPAIGN_BUSINESS_UNIT_KEY)) {
            val businessUnit = (campaign \ Constants.CAMPAIGN_BUSINESS_UNIT_KEY).extract[String]
            MoviesAnywhereFilter.applyFilterToQueryBuilder(businessUnit, queryBuilder)
        } else {
            val msg = s"Missing mandatory field (${Constants.CAMPAIGN_BUSINESS_UNIT_KEY}) in campaign."
            ArrowLogger.log.error(msg)
            throw new RuntimeException(msg)
        }
        
        val hasLastExported = audience.has(Constants.DATE_LAST_EXPORTED_KEY)
        val hasDateUserModified = audience.has(Constants.DATE_USER_MODIFIED_KEY)
        
        if (hasLastExported && !hasDateUserModified) {
           ArrowLogger.log.warn(s"${Constants.DATE_LAST_EXPORTED_KEY} was found, but not " +
                                s"${Constants.DATE_USER_MODIFIED_KEY}. Adding Filter.")
          val lastSyndicationDate = (audience \ Constants.DATE_LAST_EXPORTED_KEY).extract[Long]

          val filter = Map("range" ->
            Map("time_stats.time" ->
            Map("gte" -> lastSyndicationDate)
            )
          )
          queryBuilder.addFilter(filter)
         }
        else if (hasLastExported && hasDateUserModified) {
          val lastSyndicationDate = (audience \ Constants.DATE_LAST_EXPORTED_KEY).extract[Long]
          val dateUserModified = (audience \ Constants.DATE_USER_MODIFIED_KEY).extract[Long]
          if (dateUserModified > lastSyndicationDate) {
                ArrowLogger.log.info("The audience user modification date is greater than the last syndication date. No filter will be added.")
           } else {
                ArrowLogger.log.info("The audience user modification date is less than or equal to the last syndication date. Adding filter.")
                val filter = Map("range" ->
                  Map("time_stats.time" ->
                    Map("gte" -> lastSyndicationDate)
                  )
                )
                queryBuilder.addFilter(filter)
          }
         } else {
           ArrowLogger.log.info(s"No ${Constants.DATE_LAST_EXPORTED_KEY} key found in audience document. No filter will be added.")
        }
        
        queryBuilder
  }

  /**
    * Method to perform Facebook Syndication
    *
    * @param syn
    * @param spark
    * @param data
    * @param customAudienceId
    * @param partnerKeys
    * @param jsonStub
    * @return
    */
  
  def performFacebookSyndication(
                       syn: FacebookSyndication,
                       spark: SparkSession,
                       data: DataFrame,
                       typeName:String,
                       customAudienceId:Long,
                       allowedFacebookSyndicationTypes:Map[String,String],
                       partnerKeys: Seq[String],
                       jsonStub: JValue): JValue = {
    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration
    var json = jsonStub
    var dataOut = handleHashing(data)
    val endPointBase = syn.endPointBase
    val videoBase=syn.videoBase
    val apiVersion=syn.apiVersion
    ArrowLogger.log.info("Partner Keys is " +partnerKeys.toString())
    for (p <- partnerKeys) {
      val schema = StructType(Array(StructField("ids", StringType, nullable = true)))
      val limit = syn.bulkmax;
      
      val totalAudienceUIDs = dataOut.count
      val numberOfGroups = math.ceil(totalAudienceUIDs.toDouble / syn.bulkmax).toInt

      ArrowLogger.log.info("Total "+typeName+"'s from dataframe: "+totalAudienceUIDs)
      ArrowLogger.log.info("Number of groups to make: " + numberOfGroups)

    if (numberOfGroups > 0) {
      val countAccum = sc.longAccumulator
      val chunk_rdd = dataOut.rdd.map(x => {
        countAccum.add(1L)
        (countAccum.value % numberOfGroups, List(x.getAs[String](0)))
      }
      ).reduceByKey(_ ++ _)
    
    val metrics = chunk_rdd.map(x => {
        FacebookUtils.addUsersToCustomAudience(customAudienceId, allowedFacebookSyndicationTypes.getOrElse(typeName,""), x._2,endPointBase,videoBase,apiVersion)
      }).collect()
    }
    else {
      ArrowLogger.log.info("No groups to syndicate to Facebook for Facebook audience: " +customAudienceId )
    }
    }
    val dateExportedJSON: JValue = (Constants.DATE_LAST_EXPORTED_KEY,System.currentTimeMillis())
    json = json merge dateExportedJSON
    json
  }
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("SyndicateToFacebook")
      .getOrCreate()
    try{
    syndicateFromS3(spark,args(0),args(1),args(2))}
    catch {
      case e: Throwable =>
        ArrowLogger.log.error("Unable to complete Facebook Syndication for input audience type: "+args(0) +" file path: "+ args(1)+" audience Name: "+args(2) )
        ArrowLogger.log.error(s"$e\n\n${e.getStackTrace.mkString("\n")}")
  }
}}