package arrow.facebook

import com.facebook.ads.sdk._
import scala.collection.JavaConversions._
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, JObject, JValue, _}
import arrow.config.{ArrowLogger,Util}
import com.typesafe.config.{Config,ConfigFactory}
import scala.util.parsing.json._

object FacebookUtils {
  implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }
  
  val configs = ConfigFactory.load()
  val accessToken = Util.getSecureCredential("facebook.access.token")
  val appSecret = Util.getSecureCredential("facebook.app.secret")
  val appId = configs.getString("facebook.app.id")
  val adAccount = configs.getString("facebook.ad.account")
  

  
   def generateAudienceId(name:String,description: String,endPointBase:String=APIConfig.DEFAULT_API_BASE,videoBase:String=APIConfig.DEFAULT_VIDEO_API_BASE,apiVersion:String=APIConfig.DEFAULT_API_VERSION,
       subType:CustomAudience.EnumSubtype = CustomAudience.EnumSubtype.VALUE_CUSTOM,customerFileSource:CustomAudience.EnumCustomerFileSource = CustomAudience.EnumCustomerFileSource.VALUE_USER_PROVIDED_ONLY):Long = {
      implicit val formats: org.json4s.DefaultFormats = DefaultFormats

      val context:APIContext  = new APIContext(endPointBase, videoBase, apiVersion, accessToken, appSecret).enableDebug(false);
      try{
      ArrowLogger.log.info("Name is " + name + " and description is " + description)
      
       ArrowLogger.log.info("Audience does not exist and hence creating new Custom Audience ID" )
       val resp = new AdAccount(FacebookUtils.adAccount, context).createCustomAudience()
      .setName(name)
      .setSubtype(subType)
      .setDescription(description)
      .setCustomerFileSource(customerFileSource)
      .execute()
      val audienceObj = parse(resp.toString()).asInstanceOf[JObject]
      val customAudienceId =(audienceObj \"id") match {
            case JString(s) => s.toLong
            case _ => 0
      } 
      ArrowLogger.log.info("Audience ID Generated/provided by Facebook is " + customAudienceId)
      if (customAudienceId == 0 ){
        throw new Exception("Problem generating the Audience ID in Facebook")
      }
      customAudienceId  
      }
      catch{
        case e: Exception => {
          ArrowLogger.log.error("Exception Message is " + e.getMessage())
          throw e
        }
      } 
  }
  
  def addUsersToCustomAudience(customAudienceId:Long, schema:String, listOfUsers:List[String],endPointBase:String=APIConfig.DEFAULT_API_BASE,videoBase:String=APIConfig.DEFAULT_VIDEO_API_BASE,apiVersion:String=APIConfig.DEFAULT_API_VERSION) = {
      implicit val formats: org.json4s.DefaultFormats = DefaultFormats
      try {
      if (customAudienceId == 0){
        ArrowLogger.log.info("No Custom Audience ID being passed and hence users cannot be added")
        throw new Exception("No Custom Audience ID being passed and hence users cannot be added")
      }
      else{
      val context:APIContext  = new APIContext(endPointBase, videoBase, apiVersion, accessToken, appSecret).enableDebug(false)
      var json:JValue = ("schema", schema) ~ ("data", listOfUsers)
      ArrowLogger.log.info("Size of List " + listOfUsers.size) 
      val resp = new CustomAudience(customAudienceId, context).createUser().setPayload(compact(render(json)))
      .execute()
      val respObject = parse(resp.getRawResponseAsJsonObject().toString())
      ArrowLogger.log.info("Number of entries " + (respObject \ "num_received").extract[Int])
      ArrowLogger.log.info("Number of invalid entries " + (respObject \ "num_invalid_entries").extract[Int])
      ArrowLogger.log.info("Invalid entry samples " + (respObject \ "invalid_entry_samples").extract[Object])
     }
     }     
     catch{
        case e: Exception => {
          ArrowLogger.log.error("Exception Message is " + e.getMessage())
          throw e 
        }
      }
  }
  /*Following method can be used for checking for existence of Audience based on Audience Name and it returns the Audience ID from Facebook.
  */

  def checkAudienceExistence(name:String,typeName: String,endPointBase:String=APIConfig.DEFAULT_API_BASE,videoBase:String=APIConfig.DEFAULT_VIDEO_API_BASE,apiVersion:String=APIConfig.DEFAULT_API_VERSION): Long = {
    try{
      implicit val formats: org.json4s.DefaultFormats = DefaultFormats
    val context: APIContext = new APIContext(endPointBase, videoBase, apiVersion, accessToken, null).enableDebug(false)
    ArrowLogger.log.info("audienceName: " + name + " ,type name :" + typeName)
    val filter = "[{ \"field\": \"name\",\"operator\": \"CONTAIN\", \"value\":\"" + name + "\"}]"
    val respExistingAudience = new AdAccount(adAccount, context).getCustomAudiences().setFiltering(filter).requestField("id").requestField("description").execute().getRawResponseAsJsonObject

    val dataArray = respExistingAudience.get("data").getAsJsonArray()

    var audienceId: Long = 0L
    var description: String = null
    if (dataArray.size() > 0) {
      if (typeName.toLowerCase.trim == "email") description = "Email IDs" else if (typeName.trim.toLowerCase == "maid") description = "Mobile IDs"
      for (s <- 0 until dataArray.size) {

        if (dataArray.get(s).getAsJsonObject.get("description").getAsString == description)
          audienceId = dataArray.get(s).getAsJsonObject.get("id").getAsLong
      }
    }
    audienceId
  }
    catch{
      case e: Exception => {
        ArrowLogger.log.error("Exception Message is " + e.getMessage())
        throw e
      }
    }
  }


}