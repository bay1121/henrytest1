//package arrow.bluekai
//
//import arrow.Syndication
//import arrow.bluekai.Util._
//import arrow.config.ArrowLogger
//import arrow.constants.Constants._
//import arrow.util.ESUtil
//import org.apache.spark.sql.SparkSession
//import org.joda.time.DateTimeZone
//import org.joda.time.format.DateTimeFormat
//import org.json4s._
//import org.json4s.jackson.JsonMethods.parse
//
//object NightlySyndication {
//  def run(syn: Syndication, spark: SparkSession) {
//    implicit val formats: org.json4s.DefaultFormats = DefaultFormats
//
//    implicit class JValueExtended(value: JValue) {
//      def has(childString: String): Boolean = {
//        (value \ childString) != JNothing
//      }
//    }
//
//    val sc = spark.sparkContext
//    //Get All Audiences
//    val audienceMetas = ESUtil.getAllAudienceMetaData(syn.esResourceAudience, syn.esReadConf, sc)
//
//    //Iterate over audiences
//    for (audienceMeta <- audienceMetas) {
//      //For Each Audience load audience data
//      val audienceID = audienceMeta.get("id").orNull
//      val audienceName = audienceMeta.get("name").orNull
//
//      if (audienceID != null && audienceName != null) {
//        val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).withTime(23, 59, 59, 0)
//
//        try {
//          ArrowLogger.log.info("Working on Audience: " + audienceID + " aka " + audienceName)
//
//          val audience = ESUtil.getAudienceByID(audienceID, syn.esResourceAudience, syn.esReadConf, sc)
//          val audienceParsed = parse(audience.first()._2)
//
//          //Get audience data
//          val ids = ESUtil.getAudienceIDs(audienceParsed, EXPORT_TYPES.values.flatten.toArray, syn.esResourceEvent, syn.esReadConf, spark)
//          val audienceQueryParams: scala.Predef.Map[String, Array[String]] = scala.Predef.Map[String, Array[String]](
//            "audiences.id" -> Array[String](audienceID),
//            "partner.oracle" -> Array[String]("true")
//          )
//
//          //Get campaigns that reference this audience
//          val campaigns = ESUtil.esQueryWithKeyValueMap(audienceQueryParams, syn.esResourceCampaign, syn.esReadConf, sc).collect
//          val campaignIDsSyndicate = campaigns.filter(x =>
//            parse(x._2).has(IS_SYNDICATION_ACTIVE_KEY) &&
//              (parse(x._2) \ IS_SYNDICATION_ACTIVE_KEY).extract[Boolean] &&
//              parse(x._2).has(DATE_SYNDICATION_ENDS_KEY) &&
//              DateTimeFormat.forPattern(DATE_PATTERN).withZone(DateTimeZone.UTC).parseDateTime((parse(x._2) \ DATE_SYNDICATION_ENDS_KEY).extract[String]).getMillis >= now.getMillis).map(campaign => campaign._1)
//
//          val campaignIDsEnded = campaigns.filter(x =>
//            parse(x._2).has(IS_SYNDICATION_ACTIVE_KEY) &&
//              (parse(x._2) \ IS_SYNDICATION_ACTIVE_KEY ).extract[Boolean] &&
//              parse(x._2).has(DATE_SYNDICATION_ENDS_KEY) &&
//            DateTimeFormat.forPattern(DATE_PATTERN).withZone(DateTimeZone.UTC).parseDateTime((parse(x._2) \ DATE_SYNDICATION_ENDS_KEY).extract[String]).getMillis < now.getMillis).map(campaign => campaign._1)
//
//          if(campaignIDsSyndicate.length > 0) {
//            ArrowLogger.log.info("Syndicating: "  + audienceID + " aka " + audienceName + " supporting campaigns " + campaignIDsSyndicate.mkString(", "))
//            syndicateBulk(syn, spark, audienceID, campaignIDsSyndicate)
//          } else if(campaignIDsEnded.length > 0) {
//            ArrowLogger.log.info("Desyndicating: "  + audienceID + " aka " + audienceName + " supporting campaigns " + campaignIDsEnded.mkString(", "))
//            endSyndicationBulk(syn, sc, audienceID, campaignIDsEnded)
//          }
//        } catch {
//          case e: Exception =>
//            ArrowLogger.log.info("Unable to syndicate to bluekai, missing required information. audienceID: " + audienceID + ", audienceName: " + audienceName)
//            ArrowLogger.log.error(e.printStackTrace())
//        }
//      }
//    }
//  }
//}
