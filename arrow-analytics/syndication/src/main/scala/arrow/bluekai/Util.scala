package arrow.bluekai

import java.time.Instant

import arrow.Syndication
import arrow.config.ArrowLogger
import arrow.constants.Constants
import arrow.util.ESUtil.{ getAudienceByID, getCampaignByID, updateAudience, updateCampaign }
import arrow.util.{ ArrowQueryBuilder, EmailUtil, MoviesAnywhereFilter }
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.log4j.Logger
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.{ col, explode }
import org.apache.spark.sql.{ Dataset, Row, SparkSession }
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods.{ parse, render }
import org.json4s.native.Serialization.write
import org.json4s.{ DefaultFormats, JObject, JValue, _ }
import scalaj.http.{ Http, HttpRequest, HttpResponse }

import scala.util.control.NonFatal

object Util {
  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  implicit class JValueExtended(value: JValue) {
    def has(childString: String): Boolean = {
      (value \ childString) != JNothing
    }
  }

  /**
   * Generating the method request with arbitrary timeouts
   *
   * @param url
   * @param method
   * @param data
   * @param headers
   * @return
   */
  def doRequest(url: String, method: String, data: String, headers: scala.Predef.Map[String, String], syn: Syndication): HttpResponse[String] = {
    val response = Http(url).headers(headers).timeout(connTimeoutMs = 30 * 1000, readTimeoutMs = 30 * 1000).postData(data).method(method).asString
    val maxIndividualLength = 100000 // an arbitrary amount to make email look ok

    if (response.code >= 400) {
      val subject = s"${response.code} Response code from Syndication Http call"
      val body = s"Headers: ${response.headers}\n\n" +
        s"Url: $url\n\n" +
        s"Code: ${response.code}\n\n" +
        s"Response Body: ${response.body.take(maxIndividualLength)}\n\n" +
        s"Full: ${response.toString.take(maxIndividualLength)}"

      EmailUtil.send(syn.alertId, syn.defaultEmail, subject, body, syn.awsSmtpHost, syn.awsSmtpUser, syn.awsSmtpKey)
    }

    response
  }

  /**
   * Gets audience uids based on an audience's information from Elasticsearch
   */
  def getAudienceUIDs(syn: Syndication, spark: SparkSession, query: String): Dataset[Row] = {
    val es_conf = syn.esReadConf.clone()
    es_conf += ("es.output.json" -> "false")
    es_conf += ("es.resource" -> syn.esResourceEvent)
    es_conf += ("es.read.field.as.array.include" -> "uid")
    es_conf += ("es.input.max.docs.per.partition" -> "1000000")
    es_conf += ("pushdown" -> "true")
    es_conf += ("es.query" -> query)

    ArrowLogger.log.info(s"Getting audience UIDs with query: $query")
    spark.read.format("org.elasticsearch.spark.sql")
      .options(es_conf)
      .load
      .select(explode(col("uid")).alias("uid"))
      .distinct
  }

  /**
   *
   * @param uid_list
   * @param audienceID
   * @param syn
   * @return
   */
  def submitToBlueKai(syn: Syndication, uid_list: List[String], audienceID: String): (String, Int) = {

    var uidData = scala.collection.mutable.ListBuffer[JValue]()
    uid_list.foreach({ uid =>
      uidData += syn.buildUser(audienceID, uid)
    })

    // Set type to "Detail" for detail response
    val data = ("ResponseType", "Summary") ~ ("Method", "POST") ~ ("ResponseCallbackUrl", syn.callbackURL) ~ ("Scatter", uidData)

    val signedURL = syn.signatureBulkBuilder(write(data))
    val response = doRequest(signedURL, "POST", write(data), syn.BULK_HEADERS, syn)

    ArrowLogger.log.info("request headers: " + response.headers.mkString(", "))
    ArrowLogger.log.info("response: " + response)
    (audienceID + " | " + uid_list.length + " | " + uidData.length + " | " + response.headers + " | " + signedURL + " | " + response.code + " | " + response.body, response.code)
  }

  /**
   *
   * @param audience
   * @param parent
   * @return
   */
  def build_retired_category(audience: JValue, parent: Int): JObject = {
    ("name", (audience \ "name").extract[String] + "-Retired-" + Instant.ofEpochMilli(System.currentTimeMillis)) ~ ("parent_id", parent) ~ ("notes", "Retired via ARROW syn process.")
  }

  /**
   * Returns the BlueKai JSON body for a self-classification rule
   *
   * @param audience
   * @param audienceID
   * @param category
   * @param partnerID
   * @param siteID
   * @return
   */
  def build_rule(audience: JValue, audienceID: String, category: String, partnerID: String, siteID: String): String = {
    write(("name", "P-Hint Rule for " + (audience \ "name").extract[String]) ~
      ("type", "phint") ~
      ("partner_id", partnerID) ~
      ("site_ids", List(siteID)) ~
      ("category_ids", List(category)) ~
      ("phints", List((("key", "audience") ~ ("operator", "is") ~ ("value", audienceID)))))
  }

  /**
   *
   * @param audience
   * @param audienceID
   * @param parent_categoryID
   * @return
   */
  def build_category(audience: JValue, audienceID: String, parent_categoryID: Int): JObject = {
    ("name", audience \ "name") ~
      ("parent_id", parent_categoryID) ~
      ("description", "ARROW audience for ID: " + audienceID) ~
      ("analytics_excluded", "false") ~
      ("navigation_only", "false") ~
      ("mutex_children", "false") ~
      ("notes", "Created via ARROW syn process.")
  }

  def performEndSyndication(syn: Syndication, sc: SparkContext, audienceID: String) {
    val audience_tupled = getAudienceByID(audienceID, syn.esResourceAudience, syn.esReadConf, sc)
    val this_audience = parse(audience_tupled.first()._2)
    var category: String = null

    if (this_audience.has("bluekaiCategoryID")) {
      category = (this_audience \ "bluekaiCategoryID").extract[String]
    }

    //If not syndicated, do nothing - should this check one of the other values ?
    if (category == null) {
      ArrowLogger.log.error("Audience not created, category is null. AudienceID: " + audienceID)
      throw new RuntimeException("Category is null :(")
    }

    val data = build_retired_category(this_audience, syn.retirecategoryid)
    val signedURL = syn.signatureInputBuilder(syn.classificationURL + "/" + category, "PUT", write(data))
    val result: HttpResponse[String] = doRequest(signedURL, "PUT", write(data), syn.HEADERS, syn)
    ArrowLogger.log.info("Result: " + result)
    ArrowLogger.log.info("Audience update end time: " + System.currentTimeMillis)
  }

  def endSyndication(syn: Syndication, sc: SparkContext, message: JValue) {
    val audienceID = (message \ "id").extract[String]
    val campaignID = (message \ "campaign").extract[String]
    val campaign_tupled = getCampaignByID(campaignID, syn.esResourceCampaign, syn.esReadConf, sc)
    val desyndicationMonitoringJSON = ("isSyndicationActive", false) ~
      ("isSyndicationQueued", false) ~
      ("dateSyndicationEnded", System.currentTimeMillis())

    try {
      performEndSyndication(syn, sc, audienceID)
      updateCampaign(campaign_tupled, campaignID, desyndicationMonitoringJSON, syn.esResourceCampaign, syn.esWriteConf)
      ArrowLogger.log.info("Campaign update end time: " + System.currentTimeMillis)
    } catch {
      case NonFatal(e) =>
        ArrowLogger.log.warn("Updating campaign even though an exception was caught during endSyndication")
        updateCampaign(campaign_tupled, campaignID, desyndicationMonitoringJSON, syn.esResourceCampaign, syn.esWriteConf)
        ArrowLogger.log.info("Campaign update end time: " + System.currentTimeMillis)
        throw e
    }
  }

  /**
   *
   * @param syn
   * @param sc
   * @param audienceID
   * @param campaignIDs
   */
  def endSyndicationBulk(syn: Syndication, sc: SparkContext, audienceID: String, campaignIDs: Array[String]) {
    ArrowLogger.log.info("Campaign update start time: " + System.currentTimeMillis)

    performEndSyndication(syn, sc, audienceID)

    val json = ("dateSyndicationEnded", System.currentTimeMillis()) ~ ("isSyndicationActive", false) ~ ("isSyndicationQueued", false)
    for (campaignID <- campaignIDs) {
      updateCampaign(campaignID, json, syn.esResourceCampaign, syn.esWriteConf, syn.esReadConf, sc)
    }

    ArrowLogger.log.info("Campaign update end time: " + System.currentTimeMillis)
  }

  def performSyndication(syn: Syndication, spark: SparkSession, audienceID: String, campaignDocument: JValue): Unit = {
    val sc = spark.sparkContext
    val audience_tupled = getAudienceByID(audienceID, syn.esResourceAudience, syn.esReadConf, sc)
    ArrowLogger.log.debug(audience_tupled.first()._2.asInstanceOf[String])

    //De - tuple the data to make it easier to use, and assume only one audience is returned.
    val thisAudience = parse(audience_tupled.first()._2.asInstanceOf[String])

    // Go build the category based on audience data.
    // NOTE: we use the filter method on a string here to remove all non-printable characters
    var data = write(build_category(thisAudience, audienceID, syn.parentCategoryId)).filter(_ >= ' ')
    var savedCategory: String = null
    var category: HttpResponse[String] = null

    if (thisAudience.has("bluekaiCategoryID")) {
      savedCategory = (thisAudience \ "bluekaiCategoryID").extract[String]
    }

    //2. Is the audience already syndicated ?
    // We have a category, go update BlueKai.
    if (savedCategory != null) {
      val signedURL = syn.signatureInputBuilder(syn.classificationURL + '/' + savedCategory, "PUT", data)
      category = doRequest(signedURL, "PUT", data, syn.HEADERS, syn)
    } // We don't have a category, get one from BlueKai.
    else {
      val signedURL = syn.signatureInputBuilder(syn.classificationURL, "POST", data)
      category = doRequest(signedURL, "POST", data, syn.HEADERS, syn)
      savedCategory = (parse(category.body) \ "id").extract[String]
    }

    ArrowLogger.log.info("Syndication.syndicate savedCategory: " + savedCategory)
    ArrowLogger.log.info("Syndication.syndicate category: " + category)

    if (category.isError) {
      ArrowLogger.log.error("Category request to BK failed. Error: " + category.body)
      throw new RuntimeException("Category returned from BlueKai was not valid. Returned category error: " +
        category.body + " \ncode: " + category.code)
    }

    // 3. Update the audience with the category id we just got from Bluekai
    val audienceJSON: JValue = (Constants.DATE_LAST_EXPORTED_KEY, System.currentTimeMillis) ~
      (Constants.DATE_LAST_EXPORTED_ORACLE_KEY, System.currentTimeMillis) ~
      ("bluekaiCategoryID", savedCategory)

    updateAudience(audience_tupled, audienceID, audienceJSON, syn.esResourceAudience, syn.esWriteConf)
    ArrowLogger.log.info("Audience updated in ES: " + audienceID)

    //4. Create the p -hint rule for the category
    // NOTE: we use the filter method on a string here to remove all non-printable characters
    data = build_rule(thisAudience, audienceID, savedCategory, syn.partnerID, syn.siteID).filter(_ >= ' ')
    val signedURL = syn.signatureInputBuilder(syn.classificationRuleURL, "POST", data)

    // NOTE: IT IS OK IF Bluekai says this rule exists already; Other errors are NOT ok
    val classificationRule = doRequest(signedURL, "POST", data, syn.HEADERS, syn)

    ArrowLogger.log.info("Classification Rule Data " + data.toString)
    ArrowLogger.log.info("Classification Rule " + classificationRule)

    if (!thisAudience.has("query")) {
      val msg = "The audience is missing a query!"
      ArrowLogger.log.error(msg)
      throw new RuntimeException(msg)
    }

    val originalQuery = (thisAudience \ "query").extract[String]
    // TODO: What is a better way to deal with this hard-coded child type string?
    val queryBuilder = new ArrowQueryBuilder(originalQuery, childType = "arrow_child")

    val hasLastExportedOracle = thisAudience.has(Constants.DATE_LAST_EXPORTED_ORACLE_KEY)
    val hasDateUserModified = thisAudience.has(Constants.DATE_USER_MODIFIED_KEY)

    if (hasLastExportedOracle && !hasDateUserModified) {
      ArrowLogger.log.warn(s"${Constants.DATE_LAST_EXPORTED_ORACLE_KEY} was found, but not " +
        s"${Constants.DATE_USER_MODIFIED_KEY}. Adding Filter.")
      val lastSyndicationDate = (thisAudience \ Constants.DATE_LAST_EXPORTED_ORACLE_KEY).extract[Long]

      val filter = Map("range" ->
        Map("time_stats.time" ->
          Map("gte" -> lastSyndicationDate)))
      queryBuilder.addFilter(filter)

    } else if (hasLastExportedOracle && hasDateUserModified) {
      val lastSyndicationDate = (thisAudience \ Constants.DATE_LAST_EXPORTED_ORACLE_KEY).extract[Long]
      val dateUserModified = (thisAudience \ Constants.DATE_USER_MODIFIED_KEY).extract[Long]

      if (dateUserModified > lastSyndicationDate) {
        ArrowLogger.log.info("The audience user modification date is greater than the last syndication date. No filter will be added.")
      } else {
        ArrowLogger.log.info("The audience user modification date is less than or equal to the last syndication date. Adding filter.")

        val filter = Map("range" ->
          Map("time_stats.time" ->
            Map("gte" -> lastSyndicationDate)))
        queryBuilder.addFilter(filter)
      }

    } else {
      ArrowLogger.log.info(s"No ${Constants.DATE_LAST_EXPORTED_ORACLE_KEY} key found in audience document. No filter will be added.")
    }

    if (campaignDocument.has(Constants.CAMPAIGN_BUSINESS_UNIT_KEY)) {
      val businessUnit = (campaignDocument \ Constants.CAMPAIGN_BUSINESS_UNIT_KEY).extract[String]
      MoviesAnywhereFilter.applyFilterToQueryBuilder(businessUnit, queryBuilder)
    } else {
      val msg = s"Missing mandatory field (${Constants.CAMPAIGN_BUSINESS_UNIT_KEY}) in campaign."
      ArrowLogger.log.error(msg)
      throw new RuntimeException(msg)
    }

    val audienceUIDs = getAudienceUIDs(syn, spark, queryBuilder.buildAsJson())

    // NOTE: We had to revert back to counting or else the new nightly syndication delta counts would be way off
    val totalAudienceUIDs = audienceUIDs.count
    val numberOfGroups = math.ceil(totalAudienceUIDs.toDouble / syn.bulkmax).toInt

    ArrowLogger.log.info(s"Total UIDs from dataframe: $totalAudienceUIDs")
    ArrowLogger.log.info("Number of groups to make: " + numberOfGroups)

    if (numberOfGroups > 0) {
      val countAccum = sc.longAccumulator
      val chunk_rdd = audienceUIDs.rdd.map(x => {
        countAccum.add(1L)
        (countAccum.value % numberOfGroups, List(x.getAs[String](0)))
      }).reduceByKey(_ ++ _)

      val metrics = chunk_rdd.map(x => {
        submitToBlueKai(syn, x._2, audienceID)
      })

      ArrowLogger.log.info("Metrics returned")
      ArrowLogger.log.info("audienceID" + " | " + "uid_list.length" + " | " + "uidData.length" + " | " + "request.headers" + " | " + "request.url" + " | " + "response.code" + " | " + "response.body")
      val responses = metrics.collect()
      responses.foreach(metric => {
        ArrowLogger.log.info(metric._1)
      })

      if (responses.exists(_._2 != 200))
        throw new RuntimeException("HTTP Error on one or more requests")

    } else {
      ArrowLogger.log.info("No groups to syndicate for audience: " + thisAudience)
    }

    ArrowLogger.log.info("Audience syn end time: " + System.currentTimeMillis)
  }

  /**
   *
   * @param syn
   * @param spark
   * @param message
   */
  def syndicate(syn: Syndication, spark: SparkSession, message: JValue) {
    ArrowLogger.log.info("syndicate start time:" + System.currentTimeMillis)
    val sc = spark.sparkContext
    val audienceID = (message \ "id").extract[String]
    val campaignID = (message \ "campaign").extract[String]
    val campaignTupled = getCampaignByID(campaignID, syn.esResourceCampaign, syn.esReadConf, sc)
    val syndicationMonitoringJSON = ("isSyndicationActive", true) ~ ("isSyndicationQueued", false)

    try {
      val campaignSourceJSON: String = campaignTupled.first()._2
      val campaignDocument: JValue = parse(campaignSourceJSON)
      performSyndication(syn, spark, audienceID, campaignDocument)
      val jsonWithSyndicationDate = syndicationMonitoringJSON ~ ("dateSyndicated", System.currentTimeMillis())
      updateCampaign(campaignTupled, campaignID, jsonWithSyndicationDate, syn.esResourceCampaign, syn.esWriteConf)
      ArrowLogger.log.info("Campaign update end time: " + System.currentTimeMillis)
    } catch {
      case NonFatal(e) =>
        ArrowLogger.log.warn("Updating campaign even though an exception was caught during syndication")
        updateCampaign(campaignTupled, campaignID, syndicationMonitoringJSON, syn.esResourceCampaign, syn.esWriteConf)
        ArrowLogger.log.info("Campaign update end time: " + System.currentTimeMillis)
        throw e
    }
  }

  //  def syndicateBulk(syn: Syndication, spark: SparkSession, audienceID: String, campaignIDs: Array[String]) {
  //    log.info("start time:" + System.currentTimeMillis)
  //
  //    val sc = spark.sparkContext
  //    val json = ("dateSyndicated", System.currentTimeMillis()) ~ ("isSyndicationActive", true) ~ ("isSyndicationQueued", false)
  //
  //    performSyndication(syn, spark, audienceID)
  //
  //    for(campaignID <- campaignIDs) {
  //      updateCampaign(campaignID, json, syn.esResourceCampaign, syn.esWriteConf, syn.esReadConf, sc)
  //    }
  //
  //    log.info("Campaign update end time: " + System.currentTimeMillis)
  //  }
}
