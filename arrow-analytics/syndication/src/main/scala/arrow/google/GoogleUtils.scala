package arrow.google

import java.security.MessageDigest
import arrow.config.{ArrowLogger, Util}
import com.google.api.ads.adwords.axis.factory.AdWordsServices
import com.google.api.ads.adwords.axis.v201809.cm.ApiError
import com.google.api.ads.adwords.axis.v201809.cm.ApiException
import org.apache.commons.codec.digest.DigestUtils
import com.google.api.ads.adwords.axis.v201809.cm.Operator
import com.google.api.ads.adwords.axis.v201809.rm.AdwordsUserListServiceInterface
import com.google.api.ads.adwords.axis.v201809.rm.CrmBasedUserList
import com.google.api.ads.adwords.axis.v201809.rm.CustomerMatchUploadKeyType
import com.google.api.ads.adwords.axis.v201809.rm.Member
import com.google.api.ads.adwords.axis.v201809.rm.MutateMembersOperand
import com.google.api.ads.adwords.axis.v201809.rm.MutateMembersOperation
import com.google.api.ads.adwords.axis.v201809.rm.MutateMembersReturnValue
import com.google.api.ads.adwords.axis.v201809.rm.UserList
import com.google.api.ads.adwords.axis.v201809.rm.UserListOperation
import com.google.api.ads.adwords.axis.v201809.rm.UserListReturnValue
import com.google.api.ads.adwords.lib.client.AdWordsSession
import com.google.api.ads.adwords.lib.factory.AdWordsServicesInterface
import com.google.api.ads.common.lib.auth.OfflineCredentials
import com.google.api.ads.common.lib.auth.OfflineCredentials.Api
import com.google.api.client.auth.oauth2.Credential
import com.google.api.ads.adwords.lib.selectorfields.v201809.cm.AdwordsUserListField
import com.google.api.ads.adwords.axis.v201809.cm.Selector
import com.google.api.ads.adwords.axis.utils.v201809.SelectorBuilder
import com.google.common.collect.ImmutableList
import java.util.ArrayList
import com.typesafe.config.ConfigFactory
import scala.collection.JavaConversions._


object GoogleUtils {
  val configs = ConfigFactory.load()
  val developerToken = Util.getSecureCredential("google.ad.developer.token")
  val adSecret = Util.getSecureCredential("google.ad.secret")
  val adToken = Util.getSecureCredential("google.ad.refresh.token")
  val adId = configs.getString("google.ad.account.id")
  val adCustomerId = configs.getString("google.ad.customer.id")
  val appId = configs.getString("google.mobile.app.id")

  //Hashing Algorithm and input email list

  val digest = MessageDigest.getInstance("SHA-256")

  //Create a new Google Ads session

  //Get AdwordServices and userListService to create CRM based user list


  val adWordsServices: AdWordsServicesInterface = AdWordsServices.getInstance()


  def checkIfAudienceIdExists(audienceName: String, typeName: String, session: AdWordsSession = generateOAuthCredential): Long = {
    try {

      val offset = 0
      val PAGE_SIZE = 1000
      val builder = new SelectorBuilder()
      val listNameSuffix = if (typeName == "email") "Email" else if (typeName == "maid") "Maid"
      ArrowLogger.log.info("audienceName: " + audienceName + "_" + listNameSuffix)
      val selector = builder.fields(AdwordsUserListField.UploadKeyType).offset(offset).limit(PAGE_SIZE).equals("Name", audienceName + "_" + listNameSuffix).build()
      val userListService: AdwordsUserListServiceInterface = adWordsServices.get(session, classOf[AdwordsUserListServiceInterface])
      val userListRetrieved = userListService.get(selector)
      if (userListRetrieved.isEmpty)
        0L
      else userListRetrieved.getEntries(0).getId.toLong

    }
    catch {
      // ApiException is the base class for most exceptions thrown by an API request. Instances
      // of this exception have a message and a collection of ApiErrors that indicate the
      // type and underlying cause of the exception. Every exception object in the adwords.axis
      // packages will return a meaningful value from toString
      //
      // ApiException extends RemoteException, so this catch block must appear before the
      // catch block for RemoteException.
      case apiException: ApiException => {
        ArrowLogger.log.error("Request failed due to ApiException. Underlying ApiErrors:")
        if (apiException.getErrors() != null) {
          val i = 0
          for (apiError <- apiException.getErrors()) ArrowLogger.log.error("  Error " + i + 1 + ": " + apiError)
        }
        throw apiException
      }
      case ue: Throwable => ArrowLogger.log.error("Request failed due to exception: " + ue)
        throw ue

    }
  }

  def createNewAudienceList(audienceListName: String, audienceListDescription: String, session: AdWordsSession = generateOAuthCredential): Long = {
    try {
      //Create a new UserList
      val userListService: AdwordsUserListServiceInterface = adWordsServices.get(session, classOf[AdwordsUserListServiceInterface])
      val userList = new CrmBasedUserList()
      userList.setDescription(audienceListDescription)

      if (audienceListDescription.toUpperCase().contains("EMAIL")) {
        val uploadKeyType = CustomerMatchUploadKeyType.CONTACT_INFO
        userList.setUploadKeyType(uploadKeyType)
        userList.setName(audienceListName + "_Email")
        userList.setMembershipLifeSpan(10000L)
      }
      else {
        val uploadKeyType = CustomerMatchUploadKeyType.MOBILE_ADVERTISING_ID
        userList.setUploadKeyType(uploadKeyType)
        userList.setName(audienceListName + "_Maid")
        userList.setAppId(appId) //"575658129"
        userList.setMembershipLifeSpan(540L)
      }

      //Upload the newly created userlist in GoogleAds page using mutate operation
      val operation = new UserListOperation()
      operation.setOperand(userList)
      operation.setOperator(Operator.ADD)
      val result = userListService.mutate(Array(operation))
      val userListAdded = result.getValue(0)
      ArrowLogger.log.info("User list with name " + userListAdded.getName() + " and ID " + userListAdded.getId() + " was added.")
      userListAdded.getId()
    }
    catch {
      // ApiException is the base class for most exceptions thrown by an API request. Instances
      // of this exception have a message and a collection of ApiErrors that indicate the
      // type and underlying cause of the exception. Every exception object in the adwords.axis
      // packages will return a meaningful value from toString
      //
      // ApiException extends RemoteException, so this catch block must appear before the
      // catch block for RemoteException.
      case apiException: ApiException => {
        ArrowLogger.log.error("Request failed due to ApiException. Underlying ApiErrors:")
        if (apiException.getErrors() != null) {
          val i = 0
          for (apiError <- apiException.getErrors()) {
            ArrowLogger.log.error("  Error " + i + 1 + ": " + apiError)
            throw apiException
          }
        }
        throw new RuntimeException("An API exception occurred when trying to access Google Ads Account")
      }
      case t: Throwable => {
        ArrowLogger.log.error("Request failed unexpectedly due to exception: " + t)
        ArrowLogger.log.error(s"$t\n\n${t.getStackTrace.mkString("\n")}")
        throw t
      }
    }
  }


  def updateAudienceInUserList(groupNumber: Long, userList: List[String], audienceId: Long, operationType: String, listType: String, session: AdWordsSession = generateOAuthCredential): Long = {
    try {

      /*Code to close the membership status of an userlist in GoogleAds account
      ///close the membership
      val userList = new CrmBasedUserList()
      userList.setId(783001000)
      userList.setStatus(UserListMembershipStatus.CLOSED)
      val operation = new UserListOperation()
      operation: com.google.api.ads.adwords.axis.v201809.rm.UserListOperation = UserListOperation{}
      operation.setOperand(userList)
      operation.setOperator(Operator.SET)
      val result = userListService.mutate(Array(operation))
*/
      //Upload Members to the newly created userlist
      val userListService: AdwordsUserListServiceInterface = adWordsServices.get(session, classOf[AdwordsUserListServiceInterface])
      if (audienceId == 0) {
        ArrowLogger.log.info("No Custom Audience ID being passed and hence users cannot be added")
        throw new Exception("No Custom Audience ID being passed and hence users cannot be added")
      }
      val mutateMembersOperation = new MutateMembersOperation()
      val operand = new MutateMembersOperand()
      operand.setUserListId(audienceId)

      val members = scala.collection.mutable.ListBuffer[Member]()
      ArrowLogger.log.info("List Size: " + userList.size)
      if (userList.size > 0) {
        if (listType.toUpperCase().contains("EMAIL")) {
          ArrowLogger.log.info("Passed list type is of EMAILids")

          //Standardize Emailids as per Google Standards(Remove spaces and convert to lowercase with SHA256 hashing
          for (email <- userList.filterNot(x => x.trim.isEmpty).filterNot(x => x.trim == "")) {
            val normalizedEmail = email.trim.toLowerCase
            val member = new Member()
            member.setHashedEmail(DigestUtils.sha256Hex(normalizedEmail))
            members += member
          }
        }
        else {
          ArrowLogger.log.info("Passed list type is Maids")
          val maidFormat = raw"([a-fA-F0-9]{8}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{4}\-[a-fA-F0-9]{12})".r
          for (maid <- userList.filter(x => maidFormat.pattern.matcher(x).matches)) {
            val normalizedMaid = maid.trim
            val member = new Member()
            member.setMobileId(normalizedMaid)
            members += member
          }
        }

        operand.setMembersList(members.toArray)
        mutateMembersOperation.setOperand(operand)
        Thread.sleep(150000L * groupNumber)
        if (operationType == "Add") {
          mutateMembersOperation.setOperator(Operator.ADD)
          val mutateMembersResult = userListService.mutateMembers(Array(mutateMembersOperation))
          ArrowLogger.log.info(mutateMembersResult)
          for (list <- mutateMembersResult.getUserLists()) {
            ArrowLogger.log.info(userList.size() + " " + listType + "s were uploaded to user list with name: " + list.getName() + " and ID: " + list.getId() + " and are scheduled for review.%n")
          }
        }
        /* To be uncommented when remove operation needs to be supported
         else {
           mutateMembersOperation.setOperator(Operator.REMOVE)
           val mutateMembersResult = userListService.mutateMembers(Array(mutateMembersOperation))
           for (list <- mutateMembersResult.getUserLists()) {
             ArrowLogger.log.info(userList.size() + " email addresses/maids were removed from user list with name: " + list.getName() + " and ID: " + list.getId() + " and are scheduled for review.%n")
           }
         }*/
      }

      audienceId
    }

    catch {
      // ApiException is the base class for most exceptions thrown by an API request. Instances
      // of this exception have a message and a collection of ApiErrors that indicate the
      // type and underlying cause of the exception. Every exception object in the adwords.axis
      // packages will return a meaningful value from toString
      //
      // ApiException extends RemoteException, so this catch block must appear before the
      // catch block for RemoteException.
      case apiException: ApiException => {

        ArrowLogger.log.error("Request failed due to ApiException. Underlying ApiErrors:")
        if (apiException.getErrors() != null) {
          val i = 0
          for (apiError <- apiException.getErrors()) ArrowLogger.log.error("  Error " + i + 1 + ": " + apiError)

        }
        throw apiException
      }
      case t: Throwable => ArrowLogger.log.error("Request failed unexpectedly due to exception: " + t)
        ArrowLogger.log.error(s"$t\n\n${t.getStackTrace.mkString("\n")}")
        throw t

    }
  }

  def generateOAuthCredential: AdWordsSession = {
    try {

      //Generate OAuthCred using ClientID,ClientSecret,RefreshToken

      val oAuth2Credential: Credential = new OfflineCredentials.Builder().forApi(OfflineCredentials.Api.ADWORDS).withClientSecrets(adId, adSecret).withRefreshToken(adToken).build().generateCredential()

      //Create Adwords Session and set clientcustomerID to relevant WB Arrow Account
      val session = new AdWordsSession.Builder().withDeveloperToken(developerToken).withOAuth2Credential(oAuth2Credential).build()
      session.setClientCustomerId(adCustomerId)
      session
    }
    catch {
      case ve: Throwable =>
        ArrowLogger.log.error(s"Invalid configuration in the config file. Exception: %s%n", ve)
        ArrowLogger.log.error(s"$ve\n\n${ve.getStackTrace.mkString("\n")}")
        throw ve
    }

  }
}