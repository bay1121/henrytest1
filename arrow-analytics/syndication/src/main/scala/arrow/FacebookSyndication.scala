package arrow

import java.net.URI
import java.util.concurrent.Executors

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import com.facebook.ads.sdk._
import scala.collection.Map
import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.kafka010.CanCommitOffsets
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.HasOffsetRanges
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.OffsetRange
import org.json4s.DefaultFormats
import org.json4s.JNothing
import org.json4s.JValue
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.json4s.string2JsonInput

import com.typesafe.config.ConfigFactory

import arrow.config.ArrowLogger
import arrow.constants.Constants
import arrow.facebook.SyndicateToFacebook
import arrow.util.YarnFailedAppSlackMessageBuilder
import arrow.util.EmailUtil
import arrow.util.SlackAysncMessagePoster
import arrow.config.Util

class FacebookSyndication extends Serializable {

  val configs = ConfigFactory.load()

  //Secure Credentials
  val esPasswd: String = Util.getSecureCredential(Util.ES_SYND_USER)
  val kfQuorum: String = configs.getString("kafka.quorum")
  val kafka_topic: String = configs.getString("kafka.topic.facebook")
  val kafkaGroupID: String = configs.getString("kafka.group.id.facebook")
  val kafkaAutoOffsetReset: String = configs.getString("kafka.auto.offset.reset")

  val bulkmax: Int = configs.getInt("facebook.bulk.max")
  val esNodes: String = configs.getString("es.nodes")
  val esPort: String = configs.getString("es.port")
  val esUser: String = configs.getString("es.user")
  val esResourceAudience: String = configs.getString("es.resource.audience")
  val esResourceCampaign: String = configs.getString("es.resource.campaign")
  val esResourceEvent: String = configs.getString("es.resource.event")

  //Email Params
  val awsSmtpUser: String = Util.getSecureCredential(Util.AWS_SMTP_USER)
  val awsSmtpKey: String = Util.getSecureCredential(Util.AWS_SMTP_KEY)
  val awsSmtpHost: String = configs.getString("aws.smpt.host")
  val alertId: String = configs.getString("alert.email.id")
  val defaultEmail: String = configs.getString("support.email.id")

  var endPointBase = APIConfig.DEFAULT_API_BASE
  var videoBase = APIConfig.DEFAULT_VIDEO_API_BASE
  var apiVersion = APIConfig.DEFAULT_API_VERSION

  // used for slack messages
  val slackWebhookEndpoint: String = configs.getString("slack.webhook.endpoint")

  val esReadConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.nodes.wan.only" -> "true")

  val esWriteConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.batch.size.bytes" -> "24mb",
    "es.batch.size.entries" -> "24000",
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.write.operation" -> "update",
    "es.index.auto.create" -> "true")

  val topics: Array[String] = Array(kafka_topic)
  val kafkaParams: Map[String, Object] = Map[String, Object](
    // the usual params, make sure to change the port in bootstrap.servers if 9092 is not TLS
    "bootstrap.servers" -> kfQuorum,
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> kafkaGroupID,
    "auto.offset.reset" -> kafkaAutoOffsetReset,
    "enable.auto.commit" -> (false: java.lang.Boolean))
}

object FacebookSyndication {

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  def main(args: Array[String]) {

    implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }

    val spark = SparkSession.builder()
      .appName("FacebookSyndication").config("spark.io.compression.codec", "snappy")
      .getOrCreate()
    val sc = spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(10))
    val syn = new FacebookSyndication

    val executorService = Executors.newSingleThreadExecutor()
    implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(executorService)

    val messages = KafkaUtils.createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](syn.topics, syn.kafkaParams))

    // Hold a reference to the current offset ranges, so it can be used downstream
    var offsetRanges = Array.empty[OffsetRange]

    messages.transform(rdd => {
      offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd
    }).foreachRDD(rdd => {
      val rdd2 = rdd.map(x => x.value())
      val it = rdd2.collect().iterator

      /**
       * Kafka Message Format
       *
       * {"timestamp":1515463679424,"id":"x4BC9JDcWI","syndicate":true,"campaign":"xKugDgaKqS","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xl51LpIUKr","syndicate":true,"campaign":"xLvkdjzZLQ","target" :"facebook"}
       *
       * {"timestamp":1515463679424,"id":"xrchG5BmQg","syndicate":true,"campaign":"xLvkdjzZLQ","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xTC7209hdb","syndicate":true,"campaign":"xLvkdjzZLQ","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xNsnBR-MAJ","syndicate":true,"campaign":"xLvkdjzZLQ","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xvzLivKA6c","syndicate":true,"campaign":"xiWxm4xIum","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xPKF-seLyn","syndicate":true,"campaign":"xu8a8p9O_O","target" :"facebook"}
       * {"timestamp":1515463679424,"id":"xbVjd9uBoT","syndicate":true,"campaign":"xSEngVyEFP","target" :"facebook"}
       *
       *
       *
       * xoWjGX41RL
       * xUIEv8coiv xr3L5SBuvM
       */
      it.foreach(message => {
        ArrowLogger.log.info("Working on: " + message)

        try {
          val messageJson = parse(message)
          SyndicateToFacebook.run(syn, spark, messageJson)

        } catch {
          case NonFatal(e) =>
            ArrowLogger.log.error("Unable to complete Facebook Syndication from: " + message)
            ArrowLogger.log.error(s"$e\n\n${e.getStackTrace.mkString("\n")}")

            val subject = "Error while running FacebookSyndication"
            val body = s"Error:\n${e.getMessage}\n Exception:\n$e\n\n\nStack Trace:\n${e.getStackTrace.mkString("\n")}"
            EmailUtil.send(syn.alertId, syn.defaultEmail, subject, body, syn.awsSmtpHost, syn.awsSmtpUser, syn.awsSmtpKey)

            var audienceID = ""
            var campaignID = ""

            try {
              val messageJson = parse(message)
              audienceID = (messageJson \ Constants.AUDIENCE_ID_KEY).extract[String]
              campaignID = (messageJson \ Constants.CAMPAIGN_ID_KEY).extract[String]
            } catch {
              case t: Throwable => ArrowLogger.log.error("Error parsing json message for alert!")
            }

            val yarnJobUri = URI.create(s"${sc.applicationId}")
            val slackMessage = new YarnFailedAppSlackMessageBuilder(yarnJobUri, e)
              .addJsonMessage(message)
              .addCampaignId(campaignID)
              .addAudienceId(audienceID)
              .addOriginatingClass(getClass.getName)
              .build()

            val slackEndpoint = URI.create(syn.slackWebhookEndpoint)
            val future = SlackAysncMessagePoster.send(slackEndpoint, slackMessage)

            future.onComplete {
              case Success(r) =>
                ArrowLogger.log.info(s"Successfully posted to slack with response $r")
              case Failure(error) =>
                ArrowLogger.log.warn(s"Failed to post to slack with error $error")
            }
        }
      })
      // write offsets. At this point, no exceptions were thrown during syndication
      ArrowLogger.log.info("Committing offsets to Kafka")
      messages.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)

    })

    ArrowLogger.log.info("Starting FacebookSyndication App")
    ssc.start()
    ssc.awaitTermination()
    executorService.shutdown()
  }
}
