package arrow.util

import arrow.constants.Constants._
import org.apache.spark.rdd.RDD
import org.json4s.jackson.JsonMethods.parse
import org.json4s.{DefaultFormats, JObject, JValue, _}

import scala.collection.Map

object JsonUtil {


  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  /**
    * Extract export values from
    *
    * @param jv
    * @param key
    * @return
    */
  def getValidExportValues(jv: JValue, key: String, target: String = ""): Seq[String] = {
    var searchStr: JValue = JNothing
    if (target.isEmpty) {
      searchStr = jv \ key
    } else {
      searchStr = jv \ key \ target
    }
    searchStr match {
      case JNothing =>
        Seq.empty
      case obj: JObject =>
        obj.extract[Map[String, Boolean]].filter(_._2).keys.toSeq
      case _ =>
        throw new IllegalArgumentException(s"Encountered an unexpected data type when searching [$jv] for [$key]")
    }
  }

  /**
    * Helper function to extract partners from campain
    *
    * @param campaign
    * @return
    */
  def getPartners(campaign: JValue): Seq[String] = {
    getValidExportValues(campaign, PARTNER_KEY)
  }

  /**
    * Helper function to extract hash formats from campaign
    *
    * @param campaign
    * @return
    */
  def getHashFormats(campaign: JValue): Seq[String] = {
    getValidExportValues(campaign, HASH_FORMAT_KEY)
  }

  /**
    * Helper function to extract export types from campaign
    *
    * @param campaign
    * @return
    */
  def getExportTypes(campaign: JValue, target: String): Seq[String] = {
    getValidExportValues(campaign, DESTINATION_KEY,target)
  }

  /**
    * Align export types with appropriate field names
    *
    * @param exportTypes
    * @return
    */
  def getExportFields(exportTypes: Seq[String]): Array[(String, Boolean)] = {
    var exportFields = Array[(String, Boolean)]()
    for (t <- exportTypes) {
      val fieldNames = EXPORT_TYPES.getOrElse(t, null)
      if (fieldNames != null) {
        exportFields = exportFields union fieldNames
      }
    }

    exportFields.distinct
  }

  /**
    * Parse an RDD of campaign documents into an RDD of Jvalues
    *
    * @param campaignRdd
    * @return
    */
  def parseCampaign(campaignRdd: RDD[(String, String)]): RDD[JValue] = {
    val campaignParsed = campaignRdd.map(tuple => parse(tuple._2))
    return campaignParsed
  }

  /**
    * Return an array which contains all the audience ids in the campaign
    *
    * @param campaign
    * @return
    */
  def getCampaignAudienceIDs(campaign: JValue): Array[String] = {
    campaign \ CAMPAIGN_AUDIENCE_KEY match {
      case JNothing =>
        Array.empty
      case obj: JArray =>
        val campaignAudiences = obj.extract[Array[Map[String, String]]]
        campaignAudiences.map(campaign => campaign.get(AUDIENCE_ID_KEY)).map(id => id match {
          case Some(name) =>
            name
          case None =>
            null
        }).filter(id => id != null).distinct
      case _ =>
        throw new RuntimeException(s"Encountered an unexpected problem looking for the export values at ${CAMPAIGN_AUDIENCE_KEY}")
    }
  }

  /**
    * Return an array which contains all the segment ids in the campaign
    *
    * @param campaign
    * @return
    */
  def getCampaignSegmentIDs(campaign: JValue): Array[String] = {
    val campaignAudiences = (campaign \ CAMPAIGN_SEGMENTS_KEY).extract[Array[Map[String, String]]]
    campaignAudiences.map(campaign => campaign.get(AUDIENCE_ID_KEY)).map(id => id match {
      case Some(name) =>
        name
      case None =>
        null
    }).filter(id => id != null).distinct
  }

  def getSyndicationDestinations(campaign: JValue): Seq[String]={
    campaign \ DESTINATION_KEY
    match {
      case JNothing =>
        Seq.empty
      case obj:JObject => obj.extract[Map[String,Any]].keys.toList.sorted(Ordering.String.reverse)
      case _ =>
        throw new IllegalArgumentException(s"Encountered an unexpected data type when searching [$campaign] for [$DESTINATION_KEY]")
  }}
}

