package arrow.util

import java.io._
import java.security.{NoSuchProviderException, SecureRandom, Security}
import java.util.Date

import org.apache.hadoop.conf.{Configuration, Configured}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.bouncycastle.bcpg.{ArmoredInputStream, ArmoredOutputStream, CompressionAlgorithmTags, SymmetricKeyAlgorithmTags}
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory
import org.bouncycastle.openpgp.operator.jcajce.{PGPUtil => _, _}
import org.bouncycastle.openpgp.{PGPCompressedData, PGPEncryptedDataList, PGPException, PGPLiteralData, PGPOnePassSignatureList, PGPPrivateKey, PGPPublicKeyEncryptedData, _}
import org.bouncycastle.util.encoders.Base64
import org.bouncycastle.util.io.Streams

/**
  * Created by brehrey on 12/22/17.
  */
object PGPHadoop extends Configured {
  val FS_PARAM_NAME = "fs.defaultFS"

  /** *
    * Encrypt a file in hdfs to hdfs using a public key file in hdfs
    *
    * @param inPath        File to encrypt
    * @param outPath       Output file
    * @param publicKeyFile Public Key File
    */
  def encrypt(inPath: String, outPath: String, publicKeyFile: String, fs:FileSystem): Unit = {
    Security.addProvider(new BouncyCastleProvider)

    val inputPath = new Path(inPath)
    val outputPath = new Path(outPath)
    val keyPath = new Path(publicKeyFile)
    val modificationTime = fs.getFileStatus(inputPath).getModificationTime
    val length = fs.getFileStatus(inputPath).getLen
    val os = fs.create(outputPath, true)
    val is = fs.open(inputPath)
    val keyIs = fs.open(keyPath)

    encryptFile(os, is, inputPath.getName, modificationTime, length, readPublicKey(keyIs), true, true)
    os.close()
  }

  /** *
    * Encrypt in stream to out stream
    *
    * @param out      output stream
    * @param is       input stream
    * @param fileName file name
    * @param mod      mod date
    * @param len      file length
    * @param encKey   encryption key
    * @param armor    armor flag
    * @param withIntegrityCheck
    */
  @throws[IOException]
  @throws[NoSuchProviderException]
  @throws[PGPException]
  private def encryptFile(out: OutputStream, is: InputStream, fileName: String, mod: Long, len: Long, encKey: PGPPublicKey, armor: Boolean, withIntegrityCheck: Boolean): Unit = {
    // Make reference for the steam
    var myStream = out

    // If armor flag is set make an armored stream
    if (armor) {
      myStream = new ArmoredOutputStream(out)
    }

    // Write compressed file to a byte array output stream
    val bOut = new ByteArrayOutputStream()
    val comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP)
    writeFileToLiteralData(comData.open(bOut), PGPLiteralData.TEXT, fileName, mod, len, is)
    comData.close()

    // Encrypt file using PDP generator into out stream
    val c = new JcePGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.CAST5).
      setWithIntegrityPacket(withIntegrityCheck).
      setSecureRandom(new SecureRandom).setProvider("BC")
    val cPk = new PGPEncryptedDataGenerator(c)
    val d = new JcePublicKeyKeyEncryptionMethodGenerator(encKey).
      setProvider(new BouncyCastleProvider).
      setSecureRandom(new SecureRandom)
    cPk.addMethod(d)
    val bytes = bOut.toByteArray
    val cOut = cPk.open(myStream, bytes.length)
    cOut.write(bytes)
    cOut.close
    myStream.close()
    out.close
  }

  /** *
    * Write output stream to literal data generator
    * Taken from BouncyCastle PGP to manually add the file mod and length
    *
    * @param out
    * @param fileType
    * @param fileName
    * @param mod
    * @param len
    * @param is
    */
  @throws[IOException]
  private def writeFileToLiteralData(out: OutputStream, fileType: Char, fileName: String, mod: Long, len: Long, is: InputStream): Unit = {
    val lData = new PGPLiteralDataGenerator
    val pOut = lData.open(out, fileType, fileName, len, new Date(mod))
    pipeFileContents(is, pOut, 4096)
  }

  /** *
    * Pipe contents of input stream to output stream
    *
    * @param in
    * @param pOut
    * @param bufSize
    */
  @throws[IOException]
  private def pipeFileContents(in: InputStream, pOut: OutputStream, bufSize: Int) = {
    var bytes = org.apache.commons.io.IOUtils.toByteArray(in)
    pOut.write(bytes, 0, bytes.length)
    pOut.close()
    in.close()
  }

  /** *
    * Read in the public key from the input stream
    *
    * @param input
    * @return
    */
  @throws[IOException]
  @throws[PGPException]
  private def readPublicKey(input: InputStream): PGPPublicKey = {
    val pgpPub = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(input), new JcaKeyFingerprintCalculator)
    val keyRingIter = pgpPub.getKeyRings
    while ( {
      keyRingIter.hasNext
    }) {
      val keyRing = keyRingIter.next.asInstanceOf[PGPPublicKeyRing]
      val keyIter = keyRing.getPublicKeys
      while ( {
        keyIter.hasNext
      }) {
        val key = keyIter.next.asInstanceOf[PGPPublicKey]
        if (key.isEncryptionKey) return key
      }
    }
    throw new IllegalArgumentException("Can't find encryption key in key ring.")
  }

  /** *
    * Decrypt a file in hdfs to hdfs using a private key file in hdfs as well as the past code
    *
    * @param inPath
    * @param outPath
    * @param privateKeyFile
    * @param passCode
    */
  def decrypt(inPath: String, outPath: String, privateKeyFile: String, passCode: String, conf: Configuration): Unit = {
    Security.addProvider(new BouncyCastleProvider)
    val fs = FileSystem.get(conf)

    val inputPath = new Path(inPath)
    val outputPath = new Path(outPath)
    val keyPath = new Path(privateKeyFile)
    val os = fs.create(outputPath, true)
    val is = fs.open(inputPath)
    val keyIs = fs.open(keyPath)
    decryptFile(is, keyIs, passCode.toCharArray, os)
    os.close()
  }

  /** *
    * Decrypt input stream to output stream using key stream and passcode.
    *
    * @param in
    * @param keyIn
    * @param passwd
    * @param os
    */
  private def decryptFile(in: InputStream, keyIn: InputStream, passwd: Array[Char], os: OutputStream) = {
    val in2 = PGPUtil.getDecoderStream(in)

    try {
      val pgpF = new JcaPGPObjectFactory(in2)
      var enc: PGPEncryptedDataList = null
      val o = pgpF.nextObject

      // The first object might be a PGP marker packet.
      if (o.isInstanceOf[PGPEncryptedDataList]) {
        enc = o.asInstanceOf[PGPEncryptedDataList]
      }
      else {
        enc = pgpF.nextObject.asInstanceOf[PGPEncryptedDataList]
      }

      // Find the secret key in the stream
      val it = enc.getEncryptedDataObjects
      var sKey: PGPPrivateKey = null
      var pbe: PGPPublicKeyEncryptedData = null
      var test = getDecoderStream(keyIn)

      val pgpSec = new PGPSecretKeyRingCollection(test, new JcaKeyFingerprintCalculator)
      while (sKey == null && it.hasNext) {
        pbe = it.next.asInstanceOf[PGPPublicKeyEncryptedData]
        sKey = findSecretKey(pgpSec, pbe.getKeyID, passwd)
      }

      if (sKey == null) {
        throw new IllegalArgumentException("Secret key for message not found.")
      }

      // Get decryptor stream based on key
      val clear = pbe.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider("BC").build(sKey))
      val plainFact = new JcaPGPObjectFactory(clear)
      var message: AnyRef = null
      message = plainFact.nextObject

      // Check to see if data is compressed
      if (message.isInstanceOf[PGPCompressedData]) {
        val cData = message.asInstanceOf[PGPCompressedData]
        val pgpFact = new JcaPGPObjectFactory(cData.getDataStream)
        message = pgpFact.nextObject
      }

      // Check to see if it is literal data
      if (message.isInstanceOf[PGPLiteralData]) {
        val ld = message.asInstanceOf[PGPLiteralData]
        val unc = ld.getInputStream
        val fOut = os
        Streams.pipeAll(unc, fOut)
        fOut.close()
      }
      else if (message.isInstanceOf[PGPOnePassSignatureList]) {
        throw new PGPException("encrypted message contains a signed message - not literal data.")
      }
      else {
        throw new PGPException("message is not a simple encrypted file - type unknown.")
      }

      if (pbe.isIntegrityProtected) {
        if (!pbe.verify) System.err.println("message failed integrity check")
        else System.err.println("message integrity check passed")
      }
      else System.err.println("no message integrity check")
    } catch {
      case e: PGPException =>
        System.err.println(e)
        if (e.getUnderlyingException != null) e.getUnderlyingException.printStackTrace()
    }
  }

  /** *
    * Gets the private  key from the key ring collection
    *
    * @param pgpSec
    * @param keyID
    * @param pass
    * @return
    */
  @throws[PGPException]
  @throws[NoSuchProviderException]
  def findSecretKey(pgpSec: PGPSecretKeyRingCollection, keyID: Long, pass: Array[Char]): PGPPrivateKey = {
    val pgpSecKey = pgpSec.getSecretKey(keyID)
    if (pgpSecKey == null) {
      return null
    }
    pgpSecKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build(pass))
  }

  /** *
    * Parses the stream and returns the correct stream for decoding.
    *
    * @param in
    * @return
    */
  @throws[IOException]
  def getDecoderStream(in: InputStream): InputStream = {
    var bytes = org.apache.commons.io.IOUtils.toByteArray(in)
    val is = new BufferedInputStreamExt(new ByteArrayInputStream(bytes))
    var ret = new ByteArrayInputStream(bytes)

    is.mark(60)
    var ch = is.read
    if ((ch & 0x80) != 0) {
      is.reset()
      return is
    }
    else {
      if (!isPossiblyBase64(ch)) {
        is.reset()
        return new ArmoredInputStream(is)
      }
      val buf = new Array[Byte](60)
      var count = 1
      var index = 1
      buf(0) = ch.toByte
      while (count != 60) {
        ch = is.read
        if (ch >= 0) {
          if (!isPossiblyBase64(ch)) {
            is.reset()
            return new ArmoredInputStream(is)
          }
          if (ch != '\n' && ch != '\r') {
            index = index + 1
            buf(index) = ch.toByte
          }
          count += 1
        }
      }

      is.reset()

      if (count < 4) {
        return new ArmoredInputStream(is)
      }

      // test our non-blank data
      val firstBlock = new Array[Byte](8)
      System.arraycopy(buf, 0, firstBlock, 0, firstBlock.length)

      val decoded = Base64.decode(firstBlock)
      // it's a base64 PGP block.
      if ((decoded(0) & 0x80) != 0) {
        return new ArmoredInputStream(is, false)
      }

      return new ArmoredInputStream(is)

    }
  }

  /** *
    * checks if it is in base 64
    *
    * @param ch
    * @return
    */
  private def isPossiblyBase64(ch: Int) = (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9') || (ch == '+') || (ch == '/') || (ch == '\r') || (ch == '\n')

  /** *
    * Extended buffered input stream
    *
    * @param input
    */
  class BufferedInputStreamExt(val input: InputStream) extends BufferedInputStream(input) {
    @throws[IOException]
    override def available: Int = {
      var result = super.available
      if (result < 0) result = Integer.MAX_VALUE
      result
    }
  }

}


