package arrow.util

import java.net.URI

import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization
import scalaj.http.{Http, HttpResponse}

import scala.concurrent.{ExecutionContext, Future}

case class MessageAttachment(
                              pretext: String,
                              color: String,
                              author_name: String,
                              title: String,
                              title_link: String,
                              text: String,
                              ts: Long,
                              mrkdwn_in: Array[String],
                              fields: Array[Field])

case class Field(title: String, value: String, short: Boolean)

case class SlackMessage(text: String, attachments: Array[MessageAttachment])

class YarnFailedAppSlackMessageBuilder(yarnAppLink: URI, throwable: Throwable) {
  private var originatingClass: String = _
  private var campaignId: String = _
  private var audienceId: String = _
  private var jsonMessage: String = _

  def addOriginatingClass(originatingClass:String): YarnFailedAppSlackMessageBuilder = {
    this.originatingClass = originatingClass
    this
  }

  def addCampaignId(campaignId:String): YarnFailedAppSlackMessageBuilder = {
    this.campaignId = campaignId
    this
  }

  def addAudienceId(audienceId:String): YarnFailedAppSlackMessageBuilder = {
    this.audienceId = audienceId
    this
  }

  def addJsonMessage(jsonMessage:String): YarnFailedAppSlackMessageBuilder = {
    this.jsonMessage = jsonMessage
    this
  }

  def build(): SlackMessage = {
    val fields = Array(
      Field(
        title = "Message",
        value = s"`$jsonMessage`",
        short = false
      ),
      Field(
        title = "Exception Info",
        value = s"""```${throwable.toString}```""",
        short = false
      )
    )

    SlackMessage(
      text = "",
      attachments = Array(
        MessageAttachment(
          pretext = s"Error Processing Campaign `$campaignId` Audience `$audienceId`",
          color = "danger",
          author_name = originatingClass,
          title = "YARN Application Manager",
          title_link = yarnAppLink.normalize().toString,
          text = s"""```${throwable.getStackTrace.mkString("\n")}```""",
          ts = System.currentTimeMillis() / 1000,
          mrkdwn_in = Array("fields"),
          fields = fields
        )
      )
    )
  }
}

object SlackAysncMessagePoster {
  implicit val formats:org.json4s.DefaultFormats = DefaultFormats

  def send(uri: URI, slackMessage: SlackMessage)
          (implicit executionContext: ExecutionContext): Future[HttpResponse[String]] = {
    Future {
      Http(uri.normalize().toString)
        .headers("Content-type" -> "application/json")
        .postData(Serialization.write(slackMessage))
        .asString
    }
  }
}