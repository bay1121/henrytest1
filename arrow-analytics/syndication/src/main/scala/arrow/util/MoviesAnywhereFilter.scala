package arrow.util

import org.apache.log4j.Logger

object MoviesAnywhereFilter {
  val log:Logger = Logger.getLogger(getClass)

  val filter = Map("bool" ->
    Map("must_not" ->
      List(
        Map("term" ->
          Map("siterollup.keyword" ->
            Map("value" -> "Movies Anywhere")
          )
        )
      )
    )
  )

  val allowableBusinessUnits = Set("Home Entertainment", "Theatrical")

  def applyFilterToQueryBuilder(businessUnit:String, queryBuilder:ArrowQueryBuilder): Unit = {
    if (!allowableBusinessUnits.contains(businessUnit)) {
      log.debug(s"Movies Anywhere exclusion filter will be added for businessUnit: $businessUnit")
      queryBuilder.addFilter(filter)
    }
  }
}
