package arrow.util

import arrow.config.ArrowLogger
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.elasticsearch.spark._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods.{compact, parse, render}
import org.json4s.native.Serialization.write
import org.json4s.{DefaultFormats, JValue, _}
import scalaj.http.Http

import scala.collection.mutable

object ESUtil {

  implicit val formats : org.json4s.DefaultFormats = DefaultFormats

  implicit class JValueExtended(value: JValue) {
    def has(childString: String): Boolean = {
      (value \ childString) != JNothing
    }
  }

  // Helper function to get campaign document by ID
  def getCampaignByID(campaignID: String, esResourceCampaign: String, esReadConf: mutable.Map[String,String], sc: SparkContext): RDD[(String, String)] = {
    var out : RDD[(String,String)] = null
    try {
      out = esQueryByID(campaignID, esResourceCampaign, esReadConf,  sc)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to find campaign: " +  campaignID)
        ArrowLogger.log.error(e.printStackTrace())
    }

    out
  }

  // Helper function to get multiple audience ids based on an array of document ids
  def getAudiencesByIDs(audienceIDs: Array[String], exportTypes: Array[(String, Boolean)],
                        esResourceAudience: String, esResourceEvent: String,
                        esReadConf: mutable.Map[String, String], spark: SparkSession): Array[Dataset[Row]] = {

    val allAudiences = audienceIDs.map(audience => getAudienceByID(audience, esResourceAudience, esReadConf, spark.sparkContext))
    val parsedAudiences = allAudiences.map(audienceRdd => audienceRdd.map(audienceTuple => parse(audienceTuple._2)).collect()(0))

    parsedAudiences.map(audienceJValue => {
      if (!audienceJValue.has("query")) {
        val msg = "The audience is missing a query!"
        ArrowLogger.log.error(msg)
        throw new RuntimeException(msg)
      }

      val originalQuery:String = (audienceJValue \ "query").extract[String]
      getAudienceIDs(originalQuery, exportTypes, esResourceEvent, esReadConf, spark)
    })
  }


  // Helper function to get audience document by ID
  def getAudienceByID(audienceID: String, esResourceAudience: String, esReadConf: mutable.Map[String,String], sc: SparkContext): RDD[(String, String)] = {
    var out : RDD[(String,String)] = null
    try {
      out = esQueryByID(audienceID, esResourceAudience, esReadConf, sc)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("Unable to find audience: " +  audienceID)
        ArrowLogger.log.error(e.printStackTrace())
    }

    out
  }

  // Get a document from ElasticSearch by ID
  def esQueryByID(id:String, esResource:String, esReadConf: mutable.Map[String,String], sc: SparkContext): RDD[(String, String)] = {
    try {
      val esConf = esReadConf.clone()
      val q = "?q=_id:" + id
      ArrowLogger.log.info("Query: " + q)
      esConf += ("es.query" -> q)
      esQuery(esResource, esConf, sc)
    }
    catch {
      case e: Exception => throw new Exception(e.getMessage)
    }
  }

  def esQueryWithKeyValueMap(kvMap:Map[String,Array[String]], esResource:String, esReadConf: mutable.Map[String,String], sc: SparkContext): RDD[(String, String)] = {
    try {
      val esConf = esReadConf.clone()
      val q = s"?q=(${kvMap.map(x => s"${x._1}:(${x._2.mkString(" OR ")})").mkString(" AND ")})"
      esConf += ("es.query" -> q)
      esQuery(esResource, esConf, sc)
    }
    catch {
      case e: Exception => throw new Exception(e.getMessage)
    }
  }

  // Return the result of an ES Query
  def esQuery(esResource: String, esReadConf: mutable.Map[String, String], sc: SparkContext): RDD[(String, String)] = {
    try {
      val esConf = esReadConf.clone()
      esConf += ("es.output.json" -> "true")
      esConf += ("es.resource" -> esResource)
      sc.esJsonRDD(esConf)
    }
    catch {
      case e: Exception => throw new Exception(e.getMessage)
    }
  }

  // Update an audience based on passed-in doc
  def updateAudience(audience: RDD[(String, String)], audienceID: String, doc: JValue, esResourceAudience: String, esWriteConf: mutable.Map[String,String]) {

    ArrowLogger.log.info("Updating audience")
    ArrowLogger.log.info(write(doc))

    //UDF that updates a row with the incoming doc object
    val mod_audience = audience.map(
      rdd => {
        var json : JValue = render(write(rdd._2))
        ArrowLogger.log.info(write(json))
        json = json merge doc

        val audienceID_json : JValue = ("sparkID", audienceID)
        json = json merge audienceID_json

        ArrowLogger.log.info(json)

        write(json)
      }
    )
    val es_conf = esWriteConf.clone()
    es_conf += ("es.mapping.id" -> "sparkID")
    es_conf += ("es.resource" -> esResourceAudience)
    es_conf += ("es.input.json" -> "true")
    mod_audience.saveJsonToEs(es_conf)
  }

  // Update a campaign based on passed-in doc
  def updateCampaign(campaignID: String, doc: JValue, esResourceCampaign: String, esWriteConf: mutable.Map[String,String], esReadConf: mutable.Map[String,String], sc: SparkContext) {

    val campaignRDD = getCampaignByID(campaignID, esResourceCampaign, esReadConf, sc)
    updateCampaign(campaignRDD, campaignID, doc, esResourceCampaign, esWriteConf)
  }

  // Update a campaign based on passed-in doc
  def updateCampaign(campaign: RDD[(String, String)], campaignID: String, doc: JValue, esResourceCampaign: String, esWriteConf: mutable.Map[String,String]) {

    //UDF that updates a row with the incoming doc object
    val mod_campaign = campaign.map(
      rdd => {
        var json : JValue = render(write(rdd._2))
        ArrowLogger.log.info("Updating campaign")
        ArrowLogger.log.info(write(json))
        ArrowLogger.log.info(write(doc))
        json = json merge doc

        val campaignID_json : JValue = ("sparkID", campaignID)
        json = json merge campaignID_json

        ArrowLogger.log.info(json)

        write(json)
      }
    )

    val es_conf = esWriteConf.clone()
    es_conf += ("es.mapping.id" -> "sparkID")
    es_conf += ("es.resource" -> esResourceCampaign)
    es_conf += ("es.input.json" -> "true")
    mod_campaign.saveJsonToEs(es_conf)
  }


  // Gets audience uids based on an audience's information from Elasticsearch
  def getAudienceIDs(
                      query: String,
                      exportFields: Array[(String, Boolean)],
                      esResourceName: String,
                      esReadConf: mutable.Map[String,String],
                      spark: SparkSession): Dataset[Row] = {
    var fieldString = ""
    for (ef <- exportFields) {
      if (ef._2) {
        fieldString += ef._1 + ","
      }
    }
    fieldString = fieldString.dropRight(1)

    val es_conf = esReadConf.clone()
    es_conf += ("es.output.json" -> "false")
    es_conf += ("es.resource" -> esResourceName)
    es_conf += ("es.read.field.as.array.include" -> fieldString)
    es_conf += ("es.input.max.docs.per.partition" -> "1000000")
    es_conf += ("pushdown" -> "true")
    es_conf += ("es.query" -> query)

    ArrowLogger.log.info(s"Getting audience IDs with query: $query")
    spark.read.format("org.elasticsearch.spark.sql")
      .options(es_conf)
      .load
  }

  // Gets audience uids based on an audience's information from Elasticsearch
  def getAllAudienceMetaData(
                      esResourceName: String,
                      esReadConf: mutable.Map[String,String],
                      sc: SparkContext): Array[Map[String, String]] = {

    try {
      val es_conf = esReadConf.clone()
      es_conf += ("es.output.json" -> "true")
      es_conf += ("es.resource" -> esResourceName)

      val audiences = sc.esJsonRDD(es_conf)
      audiences.map(x => Map[String,String](
        "id" -> x._1,
        "name" -> {implicit val formats:org.json4s.DefaultFormats = DefaultFormats; (parse(x._2) \ "name").extract[String]})
      ).collect()
    }
    catch {
      case e: Exception => throw e
    }
  }

  // Utility function to retrieve all documents from a specified index
  def getAllDocuments(
                  esResourceName: String,
                  esReadConf: mutable.Map[String,String],
                  sc: SparkContext): RDD[(String,String)] = {

    try {
      val es_conf = esReadConf.clone()
      es_conf += ("es.output.json" -> "true")
      es_conf += ("es.resource" -> esResourceName)

      sc.esJsonRDD(es_conf)
    }
    catch {
      case e: Exception => throw e
    }
  }

  def getEmailFromId(esUsr: String, pwd: String, esHost: String, esPort: String,
                     usr: String, defEmail: String): String = {
    var result: String = null
    try {
      if (usr.nonEmpty && pwd.nonEmpty && esHost.nonEmpty && esPort.nonEmpty && usr.nonEmpty) {
        val req = Http(s"http://$esHost:$esPort/_xpack/security/user/$usr").auth(s"$esUsr", s"$pwd").asString.body
        val email = compact(render(parse(req) \\ "email")).split("\"").mkString
        result = email match {
          case "{}" => ArrowLogger.log.warn("getEmailId unable to extract information for: " + usr + "=>" + esHost +
            "Defaulting email to support email: " + defEmail)
            defEmail
          case _ => email
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("getEmailId unable to extract information for: " + usr + "=>" + esHost +
          " due to Error: " + e)
        ArrowLogger.log.error(e.printStackTrace())
    }
    result
  }
}
