package arrow.util

import javax.mail.Message.RecipientType
import javax.mail._
import javax.mail.internet.{InternetAddress, MimeMessage}

import arrow.config.ArrowLogger

object EmailUtil {
  private def createSession(host: String, usr: String, pwd: String): Session = {
    val props = System.getProperties()
    props.put("mail.smtp.host", host)
    props.put("mail.smtp.user", usr)
    props.put("mail.smtp.password", pwd)
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.port", "587")
    props.put("mail.smtp.starttls.enable", "true")

    val auth: Authenticator = new Authenticator() {
      override def getPasswordAuthentication = new PasswordAuthentication(usr, pwd)
    }

    Session.getInstance(props, auth)
  }

  def send(session: Session, from: String, to: Iterable[String], subject: String, text: String): Unit = {
    try {
      val message = new MimeMessage(session)
      val recipients: Array[Address] = to.map(address => new InternetAddress(address)).toArray

      message.setFrom(new InternetAddress(from))
      message.addRecipients(RecipientType.TO, recipients)
      message.setSubject(subject)
      message.setText(text)

      Transport.send(message)
    } catch {
      case messagingException: MessagingException =>
        ArrowLogger.log.error("Transport.send within EmailUtil.send could not send email due to Error:" + messagingException)
        ArrowLogger.log.error(messagingException.printStackTrace())
      case e: Exception =>
        ArrowLogger.log.error("EmailUtil.send could not send email due to Error:" + e)
        ArrowLogger.log.error(e.printStackTrace())
    }
  }

  // Convenience method for a single "to" of type String
  def send(session: Session, from: String, to: String, subject: String, text: String): Unit = {
    send(session, from, Array(to), subject, text)
  }

  // Convert host/usr/pwd to Session
  def send(from: String, to: Iterable[String], subject: String,
           text: String, host: String, usr: String, pwd: String): Unit = {
      val session = createSession(host, usr, pwd)
      send(session, from, to, subject, text)
  }

  // Convenience method for a single "to" of type String
  def send(from: String, to: String, subject: String,
           text: String, host: String, usr: String, pwd: String): Unit = {
    send(from, Array(to), subject, text, host, usr, pwd)
  }
}
