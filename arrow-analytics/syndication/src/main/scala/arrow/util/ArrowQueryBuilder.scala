package arrow.util

import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.spark_project.guava.base.Preconditions

import scala.collection.mutable.ListBuffer

class ArrowQueryBuilder(originalQuery:String, childType:String) {
  Preconditions.checkNotNull(originalQuery)
  Preconditions.checkArgument(originalQuery.nonEmpty)

  val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  val filters:ListBuffer[Map[String, Any]] = ListBuffer.empty[Map[String, Any]]

  def addFilter(filter: Map[String, Any]): ArrowQueryBuilder = {
    filters +=
      Map("has_child" ->
        Map("type" -> childType, "query" -> filter)
      )

    this
  }

  def build(): Map[String, Any] = {
    buildHelper()
  }

  def buildAsJson(pretty:Boolean = false): String = {
    if (pretty)
      mapper.enable(SerializationFeature.INDENT_OUTPUT)
    else
      mapper.disable(SerializationFeature.INDENT_OUTPUT)

    mapper.writeValueAsString(buildHelper())
  }

  private def buildHelper(): Map[String, Any] = {
    val queryMap = mapper.readValue(originalQuery, classOf[Map[String, Any]])

    if (filters.nonEmpty) {
      Map("query" ->
        Map("bool" ->
          (queryMap("bool").asInstanceOf[Map[String, Any]] ++ Map("filter" -> filters, "minimum_should_match" -> 1))
        )
      )
    } else {
      Map("query" -> queryMap)
    }
  }
}

