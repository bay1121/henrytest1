package arrow.config

import org.apache.log4j.Logger

/**
  * Created by natha on 2/11/2017.
  */
private[arrow] object ArrowLogger extends Serializable {
  @transient lazy val log = Logger.getLogger(getClass.getName)
}
