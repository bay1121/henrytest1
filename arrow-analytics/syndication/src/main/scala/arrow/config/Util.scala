package arrow.config

import java.io.File

import com.typesafe.config.{ Config, ConfigFactory }
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.security.alias.CredentialProviderFactory
import org.apache.spark.{ SparkContext, SparkFiles }
import org.kohsuke.args4j.{ CmdLineException, CmdLineParser }
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.amazonaws.services.secretsmanager.AWSSecretsManager
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.collection.JavaConversions._
import com.amazonaws.services.sns.AmazonSNSClient
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.auth.InstanceProfileCredentialsProvider
import com.amazonaws.{ ClientConfiguration, auth }
import com.amazonaws.services.s3.{ AmazonS3, AmazonS3ClientBuilder }

/**
 * Created by stephanieleighphifer on 2/14/17.
 */
trait Util {

  val ES_SYND_USER = "es.synd.user"
  val ES_USER_KEY = "es.user"
  val NS_USER_KEY = "ns.user"
  val BK_UID = "bk.uid"
  val BK_SECRET_KEY = "bk.secret.key"
  val BK_API_KEY = "bk.api.key"
  val AWS_SMTP_KEY = "aws.smtp.key"
  val AWS_SMTP_USER = "aws.smtp.user"
  val AWS_SECRETS_NAME = "aws.secrets.name"
  lazy val secrets = {
    val secret = getAwsSecrets(ConfigFactory.load().getString(AWS_SECRETS_NAME))

    implicit val formats = org.json4s.DefaultFormats
    parse(secret).extract[Map[String, String]]
  }

  def getSecureCredential(name: String): String = {
    var passwd: String = null
    try {
      passwd = secrets(name)
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("Unable to obtain secure credential for: " + name + " provider")
        ArrowLogger.log.warn(e.printStackTrace())
    }
    passwd
  }

  def getAwsSecrets(secretName: String): String

  def publishSNSMessage(topicARN: String, message: String) {
    try {
      val snsClient = AmazonSNSClient.builder()
        .withRegion(Regions.US_WEST_2)
        .withCredentials((new InstanceProfileCredentialsProvider(false)))
        .build()
      val publishRequest = new PublishRequest(topicARN, message)
      snsClient.publish(publishRequest)
    } catch {
      case t: Throwable =>
        ArrowLogger.log.error("Unable to post SNS message to the topic: " + topicARN)
        ArrowLogger.log.error(t.printStackTrace())
        throw t
    }
  }

}

object Util extends Util {

  def getAwsSecrets(secretName: String): String = {
    val client = AWSSecretsManagerClientBuilder.standard().build()
    val getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName)

    client.getSecretValue(getSecretValueRequest).getSecretString()
  }
}
