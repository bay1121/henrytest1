package arrow.syn

import arrow.{Desyndication, IDExport}
import arrow.config.ArrowLogger
import arrow.constants.Constants._
import arrow.util.ESUtil
import arrow.util.ESUtil.updateCampaign
import org.apache.spark.sql._
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

object NightlyDesyndication {

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  def run(syn: Desyndication, spark: SparkSession) {

    implicit class JValueExtended(value: JValue) {
      def has(childString: String): Boolean = {
        (value \ childString) != JNothing
      }
    }

    val json: JValue = (IS_SYNDICATION_ACTIVE_KEY, false) ~ (IS_SYNDICATION_QUEUED_KEY, false)
    val sc = spark.sparkContext

    //Find all campaigns that are falling out of syn
    val campaigns = ESUtil.getAllDocuments(syn.esResourceCampaign, syn.esReadConf, sc)

    campaigns.collect.map(campaign => {
      implicit val formats: org.json4s.DefaultFormats = DefaultFormats

      val campaignID = campaign._1
      val campaignObj = parse(campaign._2)
      var endedSyndication = false

      val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).withTime(23, 59, 59, 0)

      try {
        implicit val formats: org.json4s.DefaultFormats = DefaultFormats
        val campaignName = (campaignObj \ CAMPAIGN_NAME_KEY).extract[String]
        val campaignSyndicationEndDate = if (campaignObj.has(DATE_SYNDICATION_ENDS_KEY)) (campaignObj \ DATE_SYNDICATION_ENDS_KEY).extract[String] else ""
        val campaignInSyndication = if (campaignObj.has(IS_SYNDICATION_ACTIVE_KEY)) (campaignObj \ IS_SYNDICATION_ACTIVE_KEY).extract[Boolean] else false

        if (campaignInSyndication &&
          !campaignSyndicationEndDate.isEmpty &&
          DateTimeFormat.forPattern(DATE_PATTERN).withZone(DateTimeZone.UTC).parseDateTime(campaignSyndicationEndDate).getMillis < now.getMillis
        ) {
          ArrowLogger.log.warn("Ending Syndication for " + campaignID + " aka " + campaignName + ". End date is: " + campaignSyndicationEndDate)

          updateCampaign(campaignID,
            json,
            syn.esResourceCampaign,
            syn.esWriteConf,
            syn.esReadConf,
            sc)

          endedSyndication = true
        }
      }
      catch {
        case e: Exception =>
          ArrowLogger.log.warn("Unable to determine syn status for " + campaignID)
      }

      (campaignID, endedSyndication)

    })
  }
}


