package arrow

import java.net.URI

import arrow.config.{ ArrowLogger, Util }
import arrow.constants.Constants
import arrow.util.{ ESUtil, EmailUtil }
import com.google.common.net.InetAddresses
import com.typesafe.config.Config
import org.apache.hadoop.fs.{ FileSystem, Path }
import org.apache.hadoop.io.{ LongWritable, Text }
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark.rdd._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.streaming.StreamingContext
import org.elasticsearch.spark._
import org.elasticsearch.spark.sql._
import org.joda.time._
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.{ CmdLineException, CmdLineParser }

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.reflect.ClassTag
import com.typesafe.config.{ Config, ConfigFactory }

object CustomSegments {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.
      builder().
      appName("CustomAttributes").
      getOrCreate()

    val getValidTime = spark.udf.register("getValidTime", parseDateTime _)
    val getValidPii = spark.udf.register("getValidations", colValidator _)

    val configs = ConfigFactory.load()

    val esUser = configs.getString("es.user")
    val esPasswd = Util.getSecureCredential(Util.ES_USER_KEY)
    val esNodes = configs.getString("es.nodes")
    val esPort = configs.getString("es.port")

    //Email Params
    val awsSmtpUser = Util.getSecureCredential(Util.AWS_SMTP_USER)
    val awsSmtpKey = Util.getSecureCredential(Util.AWS_SMTP_KEY)
    val awsSmtpHost = configs.getString("aws.smpt.host")
    val alertId = configs.getString("alert.email.id")
    val defaultEmail = configs.getString("support.email.id")

    //Create Spark Streaming Context.
    val sc = spark.sparkContext
    val hadoopConfiguration = spark.sparkContext.hadoopConfiguration
    val ssc = new StreamingContext(sc, org.apache.spark.streaming.Seconds(configs.getInt("refresh.interval")))

    //File properties
    val inputBucket = configs.getString("bucket.prefix")

    //bucket paths
    val errorBucket = inputBucket.replace(inputBucket.split("/")(inputBucket.split("/").length - 1), "errored")
    val processedBucket = inputBucket.replace(inputBucket.split("/")(inputBucket.split("/").length - 1), "processed")
    val tempBucket = inputBucket.replace(inputBucket.split("/")(inputBucket.split("/").length - 1), "tempProcess")

    var processed = Map[String, String]()
    var errored = Map[String, String]()

    val cStream = ssc.fileStream[LongWritable, Text, TextInputFormat](inputBucket).
      transform(rdd => new UnionRDD(rdd.context, rdd.dependencies.
        map(dep => dep.rdd.asInstanceOf[RDD[(LongWritable, Text)]].
          map(_._2.toString).setName(dep.rdd.name))))
    val flNameRdd = cStream.transform(rdd => transformByFile(rdd, byFileTransformer))
    var fileName: String = null
    var filePath: String = null
    var createdBy: String = null
    var createDate: String = null

    val fs = FileSystem.get(new URI(inputBucket), hadoopConfiguration)
    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC)
    var esSegEnt: org.apache.spark.sql.Dataset[org.apache.spark.sql.Row] = null
    //Schema for custom_seg_lookup idx
    val custom_seg = StructType(Array(
      StructField("name", StringType, true),
      StructField("type", StringType, true),
      StructField("source", StringType, true),
      StructField("s3Path", StringType, true),
      StructField("count", IntegerType, true),
      StructField("dateCreated", StringType, true),
      StructField("dateLastExported", StringType, true),
      StructField("createdBy", StringType, true),
      StructField("index", StringType, true)))

    val esResourceName = configs.getString("customsegment.index")

    //ES options
    var esOutSeg = mutable.Map[String, String]()
    esOutSeg += "es.nodes" -> esNodes
    esOutSeg += "es.port" -> esPort
    esOutSeg += "es.index.auto.create" -> "true"
    esOutSeg += "es.net.http.auth.user" -> esUser
    esOutSeg += "es.net.http.auth.pass" -> esPasswd
    esOutSeg += "es.net.ssl" -> "true"
    esOutSeg += "es.net.ssl.cert.allow.self.signed" -> "true"
    esOutSeg += "es.resource" -> s"$esResourceName"
    esOutSeg += "es.batch.size.bytes" -> "24mb"
    esOutSeg += "es.batch.size.entries" -> "24000"

    var esOutEntry = esOutSeg.clone()
    var esRead = esOutSeg.clone()

    /**
     * this function is used to check if the current audience name already exist
     * in Custom Segment Index.
     *
     * @param audName (audience name)
     * @param config  (esOut_write)
     * @return
     */
    def isAudenceExist(audName: String, config: collection.mutable.Map[String, String]): Boolean = {
      try {
        val q = s"?q=name:$audName"
        config += "es.query" -> q
        val audCount = sc.esRDD(config).count
        if (audCount > 0) true else false
      } catch {
        case e: Exception => {
          if (e.getMessage.contains("missing and settings [es.index.read.missing.as.empty]"))
            false
          else throw new Exception(e.getMessage)
        }
      }
    }

    if (esPasswd == null) {
      throw new RuntimeException("Unable to obtain secure credential for ElasticSearch user")
    }

    //process Stream
    flNameRdd.foreachRDD(rdd => {
      try {
        if (rdd.partitions.nonEmpty) {
          import spark.implicits._
          val tempDF = rdd.toDF
          var outTemp = tempDF.withColumn("temp", split($"_2", "\\,")).
            select($"_1".as("file_path"), $"temp".getItem(0).as("pii_col"),
              $"temp".getItem(1).as("last_seen")).drop("temp")
          val inPath = outTemp.select("file_path").distinct.collectAsList
          inPath.foreach(
            path => {
              //file identifiers
              filePath = path.mkString
              val fnLen = filePath.split("/").length
              fileName = filePath.split("/")(fnLen - 1).mkString
              val audienceName = fileName.split("_")(0).split("-").mkString
              val audienceType = fileName.split("_")(1).split("-").mkString
              val source = fileName.split("_")(2).split("-").mkString
              val piiType = fileName.split("_")(3).split("-").mkString
              createdBy = fileName.split("_")(4).mkString
              createDate = fileName.split("_")(5).mkString.split("\\.")(0).mkString
              val out = outTemp.select(
                col("file_path"),
                when(col("pii_col") === "", null).otherwise(col("pii_col")).alias(piiType),
                when(col("last_seen") === "", null).
                  when(col("last_seen").isNotNull, getValidTime(col("last_seen"))).
                  when(col("last_seen").isNull, now.toString).alias("last_seen")).
                where(col("file_path") === filePath &&
                  col(s"$piiType").isNotNull)

              //Check to see if mandatory fields are not null and file Format validation
              val inValidPiiCt = out.select(getValidPii(lit(s"$piiType"), col(s"$piiType"))
                as "piiCheck").filter(col("piiCheck") === false).count
              val inValiDateCt = out.select(col("last_seen")).filter(col("last_Seen").isNull).count()
              val piiCheck = out.filter(col(s"$piiType").isNull).count()

              //filename format check
              if (!fnValidator(fileName)) {
                ArrowLogger.log.error("Error:file name does not meet process " +
                  "requirement for file:" + fileName)
                errored += fileName + " does not meet custom segment file naming conventions " -> filePath
              } //Check if Audience already exists
              else if (isAudenceExist(audienceName, esRead)) {
                errored += fileName + "Audience with " + audienceName + " already exists. Please rename" +
                  " the audience name " -> filePath
              } //check if PII field is null
              else if (piiCheck > 0) {
                ArrowLogger.log.error("Error:pii field has NULL values for:" + fileName)
                errored += fileName + " has null values in " + piiType + " field" -> filePath
              } //check invalid PII data count
              else if (inValidPiiCt > 0) {
                ArrowLogger.log.error("Error: error in PII field for File:" + fileName)
                errored += fileName + " has invalid PII data for " + piiType -> filePath
              } //check invalid dates
              else if (inValiDateCt > 0) {
                ArrowLogger.log.error("Error: error in Date field for File:" + fileName)
                errored += fileName + " has invalid format for Date field" -> filePath
              } else {
                val esInCnt = out.count().toInt
                piiType match {
                  case Constants.MAID_KEY => {
                    esSegEnt = out.withColumn(
                      s"${Constants.WBCID_KEY}",
                      sha2(sha2(trim(col(Constants.MAID_KEY)), 384), 256)).
                      withColumn("segment_name", lit(audienceName)).
                      drop("file_path")

                  }

                  case "email" => {
                    esSegEnt = out.withColumnRenamed("email", s"${Constants.EMAIL_ADDRESS_KEY}").
                      withColumn(
                        s"${Constants.WBCID_KEY}",
                        sha2(sha2(lower(trim(col(s"${Constants.EMAIL_ADDRESS_KEY}"))), 384), 256)).
                        withColumn("segment_name", lit(audienceName)).
                        drop("file_path")
                  }

                  case "bk id" => {
                    esSegEnt = out.withColumnRenamed("bk id", s"${Constants.UID_KEY}").
                      withColumn(
                        s"${Constants.WBCID_KEY}",
                        sha2(sha2(trim(col("bk id")), 384), 256)).
                        withColumn("segment_name", lit(audienceName)).
                        drop("file_path")
                  }

                  case "ip address" => {
                    esSegEnt = out.
                      withColumnRenamed("ip address", s"${Constants.CLIENT_IP_KEY}").
                      withColumn("segment_name", lit(audienceName)).
                      drop("file_path")

                  }

                  case "zip code" => {
                    esSegEnt = out.withColumnRenamed("zip code", s"${Constants.ZIP_CODE}").
                      withColumn("segment_name", lit(audienceName)).
                      drop("file_path")
                  }

                  case "neu hh id" => {
                    esSegEnt = out.withColumnRenamed("neu hh id", s"${Constants.NEUSTAR_HH_ID}").
                      withColumn("segment_name", lit(audienceName)).
                      drop("file_path")

                  }
                }
                val idIdx = s"arrow-segment-v2-$audienceName/segment_entry".toLowerCase
                esOutEntry += "es.resource" -> idIdx
                val esSegDesc = Seq((audienceName, audienceType, source, filePath,
                  esInCnt, now.toString, now.toString, createdBy, idIdx)).toDF().rdd

                val esSegOut = spark.createDataFrame(esSegDesc, custom_seg)

                processed += fileName -> filePath
                esSegOut.saveToEs(esOutSeg)
                esSegEnt.saveToEs(esOutEntry)
              }
            })
          if (processed.nonEmpty) {
            processed.foreach(currentBucket => {
              fs.rename(new Path(currentBucket._2), new Path(processedBucket))
              val emailId = ESUtil.getEmailFromId(esUser, esPasswd, esNodes, esPort, createdBy, defaultEmail)
              val subject = "Custom Segment process Status: success"
              val text = "Custom file process for:" + currentBucket._1 + " is completed " +
                " and file is moved to:" + processedBucket
              EmailUtil.send(alertId, emailId, subject, text, awsSmtpHost, awsSmtpUser, awsSmtpKey)
            })
            processed = Map[String, String]()
          }
          if (errored.nonEmpty) {
            errored.foreach(currentBucket => {
              fs.rename(new Path(currentBucket._2), new Path(errorBucket))
              val emailId = ESUtil.getEmailFromId(esUser, esPasswd, esNodes, esPort, createdBy, defaultEmail)
              val subject = "Custom Segment process Status: Failed"
              val text = "Custom file process failed due to " + currentBucket._1 +
                " and file is moved to:" + errorBucket
              EmailUtil.send(alertId, emailId, subject, text, awsSmtpHost, awsSmtpUser, awsSmtpKey)
            })
            errored = Map[String, String]()
          }
        }
        val listFile = fs.listStatus(new Path(tempBucket))

        if (listFile.nonEmpty) {
          listFile.foreach(x => {
            fs.rename(x.getPath, new Path(inputBucket))
          })
        }
      } catch {
        case e: Exception =>
          ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
          EmailUtil.send(alertId, defaultEmail, "error in Custom Segment" +
            " process ", s"Error:${e.getMessage}\n Usage:\n", awsSmtpHost,
            awsSmtpUser, awsSmtpKey)
      }
    })
    ssc.start()
    ssc.awaitTermination()
  }

  /**
   * Standardize date format to WB std for any given date format and delimiters
   *
   * @param datePart date(Sting)
   * @return time Date(String)
   */
  def parseDateTime(datePart: String): String = {
    var time: String = null
    try {
      val dateDelimiter = if (datePart.indexOf('.') != -1) "." else if (datePart.indexOf('-') != -1) "-" else "/"
      val dateFactors = dateDelimiter match {
        case "." => datePart.split("\\" + dateDelimiter)
        case _ => datePart.split(dateDelimiter)
      }
      val dayPattern = if (dateFactors(1).size == 1) "d" else "dd"
      val monthPattern = if (dateFactors(0).size == 1) "M" else "MM"
      val yearPattern = if (dateFactors(2).size == 2) "yy" else "yyyy"
      val formatter = DateTimeFormat.forPattern(monthPattern + dateDelimiter + dayPattern + dateDelimiter + yearPattern)
      time = formatter.withZone(DateTimeZone.UTC).parseDateTime(datePart).toString()
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("getValidTimeFcn unable to extract time information from: " + datePart + " due to Error: " + e)
        ArrowLogger.log.warn(e.printStackTrace())
        time
    }
    time
  }

  /**
   * validates FileName:
   *
   * @param inpfileName (fileName as String)
   * @return Boolean
   */
  def fnValidator(inpfileName: String): Boolean = {
    val filePattern = "^([A-Za-z0-9-]+)_([A-Za-z0-9-]+)_([A-Za-z0-9-]+)_([A-Za-z0-9-]+)_([._A-Za-z0-9-]+)_([0-9]+).csv".r
    inpfileName match {
      case filePattern(_*) => true
      case _ => false
    }
  }

  /**
   * validates PII column based on Pii Types
   *
   * @param piiCol (piiType)
   * @param colVal (piiColumn)
   * @return
   */
  def colValidator(piiCol: String, colVal: String): Boolean = {
    piiCol match {
      case Constants.MAID_KEY => maidValidation(colVal)
      case "email" => emailValidation(colVal)
      case "zip code" => zipValidation(colVal)
      case "ip address" => ipValidation(colVal)
    }
  }

  /**
   * Validates emailID:
   *
   * @param emailCol
   * @return Boolean
   */
  def emailValidation(emailCol: String): Boolean = {
    val emailPattern = """(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""".r
    if (emailPattern.findAllMatchIn(emailCol).nonEmpty) true else false
  }

  /**
   * Validates zip:
   *
   * @param zipCol (Zip as String)
   * @return Boolean
   */
  def zipValidation(zipCol: String): Boolean = {
    val isIntRegex = "^\\d+$".r
    zipCol match {
      case isIntRegex() => true
      case _ => false
    }
  }

  /**
   * validates ip address:
   *
   * @param ipCol Ip address as String
   * @return Boolean
   */
  def ipValidation(ipCol: String): Boolean = {
    InetAddresses.isInetAddress(ipCol)
  }

  /**
   * validates maid:
   *
   * @param maidCol
   * @return Boolean
   */
  def maidValidation(maidCol: String): Boolean = {
    if (maidCol.nonEmpty && maidCol != "00000000-0000-0000-0000-000000000000")
      true
    else false
  }

  def transformByFile[U: ClassTag](unionrdd: RDD[String], transformFunc: String => RDD[String] => RDD[U]): RDD[U] = {
    new UnionRDD(
      unionrdd.context,
      unionrdd.dependencies.flatMap { dep =>
        if (dep.rdd.isEmpty) None
        else {
          val filename = dep.rdd.name
          Some(transformFunc(filename)(dep.rdd.asInstanceOf[RDD[String]]).setName(filename))
        }
      })
  }

  def byFileTransformer(filename: String)(rdd: RDD[String]): RDD[(String, String)] =
    rdd.map(line => (filename, line))
}
