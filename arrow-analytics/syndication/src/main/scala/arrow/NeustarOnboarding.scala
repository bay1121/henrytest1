package arrow

import java.net.URI

import arrow.config.{ArrowLogger, Util}
import arrow.constants.Constants._
import arrow.util.ESUtil._
import arrow.util.JsonUtil._
import com.typesafe.config.Config
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, KafkaUtils, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable.ArrayBuffer
import scala.collection.{Map, mutable}
import org.apache.hadoop.conf.Configuration
import com.typesafe.config.{Config, ConfigFactory}

class NeustarOnboarding extends Serializable {
  val configs: Config = ConfigFactory.load()
  //Secure Credentials
  
  val esPasswd: String = Util.getSecureCredential(Util.ES_SYND_USER)
  val kfQuorum: String = configs.getString("kafka.quorum")
  val kafkaTopic: String = configs.getString("kafka.topic.neustar")


  val kafkaGroupID: String = configs.getString("kafka.group.id.neustar")
  val esNodes: String = configs.getString("es.nodes")
  val esPort: String = configs.getString("es.port")
  val esUser: String = configs.getString("es.user")
  val esResourceAudience: String = configs.getString("es.resource.audience")
  val esResourceCampaign: String = configs.getString("es.resource.campaign")
  val esResourceEvent: String = configs.getString("es.resource.event")
  val esResourceSegment: String = configs.getString("es.resource.segment")
  val esReadConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true"
  )

  val esWriteConf: mutable.Map[String, String] = mutable.Map[String, String](
    "es.nodes" -> esNodes,
    "es.port" -> esPort,
    "es.batch.size.bytes" -> "24mb",
    "es.batch.size.entries" -> "24000",
    "es.net.http.auth.user" -> esUser,
    "es.net.http.auth.pass" -> esPasswd,
    "es.net.ssl" -> "true",
    "es.net.ssl.cert.allow.self.signed" -> "true",
    "es.write.operation" -> "update",
    "es.index.auto.create" -> "true"
  )
  val topics: Array[String] = Array(kafkaTopic)
  val kafkaParams: Map[String, Object] = Map[String, Object](
    // the usual params, make sure to change the port in bootstrap.servers if 9092 is not TLS
    "bootstrap.servers" -> kfQuorum,
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> kafkaGroupID,
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )
  val outputMaxFileLength: Int = configs.getInt("output.max.filesize")
  val awsLocationRoot: String = configs.getString("aws.location.root.neustar")

}

/** *
  * Serializable class so that we can do work on the segments while passing in the field names we need
  *
  * @param fieldNames
  */
class NeustarSegmentWork(fieldNames: Array[String]) extends Serializable {

  /** *
    * Turn an RDD[Map] into RDD[Row]
    *
    * @param rdd
    * @return
    */
  def mapToRow(rdd: RDD[Map[String, String]]): RDD[Row] = {
    rdd.map(map => {
      var ret = ArrayBuffer[String]();
      fieldNames.foreach(field => ret += map.getOrElse(field, null));
      Row.fromSeq(ret)
    })
  }
}

object NeustarOnboarding {

  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("NeustarOnboarding")
      .getOrCreate()
    val sc = spark.sparkContext
    val ssc = new StreamingContext(sc, Seconds(10))
    
    val conf = sc.hadoopConfiguration
    val neu = new NeustarOnboarding

    // Hold a reference to the current offset ranges, so it can be used downstream
    var offsetRanges = Array.empty[OffsetRange]

    val messages = KafkaUtils.createDirectStream[String, String](ssc, PreferConsistent, Subscribe[String, String](neu.topics, neu.kafkaParams))
    messages.transform(rdd => {
      offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd
    }).foreachRDD(rdd => {
      val rdd2 = rdd.map(x => x.value())
      var processedRecord = false
      val kafkaMessages = rdd2.collect()

      kafkaMessages.foreach(kafkaMessage => {
        try {
          val campSynd = parse(kafkaMessage)
          val campaignID = (campSynd \ CAMPAIGN_ID_KEY).extract[String]
          val syndicate = (campSynd \ IS_SYNDICATION_KEY).extract[Boolean]

          ArrowLogger.log.info("Working on: " + campaignID)

          var isActive: JValue = ("isSyndicationActive", syndicate)

          val campaignTupled = getCampaignByID(campaignID, neu.esResourceCampaign, neu.esReadConf, sc)

          if (syndicate) {
            // Obtain a list of audiences within a campaign
            val parsedCampaigns = parseCampaign(campaignTupled).collect()(0)
            val exportTypes = getExportFields(getExportTypes(parsedCampaigns,""))
            val audienceIDs = getCampaignAudienceIDs(parsedCampaigns)
            val audiences = getAudiencesByIDs(audienceIDs, exportTypes, neu.esResourceAudience, neu.esResourceEvent, neu.esReadConf, spark)

            // Build Schema
            var fields = ArrayBuffer[StructField]()
            for (ef <- exportTypes) {
              var sf = StructField(ef._1, StringType, true)
              if (ef._2) {
                sf = StructField(ef._1, ArrayType(StringType, true), true)
              }

              fields += sf
            }
            val schema = StructType(fields.toArray[StructField])
            val fieldNames = exportTypes.map(field => field._1)

            // Dataframe for storing the unexploded data
            var dataToExplode = spark.createDataFrame(sc.emptyRDD[Row], schema)

            // Union all audiences together
            audiences.foreach(audience => dataToExplode = dataToExplode.union(audience.select(fieldNames.head, fieldNames.tail: _*)))

            // Set empty arrays to null for explode to work
            dataToExplode = setEmptyToNull(dataToExplode)

            // Explode all columns that are expected to be arrays
            // Except for client_ip, which we just take the first value
            for (ef <- exportTypes) {
              if (ef._2) {
                if (ef._1 == CLIENT_IP_KEY) {
                  dataToExplode = dataToExplode.withColumn(ef._1, col(ef._1).getItem(0))
                } else {
                  dataToExplode = dataToExplode.withColumn(ef._1, explode(col(ef._1)))
                }
              }
            }

            // Set the exploded data to the dataOut var
            var dataOut = dataToExplode

            // Retrieve the Segment Ids from the campaign Array[String]
            val segmentIDs = getCampaignSegmentIDs(parsedCampaigns)

            // Get the segments from the segment index Array[RDD[(String, String)]]
            val segments = segmentIDs.map(id => getAudienceByID(id, neu.esResourceSegment, neu.esReadConf, sc))

            // Parse the audience Array[RDD[JValue]]
            val parsedSegments = segments.map(segment => segment.map(tuple => {
              implicit val formats: org.json4s.DefaultFormats = DefaultFormats;
              parse(tuple._2)
            }))

            // Extract the IDs from the parsed segment Array[RDD[Array[Map[String,String]]]]
            val ids = parsedSegments.map(audienceRDD => audienceRDD.map(audience => {
              implicit val formats: org.json4s.DefaultFormats = DefaultFormats;
              (audience \ "ids").extract[Array[Map[String, String]]]
            }))

            // Flatten the segment id map Array[RDD[Map[String,String]]]
            val flattenedIds = ids.map(rdd => rdd.flatMap(arrayOfMaps => arrayOfMaps))

            // Create a NeustarSegmentWork class with the fieldnames we would like to export
            val nsw = new NeustarSegmentWork(fieldNames)

            // For each of the RDDs, Remove the fields we want from the map and put them into a row in the RDD
            // Array[RDD[Row]]
            val segmentRows = flattenedIds.map(rdd => nsw.mapToRow(rdd))

            // Convert the RDD to a dataframe with the supplied schema, and union to dataOut
            segmentRows.foreach(rdd => dataOut = spark.createDataFrame(rdd, dataOut.schema).union(dataOut))

            // Determine the number of files
            var parts = 1
            if (neu.outputMaxFileLength > 0) {
              val dfSize = dataOut.count //audienceSize
              parts = math.ceil(dfSize / neu.outputMaxFileLength).toInt
              if (parts == 0) {
                parts = 1
              }
            }

            // Check to see if the staging directory is in S3, create if not
            val stagingDir = neu.awsLocationRoot + "/staging/"
            val bucketPath = new Path(stagingDir)
            val fs = FileSystem.get(new URI(stagingDir), conf)

            if (!fs.exists(bucketPath)) {
              fs.create(bucketPath)
            }

            // Write the data out
            dataOut.repartition(parts).write.option("header", "true").option("delimiter", "|").csv(stagingDir + "/" + campaignID + "-" + (System.currentTimeMillis / 1000))

          } else {
            isActive = ("isSyndicationActive", syndicate) ~ ("isSyndicationQueued", false)
          }

          // Update the campaign
          updateCampaign(campaignTupled,
            campaignID,
            isActive,
            neu.esResourceCampaign,
            neu.esWriteConf)
        }
        catch {
          case e: Exception =>
            ArrowLogger.log.error("Unable to complete Neustar processing in: " + kafkaMessage)
            ArrowLogger.log.error(e)
        }
      })

      // After batch completes, write offsets to HDFS
      if (processedRecord) {
        ArrowLogger.log.info("Committing offsets to Kafka")
        messages.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)
      }
    })

    ArrowLogger.log.info("Starting Neustar App")
    ssc.start()
    ssc.awaitTermination()
  }

  /** *
    * Set empty arrays to an array with 1 null element so explode doesn't delete the row
    *
    * @param df
    * @return
    */
  def setEmptyToNull(df: DataFrame): DataFrame = {
    val exprs = df.schema.map {
      f =>
        f.dataType match {
          case StringType => when(length(col(f.name)) === 0, lit(null: String).cast(StringType)).otherwise(col(f.name)).as(f.name)
          case ArrayType(StringType, true) => {
            when(size(col(f.name)) === 0, lit(Array(null): Array[String]).cast(ArrayType(StringType))).otherwise(col(f.name)).as(f.name)
          }
          case _ => {
            col(f.name)
          }
        }
    }

    val newDF = df.select(exprs: _*)
    newDF
  }
}
