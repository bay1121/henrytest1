package arrow

import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}

import scala.reflect.io.Path
import scala.util.Try

/**
  * Created by sanush on 03/22/2018
  */
class CustomSegmentsTest extends AsyncFlatSpec
  with Matchers with Eventually with BeforeAndAfterAll {

  private val master = "local[1]"
  private val appName = "CustomSegmentTest"
  private val filePath: String = "target/testfile"

  private var sc: SparkContext = _

  override def beforeAll(): Unit = {
    val sparkConf = new SparkConf().setMaster(master).setAppName(appName).set("spark.driver.allowMultipleContexts", "true")
    val sc = new SparkContext(sparkConf)
  }

  override def afterAll(): Unit = {
    Try(Path(filePath + "-1000").deleteRecursively)
  }


  "CustomSegments.parseDateTime" should "return the appropriate UTC date format when provided a valid key" in {
    val datePart1 = "09/16/2008"
    val datePart2 = "09.16.2008"
    val datePart3 = "09-16-2008"
    val dateResult = "2008-09-16T00:00:00.000Z"
    assert(CustomSegments.parseDateTime(datePart1) == dateResult)
    assert(CustomSegments.parseDateTime(datePart2) == dateResult)
    assert(CustomSegments.parseDateTime(datePart3) == dateResult)
  }

  "CustomSegments.fnValidator" should "return true" in {
    val fileName = "testAudience_wbBros_wbB_maid_v.sanus_20180322.csv"
    assert(CustomSegments.fnValidator(fileName) == true)
  }

  "CustomSegments.emailValidation" should "return true" in {
    val email = "test@warnerbros.com"
    assert(CustomSegments.emailValidation(email) == true)
  }

  "CustomSegments.maidValidation" should "return true" in {
    val maid = "123"
    assert(CustomSegments.maidValidation(maid) == true)
  }

  "CustomSegments.ipValidation" should "return true" in {
    val ipAddress = "10.0.0.0"
    assert(CustomSegments.ipValidation(ipAddress) == true)
  }

  "CustomSegments.zipValidation" should "return true" in {
    val zip = "12345"
    assert(CustomSegments.zipValidation(zip) == true)
  }
}
