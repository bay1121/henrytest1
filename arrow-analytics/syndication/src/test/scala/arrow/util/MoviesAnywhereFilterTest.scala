package arrow.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

import scala.collection.mutable.ListBuffer

class MoviesAnywhereFilterTest extends FlatSpec
  with Matchers
  with BeforeAndAfterEach {

  val childType: String = "arrow_child"
  var mockQuery: String = _

  override protected def beforeEach(): Unit = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    val mockQueryStream = getClass.getClassLoader.getResourceAsStream("mockAudienceQuery.json")
    mockQuery = mapper.readTree(mockQueryStream).toString
  }

  it should "add Movies Anywhere exclusion filter when not using an allowable business unit" in {
    val builder = new ArrowQueryBuilder(originalQuery = mockQuery, childType = childType)
    val businessUnit = "Fail Whale"

    MoviesAnywhereFilter.applyFilterToQueryBuilder(businessUnit, builder)
    val query = builder.build()

    val filters = query("query")
      .asInstanceOf[Map[String, Any]]("bool")
      .asInstanceOf[Map[String, Any]]("filter")
      .asInstanceOf[ListBuffer[Map[String, Any]]]

    filters.size should be(1)

    val mustNot = filters.head
      .asInstanceOf[Map[String, Any]]("has_child")
      .asInstanceOf[Map[String, Any]]("query")
      .asInstanceOf[Map[String, Any]]("bool")
      .asInstanceOf[Map[String, Any]]("must_not")
      .asInstanceOf[List[Map[String, Any]]]

    mustNot.size should be(1)

    val result = mustNot.head
      .asInstanceOf[Map[String, Any]]("term")
      .asInstanceOf[Map[String, Any]]("siterollup.keyword")
      .asInstanceOf[Map[String, Any]]("value")

    result should be("Movies Anywhere")
  }

  it should "do not add Movies Anywhere exclusion filter when using an allowable business unit" in {
    val builder = new ArrowQueryBuilder(originalQuery = mockQuery, childType = childType)
    val businessUnit = MoviesAnywhereFilter.allowableBusinessUnits.head

    MoviesAnywhereFilter.applyFilterToQueryBuilder(businessUnit, builder)
    val query = builder.build()

    a [NoSuchElementException] should be thrownBy query("query")
      .asInstanceOf[Map[String, Any]]("bool")
      .asInstanceOf[Map[String, Any]]("filter")
  }
}
