package arrow.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest._

import scala.collection.mutable.ListBuffer

class ArrowQueryBuilderTest extends FlatSpec
  with Matchers
  with BeforeAndAfterEach {

  val ChildType:String = "arrow_child"
  var mockQuery:String = _

  override protected def beforeEach(): Unit = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    val mockQueryStream = getClass.getClassLoader.getResourceAsStream("mockAudienceQuery.json")
    mockQuery = mapper.readTree(mockQueryStream).toString
  }

  it should "add filters in the correct location of the nested structure" in {
    val aMockFilter = Map("range" ->
      Map("someField" ->
        Map("someDateField" ->
          Map("gte" -> 0))
      )
    )

    val anotherMockFilter = Map("range" ->
      Map("someField" ->
        Map("someDateField" ->
          Map("gte" -> 0))
      )
    )

    val builder = new ArrowQueryBuilder(originalQuery = mockQuery, childType = ChildType)
    builder.addFilter(aMockFilter)
    builder.addFilter(anotherMockFilter)

    val query = builder.build()
    val filters = query("query")
      .asInstanceOf[Map[String, Any]]("bool")
      .asInstanceOf[Map[String, Any]]("filter")
      .asInstanceOf[ListBuffer[Map[String, Any]]]

    filters.size should be(2)
  }

  it should "include the minimum_should_match field when using filters" in {
    val builder = new ArrowQueryBuilder(originalQuery = mockQuery, childType = ChildType)
    builder.addFilter(Map("not" -> "important for test"))
    val query = builder.build()

    val minimumShouldMatch = query("query")
      .asInstanceOf[Map[String, Any]]("bool")
      .asInstanceOf[Map[String, Any]]("minimum_should_match")
      .asInstanceOf[Int]

    minimumShouldMatch should be(1)
  }

  it should "simply wrap the original query with a query, when no filters are added" in {
    val builder = new ArrowQueryBuilder(originalQuery = mockQuery, childType = ChildType)
    val query = builder.build()
    query.get("query").nonEmpty shouldBe true

    query("query")
      .asInstanceOf[Map[String, Any]]
      .get("bool").isEmpty shouldNot be(true)
  }
}
