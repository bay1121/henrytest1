package arrow.util

import arrow.SparkSessionSetup
import org.apache.spark.SparkException
import org.json4s.jackson.JsonMethods.parse
import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}


class JsonUtilTest extends WordSpec
  with Matchers
  with SparkSessionSetup
  with Eventually
  with BeforeAndAfterAll {

  "JsonUtils." should {
    "getValidExportValues " should {
      "successfully extract parse true export values" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonexportvalues.jsonl").getPath).map(row => ("myjson", row))
        val test = json.map(tuple => parse(tuple._2))
        val export = test.map(x => JsonUtil.getValidExportValues(x, "my_export_value"))
        val values = export.collect
        test.foreach(println)
        values.length should be(1)
        values(0) should contain("my_true_bool")
        values(0) should not contain ("my_false_bool")
      }

      "return empty sequence when no string is present" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonexportvalues.jsonl").getPath).map(row => ("myjson", row))
        val test = json.map(tuple => parse(tuple._2))
        val export = test.map(x => JsonUtil.getValidExportValues(x, "field_not_present"))
        val values = export.collect
        values(0).length should be(0)
      }

      "error when invalid type is passed" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonstring.jsonl").getPath).map(row => ("myjson", row))
        val test = json.map(tuple => parse(tuple._2))
        try {
          test.map(x => JsonUtil.getValidExportValues(x, "my_export_value")).collect()
        } catch {
          case e:SparkException => assert(e.getCause.isInstanceOf[IllegalArgumentException])
          case e:Throwable => throw e
        }
      }

      "log when map is passed with wrong types" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonmapintint.jsonl").getPath).map(row => ("myjson", row))
        val test = json.map(tuple => parse(tuple._2))
        val export = test.map(x => JsonUtil.getValidExportValues(x, "my_export_value"))
        try {
          val values = export.collect
          values.foreach(println)
        } catch {
          case e => e.getMessage.contains("Do not know how to convert JInt(1) into boolean") should be(true)
        }
      }
    }

    "getCampaignAudienceIDs " should {
      "log when map is passed with wrong types" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonaudienceids.jsonl").getPath)
        val test = json.map(j => parse(j))
        val result = test.map(x => JsonUtil.getCampaignAudienceIDs(x)).collect()(0)
        result should contain ("1")
        result should contain ("2")
      }

      "return empty sequence when no audience is present" in withSparkSession { (sparkSession) =>
        val loader = getClass.getClassLoader
        val json = sparkSession.sparkContext.textFile(loader.getResource("json/jsonstring.jsonl").getPath).map(row => ("myjson", row))
        val test = json.map(tuple => parse(tuple._2))
        val result = test.map(x => JsonUtil.getCampaignAudienceIDs(x))
        val values = result.collect
        values(0).length should be(0)
      }
    }
  }
}
