package arrow.google

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.{aResponse, givenThat, post, urlEqualTo}
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import com.google.api.ads.adwords.lib.client.AdWordsSession
import com.google.api.client.auth.oauth2.{BearerToken, Credential}
import org.apache.spark.sql.SparkSession
import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

import scala.beans.BeanProperty

class GoogleUtilsTest extends FlatSpec
  with Matchers
  with Eventually
  with BeforeAndAfter {
  private val master = "local[1]"
  private val appName = "GoogleUtilsTest"
  val syn = new arrow.GoogleSyndication()
  System.setProperty("javax.net.ssl.trustStore", System.getProperty("trustStore"))
  private var spark: SparkSession = _
  val loader = getClass.getClassLoader
  private val wireMockServer = new WireMockServer(wireMockConfig().port(8080).httpsPort(8444).keystorePath(System.getProperty("trustStore")))
  WireMock.configureFor("localhost", 8080)
  val credential = new Credential(BearerToken.authorizationHeaderAccessMethod())
  val session = new AdWordsSession.Builder()
    .withClientCustomerId("123-456-7891")
    .withDeveloperToken("AWEDEEDDDDDTREWQSDFGYTR")
    .withEndpoint("https://localhost:8444")
    .withUserAgent("foo")
    .withOAuth2Credential(credential).build()
  before {
    wireMockServer.start()
    spark = SparkSession
      .builder()
      .master(master)
      .appName(appName).config("spark.driver.allowMultipleContexts", "true").config("spark.io.compression.codec", "snappy")
      .config("spark.testing.memory", 771859200L)
      .getOrCreate()
  }
  after {
    wireMockServer.stop()
  }


  "GoogleUtils.generateEMAILAudienceId" should " throw exceptions when Google generation of Custom Audience ID Fails" in {

    val mapper = new ObjectMapper()


    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("Dummy audience ID created by Wiremock")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))

    assertThrows[java.lang.NullPointerException] {
      GoogleUtils.createNewAudienceList("newlist", "EMAIL", session)
    }
  }
  "GoogleUtils.generateMAIDAudienceId" should " throw exceptions when Google generation of Custom Audience ID Fails" in {

    val mapper = new ObjectMapper()
    val arrayNode = mapper.createArrayNode()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("Dummy audience ID created by Wiremock")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))

    assertThrows[java.lang.NullPointerException] {
      GoogleUtils.createNewAudienceList("newlist", "MAID", session)
    }
  }

  "GoogleUtils.updateEmailAudienceList" should " throw exceptions when update of Google Email Custom Audience list Fails" in {

    val mapper = new ObjectMapper()
    val arrayNode = mapper.createArrayNode()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))
    val userList = List("abc@gmail.com", "def@gmail.com")
    assertThrows[java.lang.NullPointerException] {
      GoogleUtils.updateAudienceInUserList(1, userList, 1234567890, "Add", "EMAIL", session)
    }
  }
  "GoogleUtils.updateMAIDAudienceList" should " throw exceptions when update of Google MAID Custom Audience list Fails" in {

    val mapper = new ObjectMapper()
    val arrayNode = mapper.createArrayNode()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))

    val userList = List("B5A47A5E-B4A0-4850-AD00-3B2319AA922B", "8F59C580-6BDC-4D71-BADF-61E902DC68AF", "D4DDEB5F-2DCF-4D69-BFDA-0AF84C9EE194")
    assertThrows[java.lang.NullPointerException] {
      GoogleUtils.updateAudienceInUserList(1, userList, 1234567890, "Add", "MAID", session)
    }
  }
  "GoogleUtils.GenerateSession" should " throw exceptions as correct configuration is not passed" in {
    assertThrows[Exception] {
      GoogleUtils.generateOAuthCredential
    }
  }
  "GoogleUtils.checkIfAudienceIdExists" should " throw exceptions when selector query doesnt reach the endpoint" in {
    val mapper = new ObjectMapper()
    val arrayNode = mapper.createArrayNode()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))

    val loader = getClass.getClassLoader
    assertThrows[java.lang.NullPointerException] {
      val p = GoogleUtils.checkIfAudienceIdExists("audience1", "email", session)
    }

  }
}
