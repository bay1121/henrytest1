package arrow.google

import arrow.GoogleSyndication
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import com.google.api.ads.adwords.lib.client.AdWordsSession
import com.google.api.client.auth.oauth2.{BearerToken, Credential}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.json4s.{JLong, JObject, JString, JValue}
import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.json4s.DefaultFormats
import org.json4s.JNothing
import org.json4s.JValue
import org.json4s.JsonDSL.boolean2jvalue
import org.json4s.JsonDSL.long2jvalue
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.json4s.string2JsonInput

import scala.beans.BeanProperty


class SyndicateToGoogleTest extends FlatSpec
  with Matchers
  with Eventually
  with BeforeAndAfter {
  private val master = "local[1]"
  private val appName = "SyndicateToGoogleTest"
  val syn = new arrow.GoogleSyndication()
  System.setProperty("javax.net.ssl.trustStore", System.getProperty("trustStore"))

  private var spark: SparkSession = _
  val loader = getClass.getClassLoader
  private val wireMockServer = new WireMockServer(wireMockConfig().port(8080).httpsPort(8444).keystorePath(System.getProperty("trustStore")))
  WireMock.configureFor("localhost", 8080)
  val credential = new Credential(BearerToken.authorizationHeaderAccessMethod())
  val session = new AdWordsSession.Builder()
    .withClientCustomerId("123-456-7891")
    .withDeveloperToken("AWEDEEDDDDDTREWQSDFGYTR")
    .withEndpoint("https://localhost:8444")
    .withUserAgent("foo")
    .withOAuth2Credential(credential).build()
  before {
    wireMockServer.start()
    spark = SparkSession
      .builder()
      .master(master)
      .appName(appName).config("spark.driver.allowMultipleContexts", "true").config("spark.io.compression.codec", "snappy")
      .getOrCreate()
  }
  after {
    wireMockServer.stop()
    spark.stop()
  }

  "syndicateToGoogle.getCustomAudienceId" should "return custom id from dummy JSON passed as parameter" in {
    val output = SyndicateToGoogle.getCustomAudienceId(syn, parse("{\"name\":\"dummyaudiencelist\",\"google\":{\"customAudienceId_email\":123455}}"), "email")
    assert(output == 123455)
  }
  "syndicateToGoogle.getCustomAudienceId" should "throw Validation Exception due to missing secrets in Google Ads when corresponding audience id is missing" in {
    assertThrows[com.google.api.ads.common.lib.exception.ValidationException](SyndicateToGoogle.getCustomAudienceId(syn, parse("{\"name\":\"dummyaudiencelist\",\"google\":{\"customAudienceId_maid\":123455}}"), "email"))

  }
  "syndicateToGoogle.getCustomAudienceId" should "throw Validation Exception due to missing secrets in Google Ads when google tag is missing in doc" in {
    assertThrows[com.google.api.ads.common.lib.exception.ValidationException](SyndicateToGoogle.getCustomAudienceId(syn, parse("{\"name\":\"dummyaudiencelist\"}"), "email"))

  }

  "SyndicateToGoogle.performGoogleSyndication" should "fail with exception" in {
    val mapper = new ObjectMapper()
    val arrayNode = mapper.createArrayNode()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))

    val customAudienceIdJson = ("customAudienceId_Email", 12345678910L)

    val schema = StructType(Array(
      StructField("ids", StringType, nullable = true)
    ))
    val inputEmails = Seq(Row("dummy@gmail.com"), Row("dummy@hotmail.com"))
    val data = spark.createDataFrame(spark.sparkContext.parallelize(inputEmails), schema)
    assertThrows[Exception] {
      arrow.google.SyndicateToGoogle.performGoogleSyndication(syn,
        spark,
        data,
        "email",
        12345678910L,
        Map(
          "email" -> "EMAIL_SHA256",
          "maid" -> "MOBILE_ADVERTISER_ID"
        ),
        Seq("Dummy"),
        customAudienceIdJson)
    }
  }

  "SyndicateToGoogle.syndicateFromS3" should "fail with exception" in {
    val mapper = new ObjectMapper()

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/api/adwords/rm/v201809/AdwordsUserListService"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse().withHeader("Content-Type", "application/json").withBody(mapper.writeValueAsString((postData)))
          .withStatus(200)))
    val loader = getClass.getClassLoader
    assertThrows[Exception] {
      arrow.google.SyndicateToGoogle.syndicateFromS3(spark,"email", loader.getResource("dummy_emails.txt").getPath, "test")
    }
  }

  "SyndicateToGoogle.isAudienceValid" should " return true when the audience document from elastic contains an attribute by name query" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("query", JString(mockedQuery)),
      ("customAudienceId_email", JLong(9999999L))))
    assert(SyndicateToGoogle.isAudienceValid(audience) == true)
  }

  "SyndicateToGoogle.isAudienceValid" should " return false when the audience document from elastic does not contains an attribute by name query" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L))))
    assertThrows[Exception] {
      SyndicateToGoogle.isAudienceValid(audience)
    }
  }

  "SyndicateToGoogle.getQueryBuilder" should "throw exception as Campaign is missing Campaign business unit key" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign"))))

    assertThrows[Exception] {
      SyndicateToGoogle.getQueryBuilder(audience, campaign)
    }
  }
  "SyndicateToGoogle.getQueryBuilder" should "should not have any filters on date time as date time fields are missing in campaign" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToGoogle.getQueryBuilder(audience, campaign).filters.size == 1)

  }

  "SyndicateToGoogle.getQueryBuilder" should "have date filters on date time when date last exported data are available in audience and date user modified is missing" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("google", JObject(List(("dateLastExported", JLong(6)))))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToGoogle.getQueryBuilder(audience, campaign).filters.size == 2)
  }

  "SyndicateToGoogle.getQueryBuilder" should "not have date filters on date time when both date last exported and date user modified is available in audience and date user modified > date last exported" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("dateLastExported", JLong(0)),
      ("dateUserModified", JLong(1))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToGoogle.getQueryBuilder(audience, campaign).filters.size == 1)
  }


  "SyndicateToGoogle.getQueryBuilder" should "have date filters on date time when both date last exported and date user modified is available in audience and date user modified < date last exported" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Google Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("google", JObject(List(("dateLastExported", JLong(6))))),
      ("dateUserModified", JLong(5))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToGoogle.getQueryBuilder(audience, campaign).filters.size == 2)
  }

}

/*Sample Soap Request
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Header>
        <ns1:RequestHeader xmlns:ns1="https://adwords.google.com/api/adwords/rm/v201809" soapenv:mustUnderstand="0">
            <ns2:clientCustomerId xmlns:ns2="https://adwords.google.com/api/adwords/cm/v201809">123-456-7891</ns2:clientCustomerId>
            <ns3:developerToken xmlns:ns3="https://adwords.google.com/api/adwords/cm/v201809">REDACTED</ns3:developerToken>
            <ns4:userAgent xmlns:ns4="https://adwords.google.com/api/adwords/cm/v201809">unknown (AwApi-Java, AdWords-Axis/4.4.0, Common-Java/4.4.0, Axis/1.4, Java/1.8.0_201, maven)</ns4:userAgent>
            <ns5:validateOnly xmlns:ns5="https://adwords.google.com/api/adwords/cm/v201809">false</ns5:validateOnly>
            <ns6:partialFailure xmlns:ns6="https://adwords.google.com/api/adwords/cm/v201809">false</ns6:partialFailure>
        </ns1:RequestHeader>
    </soapenv:Header>
    <soapenv:Body>
        <mutate xmlns="https://adwords.google.com/api/adwords/rm/v201809">
            <operations>
                <ns7:operator xmlns:ns7="https://adwords.google.com/api/adwords/cm/v201809">ADD</ns7:operator>
                <operand xmlns:ns8="https://adwords.google.com/api/adwords/rm/v201809" xsi:type="ns8:CrmBasedUserList">
                    <ns8:name>newlist_Email</ns8:name>
                    <ns8:description>new_email_list</ns8:description>
                    <ns8:membershipLifeSpan>10000</ns8:membershipLifeSpan>
                    <ns8:uploadKeyType>CONTACT_INFO</ns8:uploadKeyType>
                </operand>
            </operations>
        </mutate>
    </soapenv:Body>
</soapenv:Envelope>*/