package arrow.facebook

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode

import scala.beans.BeanProperty
import org.json4s.DefaultFormats
import org.json4s.JNothing
import org.json4s.JValue
import org.json4s.jvalue2monadic
import org.scalatest.BeforeAndAfter
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.concurrent.Eventually
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._
import com.github.tomakehurst.wiremock.client.WireMock.givenThat
import org.json4s.JsonAST.JObject
import org.json4s.JsonDSL.boolean2jvalue
import org.json4s.JsonDSL.long2jvalue
import org.json4s.JsonDSL.string2jvalue
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.json4s.string2JsonInput

class FacebookUtilsTest extends FlatSpec
  with Matchers
  with Eventually
  with BeforeAndAfter {

  private val master = "local[1]"
  private val appName = "FacebookUtilsTest"
  private val hostname = "localhost"
  private val port = 8080
  private val httpsPort = 8443
  val syn = new arrow.FacebookSyndication()
  syn.apiVersion = "v3.2"
  syn.endPointBase = "https://localhost:" + httpsPort
  syn.videoBase = "dummy"

  // Run wiremock server on local machine with specified port.


  System.setProperty("javax.net.ssl.trustStore", System.getProperty("trustStore"))
  private val wireMockServer = new WireMockServer(wireMockConfig().port(8080).httpsPort(8443).keystorePath(System.getProperty("trustStore"))) //.notifier(new ConsoleNotifier(true)

  WireMock.configureFor("localhost", 8080);
  private var audienceId: Long = 0
  private var allowedFacebookSyndicationTypes: Map[String, String] = Map(
    "email" -> "EMAIL_SHA256",
    "maid" -> "MOBILE_ADVERTISER_ID"
  )
  private var adAccountId: Long = 1117278608420055L
  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  implicit class JValueExtended(value: JValue) {
    def has(childString: String): Boolean = {
      (value \ childString) != JNothing
    }
  }

  before {
    wireMockServer.start()
  }
  after {
    wireMockServer.stop()
  }


  "FacebookUtils.generateAudienceId" should " throw exceptions when Facebook generation of Custom Audience ID Fails" in {

    val mapper = new ObjectMapper();
    val arrayNode = mapper.createArrayNode();
    case class GetData(
                        @BeanProperty val data: ArrayNode
                      )
    val getData = GetData(arrayNode)
    val getJsonString = mapper.writeValueAsString(getData);
    val path = s"/v3.2/act_1234/customaudiences?access_token=null&fields=id&filtering=%5B%7B+%22field%22%3A+%22name%22%2C%22operator%22%3A+%22CONTAIN%22%2C+%22value%22%3A%22Test+Audience%22%7D%5D"
    givenThat(
      get(urlEqualTo(path))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(getJsonString)
          .withStatus(200)))

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("0")
    val postPath = s"/v3.2/act_1234/customaudiences"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(mapper.writeValueAsString(postData))
          .withStatus(200)))
    assertThrows[Exception] {
      FacebookUtils.generateAudienceId("Test Audience", "Description", syn.endPointBase, syn.videoBase, syn.apiVersion)
    }
  }

  "FacebookUtils.generateAudienceId" should " generate custom audience id in Facebook" in {

    val mapper = new ObjectMapper();
    val arrayNode = mapper.createArrayNode();
    case class GetData(
                        @BeanProperty val data: ArrayNode
                      )
    val getData = GetData(arrayNode)
    val getJsonString = mapper.writeValueAsString(getData);
    val path = s"/v3.2/act_1234/customaudiences?access_token=null&fields=id&filtering=%5B%7B+%22field%22%3A+%22name%22%2C%22operator%22%3A+%22CONTAIN%22%2C+%22value%22%3A%22Test+Audience%22%7D%5D"
    givenThat(
      get(urlEqualTo(path))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(getJsonString)
          .withStatus(200)))

    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("23843275445000724")
    val postPath = s"/v3.2/act_1234/customaudiences"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(mapper.writeValueAsString(postData))
          .withStatus(200)))
    assert(FacebookUtils.generateAudienceId("Test Audience", "Description", syn.endPointBase, syn.videoBase, syn.apiVersion) == "23843275445000724".toLong)
  }

  "FacebookUtils.addUsersToCustomAudience" should " throw exception as the custom audience id passed is 0" in {
    assertThrows[Exception] {
      FacebookUtils.addUsersToCustomAudience(0L, "EMAIL_SHA256", Nil)
    }
  }

  "FacebookUtils.addUsersToCustomAudience" should " throw exception as this will be pointing to direct Facebook URL instead of Wiremock URL" in {

    val mapper = new ObjectMapper();
    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("23843275445000724")
    val postPath = s"/v3.2/act_1234/customaudiences"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(mapper.writeValueAsString(postData))
          .withStatus(200)))
    assertThrows[Exception] {
      FacebookUtils.addUsersToCustomAudience(12345678L, "EMAIL_SHA256", List("14683d88281fc3ad43f39f8ceab111c96cc145be2a3feec98f914661f18d"))
    }
  }

  "FacebookUtils.addUsersToCustomAudience" should " add users to Custom Audience in Facebook" in {
    case class PostData(
                         @BeanProperty val audience_id: String,
                         @BeanProperty val session_id: String,
                         @BeanProperty val num_received: Int,
                         @BeanProperty val num_invalid_entries: Int,
                         @BeanProperty val invalid_entry_samples: String
                       )
    val postData = PostData("23843275445000724", "7684494835045627632", 1, 0, "")
    val mapper = new ObjectMapper();
    val postJsonString = mapper.writeValueAsString(postData)
    val postPath = s"/v3.2/12345678/users"
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(postJsonString)
          .withStatus(200)))
    assert(FacebookUtils.addUsersToCustomAudience(12345678L, "EMAIL_SHA256", List("14683d88281fc3ad43f39f8ceab111c96cc145be2a3feec98f914661f18d"), syn.endPointBase, syn.videoBase, syn.apiVersion).equals(()))
  }
  "FacebookUtils.checkAudienceExistence" should " return custom audience id from Facebook" in {

    val mapper = new ObjectMapper()
    case class GetData(
                        @BeanProperty id: String, @BeanProperty description: String

                      )
    case class GetData1(@BeanProperty val data: Array[GetData])
    val getData = GetData1(Array(GetData("23843275445000724", "Email IDs"), GetData("23843275445000723", "Mobile IDs")))

    val getJsonString = mapper.writeValueAsString(getData)

    val path = s"/v3.2/act_1234/customaudiences?access_token=null&fields=id%2Cdescription&filtering=%5B%7B+%22field%22%3A+%22name%22%2C%22operator%22%3A+%22CONTAIN%22%2C+%22value%22%3A%22FAN%22%7D%5D"
    givenThat(
      get(urlEqualTo(path))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(getJsonString)
          .withStatus(200)))

    assert(FacebookUtils.checkAudienceExistence("FAN", "email", syn.endPointBase, syn.videoBase, syn.apiVersion) == "23843275445000724".toLong)
    assert(FacebookUtils.checkAudienceExistence("FAN", "maid", syn.endPointBase, syn.videoBase, syn.apiVersion) == "23843275445000723".toLong)
  }

}
