package arrow.facebook

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import scala.beans.BeanProperty
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.sha2
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.json4s.DefaultFormats
import org.json4s.JLong
import org.json4s.JNothing
import org.json4s.JObject
import org.json4s.JString
import org.json4s.JValue
import org.json4s.JsonDSL.long2jvalue
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.scalatest.BeforeAndAfter
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.concurrent.Eventually

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.givenThat
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.client.WireMock.urlMatching
import com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import com.github.tomakehurst.wiremock.core.WireMockConfiguration._

import arrow.FacebookSyndication


class SyndicateToFacebookTest extends FlatSpec
  with Matchers
  with Eventually
  with BeforeAndAfter {

  private val master = "local[1]"
  private val appName = "SyndicateToFacebookTest"
  private val hostname = "localhost"
  private val port = 8080
  private val httpsPort = 8443
  val syn = new FacebookSyndication()
  syn.apiVersion = "v3.2"
  syn.endPointBase = "https://localhost:" + httpsPort
  syn.videoBase = "dummy"
  System.setProperty("javax.net.ssl.trustStore", System.getProperty("trustStore"))
  // Run wiremock server on local machine with specified port.
  private val wireMockServer = new WireMockServer(wireMockConfig().port(8080).httpsPort(8443).keystorePath(System.getProperty("trustStore"))) //.notifier(new ConsoleNotifier(true)

  WireMock.configureFor("localhost", 8080);
  private var spark: SparkSession = _
  private var audienceId: Long = 0
  private var allowedFacebookSyndicationTypes: Map[String, String] = Map(
    "email" -> "EMAIL_SHA256",
    "maid" -> "MOBILE_ADVERTISER_ID"
  )
  private var adAccountId: Long = 1117278608420055L
  implicit val formats: org.json4s.DefaultFormats = DefaultFormats

  implicit class JValueExtended(value: JValue) {
    def has(childString: String): Boolean = {
      (value \ childString) != JNothing
    }
  }


  before {
    spark = SparkSession
      .builder()
      .master(master)
      .appName(appName).config("spark.driver.allowMultipleContexts", "true").config("spark.io.compression.codec", "snappy")
      .getOrCreate()
    wireMockServer.start()
    val mapper = new ObjectMapper();
    val arrayNode = mapper.createArrayNode();
    case class GetData(
                        @BeanProperty val data: ArrayNode
                      )
    case class PostData(
                         @BeanProperty val id: String
                       )
    val postData = PostData("23843275445000724")
    val getData = GetData(arrayNode)
    val postJsonString = mapper.writeValueAsString(postData);
    val getJsonString = mapper.writeValueAsString(getData);
    val postPath = s"/v3.2/act_1234/customaudiences"
    val getPath = s"/v3.2/act_1234/customaudiences?access_token=null&fields=id&filtering=%5B%7B+%22field%22%3A+%22name%22%2C%22operator%22%3A+%22CONTAIN%22%2C+%22value%22%3A%22Test+Audience_email%22%7D%5D"
    givenThat(
      get(urlEqualTo(getPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(getJsonString)
          .withStatus(200)))
    givenThat(
      post(urlEqualTo(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(postJsonString)
          .withStatus(200)))
  }
  after {
    wireMockServer.stop()
  }


  "SyndicateToFacebook.handleHashing" should " hash the passed dataframe containing id as column using SHA 256" in {

    val schema = StructType(Array(
      StructField("ids", StringType, nullable = true)
    ))

    val inputEmails = Seq(Row("dummy@gmail.com"), Row("dummy@hotmail.com"))
    val input = spark.createDataFrame(spark.sparkContext.parallelize(inputEmails), schema)
    val expected = input.withColumn("hashed", sha2(lower(trim(col("ids"))), 256)).
      drop("ids").
      withColumnRenamed("hashed", "ids")
    val hashedDF = SyndicateToFacebook.handleHashing(input)
    assert(hashedDF.except(expected).count() == 0 && expected.except(hashedDF).count() == 0)
  }

  "SyndicateToFacebook.performFacebookSyndication" should " perform syndication to Facebook" in {
    case class PostData(
                         @BeanProperty val audience_id: String,
                         @BeanProperty val session_id: String,
                         @BeanProperty val num_received: Int,
                         @BeanProperty val num_invalid_entries: Int,
                         @BeanProperty val invalid_entry_samples: String
                       )
    val postData = PostData("23843275445000724", "7684494835045627632", 2, 0, "")
    val mapper = new ObjectMapper();
    val postJsonString = mapper.writeValueAsString(postData)
    val postPath = s"/v3.2/23843275445000724/users"
    givenThat(
      post(urlMatching(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(postJsonString)
          .withStatus(200)))
    val schema = StructType(Array(
      StructField("ids", StringType, nullable = true)
    ))

    val inputEmails = Seq(Row("dummy@gmail.com"), Row("dummy@hotmail.com"))
    val input = spark.createDataFrame(spark.sparkContext.parallelize(inputEmails), schema)
    val data = input.withColumn("hashed", sha2(lower(trim(col("ids"))), 256)).
      drop("ids").
      withColumnRenamed("hashed", "ids")
    audienceId = 23843275445000724L
    var json: JValue = ("customAudienceId_email", audienceId)
    val retJsonStub = SyndicateToFacebook.performFacebookSyndication(syn,
      spark,
      data,
      "email",
      audienceId,
      allowedFacebookSyndicationTypes,
      Seq("facebook"),
      json)
    assert((retJsonStub \ "customAudienceId_email").extract[Long] == 23843275445000724L)
    assert((retJsonStub \ "dateLastExported").extract[Long] > 0)
  }
  "SyndicateToFacebook.SyndicateFromS3" should " return custom audience id existing in audience document" in {
    val mapper = new ObjectMapper()
    case class GetData(
                        @BeanProperty id: String, @BeanProperty description: String

                      )
    case class GetData1(@BeanProperty val data: Array[GetData])
    val getData = GetData1(Array(GetData("23843275445000724", "Email IDs"), GetData("23843275445000723", "Mobile IDs")))

    val getJsonString = mapper.writeValueAsString(getData)

    val path = s"/v3.2/act_1234/customaudiences?access_token=null&fields=id%2Cdescription&filtering=%5B%7B+%22field%22%3A+%22name%22%2C%22operator%22%3A+%22CONTAIN%22%2C+%22value%22%3A%22FAN1%22%7D%5D"
    givenThat(
      get(urlEqualTo(path))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(getJsonString)
          .withStatus(200)))

    case class PostData(
                         @BeanProperty val audience_id: String,
                         @BeanProperty val session_id: String,
                         @BeanProperty val num_received: Int,
                         @BeanProperty val num_invalid_entries: Int,
                         @BeanProperty val invalid_entry_samples: String
                       )
    val postData = PostData("23843275445000724", "7684494835045627632", 2, 0, "")
    val postJsonString = mapper.writeValueAsString(postData)
    val postPath = s"/v3.2/23843275445000724/users"
    givenThat(
      post(urlMatching(postPath))
        .willReturn(aResponse()
          .withHeader("Content-Type", "application/json")
          .withBody(postJsonString)
          .withStatus(200)))
    val loader = getClass.getClassLoader
    arrow.facebook.SyndicateToFacebook.main(Array("email", loader.getResource("dummy_emails.txt").getPath, "FAN1"))
  }
  "SyndicateToFacebook.getCustomAudienceId" should " return custom audience id existing in audience document" in {

    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val response = "{\"data\":[]}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("query", JString(mockedQuery)),
      ("customAudienceId_email", JLong(9999999L))))

    assert(SyndicateToFacebook.getCustomAudienceId(syn, audience, "email") == "9999999".toLong)
  }

  /*Commented out as ES update is included and ES interface test coverage is yet to be build
  "SyndicateToFacebook.getCustomAudienceId" should " generate new custom audience id from Facebook as it is not available in audience document" in {

    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("query", JString(mockedQuery))))
    assert(SyndicateToFacebook.getCustomAudienceId(syn, audience, "email") == "23843275445000724".toLong)
  }*/

  "SyndicateToFacebook.isAudienceValid" should " return true when the audience document from elastic contains an attribute by name query" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("query", JString(mockedQuery)),
      ("customAudienceId_email", JLong(9999999L))))
    assert(SyndicateToFacebook.isAudienceValid(audience) == true)
  }

  "SyndicateToFacebook.isAudienceValid" should " return false when the audience document from elastic does not contains an attribute by name query" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L))))
    assertThrows[Exception] {
      SyndicateToFacebook.isAudienceValid(audience)
    }
  }

  "SyndicateToFacebook.getQueryBuilder" should "throw exception as Campaign is missing Campaign business unit key" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign"))))

    assertThrows[Exception] {
      SyndicateToFacebook.getQueryBuilder(audience, campaign)
    }
  }

  "SyndicateToFacebook.getQueryBuilder" should "should not have any filters on date time as date time fields are missing in campaign" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToFacebook.getQueryBuilder(audience, campaign).filters.size == 1)

  }

  "SyndicateToFacebook.getQueryBuilder" should "should have date filters on date time when date last exported data are available in audience and date user modified is missing" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("dateLastExported", JLong(0))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToFacebook.getQueryBuilder(audience, campaign).filters.size == 2)
  }

  "SyndicateToFacebook.getQueryBuilder" should "should not have date filters on date time when both date last exported and date user modified is available in audience and date user modified > date last exported" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("dateLastExported", JLong(0)),
      ("dateUserModified", JLong(1))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToFacebook.getQueryBuilder(audience, campaign).filters.size == 1)
  }


  "SyndicateToFacebook.getQueryBuilder" should "should have date filters on date time when both date last exported and date user modified is available in audience and date user modified < date last exported" in {
    val mockedQuery = "{\"bool\":{\"should\":[{\"has_child\":{\"type\":\"arrow_child\",\"query\":{\"bool\":{\"must\":[{\"simple_query_string\":{\"fields\":[\"movie_title\"],\"query\":\"\\\"The Eagle Huntress\\\"\"}}],\"must_not\":[]}}}}],\"must_not\":[]}}"
    val audience = new JObject(List(
      ("name", JString("Test Audience")),
      ("customAudienceId_email", JLong(9999999L)),
      ("query", JString(mockedQuery)),
      ("dateLastExported", JLong(6)),
      ("dateUserModified", JLong(5))))

    val campaign = new JObject(List(
      ("name", JString("Test Campaign")),
      ("businessUnit", JString("Test"))))
    assert(SyndicateToFacebook.getQueryBuilder(audience, campaign).filters.size == 2)
  }
}
