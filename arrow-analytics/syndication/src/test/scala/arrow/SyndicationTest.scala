package arrow

import org.apache.spark.{SparkConf, SparkContext}
import org.json4s._
import org.scalatest._
import org.scalatest.concurrent.Eventually

import scala.reflect.io.Path
import scala.util.Try
import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by stephanieleighphifer on 5/23/17.
  */
class SyndicationTest extends FlatSpec
  with Matchers
  with Eventually
  with BeforeAndAfter {

  private val master = "local[1]"
  private val appName = "SyndicationTest"
  private val filePath: String = "target/testfile"

  private var sc: SparkContext = _
  private var Syndication: Syndication = _

  before {
    val conf = new SparkConf().
      setMaster(master).
      setAppName(appName).
      set("spark.driver.allowMultipleContexts", "true")

    sc = new SparkContext(conf)
    Syndication = new Syndication(ConfigFactory.load())
  }

  after {
    Try(Path(filePath + "-1000").deleteRecursively)
  }

  "Syndication.buildBulkUser" should " create the approprite JObject based on properties file" in {
    val expected = new JObject(List(
      ("ResponseType", JString("Summary")),
      ("Method", JString("POST")),
      ("ResponseCallbackUrl", JString("http://52.89.146.143:57377/process/remote/response")),
      ("Scatter", JArray(List())))
    )

    assert(Syndication.buildBulkUser() == expected)
  }

  "Syndication.buildUser" should " create the approprite JObject based on properties file" in {
    val expected = new JObject(List(
      ("Method", JString("POST")),
      ("URIPath", JString("/getdata/44957/v1.2?bkuid=null&userid=USER_ID_456&phint=audience=AUDIENCE_ID_123")),
      ("RequestID", JString("AUDIENCE_ID_123|USER_ID_456"))))

    assert(Syndication.buildUser("AUDIENCE_ID_123", "USER_ID_456") == expected)
  }

  "Syndication.signatureBulkBuilder" should " generate the appropriate url" in {
    val expected = "https://bulkapi.bluekai.com/2/api?bksig=hEDQsgyS54qYHgCrPejKyvlcMVY7XWnEocXhT7KMimI="

    assert(Syndication.signatureBulkBuilder("DATA_STRING", "KEY") == expected)
  }

  "Syndication.signatureBulkBuilder" should " throw an NPE if the key is null" in {
    eventually {
      a[NullPointerException] should be thrownBy {
        Syndication.signatureBulkBuilder("DATA_STRING")
      }
    }
  }

  "Syndication.signatureInputBuilder" should " generate the appropriate url" in {
    val expected = "http://some.url.com:123/a/b?q=true?bkuid=UID&bksig=MUCoEiNB5i093HlZRswTlTyMWfHWXA0gMQ0iu2XrH4M%3D"

    assert(Syndication.signatureInputBuilder("http://some.url.com:123/a/b?q=true", "POST", "DATA_STRING", "UID", "KEY") == expected)
  }

  "Syndication.signatureInputBuilder" should " throw an NPE if the key is null" in {
    eventually {
      a[NullPointerException] should be thrownBy {
        Syndication.signatureInputBuilder("http://some.url.com:123/a/b?q=true", "POST", "DATA_STRING")
      }
    }
  }
}
