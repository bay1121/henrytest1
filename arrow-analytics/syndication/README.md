# Syndication

The bulk syndication analytic performs the back-end lookups and publishing for Bluekai audiences. Using and audience ID from the UI, it will query Elasticsearch for the audience members defined in the Arrow query, then publish those to Bluekai in batches (default250000 audience members per batch).

## Parameters and Variables
Default values included are:
```
parentcategoryid : 769833
retiredcategoryid : 821534
bulkmax : 1000
siteid : 28299
partnerid : 3217
topic : wb-arrow-syndication
```

## Unit and Integration Tests
All tests are located in `src/main/test`. Unit tests are written in the typical way where they do not include running an actual spark session, and only test specific methods and classes in isolation. Integration tests are used to ensure end-to-end correctness within the spark environment. At this time, there are no integration tests for the streaming jobs.

## Building
* "mvn clean package" results in a syndication*.tgz in the target directory
* Untar the tgz and execute "sh syndication-v<version>/ENV/package-and-deploy.sh"
  * The ENV is one of {dev, prod} for dev and prod environments respectively
  * The deploy script copies the jar and configuration files to a s3 location (make sure you have aws-credentials set-up for the s3 location)


#### Properties

 * They are located in "deploy", Properties specify to ENV are located in
   * dev.config.properties
   * prod.config.properties
 * Maven filtering is used for property substitution.
 * The properties are rendered as a "application.properties" file that is made available on the spark classpath (added as spark --files option).
 
#### EMR cluster
 * The emr create-cluster for syndication is create\_syndication_emr.sh
 * Create a EMR cluster with the 
   * bootstrap action  s3://<bucket-name>/bdd/arrow/syndication/<version>/bootstrap.sh. 
   * EMR cluster configuration (give below)
   * From the emr master node, ssh to all slave nodes and setup a cron job to run the clean_appcache.sh every hour (the spark streaming job writes to the app cache and fills the disk in 2-3 days, clean up older files)
   * TODO - automate the setup of the cron job on slaves or find a spark configuration that cleans up the appcache

 * The streaming job executes elastic search queries and brings down data (mostly 2-3 ID fields), this is not CPU intensive and most of time is spent on network IO.
 * The cluster and the streaming jobs are tuned for 16core 64G m5.4 machines, the CPU usage on a default cluster with 6 worker nodes was just 3%. So the proposal is to change the yarn vcores from 16 to 56 for each worker node and bring down the number of workers from 6 to 3, (yarn gives 56G memory to the cluster so to keep it simple we allocate 1 core / gig memory)
 * The spark submit for streaming jobs (syndication.sh) specifies the number of cores and memory for each job to make use of these tunings.
 
 * The usage was just about 20% even with 56 cores, so setting it to 336 and bringing the number of machines to 2. This setting failed with memory issues dialing back to 32 cores and 6 per executor
 * The final EMR cluster configuration looks thus
 
 ```javascript
 [
   {
      "classification":"spark",
      "properties":{
         "maximizeResourceAllocation":"true"
      }
   },
   {
      "classification":"yarn-site",
      "properties":{
         "yarn.nodemanager.resource.cpu-vcores":"108"
      }
   },
   {
      "classification":"hdfs-site",
      "properties":{
         "dfs.replication":"2"
      }
   },
   {
      "Classification":"capacity-scheduler",
      "Properties":{
         "yarn.scheduler.capacity.resource-calculator":"org.apache.hadoop.yarn.util.resource.DominantResourceCalculator"
      }
   },
   {
      "classification":"spark-defaults",
      "properties":{
         "spark.eventLog.enabled":"false",
         "spark.history.fs.cleaner.enabled":"true",
         "spark.history.fs.cleaner.maxAge":"48h",
         "spark.history.fs.cleaner.interval":"1h"
      }
   },
   {
      "classification":"spark-log4j",
      "properties":{
         "log4j.rootCategory":"INFO,RollingAppender",
         "log4j.appender.RollingAppender":"org.apache.log4j.rolling.RollingFileAppender",
         "log4j.appender.RollingAppender.rollingPolicy.FileNamePattern":"${spark.yarn.app.container.log.dir}/spark.log.%d{yyyyMMdd}.gz",
         "log4j.appender.RollingAppender.rollingPolicy":"org.apache.log4j.rolling.TimeBasedRollingPolicy",
         "log4j.appender.RollingAppender.layout":"org.apache.log4j.PatternLayout",
         "log4j.appender.RollingAppender.layout.ConversionPattern":"[%p] %d %c - %m%n"
      }
   }
]
 ```
 

## Running
Once the tar file has been transferred over, you can use the submit scripts. Review the deploy script for more information.

#### Properties
Unlike `arrow_analytics`, application properties are deployed independently. The submit scripts will explicitly reference and use them, as opposed to what is embedded in the jar.

## Testing the Pipeline on Dev
In order to ensure that the syndication is working properly, you may want to send messages to kafka. The easiest way to do this by using the `arrow` UI. Create a new campaign with one audience. I recommend using the audience already there, if there is one, otherwise make sure your new audience is not empty. Once you're done with that, click syndicate to send the message to kafka. 

Within the yarn application manager, you can navigate to the running spark job by clicking on the `applicationmaster` link. From there, click on the `steaming` tab. That will show you all the microbatches the streaming job has processed. The important thing to note is whether or not a message came in through one of those batches and was processed. Assuming your message was received by the streaming job, you can then look through the application logs to ensure everything is working properly.
 
### Import Note
After the first time you syndicate your campaign, the UI will prevent you from syndicating again on the same day (not 24 hours). To get around this, you need to modify the field that the `arrow` UI is using for this logic.

From Kibana, you can run this in order to work-around this feature:

```
POST arrow-audience/audience/YOUR_AUDIENCE_ID_GOES_HERE/_update
{
    "doc": {
      "dateLastExportedOracle": 0
    }
}
```

## Verifying results from Bluekai
The Bluekai bulk syndication analytic receives results through its asynchronous process on a remote server: 
52.89.146.143
Log into that machine to view the logs in /var/logs/arrow-syn/

## The Anatomy of Elasticsearch over Spark

### Elastic Search Read/Write

* [A good blurb on read performance](https://www.elastic.co/guide/en/elasticsearch/hadoop/current/performance.html#_read_performance)
* Spark uses the [scroll api](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-scroll.html) in order to get faster throughput when dealing with large data sets.
* When you use the `DataFrame` interface, and use the `pushdown` feature, the underlying connector, in this case Elasticsearch, will attempt to optimize all the transformations into single queries against the shards.
* [Configuration Reference](https://www.elastic.co/guide/en/elasticsearch/hadoop/5.6/configuration.html) 
    * `es.input.max.docs.per.partition` and `es.read.field.as.array.include` are important  
 
### View the queries that are being run

It can be handy to look at what the Elasticsearch connector is doing in spark, in order to optimize or debug. Here is one method:

1. Open a spark shell 
2. You can use this on dev to get you started:

```
import org.elasticsearch.spark.sql._;
import org.apache.log4j.Logger;

val logger = Logger.getLogger("org.elasticsearch.hadoop.rest")
logger.setLevel(org.apache.log4j.Level.TRACE)

val options = Map("es.nodes" -> "10.176.29.21", "es.port" -> "9200", "es.net.http.auth.user" -> "ingest-user", "es.net.http.auth.pass" -> "the password")
val df = spark.read.format("org.elasticsearch.spark.sql").options(options).load("theIndex")
df.select(...).filter(...)
```

###Run FaceBook Syndication and Google Syndication from command line

We can use the following commands to syndicate s3 files directly to FB/Google. Please note that we can only syndicate one type at a time, either email or maid. So file should have only 1 column and not hold any headers. Google syndication will specifically fail if MAID format doesnt follow the hex format. If any parameter other than email or maid is passed then no syndication will happen. Please  note that FB allows two different audience sets with same name whereas google doesnt allow that so we append _email or _maid accordingly at the end. This suffix append need not be sent when passing the audience name parameter

/usr/bin/spark-submit --class arrow.google.SyndicateToGoogle --deploy-mode cluster --files /home/hadoop/application.properties /home/hadoop/syndication-v1.7.18-jar-with-dependencies.jar <email or maid> <S3 file path> <whole_audience_name_without_email_maid_keyword>

/usr/bin/spark-submit --class arrow.facebook.SyndicateToFacebook --deploy-mode cluster --files /home/hadoop/application.properties /home/hadoop/syndication-v1.7.18-jar-with-dependencies.jar <email or maid> <S3 file path> <whole_audience_name_without_email_maid_keyword>


