#!/bin/bash

# the submit job is tuned for 16core 64G m5.4 machines, with custom yarn settings, see README.md, executor and driver memory leave room for yarn overhead

submit_job() {

/usr/bin/spark-submit \
    --class $1 \
    --deploy-mode cluster \
    --files /home/hadoop/application.properties \
    --driver-java-options "-XX:-UseGCOverheadLimit" \
    --conf "spark.executor.extraJavaOptions=-XX:-UseGCOverheadLimit" \
    --conf spark.hadoop.fs.s3a.enableServerSideEncryption=true \
    --conf spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256 \
    --conf spark.hadoop.fs.hdfs.impl.disable.cache=true \
    --conf spark.dynamicAllocation.enabled=true \
    --conf spark.dynamicAllocation.initialExecutors=1 \
    --conf spark.dynamicAllocation.minExecutors=1 \
    --executor-cores $2 \
    --executor-memory $3 \
    --driver-cores $4 \
    --driver-memory $5 \
    /home/hadoop/syndication-v1.7.25-jar-with-dependencies.jar    

}

submit_job arrow.Syndication 6 8G 4 8G 2>&1 | nc -w 600 -u 127.0.0.1 8197 &
submit_job arrow.IDExport 24 8G 4 8G 2>&1 | nc -w 600 -u 127.0.0.1 8198 &
submit_job arrow.CustomSegments 6 8G 4 8G 2>&1 | nc -w 600 -u 127.0.0.1 8199 &
submit_job arrow.FacebookSyndication 6 8G 4 8G 2>&1 | nc -w 600 -u 127.0.0.1 8196 & 
submit_job arrow.GoogleSyndication 6 8G 4 8G 2>&1 | nc -w 600 -u 127.0.0.1 8196 & 

