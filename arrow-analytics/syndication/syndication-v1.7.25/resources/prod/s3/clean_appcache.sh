#!/bin/bash

# invoked using a clean_appcache_cron on all datanodes

BASE_LOC=/mnt/yarn/usercache/hadoop/appcache
find $BASE_LOC/ -mmin +1440 -exec rmdir {} \;
find $BASE_LOC/ -mmin +1440 -exec rm {} \;
