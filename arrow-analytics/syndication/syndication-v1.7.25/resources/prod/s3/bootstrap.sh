#!/bin/bash

ROOT_LOC=s3://cmdt-software/bdd/arrow

BASE_LOC=$ROOT_LOC/syndication
BASE_LOC_VERSION=$BASE_LOC/1.7.25
JAR=syndication-v1.7.25-jar-with-dependencies.jar

aws s3 cp ${BASE_LOC_VERSION}/${JAR} /home/hadoop/${JAR}
aws s3 cp ${BASE_LOC_VERSION}/application.properties /home/hadoop/application.properties
aws s3 cp ${BASE_LOC_VERSION}/syndication.sh /home/hadoop/syndication.sh
aws s3 cp ${BASE_LOC_VERSION}/clean_appcache.sh /home/hadoop/clean_appcache.sh
chmod 755 /home/hadoop/clean_appcache.sh
sudo aws s3 cp ${BASE_LOC_VERSION}/clean_appcache_cron /etc/cron.d/
sudo service crond reload
