#!/bin/bash

BASE_LOC=${s3.base.loc}/${project.name}
BASE_LOC_VERSION=$BASE_LOC/${project.version}

aws emr create-cluster --name arrow_syndication_streaming_persistent \
    --name arrow_syndication_streaming_persistent \
    --release-label emr-5.20.0 \
    --instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=m5.xlarge InstanceGroupType=CORE,InstanceCount=3,InstanceType=r5.2xlarge \
    --applications Name=Spark Name=Ganglia \
    --service-role ${emr.service.role} \
    --ec2-attributes InstanceProfile=${emr.instance.role},KeyName=${ec2.key},SubnetId=${ec2.subnet} \
    --log-uri ${emr.log.location} \
    --tags owner="arrowsupport@warnerbros.com" Project="Arrow" Name="priv-bane-emr-bdd-arrow-arrow_syndication" "Financial Owner"="Lawrence Smith" \
           "Application Name"="Arrow - Audience Record Repository" "TS3 Number"="TS3.40680" "Cost Center"="C0100763" \
    --bootstrap-action Path=${BASE_LOC_VERSION}/bootstrap.sh,Name=BootstrapAction1 \
    --configurations '[
   {
      "Classification":"spark",
      "Properties":{
         "maximizeResourceAllocation":"true"
      }
   },
   {
      "Classification":"yarn-site",
      "Properties":{
         "yarn.nodemanager.resource.cpu-vcores":"108"
      }
   },
   {
      "Classification":"hdfs-site",
      "Properties":{
         "dfs.replication":"2"
      }
   },
   {
      "Classification":"capacity-scheduler",
      "Properties":{
         "yarn.scheduler.capacity.resource-calculator":"org.apache.hadoop.yarn.util.resource.DominantResourceCalculator"
      }
   },
   {
      "Classification":"spark-defaults",
      "Properties":{
         "spark.eventLog.enabled":"false",
         "spark.history.fs.cleaner.enabled":"true",
         "spark.history.fs.cleaner.maxAge":"48h",
         "spark.history.fs.cleaner.interval":"1h"
      }
   },
   {
      "Classification":"spark-log4j",
      "Properties":{
         "log4j.rootCategory":"INFO,RollingAppender",
         "log4j.appender.RollingAppender":"org.apache.log4j.rolling.RollingFileAppender",
         "log4j.appender.RollingAppender.rollingPolicy.FileNamePattern":"${spark.yarn.app.container.log.dir}/spark.log.%d{yyyyMMdd}.gz",
         "log4j.appender.RollingAppender.rollingPolicy":"org.apache.log4j.rolling.TimeBasedRollingPolicy",
         "log4j.appender.RollingAppender.layout":"org.apache.log4j.PatternLayout",
         "log4j.appender.RollingAppender.layout.ConversionPattern":"[%p] %d %c - %m%n"
      }
   }
]'