#!/bin/bash

ROOT_LOC=${s3.base.loc}

BASE_LOC=$ROOT_LOC/${project.name}
BASE_LOC_VERSION=$BASE_LOC/${project.version}
JAR=${assembly.final.name}.jar

aws s3 cp ${BASE_LOC_VERSION}/${JAR} /home/hadoop/${JAR}
aws s3 cp ${BASE_LOC_VERSION}/application.properties /home/hadoop/application.properties
aws s3 cp ${BASE_LOC_VERSION}/syndication.sh /home/hadoop/syndication.sh
aws s3 cp ${BASE_LOC_VERSION}/clean_appcache.sh /home/hadoop/clean_appcache.sh
chmod 755 /home/hadoop/clean_appcache.sh
sudo aws s3 cp ${BASE_LOC_VERSION}/clean_appcache_cron /etc/cron.d/
sudo service crond reload
