#!/usr/bin/env bash

set -e

trap 'printf "\n\n $(tput bold; tput setaf 7; tput setab 1) INSTALL FAILED !!!!!! $(tput sgr0)\n\n"' INT TERM QUIT EXIT

BASE_DIR=`dirname "$0"`

BASE_LOC=${s3.base.loc}/${project.name}
BASE_LOC_VERSION=$BASE_LOC/${project.version}

JAR=${assembly.final.name}.jar

aws s3 cp $BASE_DIR/../../$JAR ${BASE_LOC_VERSION}/${JAR} --sse
aws s3 cp $BASE_DIR/s3/application.properties ${BASE_LOC_VERSION}/application.properties --sse
aws s3 cp $BASE_DIR/s3/syndication.sh ${BASE_LOC_VERSION}/syndication.sh --sse
aws s3 cp $BASE_DIR/s3/clean_appcache.sh ${BASE_LOC_VERSION}/clean_appcache.sh --sse
aws s3 cp $BASE_DIR/s3/clean_appcache_cron ${BASE_LOC_VERSION}/clean_appcache_cron --sse
aws s3 cp $BASE_DIR/s3/bootstrap.sh ${BASE_LOC_VERSION}/bootstrap.sh --sse

trap - INT TERM QUIT EXIT

printf "\n\nINSTALL SUCCCESSFUL ${project.name}-v${project.version}\n\n"
