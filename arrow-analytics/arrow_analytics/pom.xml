<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>wb</groupId>
        <artifactId>arrow_analytics_parent</artifactId>
        <version>1.7.27-SNAPSHOT</version>
    </parent>

    <artifactId>arrow_analytics</artifactId>

    <!-- TODO: Pull out common props and put in parent pom -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <sbt.version>0.13</sbt.version>

        <compiler.plugin.version>3.5.1</compiler.plugin.version>
        <project-info-reports.plugin.version>2.9</project-info-reports.plugin.version>
        <site.plugin.version>3.5.1</site.plugin.version>

        <sbt-compiler.plugin.version>1.0.0</sbt-compiler.plugin.version>
        <scoverage.plugin.version>1.3.0</scoverage.plugin.version>

        <spark-csv.version>1.2.0</spark-csv.version>

        <jodaconvert.version>2.0.1</jodaconvert.version>

        <aws-java-sdk.version>1.10.22</aws-java-sdk.version>
        <elasticsearch-spark.version>6.2.4</elasticsearch-spark.version>
        <jackson.version>2.4.4</jackson.version>
        <maven-assembly-plugin.version>2.4</maven-assembly-plugin.version>
        <redshift.version>1.2.1.1001</redshift.version>

        <spark-redshift.version>2.0.1</spark-redshift.version>
        <sqljdbc.version>4.0</sqljdbc.version>
        <git-commit-id-plugin.version>2.2.4</git-commit-id-plugin.version>
        <assembly.final.name>${project.artifactId}-v${project.version}-jar-with-dependencies</assembly.final.name>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.databricks</groupId>
            <artifactId>spark-redshift_2.11</artifactId>
            <version>${spark-redshift.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>*</artifactId>
                    <groupId>org.apache.spark</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.databricks</groupId>
            <artifactId>spark-csv_2.11</artifactId>
            <version>${spark-csv.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>*</artifactId>
                    <groupId>org.apache.spark</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.univocity</groupId>
            <artifactId>univocity-parsers</artifactId>
            <version>2.7.5</version>
        </dependency>
        <dependency>
            <groupId>com.amazon.redshift</groupId>
            <artifactId>redshift-jdbc42</artifactId>
            <version>${redshift.version}</version>
        </dependency>

        <dependency>
            <groupId>com.maxmind.geoip2</groupId>
            <artifactId>geoip2</artifactId>
            <version>2.5.0</version>
        </dependency>

        <dependency>
            <groupId>com.datadoghq</groupId>
            <artifactId>java-dogstatsd-client</artifactId>
            <version>2.5</version>
        </dependency>

<!-- https://mvnrepository.com/artifact/net.snowflake/spark-snowflake -->
         <dependency>
              <groupId>net.snowflake</groupId>
               <artifactId>spark-snowflake_2.11</artifactId>
               <version>2.4.11-spark_2.4</version>
        </dependency>
    </dependencies>

    <build>
        <sourceDirectory>${basedir}/src/main/scala</sourceDirectory>
        <testSourceDirectory>${basedir}/src/test/scala</testSourceDirectory>

            <plugins>
                <plugin>
                    <groupId>org.scalatest</groupId>
                    <artifactId>scalatest-maven-plugin</artifactId>
                    <version>${scalatest.plugin.version}</version>
                    <executions>
                        <execution>
                            <id>test</id>
                            <phase>test</phase>
                            <goals>
                                <goal>test</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>com.google.code.sbt-compiler-maven-plugin</groupId>
                    <artifactId>sbt-compiler-maven-plugin</artifactId>
                    <version>${sbt-compiler.plugin.version}</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>compile</goal>
                                <goal>testCompile</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.scoverage</groupId>
                    <artifactId>scoverage-maven-plugin</artifactId>
                    <version>${scoverage.plugin.version}</version>
                    <configuration>
                        <highlighting>true</highlighting>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.1.0</version>
                    <executions>
                        <execution>
                            <id>dev-resources</id>
                            <phase>package</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.build.directory}/${project.artifactId}-v${project.version}/dev</outputDirectory>
                                <resources>
                                    <!-- Placeholders that are found from 
                                        the files located in the configured resource directories are replaced with 
                                        the property values found from the profile specific configuration file. -->
                                    <resource>
                                        <filtering>true</filtering>
                                        <directory>${project.basedir}/deploy</directory>
                                        <includes>
                                            <include>**/*.py</include>
                                            <include>s3/reference.conf</include>
                                            <include>**/*.sh</include>
                                        </includes>
                                    </resource>
                                    <resource>
                                        <filtering>false</filtering>
                                        <directory>${project.basedir}/deploy</directory>
                                        <includes>
                                            <include>s3/application.conf</include>
                                        </includes>
                                    </resource>
                                </resources>
                                <filters>
                                    <!-- Ensures that the config.properties 
                                        file is always loaded from the configuration directory of the active Maven 
                                        profile. -->
                                    <filter>${project.basedir}/deploy/dev.config.properties</filter>
                                </filters>
                            </configuration>
                        </execution>
                        <execution>
                            <id>prod-resources</id>
                            <phase>package</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.build.directory}/${project.artifactId}-v${project.version}/prod</outputDirectory>
                                <resources>
                                    <resource>
                                        <filtering>true</filtering>
                                        <directory>${project.basedir}/deploy</directory>
                                        <includes>
                                            <include>**/*.py</include>
                                            <include>s3/reference.conf</include>
                                            <include>**/*.sh</include>
                                        </includes>
                                    </resource>
                                    <resource>
                                        <filtering>false</filtering>
                                        <directory>${project.basedir}/deploy</directory>
                                        <includes>
                                            <include>s3/application.conf</include>
                                        </includes>
                                    </resource>
                                </resources>
                                <filters>
                                    <filter>${project.basedir}/deploy/prod.config.properties</filter>
                                </filters>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.1.0</version>
                    <executions>
                        <execution>
                            <id>make-assembly</id>
                            <phase>package</phase>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <configuration>
                                <finalName>${assembly.final.name}</finalName>
                                <appendAssemblyId>false</appendAssemblyId>
                                <descriptorRefs>
                                    <descriptorRef>jar-with-dependencies</descriptorRef>
                                </descriptorRefs>
                            </configuration>
                        </execution>
                        <execution>
                            <id>make-tar</id>
                            <phase>package</phase>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <configuration>
                                <finalName>${project.artifactId}-v${project.version}</finalName>
                                <tarLongFileMode>posix</tarLongFileMode>
                                <appendAssemblyId>false</appendAssemblyId>
                                <descriptors>
                                    <descriptor>deploy/assembly.xml</descriptor>
                                </descriptors>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>1.5</version>
                    <executions>
                        <execution>
                            <phase>initialize</phase>
                            <id>parse-version</id>
                            <goals>
                                <goal>parse-version</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

            </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>${project-info-reports.plugin.version}</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>index</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>

            <plugin>
                <groupId>org.scoverage</groupId>
                <artifactId>scoverage-maven-plugin</artifactId>
                <version>${scoverage.plugin.version}</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>report</report> <!-- select only one report from: report, integration-report and report-only 
                                reporters -->
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
        </plugins>
    </reporting>
</project>
