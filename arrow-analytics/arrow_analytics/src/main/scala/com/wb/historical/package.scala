package com.wb

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions.seqAsJavaList
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.spark.sql.functions.col
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.types.StructType

import scala.collection.Map

package object historical {

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def createEmptyDataframe(spark: SparkSession): DataFrame = {
    val emptySchema = StructType(Seq())
    spark.createDataFrame(spark.sparkContext.emptyRDD[Row], emptySchema)
  }

  def writeToTarget(dataFrameToSave: DataFrame, numberOfOutputFiles: Int, outputDir: String): DataFrame = {
    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      dataFrameToSave.repartition(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      dataFrameToSave.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }

    dataFrameToSave

  }

  def getDate(dateStr: String): Date = {
    val date = new java.sql.Date(
      DateTimeFormat
        .forPattern("yyyyMMdd")
        .withZoneUTC()
        .parseDateTime(dateStr)
        .withTime(0, 0, 0, 0)
        .getMillis
    )

    date
  }

  def getDateStr(runDate: Date, dateFormat: String): String = {
    val dirDateFormat = new SimpleDateFormat(dateFormat)
    dirDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

    if (runDate.getTime == 0) {
      dirDateFormat.format(Calendar.getInstance().getTime)
    } else {
      dirDateFormat.format(runDate)
    }
  }

  def getDirectoryDate(runDate: Date): String = {
    val dirDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    dirDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

    if (runDate.getTime == 0) {
      dirDateFormat.format(Calendar.getInstance().getTime)
    } else {
      dirDateFormat.format(runDate)
    }
  }

  def filterGreaterThanDate(df: DataFrame, runDate: Date, dateField: String): DataFrame = {
    if (runDate.getTime > 0) {
      df.filter(col(dateField).cast("date").gt(runDate))
    } else {
      df
    }
  }

  def subtractDays(date: Date, days: Int): Date = {
    if (date.getTime > 0) {
      val c = Calendar.getInstance
      c setTime date
      c.add(Calendar.DATE, -days)
      new Date(c.getTimeInMillis())
    } else {
      date
    }
  }

  def addDays(date: Date, days: Int): Date = {
    if (date.getTime > 0) {
      val c = Calendar.getInstance
      c setTime date
      c.add(Calendar.DATE, days)
      new Date(c.getTimeInMillis())
    } else {
      date
    }
  }

  def writeToTargetCSV(dataFrameToSave: DataFrame, outputFile: String): DataFrame = {
    dataFrameToSave.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").mode(SaveMode.Overwrite).save(outputFile)

    dataFrameToSave

  }

  //This function is to rebuild the phints map from string for blukai logs
  //The string pattern in this example such as: WBUID`b61bd1ef-d026-462f-a286-add98ab3f181~ Seat`US~ TagType`ResponsiveWeb~
  def convertToMap(inputStr: String): collection.immutable.Map[String, String] = {

    val result = inputStr.split("~ ").map(_.split("`")).map(arr => arr(0) -> arr(1)).toMap

    //result foreach {case (key, value) => println (key + "-->" + value)}

    result

  }

  def filterOnRundate(df: DataFrame, runDate: Date, dateField: String): DataFrame = {
    if (runDate.getTime > 0) {
      df.filter(col(dateField).cast("date") === runDate)
    } else {
      df
    }
  }

  def mapToArrowFields(dataFrame: DataFrame, fieldMapping: Map[String, String]): DataFrame = {
    val columns = dataFrame.columns
    var retFrame = dataFrame
    for (column <- fieldMapping.keySet) {
      if (columns.contains(column)) {
        retFrame = retFrame.withColumnRenamed(column, fieldMapping(column))
      }
    }

    retFrame
  }

  def addReleaseWindow(sparkSession: SparkSession, df: DataFrame, transactionField: String): DataFrame = {
    val getReleaseWindowDaysUDF = sparkSession.udf.register("getReleaseWindowDays", Attributes.getReleaseWindowDays _)
    val getReleaseWindowUDF = sparkSession.udf.register("getReleaseWindow", Attributes.getReleaseWindow _)

    df.withColumn(Constants.RELEASE_WINDOW_DAYS_KEY, getReleaseWindowDaysUDF(col(transactionField).cast("timestamp").cast("long") * 1000, col("theater_release_date")))
      .withColumn(Constants.RELEASE_WINDOW_KEY, getReleaseWindowUDF(col(Constants.RELEASE_WINDOW_DAYS_KEY)))
  }

  def columnsRemoveFromDataFrame(df: DataFrame, columnlist: List[String]): DataFrame = {
    val df_remove = columnlist.foldLeft(df)((tempDF, colName) => tempDF.drop(colName))

    df_remove

  }

  def columnAddToDataFrame(df: DataFrame, fieldlist: List[(String, org.apache.spark.sql.Column)]): DataFrame = {
    val df_add = fieldlist.foldLeft(df) { (tempDF, colName) => tempDF.withColumn(colName._1, colName._2)}

    df_add

  }

  def replaceStrFromMap(source: String, replaces: Map[String, String]): String = {
    replaces.foldLeft (source) {case (acc,(key,value))=>acc.replaceAll(key, value)}

  }

  def validateDate(value: String): java.sql.Date = {
    try {
      new java.sql.Date(
      DateTimeFormat
        .forPattern("yyyyMMdd")
        .withZoneUTC()
        .parseDateTime(value)
        .withTime(0, 0, 0, 0)
        .getMillis
      )
    } catch {
      case ex: java.lang.IllegalArgumentException => {
        new java.sql.Date(0)
      }
    }

  }

  def checkPathExist(aPath: String): Boolean = {
    val path = new Path(aPath)

    val conf = new Configuration()
    val fileSystem = FileSystem.get(path.toUri, conf)

    val exists = fileSystem.exists(path)

    fileSystem.close

    exists
  }

  def capitalizeFirstLetters(words: String): String = {
    words.toLowerCase.split(' ').map(_.capitalize).mkString(" ")

  }

}