package com.wb.historical

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws
import org.apache.spark.sql.functions.date_format
import org.apache.spark.sql.functions.first
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.sha2
import org.apache.spark.sql.functions.sum
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.functions.when
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.config.Util
import com.wb.constants.Constants
import com.wb.constants.Phints
import com.wb.enrich.Attributes
import com.wb.maxmind.GeoEnricher
import com.wb.maxmind.MaxmindFileServer
import com.wb.maxmind.FandangoGeoEnricher
import com.wb.monitoring.Statsd

/**
 * Created by stephanieleighphifer on 1/25/17.
 */
object FandangoPurchase {

  val COMP_COLS: Array[String] = Array[String](
    "fandango_id", "fandango_user_id", "client_ip", "tickets", "sales_amount", "transaction_date",
    "transaction_guid", "transaction_tp", "performance_dm", "director", "title", "theater_release_date", "flixster_id",
    "rating", "release_year", "imdb_id", "catman_franchise", "catman_primary_brand", "uid", "siterollup", "sitename",
    "website_section", "event_type", "product_type", "product_title", "movie_title", "aid_type", "aid",
    "is_cookie_statistical", "tld_country_code", "tld", "release_window_days", "release_window",
    "mm_subdivision_name", "mm_subdivision_iso_code",
    "mm_country_code", "mm_country_name", "mm_city", "mm_zipcode", "mm_continent", "mm_geocode",
    "dma_code", "dma_name")

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)

    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val baseline = CommandLineArguments.baseline.toBoolean

    val configs = ConfigFactory.load()

    val outputBase = configs.getString("hdfs.arrow.historical.purchase.movie")
    val suffix = rundate.isEmpty() match {
      case false => s"/date=${rundate.substring(0, 4)}-${rundate.substring(4, 6)}-${rundate.substring(6, 8)}"
      case _ => ""
    }
    val outputDir = s"${outputBase}${suffix}"

    val version = configs.getString("arrow.version")
    val fdTransLoc = configs.getString(Constants.FD_TBLTRANSACTION_LOC)
    val fdTransItemLoc = configs.getString(Constants.FD_TBLTRANSACTION_ITEM_LOC)
    val fdTransComLoc = configs.getString(Constants.FD_TBLTRANSACTIONCOMMITTED_LOC)
    val fdTransPerfLoc = configs.getString(Constants.FD_TBLTRANSACTION_PERFORMANCEATTRIBUTE_LOC)
    val idMappingLoc = configs.getString("hdfs.arrow.idmapping")
    val idListLoc = configs.getString("hdfs.arrow.idlist")
    val testEmailLoc = configs.getString(Constants.FD_TEST_EMAIL_LKP_LOC)
    val flxfanLoc = configs.getString("hdfs.arrow.meta.flxfan")
    val ipblacklistLoc = configs.getString("hdfs.arrow.ipblacklist")

    val statsd: StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port"))

    val conf = new org.apache.spark.SparkConf()

    val mmdbFilename = if (!baseline) {
      val geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate(rundate)
      conf.set("spark.files", geoCityLocFile)
      geoCityLocFile.substring(geoCityLocFile.lastIndexOf("/") + 1)
    } else {
      null
    }

    val spark = SparkSession.
      builder().
      config(conf).
      appName("FandangoPurchase").
      getOrCreate()

    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis.toString

    var op = Map[String, String]()
    op += "loadtime" -> now
    op += "version" -> version
    op += "updatedtime" -> now

    def getOpFcn: Map[String, String] = {
      op
    }

    //Add UDFs
    val getOp = spark.udf.register("getOp", getOpFcn _)
    val sha1 = spark.udf.register("sha1", Attributes.sha1Fcn _)
    val getValidIP = spark.udf.register("getValidIP", Attributes.getValidIPFcn _)
    val getValidTime = spark.udf.register("getValidTime", Attributes.getValidTimeFromStringDefaultFcn _)
    val resolveSource = spark.udf.register("resolveSource", Attributes.resolveSourceFcn _)
    val getHash = spark.udf.register("getHash", Attributes.getHashFcn _)
    val getIsStatisticalIDUDF = spark.udf.register("getIsStatisticalID", Attributes.getIsStatisticalID _)
    val getReleaseWindowDaysUDF = spark.udf.register("getReleaseWindowDays", Attributes.getReleaseWindowDays _)
    val getReleaseWindowUDF = spark.udf.register("getReleaseWindow", Attributes.getReleaseWindow _)
    val normalizeGenresUDF = spark.udf.register("normalizeGenres", Attributes.normalizeGenres _)
    val fileDateUDF = spark.udf.register("fileDate", Util.getFileDate _)
    val flattenNestedTypeUDF = spark.udf.register("flattenNestedType", Util.flattenNestedType _)

    //Load Data
    val trans = spark.read.parquet(s"${fdTransLoc}${suffix}")
    val transItem = spark.read.parquet(fdTransItemLoc)
    val transCom = spark.read.parquet(fdTransComLoc)
    val transPerf = spark.read.parquet(fdTransPerfLoc)
    val ipblacklist = spark.read.parquet(ipblacklistLoc)

    val testEmails: List[String] = spark.read.parquet(testEmailLoc).select("email_nm").rdd.map(x => x.getAs[String](0)).collect.toList

    val flxfan_metadata = spark.read.parquet(flxfanLoc).repartition(1)
    val flxfan_cols = flxfan_metadata.columns.filter(!_.equalsIgnoreCase("fandango_id")).map(x => first(x, true).alias(x))
    val fandango = flxfan_metadata.
      groupBy("fandango_id").
      agg(flxfan_cols.head, flxfan_cols.tail: _*).
      cache

    val idMapFull = spark.read.parquet(idListLoc).distinct

    val transactions = trans.
      join(transCom, Seq("transaction_guid"), "inner").
      join(transItem, Seq("transaction_guid"), "left")

    val perf = transactions.join(transPerf, Seq("transaction_guid"), "left").
      withColumnRenamed("movie_detail_id", "fandango_id")

    val totalEventsCount = perf.count()

    val purchases = perf.select(
      trans("customer_guid").alias(Constants.FANDANGO_USER_ID),
      getValidIP(trans("useripaddress_cd")).alias(Constants.CLIENT_IP_KEY),
      transItem("item_qy").alias("tickets"),
      transItem("item_at").alias("sales_amount"),
      transCom("create_dm").alias(Constants.TRANSACTION_DATE_KEY),
      col("transaction_guid"),
      transItem("transaction_tp"),
      perf("fandango_id"),
      perf("performance_dm"),
      trans(Constants.CIDW_LOAD_DTTM),
      trans("email_nm")).
      filter(!col("email_nm").isin(testEmails: _*)).
      distinct

    val removeTestEmailsCount = purchases.count()

    val purchaseSelect = purchases.groupBy("transaction_guid").agg(
      first(Constants.FANDANGO_USER_ID).alias(Constants.FANDANGO_USER_ID),
      first(Constants.CLIENT_IP_KEY).alias(Constants.CLIENT_IP_KEY),
      sum("tickets").alias("tickets"),
      sum("sales_amount").cast("String").alias("sales_amount"),
      first(Constants.TRANSACTION_DATE_KEY).alias(Constants.TRANSACTION_DATE_KEY),
      first("transaction_tp").alias("transaction_tp"),
      first("fandango_id").alias("fandango_id"),
      first("performance_dm").alias("performance_dm"),
      first(Constants.CIDW_LOAD_DTTM).alias(Constants.CIDW_LOAD_DTTM),
      first("email_nm").alias("email_nm"))

    val rolledUpCount = purchaseSelect.count()

    val filtered = purchaseSelect.filter(
      (col("transaction_tp") =!= "Refund") &&
        (col("transaction_tp") =!= "GiftCertificate Purchase") &&
        (col("transaction_guid").isNotNull)).
      withColumn(Constants.FANDANGO_USER_ID_SHA256, sha1(col(Constants.FANDANGO_USER_ID)))

    val refundFilteredCount = filtered.count()

    val allHistoric = if (baseline) {
      val withDate = filtered.withColumn(Constants.RECORD_DATE, date_format(col(Constants.TRANSACTION_DATE_KEY), "yyyyMMdd"))
      val ips = withDate.select(col(Constants.RECORD_DATE), col(Constants.CLIENT_IP_KEY)).filter(col(Constants.CLIENT_IP_KEY).isNotNull &&
        col(Constants.RECORD_DATE).isNotNull).distinct()

      val ipWithLoc = FandangoGeoEnricher.enrich(spark, ips, configs)

      val withLoc = withDate.join(ipWithLoc, Seq(Constants.RECORD_DATE, Constants.CLIENT_IP_KEY), "left_outer").drop(Constants.RECORD_DATE)

      withLoc.join(idMapFull, Seq(Constants.FANDANGO_USER_ID_SHA256), "left_outer").distinct.cache
    } else {
      val start = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyyMMdd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(0, 0, 0, 0).getMillis)
      val end = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyyMMdd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(23, 59, 59, 0).getMillis)

      val newRS = filtered.filter(col(Constants.CIDW_LOAD_DTTM) <= end).filter(col(Constants.CIDW_LOAD_DTTM) >= start)

      val ips = newRS.select(Constants.CLIENT_IP_KEY).distinct()

      val ipWithLoc = FandangoGeoEnricher.enrich(spark, ips, 0, mmdbFilename, configs)

      val withLoc = newRS.join(ipWithLoc, Seq(Constants.CLIENT_IP_KEY), "left_outer")

      val idMap = idMapFull.filter(col("last_seen") <= end)

      withLoc.join(idMap, Seq(Constants.FANDANGO_USER_ID_SHA256), "left_outer").distinct.cache
    }

    val withinTimeRangeCount = allHistoric.count()

    val emailFilter = allHistoric.withColumn("temp", resolveSource(col(Constants.EMAIL_ADDRESS_KEY), col("email_nm"))).withColumn(
      s"${Constants.MAID_KEY}_stitch",
      when(col(Constants.MAID_KEY).isNotNull, lit(true)).otherwise(lit(false))).withColumn(
        s"${Constants.EMAIL_ADDRESS_KEY}_stitch",
        when(col("temp").isNotNull && !col("temp").eqNullSafe(col("email_nm")), lit(true)).otherwise(lit(false))).withColumn(
          s"${Constants.UID_KEY}_stitch",
          when(col(Constants.UID_KEY).isNotNull, lit(true)).otherwise(lit(false))).drop("email_nm", Constants.EMAIL_ADDRESS_KEY).
          withColumnRenamed("temp", Constants.EMAIL_ADDRESS_KEY)
      .withColumn(Constants.EMAIL_ADDRESS_KEY, lower(trim(col(Constants.EMAIL_ADDRESS_KEY))))

    val withID = emailFilter.
      join(ipblacklist, Seq(Constants.CLIENT_IP_KEY), "left_outer").
      withColumn(Constants.OP_KEY, getOp()).
      withColumn(Constants.SITEROLLUP_KEY, lit(Constants.FANDANGO_SITE)).
      withColumn(Constants.SITENAME_KEY, lit(Constants.FANDANGO_SITE + " Transactional")).
      withColumn(Constants.WEBSITE_SECTION_KEY, lit(Constants.websiteRollups.find(_._2.contains("purchase")).get._1)).
      withColumn(Constants.EVENT_TYPE_KEY, lit("Movie Ticket Purchase")).
      withColumn(Constants.PRODUCT_TYPE_KEY, lit(Constants.MOVIE)).
      withColumn(Constants.AID_TYPE_KEY, lit(Phints.AID_BK_VAL)).
      withColumn(Constants.AID_KEY, col("uid")).
      withColumn(Constants.IS_COOKIE_STATISTICAL, getIsStatisticalIDUDF(col("uid"))).
      withColumn(Constants.TLD_COUNTRY_CODE, lit("US")).
      withColumn(Constants.TLD, lit("com")).
      withColumn(
        Constants.TIME_STATS_KEY,
        when(
          col("last_seen").isNotNull,
          getValidTime(col("last_seen"))).when(
            col("last_seen").isNull,
            getValidTime(col(Constants.TRANSACTION_DATE_KEY)))).drop(Constants.CIDW_LOAD_DTTM, "first_seen", "last_seen").
        withColumn(
          Constants.VALID_EMAIL_ADDRESS_KEY,
          when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull &&
            col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@.+") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@(fandango|warnerbros|wb|dcentertainment|timewarner|flixster|_flixster)\\.com$") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc\\.+") &&
            !col(Constants.EMAIL_ADDRESS_KEY).isin(testEmails: _*), lit(true)).
            otherwise(false)).
          withColumn(
            Constants.VALID_MAID_KEY,
            when(col(Constants.MAID_KEY).isNotNull, lit(true)).
              otherwise(lit(false))).
            withColumn(
              Constants.VALID_IP_KEY,
              when(col(Constants.MM_COUNTRY_CODE_KEY).eqNullSafe("US") && col(Constants.INVALID_CRITERIA_KEY).isNull && col(Constants.VALID_EMAIL_ADDRESS_KEY), lit(true)).
                otherwise(lit(false))).
              withColumn(
                Constants.NSR_READY_KEY,
                when(col(Constants.VALID_EMAIL_ADDRESS_KEY) || col(Constants.VALID_MAID_KEY) || col(Constants.VALID_IP_KEY), lit(true)).
                  otherwise(lit(false))).
                withColumn(Constants.ARROW_INCLUSION_KEY, when(col(Constants.NSR_READY_KEY) || col(Constants.UID_KEY).isNotNull, lit(true)).otherwise(lit(false))).
                drop(Constants.INVALID_CRITERIA_KEY).
                withColumn(
                  Constants.WBCID_KEY,
                  when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull, sha2(sha2(col(Constants.EMAIL_ADDRESS_KEY), 384), 256)).
                    when(col(Constants.MAID_KEY).isNotNull, sha2(sha2(trim(col(Constants.MAID_KEY)), 384), 256)).
                    when(col(Constants.UID_KEY).isNotNull, sha2(sha2(trim(col(Constants.UID_KEY)), 384), 256)))

    val inclusionCount = withID.filter(col(Constants.ARROW_INCLUSION_KEY) === true).count()

    var withFF = withID.
      join(broadcast(fandango), Seq("fandango_id"), "left_outer").
      withColumn(Constants.PRODUCT_TITLE_KEY, resolveSource(col("title"), col("title"))).
      withColumn(Constants.MOVIE_TITLE_KEY, resolveSource(col("title"), col("title"))).
      withColumn(Constants.MOVIE_GENRES_KEY, normalizeGenresUDF(col("genres"))).
      withColumn(Constants.RELEASE_WINDOW_DAYS_KEY, getReleaseWindowDaysUDF((col(Constants.TRANSACTION_DATE_KEY).cast("long") * 1000), col("theater_release_date")))

    withFF = withFF.
      withColumn(Constants.RELEASE_WINDOW_KEY, getReleaseWindowUDF(col(Constants.RELEASE_WINDOW_DAYS_KEY)))

    var enrichedMovie = withFF.
      withColumn(Constants.COMPOSITE_KEY, getHash(concat_ws("\t", COMP_COLS.map(c => withFF(c).cast("String")): _*)))

    //Write Data to HDFS
    if (numberOfOutputFiles > 0) {
      enrichedMovie.
        repartition(numberOfOutputFiles).
        write.
        parquet(outputDir)
    } else {
      enrichedMovie.
        write.
        parquet(outputDir)
    }

    statsd.gauge("FandangoPurchase.PurchaseInput", totalEventsCount)
    statsd.gauge("FandangoPurchase.ValidEmails", removeTestEmailsCount)
    statsd.gauge("FandangoPurchase.RolledUp", rolledUpCount)
    statsd.gauge("FandangoPurchase.RefundsFiltered", refundFilteredCount)
    statsd.gauge("FandangoPurchase.WithinTimeRange", withinTimeRangeCount)
    statsd.gauge("FandangoPurchase.ArrowInclusion", inclusionCount)
  }
}
