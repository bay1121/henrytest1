package com.wb.historical.moviesanywhere.schema

import MAConstants.DataFields
import com.wb.constants.Constants
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object EidrDataSource extends DataSource {
  override val rawSchema: StructType = StructType(
    Array(
      StructField(DataFields.EIDR_ID, StringType),
      StructField("eidr_structural_type", StringType),
      StructField(DataFields.EIDR_TITLE_DESCRIPTION, StringType),
      StructField(DataFields.EIDR_GENRE, StringType),
      StructField("dw_source_filename", StringType),
      StructField("dw_load_ts", TimestampType),
      StructField("dw_update_ts", TimestampType),
      StructField("dw_data_dt", DateType),
      StructField("dw_etlload_id", IntegerType)
    )
  )

  override val schema = StructType(
    Array(
      StructField(DataFields.EIDR_ID, StringType),
      StructField(DataFields.EIDR_TITLE_DESCRIPTION, StringType),
      StructField(DataFields.EIDR_GENRE, StringType)

    )
  )

  override val arrowMapping = Map[String, String](
    DataFields.EIDR_TITLE_DESCRIPTION -> Constants.MOVIE_TITLE_KEY,
    DataFields.EIDR_GENRE -> Constants.MOVIE_GENRES_KEY
  )
}
