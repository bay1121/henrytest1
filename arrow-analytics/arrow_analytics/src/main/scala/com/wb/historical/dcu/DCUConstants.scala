package com.wb.historical.dcu

import com.wb.constants.Constants
import org.apache.spark.sql.functions.lit

object DCUConstants {

  //Keep these fields for raw schema, and rename them later
  object DataFields {
    val CONTEXT_APP_NAME = "CONTEXT_APP_NAME"
    val CONTEXT_DEVICE_MANUFACTURER = "CONTEXT_DEVICE_MANUFACTURER"
    val CONTEXT_DEVICE_MODEL = "CONTEXT_DEVICE_MODEL"
    val CONTEXT_DEVICE_NAME = "CONTEXT_DEVICE_NAME"
    val CONTEXT_DEVICE_TYPE = "CONTEXT_DEVICE_TYPE"
    val CONTEXT_IP = "CONTEXT_IP"
    val CONTEXT_LOCALE = "CONTEXT_LOCALE"
    val CONTEXT_NETWORK_CARRIER = "CONTEXT_NETWORK_CARRIER"
    val CONTEXT_OS_NAME = "CONTEXT_OS_NAME"
    val CONTEXT_TIMEZONE = "CONTEXT_TIMEZONE"
    val CONTEXT_USER_AGENT = "CONTEXT_USER_AGENT"
    val CONTEXT_PAGE_TITLE = "CONTEXT_PAGE_TITLE"
    val CONTEXT_PAGE_URL = "CONTEXT_PAGE_URL"
    val PLATFORM_NAME = "PLATFORM_NAME"
    val DIVISION = "DIVISION"
    val EVENT_NAME = "EVENT_NAME"
    val DW_LOAD_DT = "DW_LOAD_DT"
    val COLLECTION_NAME = "COLLECTION_NAME"
    val COLLECTION_TYPE = "COLLECTION_TYPE"
    val DCU_ASSET_NAME = "DCU_ASSET_NAME"
    val DCU_ASSET_TYPE = "DCU_ASSET_TYPE"
    val DCU_PARENT_ASSET_NAME = "DCU_PARENT_ASSET_NAME"
    val DCU_PARENT_ASSET_TYPE = "DCU_PARENT_ASSET_TYPE"
    val LOCATION = "LOCATION"
    val CTA_TEXT = "CTA_TEXT"
    val CAMPAIGN_NAME = "CAMPAIGN_NAME"
    val VIDEO_POSITION = "VIDEO_POSITION"
    val VIDEO_TOTAL_LENGTH = "VIDEO_TOTAL_LENGTH"
    val VIDEO_LANGUAGE = "VIDEO_LANGUAGE"
    val SEARCH_LIST_ITEM_TEXT = "SEARCH_LIST_ITEM_TEXT"
    val PRODUCT_REVIEW_BODY = "PRODUCT_REVIEW_BODY"
    val COMMENT_ID = "COMMENT_ID"
    val IS_DOWNLOADED = "IS_DOWNLOADED"
    val USER_ID = "USER_ID"
    val SEGMENT_TIMESTAMP = "SEGMENT_TIMESTAMP"

  }

  val colsToRename = Map[String, String](
    DataFields.CONTEXT_APP_NAME -> "app_name",
    DataFields.CONTEXT_DEVICE_MANUFACTURER -> "device_manufacture",
    DataFields.CONTEXT_DEVICE_MODEL -> "device_model",
    DataFields.CONTEXT_DEVICE_NAME -> "device_name",
    DataFields.CONTEXT_DEVICE_TYPE -> "device_type",
    DataFields.CONTEXT_LOCALE -> "locale",
    DataFields.CONTEXT_TIMEZONE -> "timezone",
    DataFields.CONTEXT_USER_AGENT -> "user_agent",
    DataFields.CONTEXT_PAGE_URL -> "page_url",
    DataFields.COLLECTION_NAME -> "collection_name",
    DataFields.COLLECTION_TYPE -> "collection_type",
    DataFields.LOCATION -> "location",
    DataFields.CTA_TEXT -> "cta_text",
    DataFields.CAMPAIGN_NAME -> "campaign",
    DataFields.VIDEO_POSITION -> "video_position",
    DataFields.VIDEO_TOTAL_LENGTH -> "video_total_length",
    DataFields.VIDEO_LANGUAGE -> "video_language",
    DataFields.SEARCH_LIST_ITEM_TEXT -> "search_list_item_text",
    DataFields.PRODUCT_REVIEW_BODY -> "product_review",
    DataFields.COMMENT_ID -> "comment_id",
    DataFields.USER_ID -> "user_id",
    DataFields.CONTEXT_DEVICE_TYPE -> "device_type",
    DataFields.CONTEXT_IP -> "client_ip",
    DataFields.DIVISION -> "wb_division",
    DataFields.PLATFORM_NAME -> "platform",
    DataFields.CONTEXT_OS_NAME -> "os_name",
    DataFields.CONTEXT_NETWORK_CARRIER -> "carrier",
    DataFields.DW_LOAD_DT -> "load_time",
    DataFields.DCU_ASSET_TYPE -> "dcu_asset_type",
    DataFields.DCU_ASSET_NAME -> "dcu_asset_name",
    DataFields.DCU_PARENT_ASSET_NAME -> "dcu_parent_asset_name",
    DataFields.DCU_PARENT_ASSET_TYPE -> "dcu_parent_asset_type",
    DataFields.CONTEXT_PAGE_TITLE -> "page_title",
    DataFields.EVENT_NAME -> "event_name"

  )

  val colsToRemove = List(
    "CONTEXT_DEVICE_ID", "CONTEXT_SCREEN_DENSITY", "ERROR_CODE", "ERROR_NAME", "ORIGINAL_TIMESTAMP", "SENT_AT", "CONTEXT_PAGE_PATH", "CONTEXT_PAGE_REFERRER",
    "COLLECTION_ID", "COLLECTION_ITEM_TYPE", "DCU_ASSET_ID", "DCU_PARENT_ASSET_ID", "SEARCH_QUERY",
    "TAB_NAME", "PAGE_SCREEN_NAME", "APP_CRASH_MESSAGE", "AUTO_PLAY", "COMICS_IS_OWNED", "COMICS_IS_USER_PREMIUM", "VIDEO_OFFLINE", "VIDEO_AUTOROLL",
    "VIDEO_CC", "VIDEO_SESSION_ID", "SLUG", "FLAG", "LANDSCAPE", "LOOP", "PANEL_VIEW", "PANEL_BY_PANEL", "SEARCH_LIST_ITEM_TYPE", "CHECKOUT_STEP", "COUPON_ID",
    "COUPON_DENIED_REASON", "LINK_TEXT", "PRODUCT_LIST_FILTERS", "IS_DOWNLOADED", "SEASON", "SEASON_INDEX", "SEARCH_INDEX", "SEARCH_LIST_ITEM_INDEX",
    "TIME_SPENT_IN_PLAYER", "PANEL_ANIMATION_DURATION", "CAROUSEL_INDEX", "COMICS_PAGE_INDEX", "COMICS_PANEL_INDEX", "TAB_INDEX", "COLLECTION_ITEM_COUNT",
    "DW_ETLLOAD_ID", "CONTEXT_SCREEN_DENSITY", "CONTEXT_DEVICE_AD_TRACKING_ENABLED", "CONTEXT_APP_BUILD", "CONTEXT_APP_NAMESPACE", "CONTEXT_APP_VERSION",
    "CONTEXT_DEVICE_ADVERTISING_ID", "CONTEXT_LIBRARY_NAME", "CONTEXT_LIBRARY_VERSION", "CONTEXT_NETWORK_BLUETOOTH", "CONTEXT_NETWORK_CELLULAR",
    "CONTEXT_NETWORK_WIFI", "CONTEXT_SCREEN_HEIGHT", "CONTEXT_SCREEN_WIDTH", "CONTEXT_TRAITS_ANONYMOUS_ID", "CONTEXT_TRAITS_APP_LANGUAGE",
    "CONTEXT_TRAITS_USER_ID", "CONTEXT_PAGE_SEARCH", "CONTEXT_DEVICE_VERSION", "CONTEXT_SCREEN_MODE", "CONTEXT_SCREEN_RATIO", "CONTEXT_SCREEN_TYPE",
    "DURATION_BETWEEN_PANEL_TRANSACTIONS", "EVENT", "EVENT_TEXT", "ID", "UUID", "UUID_TS", "IS_FREE_WITH_PREMIUM", "CHROMECAST", "AUDIO", "HEADPHONE",
    "VOLUME", "PRODUCT_LIST_NAME", "ANONYMOUS_ID", "CONTEXT_OS_VERSION", "product_revenue", "product_value", "product_price", "product_quantity","FILTER","SORT","CATEGORY",
    "event_date","idfa","adid","user_name","order_date","product_category","product_name","watched_user_guid","watched_date","episode_id","series_id",
    "dcu_parent_asset_name", "dcu_parent_asset_type","view_date","total_episode","total_season","series_publish_date","series_date_added","episode_duration", "first_name","last_name")

  val colsToAdd = List(
    //Static values, pre-defined.
    ("sitename", lit("DCU").as("StringType")),
    ("siterollup", lit("DCU").as("StringType")),
    ("is_cookie_statistical", lit("false").as("StringType"))

  )

  //Hash values of these columns for dcuevent records, to generate unique compositekey field (except segment_timestamp)
  val COMP_COLS: Array[String] = Array[String](
    "app_name","device_manufacture","device_model","device_name","device_type","locale","carrier","os_name","timezone","user_agent","user_id",
    "page_title","page_url","platform","wb_division","event_name","collection_name","collection_type","dcu_asset_name","dcu_asset_type","location",
    "cta_text","campaign","video_position","video_total_length","client_ip","video_language","search_list_item_text","product_title", "product_review",
    "comment_id","transaction_date")

}
