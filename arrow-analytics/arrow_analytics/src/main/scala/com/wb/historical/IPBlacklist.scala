package com.wb.historical

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._

object IPBlacklist {

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val daysAgo = CommandLineArguments.daysAgo.toInt

    val spark = SparkSession.
      builder().
      appName("IPBlacklist").
      getOrCreate()

    val configs = ConfigFactory.load()
    
    val outputDir = configs.getString("hdfs.arrow.ipblacklist")
        
    val fnCustLoc = configs.getString(Constants.FD_TBLCUSTOMER_LOC)
    val flxCustLoc = configs.getString(Constants.FX_USER_EMAIL_LOC)
    val testEmailLoc = configs.getString(Constants.FD_TEST_EMAIL_LKP_LOC)
    val frequencyLoc = configs.getString("hdfs.arrow.frequency")

    val getIsStatisticalIDUDF = spark.udf.register("getIsStatisticalID", Attributes.getIsStatisticalID _)
    val fileDateUDF = spark.udf.register("fileDate", Util.getFileDate _)
    val getFnUIDHash = spark.udf.register("sha1", Attributes.getFnUIDHashFcn _)

    //Load Data
    val testEmails : List[String]= spark.read.parquet(testEmailLoc).select("email_nm").rdd.map(x=>x.getAs[String](0)).collect.toList
    val flxFull = spark.read.parquet(s"${frequencyLoc}/*/flixster/*").
      withColumn(Constants.SEEN_KEY, fileDateUDF(input_file_name))
    val fnFull = spark.read.parquet(s"${frequencyLoc}/*/fandango/*").
      withColumn(Constants.SEEN_KEY, fileDateUDF(input_file_name))
    val maidFull = spark.read.parquet(s"${frequencyLoc}/*/maid/*").
      withColumn(Constants.SEEN_KEY, fileDateUDF(input_file_name))
    val ipFull = spark.read.parquet(s"${frequencyLoc}/*/ip/*").
      withColumn(Constants.SEEN_KEY, fileDateUDF(input_file_name))

    //Bad Flixster Email
    val emailAddressesFlx = spark.read.parquet(flxCustLoc).
      select(
        lower(col("email")).alias(Constants.EMAIL_ADDRESS_KEY),
        col("user_id").alias(Constants.FLIXSTER_USER_ID)
      ).
      filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull).
      filter(
        col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@(fandango|warnerbros|wb|dcentertainment|timewarner|flixster|_flixster)\\.com$") ||
          col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") ||
          col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc\\.+") ||
          col(Constants.EMAIL_ADDRESS_KEY).isin(testEmails:_*)
        ).
      distinct

    //Bad Fandango Email
    val emailAddressesFan = spark.read.parquet(fnCustLoc).
      select(
        lower(col("email_nm")).alias(Constants.EMAIL_ADDRESS_KEY),
        col("customer_guid").alias(Constants.FANDANGO_USER_ID)).
      filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull).
      filter(
        col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@(fandango|warnerbros|wb|dcentertainment|timewarner|flixster|_flixster)\\.com$") ||
          col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") ||
          col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc\\.+") ||
          col(Constants.EMAIL_ADDRESS_KEY).isin(testEmails:_*)
      ).
      distinct.
      withColumn(Constants.FANDANGO_USER_ID_SHA256, getFnUIDHash(col(Constants.FANDANGO_USER_ID)))

    //Join Emails to identify Good & Bad Accounts
    val flx = flxFull.
      select(
        col(Constants.FLIXSTER_USER_ID),
        explode(col(s"${Constants.CLIENT_IP_KEY}s")).alias(Constants.CLIENT_IP_KEY),
        col(Constants.SEEN_KEY)
      ).
      filter(col(Constants.CLIENT_IP_KEY).isNotNull).
      distinct.
      join(emailAddressesFlx,Seq(Constants.FLIXSTER_USER_ID),"left_outer")

    //Join Emails to identify Good & Bad Accounts
    val fn = fnFull.
      select(
        col(Constants.FANDANGO_USER_ID_SHA256),
        explode(col(s"${Constants.CLIENT_IP_KEY}s")).alias(Constants.CLIENT_IP_KEY),
        col(Constants.SEEN_KEY)
      ).
      filter(col(Constants.CLIENT_IP_KEY).isNotNull).
      distinct.
      join(emailAddressesFan,Seq(Constants.FANDANGO_USER_ID_SHA256),"left_outer")

    //Calculate IPs with over 2000 events on average per day
    val ipEvents = ipFull.select(Constants.CLIENT_IP_KEY,Constants.EVENT_COUNT_KEY).
      groupBy(Constants.CLIENT_IP_KEY).
      agg(
        min(Constants.EVENT_COUNT_KEY).alias("min"),
        max(Constants.EVENT_COUNT_KEY).alias("max"),
        avg(Constants.EVENT_COUNT_KEY).alias("avg")
      )

    val badCount = ipEvents.filter(col("avg") > 2000).select(Constants.CLIENT_IP_KEY).withColumn("reason",lit("High Event Count"))

    //Calculate IPs with over 5 related authenticated accounts per day
    val authenticatedAccts = fn.filter(col(Constants.EMAIL_ADDRESS_KEY).isNull).select(Constants.CLIENT_IP_KEY,Constants.SEEN_KEY).
      union(flx.filter(col(Constants.EMAIL_ADDRESS_KEY).isNull).select(Constants.CLIENT_IP_KEY,Constants.SEEN_KEY)).
      groupBy(Constants.CLIENT_IP_KEY,Constants.SEEN_KEY).
      count.
      groupBy(Constants.CLIENT_IP_KEY).
      agg(
        min(col("count")).alias("min"),
        max(col("count")).alias("max"),
        avg(col("count")).alias("avg")
      )

    val badAuth = authenticatedAccts.filter(col("avg") > 5).select(Constants.CLIENT_IP_KEY).withColumn("reason",lit("High Account Count"))

    val blacklist = badAuth.
      union(badCount).
      groupBy(Constants.CLIENT_IP_KEY).agg(collect_set("reason").alias(Constants.INVALID_CRITERIA_KEY))

    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      blacklist.repartition(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      blacklist.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }
  }
}


