package com.wb.historical.moviesanywhere

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.{ Calendar, TimeZone }

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import com.wb.historical.moviesanywhere.schema.MAConstants.DataFields
import com.wb.historical.moviesanywhere.schema._
import com.wb.model.DMADataSource
import com.wb.monitoring.Statsd
import com.wb.support.{ CSVReader, ParquetReader }
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{ DataFrame, SparkSession }
import org.kohsuke.args4j.{ CmdLineException, CmdLineParser }
import org.apache.spark.sql.expressions.UserDefinedFunction

import scala.collection.JavaConversions._
import scala.util.Try

object MoviesAnywhere {
  val log: Logger = Logger.getLogger(getClass)

  def parseArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(MACLIArgs)

    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        throw new RuntimeException("Failed to parse arguments")
    }
  }

  def getDirectoryDate(runDate: Date): String = {
    val dirDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    dirDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

    if (runDate.getTime == 0) {
      dirDateFormat.format(Calendar.getInstance().getTime)
    } else {
      dirDateFormat.format(runDate)
    }
  }

  def filterGreaterThanDate(df: DataFrame, runDate: Date, dateField: String): DataFrame = {
    if (runDate.getTime > 0) {
      df.filter(col(dateField).cast("date").gt(runDate))
    } else {
      df
    }
  }

  def subtractDays(date: Date, days: Int): Date = {
    if (date.getTime > 0) {
      val c = Calendar.getInstance
      c setTime date
      c.add(Calendar.DATE, -days)
      new Date(c.getTimeInMillis())
    } else {
      date
    }
  }

  def filterOnRundate(df: DataFrame, runDate: Date, dateField: String): DataFrame = {
    if (runDate.getTime > 0) {
      df.filter(col(dateField).cast("date") === runDate)
    } else {
      df
    }
  }

  def addCompositeKey(getHash: UserDefinedFunction, dataFrame: DataFrame, column: String): DataFrame = {
    dataFrame.withColumn(Constants.COMPOSITE_KEY, getHash(col(column)))
  }

  def mapToArrowFields(dataFrame: DataFrame, fieldMapping: Map[String, String]): DataFrame = {
    val columns = dataFrame.columns
    var retFrame = dataFrame
    for (column <- fieldMapping.keySet) {
      if (columns.contains(column)) {
        retFrame = retFrame.withColumnRenamed(column, fieldMapping(column))
      }
    }

    retFrame
  }

  def filterAndDropOnCurrent(df: DataFrame): DataFrame = {
    df.filter(s"${DataFields.DW_CURRENT_FLAG} = 'Y'").drop(DataFields.DW_CURRENT_FLAG)
  }

  def addReleaseWindow(sparkSession: SparkSession, df: DataFrame, transactionField: String): DataFrame = {
    val getReleaseWindowDaysUDF = sparkSession.udf.register("getReleaseWindowDays", Attributes.getReleaseWindowDays _)
    val getReleaseWindowUDF = sparkSession.udf.register("getReleaseWindow", Attributes.getReleaseWindow _)

    df.withColumn(Constants.RELEASE_WINDOW_DAYS_KEY, getReleaseWindowDaysUDF(col(transactionField).cast("timestamp").cast("long") * 1000, col("theater_release_date")))
      .withColumn(Constants.RELEASE_WINDOW_KEY, getReleaseWindowUDF(col(Constants.RELEASE_WINDOW_DAYS_KEY)))
  }

  private def filterAndJoinUsers(df: DataFrame, users: DataFrame): DataFrame = {
    df.join(users, Seq(MAConstants.DataFields.MA_USER_ID), "left_outer")
      .filter(s"${Constants.EMAIL_ADDRESS_KEY} != '' and ${Constants.EMAIL_ADDRESS_KEY} is not null")
  }

  private def addMovieMetadata(df: DataFrame, flxFanMetadata: DataFrame, eidrGenre: DataFrame): DataFrame = {
    val groupedGenres: DataFrame = eidrGenre
      .groupBy(MAConstants.DataFields.EIDR_ID, Constants.MOVIE_TITLE_KEY)
      .agg(collect_list(Constants.MOVIE_GENRES_KEY).alias(Constants.MOVIE_GENRES_KEY))

    val addedGenres = df.join(
      groupedGenres,
      df.col(MAConstants.DataFields.TITLE_EIDR_ID) === eidrGenre.col(MAConstants.DataFields.EIDR_ID),
      "left_outer").drop(eidrGenre.col(MAConstants.DataFields.EIDR_ID))

    // Create a list of column functions to pull the first record during the aggregation
    // We assume all records in flxfanmetadata with the same flixster id will have the same metadata
    val flixsterColumns = flxFanMetadata.columns.filter(_ != "flixster_id").map(x => first(x, ignoreNulls = true).alias(x))

    val firstOnly = flxFanMetadata
      .groupBy("flixster_id")
      .agg(flixsterColumns.head, flixsterColumns.tail: _*)

    val addedMetadata = addedGenres.join(broadcast(firstOnly), Seq("flixster_id"), "left_outer")

    // Replace movie_title and movie_genres with flixster data if possible, else leave MA data
    // Either way drop the flixster specific cols
    addedMetadata
      .withColumn(
        Constants.MOVIE_TITLE_KEY,
        when(col("flixsterTitle").isNotNull, col("flixsterTitle"))
          .otherwise(col(Constants.MOVIE_TITLE_KEY)))
      .drop("flixsterTitle")
      .withColumn(
        Constants.MOVIE_GENRES_KEY,
        when(col("flixsterGenres").isNotNull, col("flixsterGenres"))
          .otherwise(addedGenres(Constants.MOVIE_GENRES_KEY)))
      .drop("flixsterGenres")
  }

  private def convertPercentToInt(field: String, df: DataFrame): DataFrame = {
    df.withColumn(field, (col(field) * 100).cast("int"))
  }

  private def mapAndJoinLockerPercentages(df: DataFrame, lockerPercentageDF: DataFrame, nameField: String, mapField: String): DataFrame = {
    val filterCurrent = lockerPercentageDF.filter(s"${DataFields.DW_CURRENT_FLAG}= 'Y'")
    val percentAsInts = convertPercentToInt(DataFields.LOCKER_PCT, filterCurrent)
    mapAndJoinByName(nameField, DataFields.LOCKER_PCT, mapField, df, percentAsInts)
  }

  val joinMap = udf { values: Seq[Seq[String]] => values.map(x => (x(0), x(1))).toMap }

  private def mapAndJoinByName(nameField: String, valueField: String, mapField: String, df: DataFrame, toJoin: DataFrame): DataFrame = {
    val sanitizedToJoin = sanitizeField(nameField, toJoin)
    var ret = sanitizedToJoin.select(col(DataFields.MA_USER_ID), array(col(nameField), col(valueField)).alias(mapField))
    ret = ret.groupBy(DataFields.MA_USER_ID).agg((joinMap(collect_list(col(mapField))).alias(mapField)))
    df.join(ret, Seq(DataFields.MA_USER_ID), "left_outer")
  }

  private def generateWBCID(df: DataFrame): DataFrame = {
    df.withColumn(Constants.WBCID_KEY, sha2(sha2(lower(trim(col(Constants.EMAIL_ADDRESS_KEY))), 384), 256))
  }

  private def getDMACode(df: DataFrame, dmaDf: DataFrame): DataFrame = {
    val dmaRenamed = dmaDf.withColumnRenamed(Constants.MM_SOURCE_ZIPCODE, Constants.MM_ZIPCODE_KEY)
    df.join(dmaRenamed, Seq(Constants.MM_ZIPCODE_KEY), "left_outer")
  }

  private def addSiteRollup(df: DataFrame): DataFrame = {
    df.withColumn(Constants.SITEROLLUP_KEY, lit(Constants.MOVIESANYWHERE_SITE))
  }

  private def initcapField(column: String, df: DataFrame): DataFrame = {
    df.withColumn(column, initcap(col(column)))
  }

  private def sanitizeField(column: String, df: DataFrame): DataFrame = {
    if (Try(df(column)).isSuccess) {
      df.withColumn(column, initcap(regexp_replace(col(column), "[\\+,_]", " ")))
    } else {
      df
    }
  }

  private def joinFlixsterId(df: DataFrame, eidrFlixsterMapping: DataFrame): DataFrame = {
    df.join(eidrFlixsterMapping, df.col(MAConstants.DataFields.TITLE_EIDR_ID) === eidrFlixsterMapping.col("eidr_id"), "left_outer")
      .drop(eidrFlixsterMapping.col("eidr_id"))
  }

  // Movies Anywhere business logic
  def processMoviesAnywhereData(
    dataToBeProcessed: DataFrame,
    users: DataFrame,
    eidrGenre: DataFrame,
    dma: DataFrame,
    userGenre: DataFrame,
    userContent: DataFrame,
    userReleaseStatus: DataFrame,
    flxFanMetadata: DataFrame,
    eidrFlixsterMapping: DataFrame,
    statsd: StatsDClient): DataFrame = {
    val dateFilteredInputCount = dataToBeProcessed.count()

    var result: DataFrame = filterAndJoinUsers(dataToBeProcessed, users)
    val usersJoinedCount = result.count()
    result = joinFlixsterId(result, eidrFlixsterMapping)
    result = addMovieMetadata(result, flxFanMetadata, eidrGenre)
    result = getDMACode(result, dma)
    result = mapAndJoinLockerPercentages(result, userGenre, DataFields.GENRE_NAME, Constants.LIBRARY_GENRE_COMPOSITION)
    result = mapAndJoinLockerPercentages(result, userContent, DataFields.CONTENT_SOURCE_NAME, Constants.LIBRARY_CONTENT_COMPOSITION)
    result = mapAndJoinLockerPercentages(result, userReleaseStatus, DataFields.RELEASE_STATUS_NAME, Constants.LIBRARY_RELEASE_COMPOSITION)
    result = generateWBCID(result)
    result = addSiteRollup(result)
    result = initcapField(Constants.CONTENT_TYPE, result)
    result = sanitizeField(Constants.EVENT_TYPE_KEY, result)
    result = sanitizeField(Constants.DEVICE_TYPE_KEY, result)

    val outputCount = result.count()
    statsd.gauge(s"MoviesAnywhere.DateFilteredInput", dateFilteredInputCount)
    statsd.gauge(s"MoviesAnywhere.UserJoined", usersJoinedCount)
    statsd.gauge(s"MoviesAnywhere.Output", outputCount)

    result
  }

  def main(args: Array[String]): Unit = {
    parseArgs(args)

    val spark = SparkSession
      .builder()
      .appName("MoviesAnywhere")
      .getOrCreate()
    val configs = ConfigFactory.load()
    val consumptionLoc = configs.getString("hdfs.sf.ma_consumption")
    val dmaLoc = configs.getString("hdfs.static.dma_zipcodes")
    val eidrGenreLoc = configs.getString("hdfs.sf.ma_eidr_genre")
    val transactionsLoc = configs.getString("hdfs.sf.ma_transaction")
    val usersLoc = configs.getString("hdfs.sf.ma_user")
    val userGenreLoc = configs.getString("hdfs.sf.ma_user_genre")
    val userContentSourceLoc = configs.getString("hdfs.sf.ma_user_content_source")
    val userReleastStatusLoc = configs.getString("hdfs.sf.ma_user_release_status")
    val eidrFlixsterMappingLoc = configs.getString(Constants.EIDR_FLX_MAPPING_LOC)
    val flixsterMetadataLoc = configs.getString("hdfs.arrow.meta.flxfan")

    val getHash = spark.udf.register("getHash", Attributes.getHashFcn _)

    val statsdPrefix = configs.getString("statsd.prefix")
    val statsdHost = configs.getString("statsd.host")
    val statsdPort = configs.getInt("statsd.port")

    val transactionsStatsd: StatsDClient = Statsd.getClient(s"$statsdPrefix.Transactions", statsdHost, statsdPort)
    val consumptionStatsd: StatsDClient = Statsd.getClient(s"$statsdPrefix.Consumption", statsdHost, statsdPort)

    val dirDateString = getDirectoryDate(MACLIArgs.runDate)

    val parquetReader = new ParquetReader(spark)
    val csvReader = new CSVReader(spark, hasHeader = true)

    val users = mapToArrowFields(parquetReader.read(usersLoc, Some(UsersDataSource.schema)), UsersDataSource.arrowMapping)
    val currentUsers = filterAndDropOnCurrent(users)

    val eidrGenre = mapToArrowFields(parquetReader.read(eidrGenreLoc, Some(EidrDataSource.schema)), EidrDataSource.arrowMapping)
    val dma = csvReader.read(dmaLoc, Some(DMADataSource.schema))

    val userGenre = parquetReader.read(userGenreLoc, Some(UserGenreDataSource.schema))
    val currentUserGenre = filterAndDropOnCurrent(userGenre)

    val userContentSource = parquetReader.read(userContentSourceLoc, Some(UserContentDataSource.schema))
    val currentUserContent = filterAndDropOnCurrent(userContentSource)

    val userReleaseStatus = parquetReader.read(userReleastStatusLoc, Some(UserReleaseStatusDataSource.schema))
    val currentUserRelease = filterAndDropOnCurrent(userReleaseStatus)

    val eidrFlixsterMapping = mapToArrowFields(parquetReader.read(eidrFlixsterMappingLoc, Some(EidrFlixsterMapping.schema)), EidrFlixsterMapping.arrowMapping)

    // TODO: This massaging of the FlxFanMetadata analytic output should just be done up front in that analytic
    val flixsterMetaData = mapToArrowFields(parquetReader.read(flixsterMetadataLoc, Some(FlxFanMetadataSource.schema)), FlxFanMetadataSource.arrowMapping)
    val normalizeGenresUDF = spark.udf.register("normalizeGenres", Attributes.normalizeGenres _)
    val improvedFlixsterMetadata = flixsterMetaData.withColumn("flixsterGenres", normalizeGenresUDF(col("flixsterGenres")))

    if (!MACLIArgs.skipTransactions) {
      log.info("Running transactions...")
      val transactions = mapToArrowFields(parquetReader.read(transactionsLoc, Some(TransactionsDataSource.schema)), TransactionsDataSource.arrowMapping)
      val currentTransactions = filterAndDropOnCurrent(transactions)

      // DW loads the entire set of the transactions every day, so we cannot rely on dw_load_ts, we retain that filter because there is a very small subset of data
      // that has older dw_load_ts. To avoid processing older transactions everyday we filter transactions that were modified in last 10 days  The modifed date comes from
      // the source (movies_anywhere)
      val dateFilteredTransactions = filterGreaterThanDate(
        filterOnRundate(currentTransactions, MACLIArgs.runDate, "dw_load_ts"),
        subtractDays(MACLIArgs.runDate, configs.getInt(Constants.MA_TRANSACTION_LOOKBACK_DAYS)),
        DataFields.LAST_MODIFIED_DT)

      // composite key is the elastic search "_id" field, calculating it here will squash any new records coming in from joins with other tables.
      // leaving the logic as is because the joins are not supposed to blow up record counts.
      // generate composite key on the unique key field from the dataframe instead of all fields in dataframe
      val transactionsWithCompositeKey = addCompositeKey(getHash, dateFilteredTransactions, DataFields.TRANSACTION_ID)
      val transactionsResult = processMoviesAnywhereData(
        dataToBeProcessed = transactionsWithCompositeKey,
        users = currentUsers,
        eidrGenre = eidrGenre,
        dma = dma,
        userGenre = currentUserGenre,
        userContent = currentUserContent,
        userReleaseStatus = currentUserRelease,
        flxFanMetadata = improvedFlixsterMetadata,
        eidrFlixsterMapping = eidrFlixsterMapping,
        statsd = transactionsStatsd)
      val finalResult = addReleaseWindow(spark, transactionsResult, Constants.TRANSACTION_DATE_KEY)
      finalResult.write.parquet(s"${configs.getString("hdfs.arrow.historical.ma.transactions")}/date=$dirDateString")
      log.info("Finished writing transactions records")
    }

    if (!MACLIArgs.skipConsumption) {
      log.info("Running consumption...")
      val consumption = mapToArrowFields(parquetReader.read(consumptionLoc, Some(ConsumptionDataSource.schema)), ConsumptionDataSource.arrowMapping)
      val withContentViewAsPercent = convertPercentToInt(Constants.CONTENT_VIEWED_PERCENT, consumption)
      val dateFilteredConsumption = filterOnRundate(withContentViewAsPercent, MACLIArgs.runDate, "dw_load_ts") //Constants.CONTENT_VIEWED_DATE)
      val consumptionWithCompositeKey = addCompositeKey(getHash, dateFilteredConsumption, DataFields.VIEWING_ID)
      val consumptionResult = processMoviesAnywhereData(
        dataToBeProcessed = consumptionWithCompositeKey,
        users = currentUsers,
        eidrGenre = eidrGenre,
        dma = dma,
        userGenre = currentUserGenre,
        userContent = currentUserContent,
        userReleaseStatus = currentUserRelease,
        flxFanMetadata = improvedFlixsterMetadata,
        eidrFlixsterMapping = eidrFlixsterMapping,
        statsd = consumptionStatsd)
      val finalResult = addReleaseWindow(spark, consumptionResult, Constants.CONTENT_VIEWED_DATE)
      finalResult.write.parquet(s"${configs.getString("hdfs.arrow.historical.ma.consumption")}/date=$dirDateString")
      log.info("Finished writing consumption records")
    }
  }
}
