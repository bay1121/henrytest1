package com.wb.historical.moviesanywhere.schema

import MAConstants.DataFields
import com.wb.constants.Constants
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object TransactionsDataSource extends DataSource {

  override val rawSchema: StructType = StructType(
    Array(
      StructField("ma_user_key", LongType),
      StructField("ma_user_profile_key", LongType),
      StructField(DataFields.MA_USER_ID, StringType),
      StructField("profile_id", StringType),
      StructField(DataFields.TRANSACTION_ID, StringType),
      StructField(DataFields.TRANSACTION_TYPE, StringType),
      StructField("internal_transaction_flag", StringType),
      StructField(DataFields.CONTENT_TYPE, StringType),
      StructField("title_description", StringType),
      StructField(DataFields.TITLE_EIDR_ID, StringType),
      StructField("edit_eidr_id", StringType),
      StructField(DataFields.DEVICE_ADVERTISING_ID, StringType),
      StructField(DataFields.DEVICE_TYPE, StringType),
      StructField(DataFields.DEVICE_MANUFACTURER, StringType),
      StructField(DataFields.DEVICE_MODEL, StringType),
      StructField("device_version", StringType),
      StructField(DataFields.DEVICE_OS, StringType),
      StructField("device_os_version", StringType),
      StructField("client_type", StringType),
      StructField("transaction_dma_cd", IntegerType),
      StructField(DataFields.TRANSACTION_COUNTRY_CD, StringType),
      StructField("transaction_country_name", StringType),
      StructField("transaction_region", StringType),
      StructField("transaction_city", StringType),
      StructField(DataFields.TRANSACTION_POSTAL_CD, StringType),
      StructField("referring_domain", StringType),
      StructField("marketing_url", StringType),
      StructField(DataFields.RETAILER_TRANSACTION_DT, DateType),
      StructField(DataFields.LAST_MODIFIED_DT, DateType),
      StructField("dw_source_filename", StringType),
      StructField("dw_load_ts", TimestampType),
      StructField("dw_update_ts", TimestampType),
      StructField("dw_data_dt", DateType),
      StructField("dw_etlload_id", IntegerType),
      StructField("dw_row_hash", StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField("dw_row_start_dt", DateType),
      StructField("dw_row_end_dt", DateType)))

  override val schema = StructType(
    Array(
      StructField(DataFields.MA_USER_ID, StringType),
      StructField(DataFields.TITLE_EIDR_ID, StringType),
      StructField(DataFields.TRANSACTION_ID, StringType),
      StructField(DataFields.TRANSACTION_TYPE, StringType),
      StructField(DataFields.CONTENT_TYPE, StringType),
      StructField(DataFields.RETAILER_TRANSACTION_DT, DateType),
      StructField(DataFields.LAST_MODIFIED_DT, DateType),
      StructField(DataFields.DEVICE_ADVERTISING_ID, StringType),
      StructField(DataFields.DEVICE_TYPE, StringType),
      StructField(DataFields.DEVICE_MANUFACTURER, StringType),
      StructField(DataFields.DEVICE_MODEL, StringType),
      StructField(DataFields.DEVICE_OS, StringType),
      StructField(DataFields.TRANSACTION_COUNTRY_CD, StringType),
      StructField(DataFields.TRANSACTION_POSTAL_CD, StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField(DataFields.LOAD_DT, TimestampType)))

  override val arrowMapping = Map[String, String](
    DataFields.DEVICE_ADVERTISING_ID -> Constants.MAID_KEY,
    DataFields.DEVICE_TYPE -> Constants.DEVICE_TYPE_KEY,
    DataFields.TRANSACTION_TYPE -> Constants.EVENT_TYPE_KEY,
    DataFields.CONTENT_TYPE -> Constants.CONTENT_TYPE,
    DataFields.RETAILER_TRANSACTION_DT -> Constants.TRANSACTION_DATE_KEY,
    DataFields.TRANSACTION_COUNTRY_CD -> Constants.MM_COUNTRY_CODE_KEY,
    DataFields.TRANSACTION_POSTAL_CD -> Constants.MM_ZIPCODE_KEY)
}
