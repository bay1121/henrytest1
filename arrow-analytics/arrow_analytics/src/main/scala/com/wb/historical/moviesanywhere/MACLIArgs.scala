package com.wb.historical.moviesanywhere

import java.sql.Date

import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.Option

object MACLIArgs {

  var runDate:Date = new Date(0)
  @Option(name = "-rundate", required = false, usage = "Specific day to process as YYYYMMdd")
  def validateDate(value: String): Unit = {
    val date = new java.sql.Date(
      DateTimeFormat
        .forPattern("yyyyMMdd")
        .withZoneUTC()
        .parseDateTime(value)
        .withTime(0, 0, 0, 0)
        .getMillis
    )

    runDate = date
  }

  @Option(name = "-skipTransactions", required = false, usage = "Option to skip the read, process, and write of transactions data", forbids = Array("-skipConsumption"))
  var skipTransactions: Boolean = false

  @Option(name = "-skipConsumption", required = false, usage = "Option to skip the read, process, and write of consumption data", forbids = Array("-skipTransactions"))
  var skipConsumption: Boolean = false
}
