package com.wb.historical

import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.joda.time.{DateTime, DateTimeZone}
import com.typesafe.config.ConfigFactory
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants
import com.wb.enrich.Attributes

object AtomticketsAction {

  val COMP_COLS: Array[String] = Array[String](Constants.TRANSACTION_DATE_KEY, Constants.MAID_KEY_HASH, Constants.EVENT_TYPE_KEY, "movie_title", "production_id")

  val conf = new org.apache.spark.SparkConf()

  val spark = SparkSession.
    builder().
    config(conf).
    appName("Atom Tickets Actions").
    getOrCreate()

  val configs = ConfigFactory.load()

  val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis.toString

  val getValidTime = spark.udf.register("getValidTime", Attributes.getValidTimeFromStringFcn _)

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)

    processAtomtickets()

  }

  def processAtomtickets(): DataFrame = {
    //main flow logics, to glue the functions together

    //Set up variables
    val rundate = CommandLineArguments.rundate
    val run_date = validateDate(rundate)

    val dirDateString = getDirectoryDate(run_date)

    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val numberOfPartitions = CommandLineArguments.numberOfPartitions.toInt

    val outputBase = configs.getString("hdfs.arrow.historical.atomtickets.action")
    val suffix = s"/date=${dirDateString}"

    val outputDir = s"${outputBase}${suffix}"

    //load atomtickets for all
    val atomtickets_path = configs.getString("hdfs.sf.atom_device_action")

    //filter the data base on rundate if provided, else return all
    val atomtickets = loadAtomTickets(atomtickets_path, run_date)

    //generate actions
    val atomtickets_actions = generateActions(atomtickets)

    //add composite key
    val atomtickets_add_key = hashCompositeKeytoEvents(atomtickets_actions)

    //Write to files
    writeToTarget(atomtickets_add_key, numberOfOutputFiles, outputDir)

    atomtickets_add_key

  }

  def loadAtomTickets(atomtickets_path: String, runDate: java.sql.Date): DataFrame = {
    val atomtickets = spark.read.parquet(atomtickets_path)

    val atomticketsFiltered = filterGreaterThanDate(
      atomtickets, subtractDays(runDate, configs.getInt(Constants.ATOM_TICKETS_LOOKBACK_DAYS)), "dw_data_dt")

    atomticketsFiltered

  }

  def generateActions(atomtickets: DataFrame): DataFrame = {
    val getCaplitalized = spark.udf.register("getCaplitalized", capitalizeFirstLetters _)

    val atomticketsSelect = atomtickets
      .groupBy("dw_data_dt", "device_id", "device_action_type", "movie_title", "atom_production_id", "dw_load_ts").count
      .withColumn(Constants.TRANSACTION_DATE_KEY, date_format(col("dw_data_dt"), "yyyy-MM-dd"))
      .withColumn("tickets", when(col("device_action_type").equalTo("PURCHASE"), col("count")))
      .withColumn("engages", when(col("device_action_type").equalTo("ENGAGEMENT"), col("count")))
      .withColumn(Constants.TIME_STATS_KEY, getValidTime(col(Constants.TRANSACTION_DATE_KEY), lit("yyyy-MM-dd")))
      .withColumn(Constants.WBCID_KEY, sha2(sha2(trim(col("device_id")), 384), 256))
      .select(
        col(Constants.TRANSACTION_DATE_KEY), col("device_id").alias(Constants.MAID_KEY_HASH), getCaplitalized(col("device_action_type")).alias(Constants.EVENT_TYPE_KEY),
        col("movie_title"), col("atom_production_id").alias("production_id"), col("tickets"), col("engages"), col(Constants.TIME_STATS_KEY),
        col(Constants.WBCID_KEY), col("dw_load_ts").alias("load_time")
      )

    atomticketsSelect
      .withColumn("siterollup", lit("Atom Tickets"))
      .withColumn("sitename", lit("Atom Tickets"))
  }

  def hashCompositeKeytoEvents(atomtickets: DataFrame): DataFrame = {
    val getHash = spark.udf.register("getHash", Attributes.getHashFcn _)

    atomtickets
      .withColumn(Constants.COMPOSITE_KEY, getHash(concat_ws("\t", COMP_COLS.map(c => atomtickets(c).cast("String")): _*)))

  }

}
