package com.wb.historical

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws
import org.apache.spark.sql.functions.first
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.sha2
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.functions.when
import org.apache.spark.sql.types.ArrayType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.config.Util
import com.wb.constants.Constants
import com.wb.constants.Phints
import com.wb.enrich.Attributes
import com.wb.model.SchemaBuilder
import com.wb.monitoring.Statsd

/**
 * Created by stephanieleighphifer on 2/21/17.
 */

object Rating {
  val unk: String = null

  val COMP_COLS_TV: Array[String] = Array[String](
    "flixster_user_id", "flixster_id", "product_type", "title_name", "transaction_date", "consumer_rating", "event_type",
    "country", "platform", "production_studio", "release_date", "dvd_release_date",
    "uid", "siterollup", "sitename", "website_section", "aid_type", "aid", "is_cookie_statistical", "tld_country_code",
    "tld", "tv_episode_id", "tv_series_title", "series_title", "product_title")

  val COMP_COLS_MOVIE: Array[String] = Array[String](
    "flixster_id", "flixster_user_id", "product_type", "title_name", "transaction_date", "consumer_rating", "event_type",
    "country", "platform", "production_studio", "release_date", "dvd_release_date", "tomatometer", "box_office",
    "uid", "siterollup", "sitename", "website_section", "aid_type", "aid", "is_cookie_statistical", "tld_country_code",
    "tld", "director", "title", "theater_release_date", "fandango_id", "rating", "release_year", "imdb_id",
    "catman_franchise", "catman_primary_brand", "movie_title", "product_title", "release_window_days", "release_window")

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)

    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val startDate = CommandLineArguments.startDate
    val baseline = CommandLineArguments.baseline.toBoolean
    val suffix = rundate.isEmpty() match {
      case false => s"/date=${rundate.substring(0, 4)}-${rundate.substring(4, 6)}-${rundate.substring(6, 8)}"
      case _ => ""
    }

    val configs = ConfigFactory.load()

    val spark = SparkSession.
      builder().
      appName("Rating").
      getOrCreate()

    val version = configs.getString("arrow.version")
    val dimConsumerLoc = configs.getString(Constants.DIM_CONSUMER_LOC)
    val dimTitleLoc = configs.getString(Constants.DIM_TITLE_LOC)
    val factEventLoc = configs.getString(Constants.FACT_EVENT_LOC)
    val idMappingLoc = configs.getString("hdfs.arrow.idmapping")
    val idListLoc = configs.getString("hdfs.arrow.idlist")
    val metaTVSeriesLoc = configs.getString("hdfs.arrow.meta.tvseries")
    val flxfanLoc = configs.getString("hdfs.arrow.meta.flxfan")
    val userEmailLoc = configs.getString("hdfs.arrow.user_email")

    val statsd: StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port"))

    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis.toString
    var op = Map[String, String]()
    op += "loadtime" -> now
    op += "version" -> version
    op += "updatedtime" -> now

    def getOpFcn: Map[String, String] = {
      op
    }

    //Add UDFs
    val getOp = spark.udf.register("getOp", getOpFcn _)
    val getValidTime = spark.udf.register("getValidTime", Attributes.getValidTimeFromStringDefaultFcn _)
    val resolveProductType = spark.udf.register("resolveProductType", Attributes.resolveProductTypeFcn _)
    val getGenres = spark.udf.register("getGenres", Attributes.getGenresFcn _)
    val resolveSource = spark.udf.register("resolveSource", Attributes.resolveSourceFcn _)
    val getHash = spark.udf.register("getHash", Attributes.getHashFcn _)
    val getIsStatisticalIDUDF = spark.udf.register("getIsStatisticalID", Attributes.getIsStatisticalID _)
    val getReleaseWindowDaysUDF = spark.udf.register("getReleaseWindowDays", Attributes.getReleaseWindowDays _)
    val getReleaseWindowUDF = spark.udf.register("getReleaseWindow", Attributes.getReleaseWindow _)
    val fileDateUDF = spark.udf.register("fileDate", Util.getFileDate _)
    val flattenNestedTypeUDF = spark.udf.register("flattenNestedType", Util.flattenNestedType _)

    //Load Data
    val dc_all = spark.read.parquet(dimConsumerLoc)
    val dt_all = spark.read.parquet(dimTitleLoc)
    val fe_all = spark.read.parquet(s"${factEventLoc}${suffix}")
    val fxUserEmail = spark.read.parquet(userEmailLoc)

    //Remove test data
    val dc = dc_all.filter(col("cidw_is_testdata") === false)
    val dt = dt_all.filter(col("cidw_is_testdata") === false)
    val fe = fe_all.filter(col("cidw_is_testdata") === false)

    val idMapFull = spark.read.parquet(idListLoc)

    val flxfan_metadata = spark.read.parquet(flxfanLoc).repartition(1)
    val flxfan_cols = flxfan_metadata.columns.filter(!_.equalsIgnoreCase("flixster_id")).map(x => first(x, true).alias(x))

    val flx = flxfan_metadata.
      groupBy("flixster_id").
      agg(flxfan_cols.head, flxfan_cols.tail: _*).
      cache

    // Build a schema based on the all cast fields in flxfan
    var modifiedTvSeriesSchema = SchemaBuilder.tvSeriesSchema

    for (column <- flxfan_metadata.columns) {
      if (column.contains("cast_all")) {
        modifiedTvSeriesSchema = modifiedTvSeriesSchema.add(StructField(column, ArrayType(StringType)))
      }
    }

    // Read in Tv metadata with the cast fields
    var tv_series = spark.read.schema(modifiedTvSeriesSchema).parquet(metaTVSeriesLoc).repartition(1)

    // Rename case fields to start with series
    for (column <- tv_series.columns) {
      if (column.contains("cast")) {
        tv_series = tv_series.withColumnRenamed(column, "series_" + column)
      }
    }

    tv_series = tv_series.withColumnRenamed("tv_talent", "series_talent")

    val events = fe.
      join(dt, Seq("title_id"), "inner").
      join(dc, Seq("consumer_id"), "inner")

    val totalEventsCount = events.count()

    val positiveRatings = List(
      "3",
      "3.5",
      "4",
      "4.5",
      "5",
      "Want to See")

    val ratings = events.filter(
      col("consumer_rating").isin(positiveRatings: _*))

    var ratingSelect = ratings.select(
      col(Constants.FLIXSTER_USER_ID),
      col("flixster_registered_email_address"),
      col("flixster_title_id").alias("flixster_id"),
      resolveProductType(col("flixster_title_type")).alias(Constants.PRODUCT_TYPE_KEY),
      col("title_name"),
      col("event_timestamp").alias(Constants.TRANSACTION_DATE_KEY),
      col("consumer_rating"),
      lit("Positively Rated").alias(Constants.EVENT_TYPE_KEY),
      col("event_country").alias("country"),
      col("platform"),
      col("production_studio"),
      col("release_date"),
      col("genre"),
      col("dvd_release_date"),
      fe("cidw_load_dttm")).filter(col(Constants.FLIXSTER_USER_ID).isNotNull).
      distinct

    val validFlixsterIDCount = ratingSelect.count()

    if (!startDate.isEmpty) {
      val start = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyyMMdd").withZone(DateTimeZone.UTC).parseDateTime(startDate).withTime(0, 0, 0, 0).getMillis)
      ratingSelect = ratingSelect.filter(col(Constants.TRANSACTION_DATE_KEY) >= start)
    }

    val relevantRatingsCount = ratingSelect.count()

    var allHistoric = spark.emptyDataFrame
    if (baseline) {
      allHistoric = ratingSelect.join(idMapFull, Seq(Constants.FLIXSTER_USER_ID), "left_outer").distinct
    } else {
      val start = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyyMMdd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(0, 0, 0, 0).getMillis)
      val end = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyyMMdd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(23, 59, 59, 0).getMillis)
      val idMap = idMapFull.filter(col("last_seen") <= end).
        withColumn("is_new", col("first_seen") === start)
      allHistoric = ratingSelect.join(idMap.drop("is_new"), Seq(Constants.FLIXSTER_USER_ID), "left_outer").distinct
    }

    val withinTimeRangeCount = allHistoric.count()

    val emailFilter = allHistoric.withColumn("temp", resolveSource(col(Constants.EMAIL_ADDRESS_KEY), col("flixster_registered_email_address"))).
      withColumn(
        s"${Constants.MAID_KEY}_stitch",
        when(col(Constants.MAID_KEY).isNotNull, lit(true)).otherwise(lit(false))).withColumn(
          s"${Constants.EMAIL_ADDRESS_KEY}_stitch",
          when(col("temp").isNotNull && !col("temp").eqNullSafe(col("flixster_registered_email_address")), lit(true)).otherwise(lit(false))).withColumn(
            s"${Constants.UID_KEY}_stitch",
            when(col(Constants.UID_KEY).isNotNull, lit(true)).otherwise(lit(false))).drop("flixster_registered_email_address", Constants.EMAIL_ADDRESS_KEY).
            withColumnRenamed("temp", Constants.EMAIL_ADDRESS_KEY)
      .withColumn(Constants.EMAIL_ADDRESS_KEY, lower(trim(col(Constants.EMAIL_ADDRESS_KEY))))

    val withID = emailFilter.
      withColumn(Constants.OP_KEY, getOp()).
      withColumn(Constants.SITEROLLUP_KEY, lit(Constants.FLIXSTER_SITE)).
      withColumn(Constants.SITENAME_KEY, lit(Constants.FLIXSTER_SITE + " Transactional")).
      withColumn(Constants.WEBSITE_SECTION_KEY, lit("Product Rating")).
      withColumn(Constants.AID_TYPE_KEY, lit(Phints.AID_BK_VAL)).
      withColumn(Constants.AID_KEY, col("uid")).
      withColumn(Constants.IS_COOKIE_STATISTICAL, getIsStatisticalIDUDF(col("uid"))).
      withColumn(Constants.TLD_COUNTRY_CODE, lit("US")).
      withColumn(Constants.TLD, lit("com")).
      withColumn(
        Constants.TIME_STATS_KEY,
        when(
          col("last_seen").isNotNull,
          getValidTime(col("last_seen"))).when(
            col("last_seen").isNull,
            getValidTime(col(Constants.TRANSACTION_DATE_KEY)))).drop("cidw_load_dttm", "first_seen", "last_seen").
        withColumn(
          Constants.VALID_EMAIL_ADDRESS_KEY,
          when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull &&
            col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@.+") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@(fandango|warnerbros|wb|dcentertainment|timewarner|flixster|_flixster)\\.com$") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") &&
            !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc\\.+"), lit(true)).
            otherwise(false)).
          withColumn(
            Constants.VALID_MAID_KEY,
            when(col(Constants.MAID_KEY).isNotNull, lit(true)).
              otherwise(lit(false))).
            withColumn(Constants.VALID_IP_KEY, lit(false)).
            withColumn(
              Constants.NSR_READY_KEY,
              when(col(Constants.VALID_EMAIL_ADDRESS_KEY) || col(Constants.VALID_MAID_KEY), lit(true)).
                otherwise(lit(false))).
              withColumn(Constants.ARROW_INCLUSION_KEY, when(col(Constants.NSR_READY_KEY) || col(Constants.UID_KEY).isNotNull, lit(true)).otherwise(lit(false))).
              withColumn(
                Constants.WBCID_KEY,
                when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull, sha2(sha2(col(Constants.EMAIL_ADDRESS_KEY), 384), 256)).
                  when(col(Constants.MAID_KEY).isNotNull, sha2(sha2(trim(col(Constants.MAID_KEY)), 384), 256)).
                  when(col(Constants.UID_KEY).isNotNull, sha2(sha2(trim(col(Constants.UID_KEY)), 384), 256)))

    val inclusionCount = withID.filter(col(Constants.ARROW_INCLUSION_KEY) === true).count()

    val withIDandCCD = withID.join(fxUserEmail, Seq(Constants.FLIXSTER_USER_ID), "left_outer")
    val withIDcCD = withIDandCCD.
      withColumn(
        Constants.MM_COUNTRY_CODE_KEY,
        when(col(Constants.MM_COUNTRY_CODE_KEY).isNull, col("country")).
          otherwise(col(Constants.MM_COUNTRY_CODE_KEY))).
        alias(Constants.MM_COUNTRY_CODE_KEY)

    val tv = withIDcCD.filter(col(Constants.PRODUCT_TYPE_KEY) === Constants.TV_SERIES)
    var enrichedTV = tv.join(tv_series, tv("flixster_id") === tv_series("tv_episode_id"), "left").
      withColumn(Constants.SERIES_GENRES_KEY, getGenres(col("genre"), col("tv_series_genres"))).
      withColumn(Constants.SERIES_TITLE_KEY, resolveSource(col("tv_series_title"), col("title_name"))).
      withColumn(Constants.PRODUCT_TITLE_KEY, resolveSource(col("title_name"), col("tv_series_title"))).
      drop("genre").drop(tv_series("tv_series_id"))

    val movie = withIDcCD.filter(col(Constants.PRODUCT_TYPE_KEY) === Constants.MOVIE)
    var enrichedMovie = movie.join(broadcast(flx), Seq("flixster_id"), "left").
      withColumn(Constants.MOVIE_GENRES_KEY, getGenres(col("genre"), col("genres"))).
      withColumn(Constants.MOVIE_TITLE_KEY, resolveSource(col("title_name"), col("title"))).
      withColumn(Constants.PRODUCT_TITLE_KEY, resolveSource(col("title_name"), col("title"))).
      withColumn(Constants.RELEASE_WINDOW_DAYS_KEY, getReleaseWindowDaysUDF((col(Constants.TRANSACTION_DATE_KEY).cast("long") * 1000), col("theater_release_date"))).
      drop("genre")

    enrichedMovie = enrichedMovie.
      withColumn(Constants.RELEASE_WINDOW_KEY, getReleaseWindowUDF(col(Constants.RELEASE_WINDOW_DAYS_KEY)))

    enrichedTV = enrichedTV.withColumn(Constants.COMPOSITE_KEY, getHash(concat_ws("\t", COMP_COLS_TV.map(c => enrichedTV(c).cast("String")): _*)))
    enrichedMovie = enrichedMovie.withColumn(Constants.COMPOSITE_KEY, getHash(concat_ws("\t", COMP_COLS_MOVIE.map(c => enrichedMovie(c).cast("String")): _*)))

    //Write Data to HDFS
    val outputBaseTv = configs.getString("hdfs.arrow.historical.rating.tv")
    val hdfsTV = s"${outputBaseTv}${suffix}"
    if (numberOfOutputFiles > 0) {
      enrichedTV.
        repartition(numberOfOutputFiles).
        write.
        parquet(hdfsTV)
    } else {
      enrichedTV.
        write.
        parquet(hdfsTV)
    }

    val outputBaseMovie = configs.getString("hdfs.arrow.historical.rating.movie")
    val hdfsMovie = s"$outputBaseMovie${suffix}"
    if (numberOfOutputFiles > 0) {
      enrichedMovie.
        repartition(numberOfOutputFiles).
        write.
        parquet(hdfsMovie)
    } else {
      enrichedMovie.
        write.
        parquet(hdfsMovie)
    }

    statsd.gauge("Rating.EventInput", totalEventsCount)
    statsd.gauge("Rating.ValidFlixsterID", validFlixsterIDCount)
    statsd.gauge("Rating.RelevantRatings", relevantRatingsCount)
    statsd.gauge("Rating.WithinTimeRange", withinTimeRangeCount)
    statsd.gauge("Rating.ArrowInclusion", inclusionCount)
  }
}
