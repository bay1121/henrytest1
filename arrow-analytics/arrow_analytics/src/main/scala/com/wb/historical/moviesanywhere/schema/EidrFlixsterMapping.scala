package com.wb.historical.moviesanywhere.schema

import com.wb.historical.moviesanywhere.schema.MAConstants.DataFields
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object EidrFlixsterMapping extends DataSource {
  // This table has 371 columns and I don't want to list them all here, so I just listed the 2 I use
  override val rawSchema: StructType = StructType(
    Array(
      StructField(DataFields.EIDR_ID, StringType),
      StructField("alternateid_flixster_com", StringType)
    )
  )

  override val schema = StructType(
    Array(
      StructField(DataFields.EIDR_ID, StringType),
      StructField("alternateid_flixster_com", StringType)
    )
  )

  override val arrowMapping:Map[String,String] = Map[String, String](
    "alternateid_flixster_com" -> "flixster_id"
  )
}
