package com.wb.historical.dcu

import scala.collection.JavaConversions.asScalaBuffer
import com.typesafe.config.ConfigFactory
import com.wb.config.CommandLineArguments
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.joda.time.{DateTime, DateTimeZone}
import com.wb.historical._

object BuildMaps {

  val spark = SparkSession.
    builder().
    appName("DCUEvents build id and cast maps").
    getOrCreate()

  val configs = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)

    processMaps()

  }

  def processMaps(): Unit = {

    //Set up variables
    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val numberOfPartitions = CommandLineArguments.numberOfPartitions.toInt

    //days is used to expire the id map records which is last seen days before TODAY
    var days_to_expire = CommandLineArguments.daysAgo.toInt

    //startDate is used to pull bluekai records from the start date
    var start_date = CommandLineArguments.startDate

    val outputBase = configs.getString("hdfs.arrow.historical.dcu.base")
    val bkLoc = configs.getString("hdfs.arrow.bluekai")

    val bk_site_id = configs.getStringList("dcu.site_id").toList

    val id_maps_path_base = s"$outputBase/maps/id"
    val cast_maps_path_base = s"$outputBase/maps/cast"

    var cur_rundate = if(start_date.isEmpty) rundate else start_date
    var prev_maps_rundate = getDateStr(subtractDays(getDate(cur_rundate), 1), "yyyyMMdd")

    while(cur_rundate <= rundate) {
      val cur_dateoutputStr = cur_rundate.substring(0,4)+"-"+cur_rundate.substring(4,6)+"-"+cur_rundate.substring(6,8)
      val prev_maps_dateoutputStr = prev_maps_rundate.substring(0,4)+"-"+prev_maps_rundate.substring(4,6)+"-"+prev_maps_rundate.substring(6,8)

      val prev_idDateLoc = s"$id_maps_path_base/date=$prev_maps_dateoutputStr"
      val prev_castDateLoc = s"$cast_maps_path_base/date=$prev_maps_dateoutputStr"

      val cur_idDateLoc = s"$id_maps_path_base/date=$cur_dateoutputStr"
      val cur_castDateLoc = s"$cast_maps_path_base/date=$cur_dateoutputStr"

      val idmapsFull = loadIDMapsAll(prev_idDateLoc, spark)
      val castmapsFull = loadCastMapsAll(prev_castDateLoc, spark)

      val cur_bkeDateLoc = s"$bkLoc/date=$cur_dateoutputStr"

      val blk = DCUEvents.loadBlukaiLogs(spark, cur_bkeDateLoc, numberOfPartitions, bk_site_id)

      val blk_dedup = DCUEvents.removeBlukaiDuplicates(blk)

      val idmapsFull_latest = buildIdMaps(blk_dedup, idmapsFull, days_to_expire)
      val castmapsFull_latest = buildCastMaps(blk_dedup, castmapsFull)

      writeToTarget(idmapsFull_latest, numberOfOutputFiles, cur_idDateLoc)
      writeToTarget(castmapsFull_latest, numberOfOutputFiles, cur_castDateLoc)

      cur_rundate = getDateStr(addDays(getDate(cur_rundate), 1), "yyyyMMdd")
      prev_maps_rundate = getDateStr(addDays(getDate(prev_maps_rundate), 1), "yyyyMMdd")

    }

  }

  def loadIDMapsAll(idmaps_path: String, spark: SparkSession): DataFrame = {
    if(checkPathExist(idmaps_path)) {
      spark.read.parquet(idmaps_path)

    } else {
      createEmptyDataframe(spark)

    }

  }

  def loadCastMapsAll(castmaps_path: String, spark: SparkSession): DataFrame = {
    if(checkPathExist(castmaps_path)) {
      spark.read.parquet(castmaps_path)

    } else {
      createEmptyDataframe(spark)

    }

  }

  def buildIdMaps(blukai_dedup: DataFrame, idmapsFull: DataFrame, days_to_expire: Int): DataFrame = {

    val dayLimit = new java.sql.Timestamp(new DateTime(DateTimeZone.UTC).minusDays(days_to_expire).getMillis)

    val id_maps = buildIdMaps(blukai_dedup)

    //Append the new map
    if(idmapsFull.count != 0) {
      val result = id_maps.union(idmapsFull).distinct
        //merge rows with same uid
        .groupBy(col("uid"))
          .agg(
            concat(collect_set("user_guids"))(0).as("user_guids"),
            concat(collect_set("compositekeys"))(0).as("compositekeys"),
            concat(collect_set("idfas"))(0).as("idfas"),
            concat(collect_set("adids"))(0).as("adids"),
            concat(collect_set("website_sections"))(0).as("website_sections"),
            max("last_seen").alias("last_seen"))

      result
        .filter(col("last_seen") > dayLimit)

    } else {
      id_maps
        .filter(col("last_seen") > dayLimit)

    }

  }

  def buildIdMaps(blukai_dedup: DataFrame): DataFrame = {
    //val id_maps = blukai_dedup.groupBy("uid").agg(collect_set("phints.WBUID").alias("user_guids")).select("uid", "user_guids")
    val id_maps = blukai_dedup.groupBy("uid")
      .agg(
        collect_set("phints.WBUID").alias("user_guids"),
        collect_set("compositekey").alias("compositekeys"),
        collect_set("phints.idfa").alias("idfas"),
        collect_set("phints.adid").alias("adids"),
        collect_set("phints.WBSiteSec").alias("website_sections"),
        max("seen").alias("last_seen"))

      .select("uid", "user_guids", "compositekeys", "idfas", "adids", "website_sections", "last_seen")

    id_maps

  }

  def buildCastMaps(bluekai_dedup: DataFrame, castmapsFull: DataFrame): DataFrame = {
    val cast_maps = buildCastMaps(bluekai_dedup)

    //Append the new map
    if(castmapsFull.count != 0) {
      val result = cast_maps.union(castmapsFull).distinct
      //merge the casts, keywords and/or urls of the same title&type
      result.groupBy(col("bluekai_content_title"), col("bluekai_content_type"))
        .agg(
          concat(collect_set("bluekai_content_casts"))(0).as("bluekai_content_casts"),
          concat(collect_set("bluekai_content_keywords"))(0).as("bluekai_content_keywords"),
          concat(collect_set("bluekai_content_urls"))(0).as("bluekai_content_urls"))
    } else {
      cast_maps

    }

  }

  def buildCastMaps(bluekai_dedup: DataFrame): DataFrame = {
    val cast_maps = bluekai_dedup.
      withColumn("bluekai_content_title",
        when(col("phints.WBCmcTitle").isNotNull, col("phints.WBCmcTitle"))
          .when(col("phints.WBMvTitle").isNotNull, col("phints.WBMvTitle"))
          .when(col("phints.WBTvSrsTitle").isNotNull, col("phints.WBTvSrsTitle"))).
      withColumn("bluekai_content_type",
        when(col("phints.WBCmcTitle").isNotNull, when((col("phints.WBSiteSec").contains("Article")), lit("Article")) otherwise(lit("Comic")))
          .when(col("phints.WBMvTitle").isNotNull, lit("Movie"))
          .when(col("phints.WBTvSrsTitle").isNotNull, lit("TV"))).
      withColumn("bluekai_content_casts", col("phints.WBCelebrity")).
      withColumn("bluekai_content_keywords",
        when(col("phints.__bk_k").isNotNull, col("phints.__bk_k"))
          .when(col("phints.WBChar").isNotNull, col("phints.WBChar"))).
      withColumn("bluekai_content_urls",
        when(col("phints.__bk_l").isNotNull, col("phints.__bk_l"))
      )
      .select("bluekai_content_title", "bluekai_content_type", "bluekai_content_casts", "bluekai_content_keywords", "bluekai_content_urls").filter(col("bluekai_content_title").isNotNull).distinct

    //merge the casts, keywords and/or urls of the same title
    cast_maps.groupBy(col("bluekai_content_title"), col("bluekai_content_type"))
      .agg(
        concat(collect_set("bluekai_content_casts"))(0).as("bluekai_content_casts"),
        concat(collect_set("bluekai_content_keywords"))(0).as("bluekai_content_keywords"),
        concat(collect_set("bluekai_content_urls"))(0).as("bluekai_content_urls"))

  }

}
