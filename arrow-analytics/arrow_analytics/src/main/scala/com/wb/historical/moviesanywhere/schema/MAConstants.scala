package com.wb.historical.moviesanywhere.schema

import com.wb.constants.Constants

object MAConstants {

  val fieldMapping = Map[String, String](
    DataFields.WBCID -> Constants.WBCID_KEY)

  object DataFields {
    val WBCID = "wbcid"
    val MA_USER_ID = "ma_user_id"
    val TITLE_EIDR_ID = "title_eidr_id"
    val VIEWING_ID = "viewing_id"
    val VIEWING_TS = "viewing_ts"
    val VIEWING_DMA_CD = "viewing_dma_cd"
    val VIEWING_COUNTRY_CD = "viewing_country_cd"
    val VIEWING_COUNTRY_NAME = "viewing_country_name"
    val VIEWING_REGION = "viewing_region"
    val VIEWING_CITY = "viewing_city"
    val VIEWING_POSTAL_CD = "viewing_postal_cd"
    val CONTENT_VIEW_PCT = "content_view_pct"
    val CONTENT_TYPE = "content_type"
    val VIEWING_CONTENT_TYPE = "viewing_content_type"
    val CONTENT_SOURCE_NAME = "content_source_name"
    val DEVICE_ADVERTISING_ID = "device_advertising_id"
    val DEVICE_TYPE = "device_type"
    val DEVICE_MODEL = "device_model"
    val DEVICE_MANUFACTURER = "device_manufacturer"
    val DEVICE_OS = "device_os"
    val EIDR_ID = "eidr_id"
    val EIDR_TITLE_DESCRIPTION = "eidr_title_description"
    val EIDR_GENRE = "eidr_genre"
    val GENRE_NAME = "genre_name"
    val LAST_MODIFIED_DT = "last_modified_dt"
    val TRANSACTION_TYPE = "transaction_type"
    val RETAILER_TRANSACTION_DT = "retailer_transaction_dt"
    val RELEASE_STATUS_NAME = "release_status_name"
    val TRANSACTION_ID = "transaction_id"
    val TRANSACTION_DMA_CD = "transaction_dma_cd"
    val TRANSACTION_COUNTRY_CD = "transaction_country_cd"
    val TRANSACTION_COUNTRY_NAME = "transaction_country_name"
    val TRANSACTION_REGION = "transaction_region"
    val TRANSACTION_CITY = "transaction_city"
    val TRANSACTION_POSTAL_CD = "transaction_postal_cd"
    val EMAIL = "email"
    val DW_CURRENT_FLAG = "dw_current_flag"
    val INITIAL_STUDIO_MARKETING_CONSENT_FLAG = "initial_studio_marketing_consent_flag"
    val DOMINANT_DMA = "dominant_dma"
    val DOMINANT_COUNTRY_CD = "dominant_country_cd"
    val DOMINANT_COUNTRY_NAME = "dominant_country_name"
    val DOMINANT_REGION = "dominant_region"
    val DOMINANT_CITY = "dominant_city"
    val DOMINANT_POSTAL_CD = "dominant_postal_cd"
    val REGISTRATION_DEVICE_ADVERTISING_ID = "registration_device_advertising_id"
    val REGISTRATION_DEVICE_MANUFACTURER = "registration_device_manufacturer"
    val REGISTRATION_DEVICE_MODEL = "registration_device_model"
    val LOCKER_TITLE_COUNT = "locker_title_count"
    val LOCKER_PCT = "locker_pct"
    val LOAD_DT = "dw_load_ts"
  }

}