package com.wb.historical

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.{Constants, Phints}
import com.wb.enrich.Attributes
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._


object FrequencyCounter {
  val unk : String = null

  def initConfiguration(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    val configDetails = initConfiguration(args)
    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val numberOfPartitions = CommandLineArguments.numberOfPartitions.toInt

    val spark = SparkSession.
      builder().
      appName("FrequencyCounter").
      enableHiveSupport().
      getOrCreate()

    val configs = ConfigFactory.load()
    
    val outputBase = configs.getString("hdfs.arrow.frequency")
    val outputDir = s"$outputBase/${rundate.substring(0,4)}-${rundate.substring(4,6)}-${rundate.substring(6,8)}"
    
    val bkLoc = configs.getString("hdfs.arrow.bluekai")
    val bkeDateLoc = s"$bkLoc/date=${rundate.substring(0,4)}-${rundate.substring(4,6)}-${rundate.substring(6,8)}/*"
    val bkdf = spark.read.parquet(bkeDateLoc).coalesce(numberOfPartitions)

    //Establish UDFS
    val getFnUIDHash = spark.udf.register("sha1", Attributes.getFnUIDHashFcn _)

    //Aggregate counts and IPs - Bluekai
    val bkid = bkdf.
      select(
        col(Constants.UID_KEY),
        col(Constants.CLIENT_IP_KEY)
      ).
      filter(col(Constants.UID_KEY).isNotNull).
      groupBy(Constants.UID_KEY).
      agg(
        sum(lit(1)).alias(Constants.EVENT_COUNT_KEY),
        collect_set(Constants.CLIENT_IP_KEY).alias(s"${Constants.CLIENT_IP_KEY}s")
      )

    //Aggregate IPs - All activity
    val clientip = bkdf.
      select(
        col(Constants.CLIENT_IP_KEY)
      ).
      filter(col(Constants.CLIENT_IP_KEY).isNotNull).
      groupBy(Constants.CLIENT_IP_KEY).
      count.
      withColumnRenamed("count",Constants.EVENT_COUNT_KEY)

    //Aggregate counts and IPs - Flixster
    val flxid = bkdf.
      select(
        col(s"${Constants.PHINTS_KEY}.${Phints.FLX_UID}").alias(Constants.FLIXSTER_USER_ID),
        col(Constants.CLIENT_IP_KEY)
      ).filter(col(Constants.FLIXSTER_USER_ID).isNotNull).
      groupBy(Constants.FLIXSTER_USER_ID).
      agg(
        sum(lit(1)).alias(Constants.EVENT_COUNT_KEY),
        collect_set(Constants.CLIENT_IP_KEY).alias(s"${Constants.CLIENT_IP_KEY}s")
      )

    //Aggregate counts and IPs - Fandango
    val fanid = bkdf.
      select(
        getFnUIDHash(col(s"${Constants.PHINTS_KEY}.${Phints.FN_UID}")).alias(Constants.FANDANGO_USER_ID_SHA256),
        col(Constants.CLIENT_IP_KEY)
      ).filter(col(Constants.FANDANGO_USER_ID_SHA256).isNotNull).
      groupBy(Constants.FANDANGO_USER_ID_SHA256).
      agg(
        sum(lit(1)).alias(Constants.EVENT_COUNT_KEY),
        collect_set(Constants.CLIENT_IP_KEY).alias(s"${Constants.CLIENT_IP_KEY}s")
      )

    //Aggregate counts and IPs - Maid
    val maids = bkdf.select(
      col(s"${Constants.PHINTS_KEY}.${Constants.IDFA_KEY}").alias(Constants.IDFA_KEY),
      col(s"${Constants.PHINTS_KEY}.${Constants.ADID_KEY}").alias(Constants.ADID_KEY),
      col(Constants.CLIENT_IP_KEY)
    ).withColumn(Constants.MAID_KEY,
        when(
          col(Phints.IDFA).isNotNull, col(Phints.IDFA)
        ).when(
          col(Phints.ADID).isNotNull, col(Phints.ADID)
        )).
      filter(col(Constants.MAID_KEY).isNotNull).
      groupBy(Constants.MAID_KEY,Phints.IDFA,Phints.ADID).
      agg(
        sum(lit(1)).alias(Constants.EVENT_COUNT_KEY),
        collect_set(Constants.CLIENT_IP_KEY).alias(s"${Constants.CLIENT_IP_KEY}s")
      )

    //Store data
    var hdfsDir = s"$outputDir/"+"flixster"
    //Write Data to HDFS - Flixster
    if(numberOfOutputFiles > 0) {
      flxid.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      flxid.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"fandango"
    //Write Data to HDFS - Fandango
    if(numberOfOutputFiles > 0) {
      fanid.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      fanid.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"maid"
    //Write Data to HDFS - Maid
    if(numberOfOutputFiles > 0) {
      maids.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      maids.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"ip"
    //Write Data to HDFS - IP
    if(numberOfOutputFiles > 0) {
      clientip.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      clientip.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"bkid"
    //Write Data to HDFS - Bluekai
    if(numberOfOutputFiles > 0) {
      bkid.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      bkid.write.parquet(hdfsDir)
    }
  }
}
