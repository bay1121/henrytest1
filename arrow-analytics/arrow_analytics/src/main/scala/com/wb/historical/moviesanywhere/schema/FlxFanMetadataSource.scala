package com.wb.historical.moviesanywhere.schema

import com.wb.model.DataSource
import org.apache.spark.sql.types._

object FlxFanMetadataSource extends DataSource {
  override val rawSchema: StructType = StructType(
    Array(
      StructField("flixster_id", IntegerType),
      StructField("fandango_id", IntegerType),
      StructField("box_office", IntegerType),
      StructField("tomatometer", IntegerType),
      StructField("cast_top_1", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_3", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_5", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_10", ArrayType(StringType, containsNull = true)),
      StructField("movie_talent", ArrayType(StringType, containsNull = true)),
      StructField("cast_all_dir", ArrayType(StringType, containsNull = true)),
      StructField("cast_all_act", ArrayType(StringType, containsNull = true)),
      StructField("cast_all_pro", ArrayType(StringType, containsNull = true)),
      StructField("cast_all_scr", ArrayType(StringType, containsNull = true)),
      StructField("cast_all_epr", ArrayType(StringType, containsNull = true)),
      StructField("genres", ArrayType(StringType, containsNull = true)),
      StructField("title", StringType),
      StructField("theater_release_date", TimestampType),
      StructField("release_year", StringType),
      StructField("rating", StringType),
      StructField("studio", StringType),
      StructField("director", StringType),
      StructField("imdb_id", StringType),
      StructField("imdb_keywords", ArrayType(StringType, containsNull = true)),
      StructField("catman_franchise", StringType),
      StructField("catman_primary_brand", StringType),
      StructField("catman_brands", ArrayType(StringType, containsNull = true)),
      StructField("franchise_brands", ArrayType(StringType, containsNull = true)),
      StructField("franchise_characters", ArrayType(StringType, containsNull = true)),
      StructField("franchise_properties", ArrayType(StringType, containsNull = true))))

  override val schema = StructType(
    Array(
      StructField("flixster_id", decimalType),
      StructField("box_office", decimalType),
      StructField("tomatometer", decimalType),
      StructField("cast_top_1", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_3", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_5", ArrayType(StringType, containsNull = true)),
      StructField("cast_top_10", ArrayType(StringType, containsNull = true)),
      StructField("movie_talent", ArrayType(StringType, containsNull = true)),
      StructField("genres", ArrayType(StringType, containsNull = true)),
      StructField("title", StringType),
      StructField("theater_release_date", TimestampType),
      StructField("release_year", StringType),
      StructField("rating", StringType),
      StructField("studio", StringType),
      StructField("director", StringType),
      StructField("imdb_keywords", ArrayType(StringType, containsNull = true)),
      StructField("catman_franchise", StringType),
      StructField("catman_primary_brand", StringType),
      StructField("catman_brands", ArrayType(StringType, containsNull = true)),
      StructField("franchise_characters", ArrayType(StringType, containsNull = true))))

  override val arrowMapping: Map[String, String] = Map[String, String](
    "genres" -> "flixsterGenres",
    "title" -> "flixsterTitle")
}
