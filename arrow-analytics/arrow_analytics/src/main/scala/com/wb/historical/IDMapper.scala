package com.wb.historical

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.{Constants, Phints}
import com.wb.enrich.Attributes
import com.wb.model.SchemaBuilder
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._
import scala.util.control.Breaks._


/**
  * Created by nathan on 2/2/2017.
  */
object IDMapper {
  val unk : String = null

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }

  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)
    
    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val numberOfPartitions = CommandLineArguments.numberOfPartitions.toInt

    val spark = SparkSession.
      builder().
      appName("IDMapper").
      enableHiveSupport().
      getOrCreate()

    val configs = ConfigFactory.load()
    
    val outputBase = configs.getString("hdfs.arrow.idmapping")
    val outputDir = s"$outputBase/${rundate.substring(0,4)}-${rundate.substring(4,6)}-${rundate.substring(6,8)}"
    
    val bkLoc = configs.getString("hdfs.arrow.bluekai")
    val categoryLoc = configs.getString("hdfs.static.bluekai_categories")

    val bkeDateLoc = s"$bkLoc/date=${rundate.substring(0,4)}-${rundate.substring(4,6)}-${rundate.substring(6,8)}/*"
    val bkdf = spark.read.parquet(bkeDateLoc).coalesce(numberOfPartitions)

    val categories_full = spark.read.schema(SchemaBuilder.categorySchema).format("com.databricks.spark.csv").option("header", "true").option("delimiter", "|").load(categoryLoc)
    val categories = categories_full.rdd.keyBy(x => x.getAs[Int](0)).mapValues(y => y.getAs[String](3).replaceAll(" --> ","-->").split("-->")).collectAsMap()
    val deviceTypes = Constants.deviceTypes

    def getDeviceTypeFcn(catstr:String,phints:Map[String,String]) : String = {
      var temp = Map[String, String]()
      var deviceType = unk
      try {
        val cats = catstr.split(" ").map(_.toInt)
        for (cat <- cats) {
          val desc = categories.getOrElse(cat, null)
          if (desc != null) {
            if (desc.length > 2) {
              var c = Map[String, String]()
              var key = desc.lift(1).get
              var value = desc.lift(2).get
              temp += key -> value
            }
          }
        }

        temp.foreach { case (key, value) =>
          temp -= key
          temp += key.replaceAll(" ", "_").toLowerCase -> value
        }

        if (phints.getOrElse(Phints.IDFA, null) != null) {
          deviceType = Constants.IOS_KEY
        } else if (phints.getOrElse(Phints.ADID, null) != null) {
          deviceType = Constants.ANDROID_KEY
        }

        if (deviceType == unk) {
          breakable {
            for (x <- deviceTypes) {
              var key = x._1
              var values = x._2.intersect(cats)
              if (values.length > 0) {
                deviceType = key
                break
              }
            }
          }
        }
      } catch {
        case e: Exception =>
          ArrowLogger.log.warn("Unable to getCategoryValues with: " + catstr + " " + phints)
          ArrowLogger.log.warn(e.printStackTrace())
      }

      deviceType
    }

    //Establish UDFS
    val getFnUIDHash = spark.udf.register("sha1", Attributes.getFnUIDHashFcn _)
    val getDeviceType = spark.udf.register("getDeviceType", getDeviceTypeFcn _)


    val flxid = bkdf.
      select(
        col(s"${Constants.PHINTS_KEY}.${Phints.FLX_UID}").alias(Constants.FLIXSTER_USER_ID),
        col(Constants.UID_KEY),
        getDeviceType(col(Constants.CATEGORIES_KEY),col(Constants.PHINTS_KEY)).alias(Constants.DEVICE_TYPE_KEY)).
      filter(col(Constants.FLIXSTER_USER_ID).isNotNull).
      filter(col(Constants.UID_KEY).isNotNull).
      groupBy(Constants.FLIXSTER_USER_ID,Constants.UID_KEY).
      agg(collect_set(Constants.DEVICE_TYPE_KEY).alias(Constants.DEVICE_TYPE_KEY)).distinct

    val fanid = bkdf.
      select(
        getFnUIDHash(col(s"${Constants.PHINTS_KEY}.${Phints.FN_UID}")).alias(Constants.FANDANGO_USER_ID_SHA256),
        col(Constants.UID_KEY),
        getDeviceType(col(Constants.CATEGORIES_KEY), col(Constants.PHINTS_KEY)).alias(Constants.DEVICE_TYPE_KEY)).
      filter(col(Constants.FANDANGO_USER_ID_SHA256).isNotNull).
      filter(col(Constants.UID_KEY).isNotNull).
      groupBy(Constants.FANDANGO_USER_ID_SHA256,Constants.UID_KEY).
      agg(collect_set(Constants.DEVICE_TYPE_KEY).alias(Constants.DEVICE_TYPE_KEY)).distinct

    val maids = bkdf.select(
      col(Constants.UID_KEY),
      col(s"${Constants.PHINTS_KEY}.${Constants.IDFA_KEY}").alias(Constants.IDFA_KEY),
      col(s"${Constants.PHINTS_KEY}.${Constants.ADID_KEY}").alias(Constants.ADID_KEY),
      getDeviceType(col(Constants.CATEGORIES_KEY),col(Constants.PHINTS_KEY)).alias(Constants.DEVICE_TYPE_KEY)).
      withColumn(Constants.MAID_KEY,
        when(
          col(Phints.IDFA).isNotNull, col(Phints.IDFA)
        ).when(
          col(Phints.ADID).isNotNull, col(Phints.ADID)
        )).
      filter(col(Constants.MAID_KEY).isNotNull).
      filter(col(Constants.UID_KEY).isNotNull).
      groupBy(Constants.MAID_KEY,Constants.UID_KEY,Phints.IDFA,Phints.ADID).
      agg(collect_set(Constants.DEVICE_TYPE_KEY).alias(Constants.DEVICE_TYPE_KEY)).distinct

    var hdfsDir = s"$outputDir/"+"flixster"
    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      flxid.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      flxid.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"fandango"
    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      fanid.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      fanid.write.parquet(hdfsDir)
    }

    hdfsDir = s"$outputDir/"+"maid"
    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      maids.coalesce(numberOfOutputFiles).write.parquet(hdfsDir)
    } else {
      maids.write.parquet(hdfsDir)
    }
  }
}
