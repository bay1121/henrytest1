package com.wb.historical

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.joda.time.DateTime
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}
import org.joda.time.DateTimeZone

import scala.collection.JavaConversions._

object IDList {

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def getTopElementsFcn(list:Seq[String],numberOfElements:Int) : Seq[String] = {
    list.take(numberOfElements)
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val daysAgo = CommandLineArguments.daysAgo.toInt

    val spark = SparkSession.
      builder().
      appName("IDList").
      getOrCreate()

    val configs = ConfigFactory.load()
    
    val outputDir = configs.getString("hdfs.arrow.idlist")
    val fnCustLoc = configs.getString(Constants.FD_TBLCUSTOMER_LOC)
    val flxCustLoc = configs.getString(Constants.FX_USER_EMAIL_LOC)
    val testEmailLoc = configs.getString(Constants.FD_TEST_EMAIL_LKP_LOC)
    val idMappingLoc = configs.getString("hdfs.arrow.idmapping")

    val getIsStatisticalIDUDF = spark.udf.register("getIsStatisticalID", Attributes.getIsStatisticalID _)
    val fileDateUDF = spark.udf.register("fileDate", Util.getFileDate _)
    val flattenNestedTypeUDF = spark.udf.register("flattenNestedType", Util.flattenNestedType _)
    val getTopElements = spark.udf.register("getTopElementsFcn", getTopElementsFcn _)
    val getFnUIDHash = spark.udf.register("sha1", Attributes.getFnUIDHashFcn _)

    val dayLimit = new java.sql.Timestamp(new DateTime(DateTimeZone.UTC).minusDays(daysAgo).getMillis)

    //Load Data
    val testEmails : List[String]= spark.read.parquet(testEmailLoc).select("email_nm").rdd.map(x=>x.getAs[String](0)).collect.toList
    val flxFull = spark.read.parquet(s"${idMappingLoc}/*/flixster/*").
      withColumn("seen", fileDateUDF(input_file_name)).
      filter(col("seen") <= dayLimit).
      filter(!getIsStatisticalIDUDF(col(Constants.UID_KEY)))
    val fnFull = spark.read.parquet(s"${idMappingLoc}/*/fandango/*").
      withColumn("seen", fileDateUDF(input_file_name)).
      filter(col("seen") <= dayLimit).
      filter(!getIsStatisticalIDUDF(col(Constants.UID_KEY)))
    val maidFull = spark.read.parquet(s"${idMappingLoc}/*/maid/*").
      withColumn("seen", fileDateUDF(input_file_name)).
      filter(col("seen") <= dayLimit).
      filter(!getIsStatisticalIDUDF(col(Constants.UID_KEY))).
      filter(col(Constants.MAID_KEY) =!= "00000000-0000-0000-0000-000000000000")

    val emailAddressesFlx = spark.read.parquet(flxCustLoc).
      select(
        lower(col("email")).alias(Constants.EMAIL_ADDRESS_KEY),
        col("user_id").alias(Constants.FLIXSTER_USER_ID)
      ).
      filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull &&
        col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@fandango\\.com") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@warnerbros\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@wb\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@dcentertainment\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@timewarner\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@_flixster\\.com+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).isin(testEmails:_*)).
      distinct.
      groupBy(Constants.FLIXSTER_USER_ID).
      agg(
        first(Constants.EMAIL_ADDRESS_KEY, true).alias(s"${Constants.EMAIL_ADDRESS_KEY}_flx")
      )

    val emailAddressesFan = spark.read.parquet(fnCustLoc).
      select(
        lower(col("email_nm")).alias(Constants.EMAIL_ADDRESS_KEY),
        col("customer_guid").alias(Constants.FANDANGO_USER_ID)).
      filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull &&
        col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@fandango\\.com") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+facebook\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@warnerbros\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@wb\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@dcentertainment\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@timewarner\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster\\.com$") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@flixster-inc.+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).rlike(".+@_flixster\\.com+") &&
        !col(Constants.EMAIL_ADDRESS_KEY).isin(testEmails:_*)).
      distinct.
      withColumn(Constants.FANDANGO_USER_ID_SHA256, getFnUIDHash(col(Constants.FANDANGO_USER_ID))).
      groupBy(Constants.FANDANGO_USER_ID_SHA256).
      agg(
        first(Constants.EMAIL_ADDRESS_KEY, true).alias(s"${Constants.EMAIL_ADDRESS_KEY}_fan")
      )

    val columnExprsMaid = maidFull.columns.filter(!_.equalsIgnoreCase(Constants.UID_KEY)).map(x => first(x, true).alias(x))
    var maid = maidFull.sort(col("seen").desc).groupBy(Constants.UID_KEY).agg(columnExprsMaid.head, columnExprsMaid.tail: _*).distinct

    val columnExprsFanTemp = fnFull.columns.filter(!_.equalsIgnoreCase(Constants.FANDANGO_USER_ID_SHA256)).map(x => first(x, true).alias(x))
    val fnTemp = fnFull.sort(col("seen").desc).groupBy(Constants.FANDANGO_USER_ID_SHA256).agg(columnExprsFanTemp.head, columnExprsFanTemp.tail: _*)
    val columnExprsFan = fnTemp.columns.filter(!_.equalsIgnoreCase(Constants.UID_KEY)).map(x => first(x, true).alias(x))
    var fn = fnTemp.
      sort(col("seen").desc).
      groupBy(Constants.UID_KEY).
      agg(columnExprsFan.head, columnExprsFan.tail: _*).
      distinct.
      join(emailAddressesFan,Seq(Constants.FANDANGO_USER_ID_SHA256),"full_outer")

    val columnExprsFlxTemp = flxFull.columns.filter(!_.equalsIgnoreCase(Constants.FLIXSTER_USER_ID)).map(x => first(x, true).alias(x))
    val flxTemp = flxFull.sort(col("seen").desc).groupBy(Constants.FLIXSTER_USER_ID).agg(columnExprsFlxTemp.head, columnExprsFlxTemp.tail: _*)
    val columnExprsFlx = flxTemp.columns.filter(!_.equalsIgnoreCase(Constants.UID_KEY)).map(x => first(x, true).alias(x))
    var flx = flxTemp.
      sort(col("seen").desc).
      groupBy(Constants.UID_KEY).
      agg(columnExprsFlx.head, columnExprsFlx.tail: _*).
      distinct.
      join(emailAddressesFlx, Seq(Constants.FLIXSTER_USER_ID),"full_outer")


    val flxDeviceTypes = flxFull.select(col(Constants.UID_KEY).alias(Constants.UID_KEY),explode(col(Constants.DEVICE_TYPE_KEY)).alias(Constants.DEVICE_TYPE_KEY))
    val fnDeviceTypes = fnFull.select(col(Constants.UID_KEY).alias(Constants.UID_KEY),explode(col(Constants.DEVICE_TYPE_KEY)).alias(Constants.DEVICE_TYPE_KEY))
    val maidDeviceTypes = maidFull.select(col(Constants.UID_KEY).alias(Constants.UID_KEY),explode(col(Constants.DEVICE_TYPE_KEY)).alias(Constants.DEVICE_TYPE_KEY))
    val deviceTypeByUID = flxDeviceTypes.
      union(fnDeviceTypes).
      union(maidDeviceTypes)

    //uid, device_type[]
    val deviceTypeSetByUID = deviceTypeByUID.
      distinct.
      groupBy(Constants.UID_KEY).
      agg(
        collect_set(col(Constants.DEVICE_TYPE_KEY)).alias(Constants.DEVICE_TYPE_KEY)
      )

    val flxUIDsSeen = flxFull.select(Constants.UID_KEY,"seen")
    val fnUIDsSeen = fnFull.select(Constants.UID_KEY,"seen")
    val maidUIDsSeen = maidFull.select(Constants.UID_KEY,"seen")
    val seenByUID = flxUIDsSeen.
      union(fnUIDsSeen).
      union(maidUIDsSeen)

    //uid, first_seen, last_seen
    val lastSeenByUID = seenByUID.
      distinct.
      groupBy(Constants.UID_KEY).
      agg(
        max(col("seen")).alias("last_seen"),
        min(col("seen")).alias("first_seen")
      )

    //uid, flixster_user_id
    flx = flx.
      drop("seen").
      drop(Constants.DEVICE_TYPE_KEY).
      distinct
    //uid, fandango_user_id_sha256
    fn = fn.
      drop("seen").
      drop(Constants.DEVICE_TYPE_KEY).
      distinct
    //uid, maid
    maid = maid.
      drop("seen").
      drop(Constants.DEVICE_TYPE_KEY).
      distinct


    val idsEnriched = lastSeenByUID.
      join(deviceTypeSetByUID, Seq(Constants.UID_KEY),"left_outer").
      join(maid,Seq(Constants.UID_KEY),"left_outer").
      join(flx,Seq(Constants.UID_KEY),"left_outer").
      join(fn,Seq(Constants.UID_KEY), "left_outer")

    val withEmail = idsEnriched.
      filter(col(s"${Constants.EMAIL_ADDRESS_KEY}_fan").isNotNull || col(s"${Constants.EMAIL_ADDRESS_KEY}_flx").isNotNull).
      withColumn(Constants.EMAIL_ADDRESS_KEY,
        when(col(s"${Constants.EMAIL_ADDRESS_KEY}_fan").isNotNull, col(s"${Constants.EMAIL_ADDRESS_KEY}_fan")).
          when(col(s"${Constants.EMAIL_ADDRESS_KEY}_flx").isNotNull, col(s"${Constants.EMAIL_ADDRESS_KEY}_flx")
        )
      ).drop(
      s"${Constants.EMAIL_ADDRESS_KEY}_fan",
      s"${Constants.EMAIL_ADDRESS_KEY}_flx"
    ).distinct

    val emailOnly = idsEnriched.
      filter(col(s"${Constants.EMAIL_ADDRESS_KEY}_fan").isNull && col(s"${Constants.EMAIL_ADDRESS_KEY}_flx").isNull).
      drop(s"${Constants.EMAIL_ADDRESS_KEY}_fan",s"${Constants.EMAIL_ADDRESS_KEY}_flx").
      distinct.
      withColumn(Constants.EMAIL_ADDRESS_KEY, lit(null))

    val columns: Array[String] = withEmail.columns
    val columnExprs = withEmail.columns.filter(!_.equalsIgnoreCase(Constants.UID_KEY)).map(x => first(x, true).alias(x))
    val enrichedEmail = withEmail.select(columns.head, columns.tail: _*)

    val singleIDs = enrichedEmail.
      sort(col("last_seen").desc).
      groupBy(Constants.UID_KEY).
      agg(columnExprs.head, columnExprs.tail: _*)

    val finalIDSet = singleIDs.select(columns.head, columns.tail: _*).
      union(emailOnly.select(columns.head, columns.tail: _*))

    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      finalIDSet.repartition(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      finalIDSet.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }
  }
}

