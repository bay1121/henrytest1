package com.wb.historical.moviesanywhere.schema

import com.wb.historical.moviesanywhere.schema.MAConstants.DataFields
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object UserContentDataSource extends DataSource {

  override val rawSchema: StructType = StructType(
    Array(
      StructField("ma_user_content_source_key", LongType),
      StructField("ma_user_key", StringType),
      StructField(DataFields.MA_USER_ID, StringType),
      StructField(DataFields.CONTENT_SOURCE_NAME, StringType),
      StructField(DataFields.LOCKER_PCT, DecimalType(5, 3)),
      StructField("dw_source_filename", StringType),
      StructField("dw_load_ts", TimestampType),
      StructField("dw_update_ts", TimestampType),
      StructField("dw_data_dt", DateType),
      StructField("dw_etlload_id", IntegerType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField("dw_row_start_dt", DateType),
      StructField("dw_row_end_dt", DateType)
    )
  )

  override val schema: StructType = StructType(
    Array(
      StructField(DataFields.MA_USER_ID, StringType),
      StructField(DataFields.CONTENT_SOURCE_NAME, StringType),
      StructField(DataFields.LOCKER_PCT, DecimalType(5, 3)),
      StructField(DataFields.DW_CURRENT_FLAG, StringType)
    )
  )

  override val arrowMapping: Map[String, String] = Map[String, String](
  )
}
