package com.wb.historical.moviesanywhere.schema

import MAConstants.DataFields
import com.wb.constants.Constants
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object UsersDataSource extends DataSource {
  override val rawSchema: StructType = StructType(
    Array(
      StructField("ma_user_key", LongType),
      StructField("ma_user_id", StringType),
      StructField("first_name", StringType),
      StructField(DataFields.EMAIL, StringType),
      StructField("first_name", StringType),
      StructField("registration_ts", TimestampType),
      StructField(DataFields.INITIAL_STUDIO_MARKETING_CONSENT_FLAG, StringType),
      StructField("dominant_client_type", StringType),
      StructField("dominant_dma", IntegerType),
      StructField("dominant_country_cd", StringType),
      StructField("dominant_country_name", StringType),
      StructField("dominant_region", StringType),
      StructField("dominant_city", StringType),
      StructField("dominant_postal_cd", StringType),
      StructField(DataFields.REGISTRATION_DEVICE_ADVERTISING_ID, StringType),
      StructField("registration_device_type", StringType),
      StructField(DataFields.REGISTRATION_DEVICE_MANUFACTURER, StringType),
      StructField(DataFields.REGISTRATION_DEVICE_MODEL, StringType),
      StructField("registration_device_version", StringType),
      StructField("registration_device_os", StringType),
      StructField("registration_device_os_version", StringType),
      StructField("account_link_count", IntegerType),
      StructField("profile_count", IntegerType),
      StructField(DataFields.LOCKER_TITLE_COUNT, IntegerType),
      StructField("dw_source_filename", StringType),
      StructField("dw_load_ts", TimestampType),
      StructField("dw_update_ts", TimestampType),
      StructField("dw_data_dt", DateType),
      StructField("dw_etlload_id", IntegerType),
      StructField("dw_row_hash", StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField("dw_row_start_dt", DateType),
      StructField("dw_row_end_dt", DateType)
    )
  )


  override val schema: StructType = StructType(
    Array(
      StructField(DataFields.MA_USER_ID, StringType),
      StructField(DataFields.EMAIL, StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField(DataFields.INITIAL_STUDIO_MARKETING_CONSENT_FLAG, StringType),
      StructField(DataFields.REGISTRATION_DEVICE_ADVERTISING_ID, StringType),
      StructField(DataFields.REGISTRATION_DEVICE_MANUFACTURER, StringType),
      StructField(DataFields.REGISTRATION_DEVICE_MODEL, StringType),
      StructField(DataFields.LOCKER_TITLE_COUNT, DecimalType(38,0))
    )
  )

  override val arrowMapping: Map[String, String] = Map[String, String](
    DataFields.EMAIL -> Constants.EMAIL_ADDRESS_KEY,
    DataFields.LOCKER_TITLE_COUNT -> Constants.LIBRARY_COUNT
  )
}
