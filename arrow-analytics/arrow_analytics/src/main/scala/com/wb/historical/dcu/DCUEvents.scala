package com.wb.historical.dcu

import java.util.{Calendar, TimeZone}
import scala.collection.JavaConversions.asScalaBuffer
import com.typesafe.config.{Config, ConfigFactory}
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import org.apache.spark.sql.functions.{when, _}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import com.wb.maxmind.{GeoEnricher, MaxmindFileServer}
import org.apache.spark.sql.types._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import com.wb.historical._

object DCUEvents {

  var spark: SparkSession = _
  var configs: Config = _

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)

    processDCUEvents()

  }

  def processDCUEvents(): DataFrame = {
    //main flow logics, to glue the functions together

    //Set up variables
    val rundate = CommandLineArguments.rundate

    val loadAll = if(rundate.isEmpty) true else false
    val run_date = validateDate(rundate)
    val dirDateString = getDirectoryDate(run_date)

    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val numberOfPartitions = CommandLineArguments.numberOfPartitions.toInt

    //Choose to load maps of which date, optional
    //When backfill, we can choose the latest maps to apply to it
    val dateField = CommandLineArguments.dateField

    //Create the spark
    val conf = new org.apache.spark.SparkConf()

    val maxmind_date = if(!loadAll) rundate else getDateStr(subtractDays(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime().getTime()), 1), "yyyyMMdd")

    //Sample: "s3://cmdt-datatree/processed/arrow/metadata/maxmind/City/20190409/GeoIP2-City_20190409.mmdb"
    val geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate(maxmind_date)
    val mmdbFilename = geoCityLocFile.substring(geoCityLocFile.lastIndexOf("/") + 1)
    conf.set("spark.files", geoCityLocFile)

    spark = SparkSession.
      builder().
      config(conf).
      appName("DCUEvents").
      getOrCreate()

    configs = ConfigFactory.load()

    //days is used to expire the id map records which is last seen days before
    //var days_to_expire = CommandLineArguments.daysAgo.toInt

    //val dateoutputStr = rundate.substring(0,4)+"-"+rundate.substring(4,6)+"-"+rundate.substring(6,8)

    val outputBase = configs.getString("hdfs.arrow.historical.dcu.base")

    val output_product_name = s"$outputBase/product/list"
    val output_product_cat = s"$outputBase/product/category"
    val output_product_char = s"$outputBase/product/character"

    val outputForDCUEvent = s"$outputBase/dcuevents/date=$dirDateString"
    val outputForBluekai = s"$outputBase/bluekai/date=$dirDateString"

    val bkLoc = configs.getString("hdfs.arrow.bluekai")
    val bkeDateLoc = s"$bkLoc/date=$dirDateString/*"

    val dcuevent_path = configs.getString("hdfs.sf.dcu_events")                 //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."EVENTS"

    //If no rundate, means load all of DCU events
    val dcueventDateLoc = if(!loadAll) s"$dcuevent_path/date=$dirDateString" else s"$dcuevent_path/*"

    val dcuusers_path = configs.getString("hdfs.sf.dcu_users")                  //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."USERS"
    val dcuemails_path = configs.getString("hdfs.sf.dcu_emails")                //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."USER_EMAILS"
    val dcuorders_path = configs.getString("hdfs.sf.dcu_orders")                //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."SHOP_ORDER_COMPLETED"
    val dcusubscriptions_path = configs.getString("hdfs.sf.dcu_subscriptions")  //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."DCU_SUBSCRIBER_STATUS"
    val dcumerchants_path = configs.getString("hdfs.sf.dcu_merchants")          //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."PRODUCT_VIEW"
    val dcuseries_path = configs.getString("hdfs.sf.dcu_series")                //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."SERIES"
    val dcuepisodes_path = configs.getString("hdfs.sf.dcu_episodes")            //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."EPISODE"
    val dcuwatched_path = configs.getString("hdfs.sf.dcu_watched")              //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."WATCHED"
    val dcucomics_path = configs.getString("hdfs.sf.dcu_comics")              //snowflake table: "CIDW_PUBLISH"."DCU_VIEWS"."COMICS"

    val id_maps_path_base = s"$outputBase/maps/id"
    val cast_maps_path_base = s"$outputBase/maps/cast"

    //We can specify which date of maps we choose to load. By default is the rundate, unless the dateField is provided
    var latest_maps_rundate = if(dateField.isEmpty) rundate else dateField
    val latest_maps_dateoutputStr = latest_maps_rundate.substring(0,4)+"-"+latest_maps_rundate.substring(4,6)+"-"+latest_maps_rundate.substring(6,8)

    val idmapDateLoc = s"$id_maps_path_base/date=$latest_maps_dateoutputStr"
    val castmapDateLoc = s"$cast_maps_path_base/date=$latest_maps_dateoutputStr"

    val bk_site_id = configs.getStringList("dcu.site_id").toList
    val blk = if(!loadAll) loadBlukaiLogs(spark, bkeDateLoc, numberOfPartitions, bk_site_id) else createEmptyDataframe(spark)

    //Load id and cast maps
    val idmapsFull = BuildMaps.loadIDMapsAll(idmapDateLoc, spark)
    val castmapsFull = BuildMaps.loadCastMapsAll(castmapDateLoc, spark)

    //Load dcu data
    val dcuevents = loadDCUEvents(dcueventDateLoc, numberOfPartitions)
    val dcuusersFull = spark.read.parquet(dcuusers_path)
    val dcuemailsFull = spark.read.parquet(dcuemails_path)
    val dcuordersFull = loadDCUOrdersAll(dcuorders_path)
    val dcuwatchedFull = loadDCUWatchedAll(dcuwatched_path)
    val dcusubscriptionsFull = spark.read.parquet(dcusubscriptions_path)
    val dcumerchantsFull = spark.read.parquet(dcumerchants_path)
    val dcuseriesFull = spark.read.parquet(dcuseries_path)
    val dcuepisodesFull = spark.read.parquet(dcuepisodes_path)
    val dcucomicsFull = spark.read.parquet(dcucomics_path)

    //Flow logics
    //Step 0: Dedeup, filter or clean up data set first
    //dedup the bluekai records
    val blk_dedup = if(!loadAll) removeBlukaiDuplicates(blk) else createEmptyDataframe(spark)

    //filter and dedup dcu events
    val dcuevents_filter = eventsFilterbyName(dcuevents)
    val dcuevents_dedup = removeEventsDuplicates(dcuevents_filter)

    //filter user subscriptions
    val dcusubscriptions_dedup = removeSubscriptions(dcusubscriptionsFull)

    //Step 1: Create User Table
    //build the user tables to share between blukai and dcu events later
    val user_table = buildUserTable(dcuusersFull, dcuemailsFull, dcusubscriptions_dedup, dcumerchantsFull)

    //Step 2: Create ID maps
    //build id maps to enrich dcu events later
    //val id_maps_full_latest = BuildMaps.buildIdMaps(blk_dedup, idmapsFull, days_to_expire)

    //Step 3: Create Cast maps
    //build cast maps to enrich dcu events later
    //val cast_maps_full_latest = BuildMaps.buildCastMaps(blk_dedup, castmapsFull)

    //Step 4: Enrich bluekai logs
    //Enrich bluekai with user informations, and will output to files later
    val blk_enriched_final = if(!loadAll) enrichUserInfotoBlukai(blk_dedup, user_table) else createEmptyDataframe(spark)

    //Step 5: Enrich dcu events
    //Enrich events with DMA information
    val dcuevents_add_dma = enrichDMAtoEvents(spark, dcuevents_dedup, mmdbFilename, configs, false)
    //Enrich events with blukai ids
    val dcuevents_add_blkIds = enrichBlkIdtoEvents(dcuevents_add_dma, idmapsFull)
    //Enrich events with cast information
    val dcuevents_add_cast = enrichCasttoEvents(dcuevents_add_blkIds, castmapsFull)

    //Step 6: Enhance events with more information
    //Enhance dcu events with user information
    val dcuevents_with_users = enhanceUserInfotoEvents(dcuevents_add_cast, user_table)
    //Enhance dcu events with order and product information
    val dcuevents_with_orders = enhanceProductInfotoEvents(dcuevents_with_users, dcuordersFull)
    //Enhance dcu events with watched and media information
    val dcuwatched_with_media = enhanceMediaInfotoWatched(dcuwatchedFull, dcuusersFull, dcuseriesFull, dcuepisodesFull)
    val dcuevents_with_watched = enhanceWatchedInfotoEvents(dcuevents_with_orders, dcuwatched_with_media)
    //Enhance dcu events with comics title and genre information
    val dcueveents_with_comics = enhanceComicsInfotoEvents(dcuevents_with_watched, dcucomicsFull)

    //Step 7: Populate action field for dcu events
    val dcuevents_with_actions = populateUserActiontoEvents(dcueveents_with_comics)

    //Step 8: Consolidate including add and rename columns
    val dcuevents_with_more_cols = columnsCosolidatedtoEvents(dcuevents_with_actions)

    //Step 9: Remove, Add and Rename columns
    val dcuevents_rename_cols = mapToArrowFields(dcuevents_with_more_cols, DCUConstants.colsToRename)
    val dcuevents_remove_cols = columnsRemoveFromDataFrame(dcuevents_rename_cols, DCUConstants.colsToRemove)
    val dcuevents_add_cols = columnAddToDataFrame(dcuevents_remove_cols, DCUConstants.colsToAdd)

    //Step 10: Validate and Dedup dcu events
    val dcuevents_validate = validateFieldValuesToEvents(dcuevents_add_cols, spark)
    val dcuevents_trim_records = deDupPerAssetNametoEvents(dcuevents_validate)

    //Finally, Hash the compositekey field
    val dcuevents_final = hashCompositeKeytoEvents(dcuevents_trim_records, spark)

    //Step 11: Output to files
    //0) Output id & cast maps
    //Utils.writeToTarget(id_maps_full_latest, numberOfOutputFiles, id_maps_path)
    //Utils.writeToTarget(cast_maps_full_latest, numberOfOutputFiles, cast_maps_path)

    //1) Output product names, categories and characters as csv files
    //generate latest product names and categories
    val dcuproductnames = generateProductName(dcuordersFull)
    val dcuproductcats = generateProductCat(dcuordersFull)
    //output to csv files
    writeToTargetCSV(dcuproductnames, output_product_name)
    writeToTargetCSV(dcuproductcats, output_product_cat)

    //2) Output bluekai logs
    //output to folder with date
    if(!loadAll) writeToTarget(blk_enriched_final, numberOfOutputFiles, outputForBluekai)

    //3) Output dcu events
    //output to folder with date
    writeToTarget(dcuevents_final, numberOfOutputFiles, outputForDCUEvent)

  }

  def loadBlukaiLogs(spark: SparkSession, bkeDateLoc: String, numberOfPartitions: Int, bk_site_id: List[String]): DataFrame = {
    val fileDateUDF = spark.udf.register("fileDate", Util.getFileDate _)

    //get blukai logs, from dev first.
    //on production, load blukai logs per the date,
    //and then filter by the site ids in:
    // 56137 (DCDigitalWeb)
    // 60276 (DCUniverseAndroid)
    // 60274 (DCDigitaliOS)
    // OR filter per WBUID (which is not null)
    val bkdf = spark.read.parquet(bkeDateLoc).coalesce(numberOfPartitions)
      //.filter(col("phints.WBUID").isNotNull)
      //.filter(col("site_id") isin ("56137", "60276", "60274"))
      .filter(col("site_id") isin (bk_site_id.toSeq: _*))
      .withColumn("seen", fileDateUDF(input_file_name))

    bkdf

  }

  //Load events from same load time, can includes many event dates
  def loadDCUEvents(dcueventsDateLoc: String, numberOfPartitions: Int): DataFrame = {
    //get events, from dev first.
    //on production, load all the events,
    //val dcu2 = dcu1.withColumn("date", $"SEGMENT_TIMESTAMP".cast(DateType))
    val dcueventsdf = spark.read.parquet(dcueventsDateLoc).coalesce(numberOfPartitions).withColumn("event_date", col("SEGMENT_TIMESTAMP").cast(DateType))
        .filter(col("user_id").isNotNull)

    dcueventsdf

  }

  def loadDCUOrdersAll(dcuorders_path: String): DataFrame = {
    //get all dcu orders, from dev first
    val dcuordersdfFull = spark.read.parquet(dcuorders_path).withColumn("order_date", col("SEGMENT_TIMESTAMP").cast(DateType))

    dcuordersdfFull

  }

  //Load all watched records.
  def loadDCUWatchedAll(dcuwatched_path: String): DataFrame = {
    //get dcu watched records within date, from dev first
    val dcuwatcheddf = spark.read.parquet(dcuwatched_path).withColumn("watched_date", col("VIEW_DATE").cast(DateType))

    dcuwatcheddf

  }

  // The characters can be a manullay managed static file, currently it only has no more than 20 records
  def generateProductCharacters(): DataFrame = {
    //get pre-defined characters to search products, from dev first

    createEmptyDataframe(spark)

  }

  //The product list can be refreshed every day through this function, and overwrite to same folder
  def generateProductName(dcuorders: DataFrame): DataFrame = {
    val product_name = dcuorders.select("product_name").distinct.orderBy("product_name")

    //output to overwrite previous files

    product_name

  }

  //The product categories can be refreshed every day through this function, and overwrite to same folder
  def generateProductCat(dcuorders: DataFrame): DataFrame = {
    val product_cat = dcuorders.select("category").distinct.orderBy("category")

    //output to overwrite previous files

    product_cat

  }

  def removeSubscriptions(subscriptions: DataFrame): DataFrame = {
    //take the lastest status from the subscription history for an user
    val w = Window.partitionBy(col("USER_GUID")).orderBy(col("TRANSITION_DTTM").desc)

    subscriptions.withColumn("rownum", row_number.over(w)).where(col("rownum") === 1).drop("rownum")

  }

  def removeBlukaiDuplicates(blukai: DataFrame): DataFrame = {
    //Add title column which sync with comic, movie or tv series title, and then sort per time, WBUID and the title
    val blukai_reduced = blukai
      .withColumn("comic_book_title", col("phints.WBCmcTitle"))
      .withColumn("movie_title", col("phints.WBMvTitle"))
      .withColumn("series_title", col("phints.WBTvSrsTitle"))
      .withColumn("website_section", col("phints.WBSiteSec"))
      .withColumn("people", col("phints.WBCelebrity"))
      .withColumn("media_title",
        when(col("phints.WBCmcTitle").isNotNull, col("phints.WBCmcTitle"))
          .when(col("phints.WBMvTitle").isNotNull, col("phints.WBMvTitle"))
          .when(col("phints.WBTvSrsTitle").isNotNull, col("phints.WBTvSrsTitle"))
          .otherwise(col("phints.WBSiteSec"))
      )

    //take the lastest record for an user who is related to same title
    val w = Window.partitionBy(col("phints.WBUID"), col("media_title")).orderBy(col("time_stats.time").desc)

    val blukai_dedup = blukai_reduced.withColumn("rownum", row_number.over(w)).where(col("rownum") === 1).drop("rownum")

    blukai_dedup.drop("media_title")

  }

  def buildUserTable(users: DataFrame, emails: DataFrame, subscriptions: DataFrame, merchantplans: DataFrame): DataFrame = {
    //join subscription with merchant first
    val merchant_trim = merchantplans.select(
                    col("id").alias("merchant_id"), col("merchant_plan_id"),
                    col("name").alias("merchant_plan_name"))
    val subscriptions_add_merchant = subscriptions.join(merchant_trim, subscriptions("product_id") === merchant_trim("merchant_id"), "left")

    val user_table = users.join(emails, users("user_id") === emails("user_id"), "left")
      .join(subscriptions_add_merchant, users("user_guid") === subscriptions_add_merchant("user_guid"), "left")
      .select(users("username").alias("user_name"), users("user_id"), users("user_guid"), users("date_joined"),
        emails("first_name"), emails("last_name"), lower(emails("email")).alias(Constants.EMAIL_ADDRESS_KEY),
        subscriptions_add_merchant("premium_status"), subscriptions_add_merchant("is_premium"), subscriptions_add_merchant("premium_state"),
        subscriptions_add_merchant("subscriber_state"), subscriptions_add_merchant("transition_dttm").alias("subscriber_transition_dt"),
        subscriptions_add_merchant("merchant_type"), subscriptions_add_merchant("merchant_plan_id"), subscriptions_add_merchant("merchant_plan_name"))
      .distinct

    user_table

  }

  def eventsFilterbyName(dcuevents: DataFrame): DataFrame = {
    //Events we are interested at, this can be expanded or updated
    val event_names = List("tile_clicked","cta_clicked","video_started","watch_trailer_clicked","download_completed","order_completed", "search_item_clicked", "thread_clicked")

    val dcuevents_filter = dcuevents.filter(col("event_name") isin (event_names:_*))

    dcuevents_filter
  }

  def removeEventsDuplicates(dcuevents: DataFrame): DataFrame = {
    //TO DO: remove duplicated events, for say, if an user watched or read the same title repeatedly during a day
    dcuevents
  }

  def enrichUserInfotoBlukai(blukai: DataFrame, userinfo: DataFrame): DataFrame = {
    val blukai_enrich = blukai
      .join(userinfo, blukai("phints.WBUID") === userinfo("user_guid"), "left")
      .drop(userinfo("is_premium"))
      .withColumn(Constants.WBCID_KEY, sha2(sha2(lower(trim(col(Constants.EMAIL_ADDRESS_KEY))), 384), 256))
      .withColumn(Constants.TRANSACTION_DATE_KEY, col(s"${Constants.TIME_STATS_KEY}.${Constants.TIME_KEY}"))
      .withColumn("maid",
        when(col("phints.idfa").isNotNull && col("phints.idfa") =!= "", col("phints.idfa"))
          .when(col("phints.adid").isNotNull && col("phints.adid") =!= "", col("phints.adid")))
      .withColumn("sitename", lit("DCU"))
      .withColumn("siterollup", lit("DCU"))

    blukai_enrich.filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull).drop("record").drop("seen")

  }

  def enrichDMAtoEvents(spark: SparkSession, dcuevents: DataFrame, mmdbFilename: String, configs: Config, isLocal: Boolean): DataFrame = {
    val ipindex = 12

    if(!isLocal) {
      GeoEnricher.enrich(spark, dcuevents, ipindex, mmdbFilename, configs)

    } else {
      //for local only, to satisfy the tests.
      GeoEnricher.enrich(spark, dcuevents, ipindex, MaxmindFileServer.getMaxMindFileForDate("20190127").replace("file:", ""), configs)
    }

  }

  def enrichBlkIdtoEvents(dcuevents: DataFrame, idmaps: DataFrame): DataFrame = {
    def remove_string: String => String = _.replaceAll("\\+", "")
    def remove_string_udf = udf(remove_string)
    def replaceStrfromMapUDF = udf((source: String) => {
      val replaceMap = Map(
        "VideoSeriesDetail" -> "tv series", "MovieDetail" -> "movie", "ComicIssueDetail" -> "", "comic" -> "",
        "ComicsBookDetail" -> "comic", "VideoTVSeriesDetail" -> "tv series", "ComicsSeriesDetail" -> "comic", "ComicSeriesDetail" -> "comic",
        "ComicBookDetail" -> "comic", "ArticleDetail" -> "article", "TopicDetail" -> "", "Hub" -> "", "," -> " ")
      replaceMap.foldLeft(source) {case (acc,(key,value))=>acc.replaceAll(key, value)}})

    val idmaps_expands = idmaps.withColumn("user_guid", explode(col("user_guids")))

    val w = org.apache.spark.sql.expressions.Window.partitionBy(col("user_guid")).orderBy(col("last_seen").desc)

    val idmaps_new = idmaps_expands.withColumn("rownum", row_number.over(w)).where(col("rownum") === 1).drop("rownum")

    //add from dcu first for 'android' and 'ios' per the device type, else then take from bluekai
    val events_enrich = dcuevents
      .join(idmaps_new, dcuevents("user_id") === idmaps_new("user_guid"), "left")
      .withColumn("bluekai_compositekey", concat_ws(",", idmaps_new("compositekeys")))
      .withColumn("idfa", concat_ws(",", idmaps_new("idfas")))
      .withColumn("adid", concat_ws(",", idmaps_new("adids")))
      .withColumn("website_section", replaceStrfromMapUDF(remove_string_udf(concat_ws(",", idmaps_new("website_sections")))))
      .drop("user_guids","user_guid", "compositekeys", "idfas", "adids", "website_sections", "last_seen")
      .withColumn("maid",
        //Get from dcu event first, if empty then fetch from bluekai
        when(col("CONTEXT_DEVICE_ADVERTISING_ID").isNotNull, col("CONTEXT_DEVICE_ADVERTISING_ID"))
          .when(col("idfa").isNotNull && col("idfa") =!= "", col("idfa"))
            .when(col("adid").isNotNull && col("adid") =!= "", col("adid")))

    events_enrich

  }

  def enrichCasttoEvents(dcuevents: DataFrame, castinfo: DataFrame): DataFrame = {
    val tempDF = dcuevents
      .withColumn("asset_type_for_join",
        when(dcuevents("dcu_asset_type").contains("Episode") || dcuevents("dcu_asset_type").contains("video"), lit("TV Series")) otherwise(col("dcu_asset_type")))
      .withColumn("asset_name_for_join",
        when(dcuevents("dcu_parent_asset_name").isNotNull && col("dcu_parent_asset_name") =!= "", col("dcu_parent_asset_name")) otherwise(col("dcu_asset_name")))

    val dcuevents_enrich = tempDF
      .join(castinfo,
        (tempDF("asset_name_for_join") === castinfo("bluekai_content_title")) &&
          tempDF("asset_type_for_join").contains(castinfo("bluekai_content_type")), "left")

    dcuevents_enrich.drop("asset_type_for_join").drop("asset_name_for_join")

  }

  def enhanceUserInfotoEvents(dcuevents: DataFrame, userinfo: DataFrame): DataFrame = {
    val dcuevents_add_userinfo = dcuevents
      .join(userinfo, dcuevents("user_id") === userinfo("user_guid"), "left")
      .drop(userinfo("user_id"))
      .withColumn(Constants.WBCID_KEY, sha2(sha2(lower(trim(col(Constants.EMAIL_ADDRESS_KEY))), 384), 256))

    dcuevents_add_userinfo

  }

  //reminder, the load date can include many order dates.
  def enhanceProductInfotoEvents(dcuevents: DataFrame, orders: DataFrame): DataFrame = {
    val product_info = orders.select(
                          col("order_date"),
                          col("revenue").alias("product_revenue"), col("value").alias("product_value"),
                          col("user_id").alias("product_user_id"), col("category").alias("product_category"),
                          col("price").alias("product_price"), col("quantity").alias("product_quantity"),
                          col("product_name"), col("sku").alias("product_sku"),
                          col("event").alias("product_event"), col("segment_timestamp").alias("product_segment_timestamp"))

    //join from order table, records in orders table are more or equal to records in events
    val dcevents_add_productinfo = dcuevents
      .join(product_info,
        dcuevents("user_id") === product_info("product_user_id") &&
        dcuevents("event_name") === product_info("product_event") &&
        dcuevents("segment_timestamp") === product_info("product_segment_timestamp"), "left")
      .drop(product_info("product_user_id")).drop(product_info("product_event")).drop(product_info("product_segment_timestamp"))

    dcevents_add_productinfo

  }

  def enhanceMediaInfotoWatched(watched: DataFrame, users: DataFrame, series: DataFrame, episodes: DataFrame): DataFrame = {
    val watched_new = watched.withColumn("episode_id", concat(col("series_id"), lit('.'), col("episode_number")))
    val watched_add_media = watched_new
      .join(users, Seq("user_id"), "left")
      .join(series, Seq("series_id"), "left")
      .join(episodes, watched_new("episode_id") === episodes("guid"), "left")
      .select(users("user_guid").as("watched_user_guid"),
        watched_new("watched_date"), watched_new("episode_id"), watched_new("site").as("watched_site"), watched_new("episode_number"), watched_new("series_id"), watched_new("view_date"),
        series("title").as("tv_series_title"), series("tv_rating"), series("mpaa_rating"), series("num_episodes").as("total_episode"), series("network"),
        series("season_number").as("total_season"),  series("year").as("series_start_year"), series("end_year").as("series_end_year"),
        series("publish_date").as("series_publish_date"), series("date_added").as("series_date_added"),
        episodes("title").as("episode_title"), episodes("duration").as("episode_duration")
      )

    watched_add_media

  }

  //reminder, the load date can include many watched dates.
  def enhanceWatchedInfotoEvents(dcuevents: DataFrame, watched: DataFrame): DataFrame = {
    //TO DO: remove duplicate records, say if the same user watched same title repeatedly during a day

    val dcuevents_add_watched = dcuevents.join(watched,
          dcuevents("event_date") === watched("watched_date") &&
          dcuevents("user_id") === watched("watched_user_guid") &&
          dcuevents("dcu_asset_name") === watched("episode_title"), "left")

    dcuevents_add_watched
  }

  def enhanceComicsInfotoEvents(dcuevents: DataFrame, comics: DataFrame): DataFrame = {
    val comics_info = comics.select(
      col("title").alias("comics_title"),
      col("series_name").alias("comics_series_title"),
      col("genre").alias("comics_genre"))
      .dropDuplicates("comics_title")

    val dcevents_add_comicinfo = dcuevents
      .join(comics_info,
        dcuevents("dcu_asset_name") === comics_info("comics_title"), "left")
      .drop(comics_info("comics_title"))

    dcevents_add_comicinfo

  }

  def populateUserActiontoEvents(dcuevents: DataFrame): DataFrame = {
    val dcuevents_add_useraction = dcuevents.withColumn("event_type",
      when(col("event_name").equalTo("order_completed"), lit("Purchase"))
        .when(col("event_name").equalTo("download_completed") && (col("dcu_asset_type").contains("Comic") || col("dcu_asset_type").contains("Book")), lit("Download Comic Book"))
        .when(col("event_name").equalTo("download_completed") && col("dcu_asset_type").contains("Episode"), lit("Download Episode Tv Series"))
        .when(col("event_name").equalTo("video_started") && (col("bluekai_content_type").contains("Movie") || col("dcu_asset_type").contains("Movie")), lit("Watch Movie"))
        .when(col("event_name").equalTo("video_started") && (col("bluekai_content_type").contains("TV") || col("dcu_asset_type").contains("Episode") || col("dcu_asset_type").contains("video")), lit("Watch Tv Series"))
        .when(col("bluekai_content_type").contains("Comic") || col("dcu_asset_type").contains("Comic")|| col("dcu_asset_type").contains("Book"), lit("Read Comic Book"))
        .when(col("bluekai_content_type").contains("Article") || col("dcu_asset_type").contains("Article"), lit("Read Article"))
        .when(!col("event_name").equalTo("video_started") && (col("website_section").isNotNull || col("context_page_title").isNotNull), lit("Browse"))
        .otherwise(lit("Action_Not_Confirmed"))

    )

    dcuevents_add_useraction

  }

  //match up column names with Audience
  def columnsCosolidatedtoEvents(dcuevents: DataFrame): DataFrame = {
    val dcuevents_consolidate = dcuevents
      .withColumn("comic_book_title", when(col("dcu_asset_type").contains("Comic"), col("dcu_asset_name")))
      .withColumn("movie_title", when(col("dcu_asset_type").contains("Movie"), col("dcu_asset_name")))
      .withColumn("release_year", when(col("dcu_asset_type").contains("Movie"), col("series_start_year")))
      .withColumn("article_title", when(col("dcu_asset_type").contains("Article"), col("dcu_asset_name")))
      .withColumn("custom_movie_person", when(col("dcu_asset_type").contains("Movie"), split(col("bluekai_content_casts"), "\\,")))
      .withColumn("custom_tv_person", when(col("dcu_asset_type").contains("TV") || col("dcu_asset_type").contains("Episode") || col("dcu_asset_type").contains("video"), split(col("bluekai_content_casts"), "\\,")))
      .withColumn("series_title",
        when(col("tv_series_title").isNotNull && col("tv_series_title") =!= "", col("tv_series_title"))
          .when(col("comics_series_title").isNotNull && col("comics_series_title") =!= "", col("comics_series_title"))
            .when((col("dcu_asset_type").contains("TV") || col("dcu_asset_type").contains("Episode") || col("dcu_asset_type").contains("video")) && col("dcu_parent_asset_name").isNotNull, col("dcu_parent_asset_name"))
              .when((col("dcu_asset_type").contains("TV") || col("dcu_asset_type").contains("Episode") || col("dcu_asset_type").contains("video")) && col("dcu_asset_name").isNotNull, col("dcu_asset_name")))
    .withColumn("video_title",
      when(col("episode_title").isNotNull, col("episode_title"))
        .when(col("event_type").equalTo("Watch Tv Series"), col("dcu_asset_name")))
      .withColumn("rating",
        when(col("mpaa_rating").isNotNull, col("mpaa_rating"))
          .when(col("tv_rating").isNotNull, col("tv_rating")))
      .withColumn("product_title", col("product_name"))
      .withColumn("product_type", col("product_category"))
      .withColumn("person", split(col("bluekai_content_keywords"), "\\,"))
      .withColumn("talent", split(col("bluekai_content_casts"), "\\,"))
      .withColumn(Constants.TRANSACTION_DATE_KEY, col("event_date"))

    dcuevents_consolidate

  }

  def hashCompositeKeytoEvents(dcuevents: DataFrame, spark: SparkSession): DataFrame = {
    val getHash = spark.udf.register("getHash", Attributes.getHashFcn _)

    val dcuevents_add_comp = dcuevents
      .withColumn(Constants.COMPOSITE_KEY, getHash(concat_ws("\t", DCUConstants.COMP_COLS.map(c => dcuevents(c).cast("String")): _*)))

    //Remove records with same compositekey, by choose the latest segment_timestamp
    val w = Window.partitionBy(col(Constants.COMPOSITE_KEY)).orderBy(col("SEGMENT_TIMESTAMP").desc)

    dcuevents_add_comp.withColumn("rownum", row_number.over(w)).where(col("rownum") === 1).
      drop("rownum").drop("SEGMENT_TIMESTAMP").drop("mm_geocode").filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull)

  }

  //Validate field values
  def validateFieldValuesToEvents(dcuevents: DataFrame, spark: SparkSession): DataFrame = {
    val validateIP = spark.udf.register("getValidIP", Attributes.getValidIPFcn _)

    val getValidTime = spark.udf.register("getValidTime", Attributes.getValidTimeFromStringFcn _)

    val tvgenres = List("Superheroes","Crime & Mystery","Crime and Mystery","General","Mystery","Romance", "Fantasy", "Science Fiction", "Horror", "Humor", "Humor,LGBT,General")

    dcuevents
      .withColumn("client_ip", validateIP(col("client_ip")))
      .withColumn("rating", when(col("rating").equalTo("Not Rated"), "Unrated").otherwise(col("rating")))
      .withColumn("comics_genre", when(col("comics_genre") isin (tvgenres:_*), col("comics_genre")) otherwise(lit(null)))
      .withColumn("devcie_type", split(col("device_type"), "\\,"))
      .withColumn(Constants.TIME_STATS_KEY, getValidTime(col(Constants.TRANSACTION_DATE_KEY), lit("yyyy-MM-dd")))

  }

  //We only want one record per asset per day for an user
  def deDupPerAssetNametoEvents(dcuevents: DataFrame): DataFrame = {

    val dcuevents_new = dcuevents.withColumn("sorting_name",
      when(col("dcu_asset_name").isNotNull, col("dcu_asset_name"))
        .when(col("product_title").isNotNull, col("product_title"))
        .when(col("page_title").isNotNull, col("page_title"))
        .otherwise(lit("not_available"))
    )

    //When an user watched, browse, download towards the same asset we only take watched, and so on, within one date.
    val w = org.apache.spark.sql.expressions.Window.partitionBy(col(Constants.TRANSACTION_DATE_KEY), col("user_guid"), col("sorting_name")).orderBy(col("event_type").desc, col("SEGMENT_TIMESTAMP").desc)

    dcuevents_new.withColumn("rownum", row_number.over(w)).where(col("rownum") === 1).drop("rownum").drop("sorting_name")

  }

}