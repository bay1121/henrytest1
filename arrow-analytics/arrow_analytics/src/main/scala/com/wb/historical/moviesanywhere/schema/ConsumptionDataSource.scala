package com.wb.historical.moviesanywhere.schema

import MAConstants.DataFields
import com.wb.constants.Constants
import com.wb.model.DataSource
import org.apache.spark.sql.types._

object ConsumptionDataSource extends DataSource {
  override val rawSchema: StructType = StructType(
    Array(
      StructField("ma_user_key", LongType),
      StructField("ma_user_profile_key", LongType),
      StructField(DataFields.MA_USER_ID, StringType),
      StructField("profile_id", StringType),
      StructField(DataFields.VIEWING_ID, StringType),
      StructField(DataFields.VIEWING_TS, TimestampType),
      StructField(DataFields.VIEWING_DMA_CD, DecimalType(38, 0)),
      StructField(DataFields.VIEWING_COUNTRY_CD, StringType),
      StructField(DataFields.VIEWING_COUNTRY_NAME, StringType),
      StructField(DataFields.VIEWING_REGION, StringType),
      StructField(DataFields.VIEWING_CITY, StringType),
      StructField(DataFields.VIEWING_POSTAL_CD, StringType),
      StructField(DataFields.VIEWING_CONTENT_TYPE, StringType),
      StructField("viewing_content_name", StringType),
      StructField("title_description", StringType),
      StructField(DataFields.TITLE_EIDR_ID, StringType),
      StructField("edit_eidr_id", StringType),
      StructField("content_length_mins", IntegerType),
      StructField("audio_language", StringType),
      StructField("playing_time_ms", LongType),
      StructField(DataFields.CONTENT_VIEW_PCT, DecimalType(5, 2)),
      StructField(DataFields.DEVICE_ADVERTISING_ID, StringType),
      StructField(DataFields.DEVICE_TYPE, StringType),
      StructField(DataFields.DEVICE_MANUFACTURER, StringType),
      StructField("device_model", StringType),
      StructField("device_version", StringType),
      StructField(DataFields.DEVICE_OS, StringType),
      StructField("device_os_version", StringType),
      StructField("client_type", StringType),
      StructField("viewing_resolution", StringType),
      StructField("dw_source_filename", StringType),
      StructField("dw_load_ts", TimestampType),
      StructField("dw_update_ts", TimestampType),
      StructField("dw_data_dt", DateType),
      StructField("dw_etlload_id", IntegerType),
      StructField("dw_row_hash", StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField("dw_row_start_dt", DateType),
      StructField("dw_row_end_dt", DateType)))

  override val schema = StructType(
    Array(
      StructField(DataFields.MA_USER_ID, StringType),
      StructField(DataFields.TITLE_EIDR_ID, StringType),
      StructField(DataFields.VIEWING_ID, StringType),
      StructField(DataFields.VIEWING_TS, TimestampType),
      StructField(DataFields.VIEWING_DMA_CD, DecimalType(38, 0)),
      StructField(DataFields.VIEWING_COUNTRY_CD, StringType),
      StructField(DataFields.VIEWING_COUNTRY_NAME, StringType),
      StructField(DataFields.VIEWING_REGION, StringType),
      StructField(DataFields.VIEWING_CITY, StringType),
      StructField(DataFields.VIEWING_POSTAL_CD, StringType),
      StructField(DataFields.VIEWING_CONTENT_TYPE, StringType),
      StructField(DataFields.CONTENT_VIEW_PCT, DecimalType(5, 2)),
      StructField(DataFields.DEVICE_ADVERTISING_ID, StringType),
      StructField(DataFields.DEVICE_TYPE, StringType),
      StructField(DataFields.DEVICE_MANUFACTURER, StringType),
      StructField(DataFields.DEVICE_OS, StringType),
      StructField(DataFields.DW_CURRENT_FLAG, StringType),
      StructField(DataFields.LOAD_DT, TimestampType)))

  override val arrowMapping = Map[String, String](
    DataFields.DEVICE_ADVERTISING_ID -> Constants.MAID_KEY,
    DataFields.DEVICE_TYPE -> Constants.DEVICE_TYPE_KEY,
    DataFields.VIEWING_COUNTRY_CD -> Constants.MM_COUNTRY_CODE_KEY,
    DataFields.VIEWING_DMA_CD -> Constants.DMA_CODE_KEY,
    DataFields.VIEWING_POSTAL_CD -> Constants.MM_ZIPCODE_KEY,
    DataFields.VIEWING_CONTENT_TYPE -> Constants.CONTENT_TYPE,
    DataFields.VIEWING_TS -> Constants.CONTENT_VIEWED_DATE,
    DataFields.CONTENT_VIEW_PCT -> Constants.CONTENT_VIEWED_PERCENT)
}
