package com.wb.enrich.tvseries

import com.wb.constants.Constants
import com.wb.enrich.FlxFanMetadata.cleanupTalentFcn
import com.wb.model.SchemaBuilder
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{ col, collect_set, first }
import org.apache.spark.sql.{ DataFrame, Row, SaveMode, SparkSession }

// TODO: We need an output abstraction injected in. Only being able to write out parquet files
// is very limiting and hard to test...
class TVSeriesMetadataAnalytic(
  val spark: SparkSession,
  val flx: DataFrame,
  val flxGenre: DataFrame,
  val flxCast: DataFrame,
  val flxPersonAll: DataFrame,
  val flxSeason: DataFrame,
  val flxEpisode: DataFrame) {

  import spark.implicits._
  import scala.collection.JavaConverters._

  val cleanupTalent: UserDefinedFunction = spark.udf.register("cleanupHTML", cleanupTalentFcn _)

  def run(): DataFrame = {
    val person = flxPersonAll.filter(col("name").isNotNull)
    val flixsterCastMembers = flx.join(flxCast, flx("id") === flxCast("tv_series_id")).
      join(person, flxCast("movie_person_id") === person("id")).
      select(
        flx("id").alias("id"),
        cleanupTalent(person("name")).alias("person"),
        flxCast("sortkey").alias("sortkey"),
        flxCast("role").alias("role"))

    val directors = flixsterCastMembers.filter(flixsterCastMembers("role") === "DIR").sort(col("sortkey").asc)
    val directorSet = directors.select(col("id"), col("person")).groupBy(col("id")).agg(first(col("person")).alias("director"))

    val topCast = flixsterCastMembers.filter(flixsterCastMembers("role") === "ACT").sort(col("sortkey").asc)

    //movie talent attribute - top5 + director
    val tvTalentSet = flixsterCastMembers.filter((flixsterCastMembers("role") === "ACT") || (flixsterCastMembers("role") === "DIR")).sort(col("sortkey"))
    val tvTalentFiltered = tvTalentSet.filter((col("sortkey") <= 5) || (col("role") === "DIR"))
    val tv_talent_distinct = tvTalentFiltered.distinct()

    val tvTalentRDD = tv_talent_distinct.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val tvTalent = spark.createDataFrame(tvTalentRDD, SchemaBuilder.top_cast_set)

    //top 1
    val topCastFiltered = topCast.filter(col("sortkey") <= 1)
    val topCastDistinct = topCastFiltered.distinct()
    val topCastRDD = topCastDistinct.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val topCastSet = spark.createDataFrame(topCastRDD, SchemaBuilder.top_cast_set)

    //top 3
    val topCastFiltered3 = topCast.filter(col("sortkey") <= 3)
    val topCastDistinct3 = topCastFiltered3.distinct()
    val topCastRDD3 = topCastDistinct3.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val topCastSet3 = spark.createDataFrame(topCastRDD3, SchemaBuilder.top_cast_set)

    //top 5
    val topCastFiltered5 = topCast.filter(col("sortkey") <= 5)
    val topCastDistinct5 = topCastFiltered5.distinct()
    val topCastRDD5 = topCastDistinct5.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val topCastSet5 = spark.createDataFrame(topCastRDD5, SchemaBuilder.top_cast_set)

    //top 10
    val topCastFiltered10 = topCast.filter(col("sortkey") <= 10)
    val topCastDistinct10 = topCastFiltered10.distinct()
    val top_cast_rdd_10 = topCastDistinct10.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val topCastSet10 = spark.createDataFrame(top_cast_rdd_10, SchemaBuilder.top_cast_set)

    var castSets = topCastSet.withColumnRenamed("cast", "cast_top_1").
      join(topCastSet3.withColumnRenamed("cast", "cast_top_3"), Seq("id"), "full_outer").
      join(topCastSet5.withColumnRenamed("cast", "cast_top_5"), Seq("id"), "full_outer").
      join(topCastSet10.withColumnRenamed("cast", "cast_top_10"), Seq("id"), "full_outer").
      join(tvTalent.withColumnRenamed("cast", Constants.TV_TALENT_KEY), Seq("id"), "full_outer")

    //All cast
    val topCastDistinctAll = flixsterCastMembers.distinct
    val roles = topCastDistinctAll.drop("sortkey").select("role").map(x => x.getString(0)).distinct.collectAsList()
    val topCastRDDAll = topCastDistinctAll.groupBy("id", "role").agg(collect_set("person").alias("cast_set"))

    roles.asScala.foreach { r: String =>
      castSets = castSets.join(topCastRDDAll.filter(col("role") === r).drop("role").withColumnRenamed("cast_set", s"cast_all_${r.toLowerCase}"), Seq("id"), "full_outer")
    }

    val allGenre = flx.join(flxGenre, flx("genre_id") === flxGenre("id")).
      select(flx("id").alias("tv_series_id"), flx("title").alias("tv_series_title"), flxGenre("name").alias("tv_series_genre"))
    val seriesCastAll = allGenre.groupBy(allGenre("tv_series_id"), allGenre("tv_series_title")).agg(collect_set("tv_series_genre").alias("tv_series_genres")).
      join(castSets, allGenre("tv_series_id") === castSets("id"), "full_outer").drop(castSets("id")).
      join(directorSet, allGenre("tv_series_id") === directorSet("id"), "full_outer").drop(directorSet("id")).where("tv_series_id is not null")

    val season = flxSeason.select("id", "tv_series_id")
      .withColumnRenamed("id", "tv_season_id")
      .where("tv_season_id is not null and tv_series_id is not null")

    val episode = flxEpisode.select("id", "tv_season_id")
      .withColumnRenamed("id", "tv_episode_id")
      .where("tv_episode_id is not null and tv_season_id is not null")

    val id_list = episode.join(season, "tv_season_id")

    seriesCastAll.join(id_list, "tv_series_id").distinct
  }
}
