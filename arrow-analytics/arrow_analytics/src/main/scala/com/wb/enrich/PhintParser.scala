package com.wb.enrich

import java.net.URL

import com.wb.config.ArrowLogger
import com.wb.constants.{ Constants, Phints }
import org.apache.commons.lang.StringEscapeUtils

import scala.collection.JavaConversions._
import scala.util.control.Breaks.{ break, breakable }

case class ParsedPhints(
  eventType: String,
  website: String,
  articleTitle: String,
  videoTitle: String,
  topic: Seq[String],
  dcSiteProperty: Seq[String])

object PhintParser {
  // Main phint extraction
  def getPhintValue(phints: Map[String, String], keys: Array[String], decode: Boolean = true): String = {
    var valueOut: String = null

    try {
      if (phints != null && keys != null) {
        val idx = keys.lastIndexWhere(phints.get(_).isDefined) //get the last key in the array that is present in phints
        if (idx != -1) {
          val key = keys(idx)
          valueOut = if (decode) Attributes.cleanupValue(phints(key)) else StringEscapeUtils.unescapeHtml(phints(key))
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getPhintValue unable to calculate title attribute from: $phints $keys")
        ArrowLogger.log.warn(e)
    }

    valueOut
  }

  // Convenience method to look up Array of phint keys from primary key
  def getPhintValueByKeyFcn(phints: Map[String, String], primaryKey: String, decode: Boolean = false): String = {
    var valueOut: String = null

    try {
      Constants.attributePhintKeys.get(primaryKey) match {
        case Some(keys) =>
          valueOut = getPhintValue(phints, keys, decode)
        case _ =>
      }

    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getPhintValueByKeyFcn failed to find primary key mapping from: $phints $primaryKey")
        ArrowLogger.log.warn(e)
    }

    valueOut
  }

  // Convenience method to look up Array of phint keys from primary key AND FORMAT
  def getPhintValueByKeyAndFormatFcn(phints: Map[String, String], primaryKey: String): Seq[String] = {
    var valueOut = Seq[String]()

    try {
      val phintValue = getPhintValueByKeyFcn(phints, primaryKey)

      if (phintValue != null) {
        valueOut = phintValue.split(",").map(_.trim().split(' ').map(_.capitalize).mkString(" ")).distinct
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getPhintValueByKeyAndFormat unable to calculate phint string array from: $phints $primaryKey")
        ArrowLogger.log.warn(e)
    }

    valueOut
  }

  // Helper method for parse
  def getEventTypeFcn(phints: Map[String, String]): String = {
    var eventType = Constants.BROWSE
    val eventTypes = Constants.eventTypes

    try {
      if (phints != null) {
        var eventDefault = Constants.BROWSE
        val hasCid = phints.getOrElse(Phints.CID, null) != null

        if (hasCid) {
          eventDefault = Constants.MEDIA_IMPRESSION
          eventType = eventDefault
        }

        if (phints.get(Phints.EVENT).orNull != null) {
          val temp = phints(Phints.EVENT)
          eventType = eventTypes.getOrDefault(temp, eventDefault)
        } else if (phints.get(Phints.VID_COMPLETE).orNull != null) {
          val temp = phints(Phints.VID_COMPLETE).toInt
          if (temp >= Constants.VIDEO_COMPLETE_MIN) {
            eventType = Constants.MEDIA_VIDEO_COMPLETE
          }
        } else if (phints.get(Phints.FN_PG_LVL_1).orNull != null) {
          if (phints(Phints.FN_PG_LVL_1).toLowerCase == "video-player") {
            eventType = Constants.VIEWED_TRAILER
          }
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getEventType unable to calculate eventType attribute from: $phints")
        ArrowLogger.log.warn(e)
    }

    eventType
  }

  // Helper method for parse
  def getWebsiteSectionFcn(phints: Map[String, String]): String = {
    var websiteRollup: String = null

    try {
      if (phints != null) {
        val websiteSection = getPhintValueByKeyFcn(phints, Constants.WEBSITE_SECTION_KEY, decode = true)

        if (websiteSection != null) {
          breakable {
            for (ws <- Constants.websiteRollups) {
              val key = ws._1
              val value = ws._2

              // If this websiteSection matches in the value Array, use this rollup key and stop processing
              if (value.exists(websiteSection.toLowerCase contains _.toLowerCase)) {
                websiteRollup = key
                break
              }
            }
          }
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getWebsiteSection unable to calculate websiteSection attribute from: $phints")
        ArrowLogger.log.warn(e)
    }

    websiteRollup
  }

  // Helper method for parse
  def getArticleTitleFcn(phints: Map[String, String]): String = {
    var title: String = null

    try {
      var tempTitle = getPhintValueByKeyFcn(phints, Constants.ARTICLE_TITLE_KEY, decode = true)

      if (tempTitle == null) {
        val section = phints.getOrElse(Phints.FLX_SITE_SEC, "")

        if (section.toLowerCase() == "article") {
          tempTitle = getPhintValue(phints, Array("__bk_t"))
        }
      }

      if (tempTitle != null) {
        val r = "<<"
        val ior = tempTitle.indexOf(r)

        if (ior > 0) {
          title = tempTitle.substring(0, ior).trim
        } else {
          title = tempTitle
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getArticleTitle unable to calculate title attribute from: $phints")
        ArrowLogger.log.warn(e)
    }

    StringEscapeUtils.unescapeHtml(title)
  }

  // Helper method for parse
  def getGenresFcn(phints: Map[String, String], genres: Seq[String]): Seq[String] = {
    var gens = Seq[String]()

    try {
      if (genres != null) {
        gens = genres
      } else {
        var genreStr = getPhintValueByKeyFcn(phints, Constants.MOVIE_GENRES_KEY)

        if (genreStr != null) {
          if (genreStr contains "++") {
            genreStr = genreStr.replaceAll("\\+\\+", ",")
          }
          if (genreStr contains "+") {
            genreStr = genreStr.replaceAll("\\+", " ")
          }

          if (!(genreStr contains ",")) {
            genreStr = genreStr.replaceAll(" +", ",")
          }
          gens = genreStr.split(",")
        }
      }

      gens = Attributes.normalizeGenres(gens)
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getGenres unable to calculate genres attribute from: $phints $genres")
        ArrowLogger.log.warn(e)
    }

    gens
  }

  // Main parsing method for phints... Extracts relevant data into a case class
  def parse(phints: Map[String, String]): ParsedPhints = {
    ParsedPhints(
      eventType = getEventTypeFcn(phints),
      website = getWebsiteSectionFcn(phints),
      articleTitle = getArticleTitleFcn(phints),
      videoTitle = getPhintValueByKeyFcn(phints, Constants.VIDEO_TITLE_KEY),
      topic = getPhintValueByKeyAndFormatFcn(phints, Constants.TOPIC_KEY),
      dcSiteProperty = getPhintValueByKeyAndFormatFcn(phints, Constants.DC_SITE_PROPERTY_KEY))
  }

  // Helper method for getProductPair
  def getTitleFcn(phints: Map[String, String], mvTitle: String): String = {
    var title: String = null

    try {
      if (mvTitle != null) {
        title = Attributes.cleanupValue(mvTitle, decode = false)
      } else {
        val phintVal = getPhintValueByKeyFcn(phints, Constants.MOVIE_TITLE_KEY, decode = true)

        if (phintVal != null) {
          title = phintVal
        } else {
          if (phints.getOrElse(Phints.WBCOM_CNT_TYPE, "").toLowerCase() == "movies") {
            title = phints.get(Phints.WBCOM_TITLE).orNull
          }
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getTitleFcn unable to calculate title attribute from: $phints $mvTitle")
        ArrowLogger.log.warn(e)
    }

    StringEscapeUtils.unescapeHtml(title)
  }

  // Helper method for getProductPair
  def getSeriesTitleFcn(phints: Map[String, String], srsTitle: String): String = {
    var title: String = null

    try {
      if (srsTitle != null) {
        title = Attributes.cleanupValue(srsTitle, decode = false)
      } else {
        val phintVal = getPhintValueByKeyFcn(phints, Constants.SERIES_TITLE_KEY, decode = true)

        if (phintVal != null) {
          title = phintVal
        } else {
          if (phints.getOrElse(Phints.WBCOM_CNT_TYPE, "").toLowerCase() == "tv series") {
            title = phints.get(Phints.WBCOM_TITLE).orNull
          }
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getSeriesTitleFcn unable to calculate title attribute from: $phints $srsTitle")
        ArrowLogger.log.warn(e)
    }

    StringEscapeUtils.unescapeHtml(title)
  }

  // Helper method for getProductPair
  def getVideoGameTitleFcn(phints: Map[String, String], siteRollup: String, msTitle: String): String = {
    var title: String = null

    try {
      if (phints != null) {
        val phintVal = getPhintValueByKeyFcn(phints, Constants.VIDEO_GAME_TITLE_KEY, decode = true)

        if (phintVal != null) {
          title = phintVal
        } else if (phints.getOrElse(Phints.WBCOM_CNT_TYPE, "").toLowerCase() == "video game") {
          title = phints.get(Phints.WBCOM_TITLE).orNull
        }
      }

      if (title == null && siteRollup == "Game Microsite" && msTitle != null) {
        title = msTitle
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getVideoGameTitleFcn unable to calculate title attribute from: $phints $siteRollup $msTitle")
        ArrowLogger.log.warn(e)
    }

    StringEscapeUtils.unescapeHtml(title)
  }

  def getProductPair(phints: Map[String, String], mvTitle: String, srsTitle: String, siteRollup: String, siteTitle: String): Seq[String] = {
    var productTitle: String = null
    var productType: String = null
    var movie: String = null
    var series: String = null
    var videoGame: String = null
    var comicBook: String = null

    try {
      movie = getTitleFcn(phints, mvTitle)
      series = getSeriesTitleFcn(phints, srsTitle)
      videoGame = getVideoGameTitleFcn(phints, siteRollup, siteTitle)
      comicBook = getPhintValueByKeyFcn(phints, Constants.COMIC_BOOK_TITLE_KEY)

      if (movie != null) {
        productTitle = movie
        productType = Constants.MOVIE
      } else if (series != null) {
        productTitle = series
        productType = Constants.TV_SERIES
      } else if (videoGame != null) {
        productTitle = videoGame
        productType = Constants.VIDEO_GAME
      } else if (comicBook != null) {
        productTitle = comicBook
        productType = Constants.COMIC_BOOK
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getProductPair unable to calculate product title attribute from: $phints")
        ArrowLogger.log.warn(e)
    }

    Seq[String](productType, productTitle, movie, series, videoGame, comicBook)
  }

  def getFandangoIDs(phints: Map[String, String]): Seq[String] = {
    var temp: String = null
    var fnuid: String = null
    var fnuid256: String = null

    try {
      if (phints != null) {
        temp = phints.get(Phints.FN_UID).orNull
      }
      if (temp != null) {
        temp = temp.toLowerCase
        if (temp.contains("-")) {
          fnuid = temp
          fnuid256 = Attributes.sha1Fcn(temp)
        } else {
          fnuid256 = temp
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getFandangoIDs unable to extract fandango user id values: $phints")
        ArrowLogger.log.warn(e)
    }

    Seq[String](fnuid, fnuid256)
  }

  def getTLDValues(bkURL: String, phints: Map[String, String]): Seq[String] = {
    var pair = Seq[String](null, null)
    var url = bkURL

    try {
      if (phints != null) {
        val bkl = phints.get(Phints.BK_L).orNull

        url = Attributes.resolveSourceFcn(bkURL, bkl)
      }

      if (url != null && url.nonEmpty) {
        val host = new URL(url).getHost
        val suffixLoc = host.lastIndexOf(".")
        val suffix = host.substring(suffixLoc + 1)

        if (suffix.length == 2) {
          pair = Seq[String](suffix, suffix.toUpperCase)
        } else if (suffix == "com") {
          pair = Seq[String](suffix, "US")
        } else {
          pair = Seq[String](suffix, null)
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"getTLDValues unable to tld from: $url")
        ArrowLogger.log.warn(e)
    }

    pair
  }

  def getAidPair(x: String): Seq[Any] = {
    var aid: String = null
    var aidType = Phints.AID_OTHER_VAL

    if (x != null) {
      aid = x
      aidType = Phints.AID_BK_VAL
    }

    Seq(aidType, aid)
  }

  def getCategoryValues(categoriesMap: Map[Int, Array[String]], catstr: String, phints: Map[String, String]): Seq[Any] = {
    var temp = Map[String, String]()
    var deviceType: String = null

    try {
      val cats = catstr.split(" ").map(_.toInt)
      for (cat <- cats) {
        val desc = categoriesMap.getOrElse(cat, null)
        if (desc != null) {
          if (desc.length > 2) {
            val key = desc.lift(1).get
            val value = desc.lift(2).get
            temp += key -> value
          }
        }
      }

      temp.foreach {
        case (key, value) =>
          temp -= key
          temp += key.replaceAll(" ", "_").toLowerCase -> value
      }

      if (phints.getOrElse(Phints.IDFA, null) != null) {
        deviceType = Constants.IOS_KEY
      } else if (phints.getOrElse(Phints.ADID, null) != null) {
        deviceType = Constants.ANDROID_KEY
      }

      if (deviceType == null) {
        breakable {
          for (x <- Constants.deviceTypes) {
            val key = x._1
            val values = x._2.intersect(cats)

            if (values.length > 0) {
              deviceType = key
              break
            }
          }
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn(s"Unable to getCategoryValues with: $catstr $phints")
        ArrowLogger.log.warn(e)
    }

    Seq(deviceType, temp, temp.getOrElse(Constants.BROWSER_LANGUAGE_KEY, null))
  }
}
