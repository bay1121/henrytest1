package com.wb.enrich.tvseries

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants
import com.wb.monitoring.Statsd
import com.wb.support.ParquetReader

object TVSeriesMetadata {
  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    parseAndValidateArgs(args)

    // Establish required contexts
    val spark = SparkSession.
      builder().
      appName("FandangoPurchase").
      enableHiveSupport.
      getOrCreate()

    val configs = ConfigFactory.load()

    val outputDir = configs.getString("hdfs.arrow.meta.tvseries")
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt

    val flxTVSeriesLoc = configs.getString(Constants.FX_TV_SERIES_LOC)
    val flxTVSeriesCast = configs.getString(Constants.FX_TV_CAST_LOC)
    val flxEpisode = configs.getString(Constants.FX_TV_EPISODE_LOC)
    val flxSeason = configs.getString(Constants.FX_TV_SEASON_LOC)
    val flxGenre = configs.getString(Constants.FX_GENRE_LOC)
    val flxPersonLoc = configs.getString(Constants.FX_MOVIE_PERSON_LOC)

    val statsd: StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port"))

    val reader = new ParquetReader(spark)

    val analytic = new TVSeriesMetadataAnalytic(
      spark = spark,
      flx = reader.read(flxTVSeriesLoc, schema = None),
      flxGenre = reader.read(flxGenre, schema = None),
      flxCast = reader.read(flxTVSeriesCast, schema = None),
      flxPersonAll = reader.read(flxPersonLoc, schema = None),
      flxSeason = reader.read(flxSeason, schema = None),
      flxEpisode = reader.read(flxEpisode, schema = None))

    val seriesFinal = analytic.run()

    val outputCounter = spark.sparkContext.longAccumulator("TVSeriesMetadata.Output")
    spark.createDataFrame(seriesFinal.rdd.map(row => {
      outputCounter.add(1)
      row
    }), seriesFinal.schema)

    //Write Data to HDFS
    if (numberOfOutputFiles > 0) {
      seriesFinal.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      seriesFinal.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }

    statsd.gauge("TVSeriesMetadata.Output", outputCounter.value)
  }
}
