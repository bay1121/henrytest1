package com.wb.enrich.bluekaienrich

import java.sql.Timestamp

import scala.collection.JavaConversions.mapAsJavaMap

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.broadcast
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.functions.concat_ws
import org.apache.spark.sql.functions.first
import org.apache.spark.sql.functions.length
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.functions.lower
import org.apache.spark.sql.functions.max
import org.apache.spark.sql.functions.sha2
import org.apache.spark.sql.functions.size
import org.apache.spark.sql.functions.trim
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.when
import org.apache.spark.sql.types.ArrayType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.Config
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants
import com.wb.constants.Phints
import com.wb.enrich.Attributes
import com.wb.enrich.PhintParser
import com.wb.maxmind.GeoEnricher
import com.wb.model.SchemaBuilder

object BluekaiEnrichAnalytic {
  private def setEmptyToNull(df: DataFrame): DataFrame = {
    val exprs = df.schema.map { f =>
      f.dataType match {
        case StringType => when(length(col(f.name)) === 0, lit(null: String).cast(StringType)).otherwise(col(f.name)).as(f.name)
        case ArrayType(StringType, true) =>
          when(size(col(f.name)) === 0, lit(null).cast(ArrayType(StringType))).otherwise(col(f.name)).as(f.name)
        case _ =>
          col(f.name)
      }
    }

    val newDF = df.select(exprs: _*)
    newDF
  }

  // Site Data - Rollup of site ID to name and microsite information
  def siteRollup(logs: DataFrame, siteDataFull: DataFrame): DataFrame = {
    val siteData = setEmptyToNull(siteDataFull)

    var bkSite = logs.join(broadcast(siteData), Seq("site_id"), "left_outer")

    if (CommandLineArguments.mode == "production") {
      ArrowLogger.log.info("Running siterollup filter")
      bkSite = bkSite.filter(col(Constants.SITEROLLUP_KEY).isNotNull)
    }

    bkSite
  }

  def filterRelatedEvents(logs: DataFrame): DataFrame = {
    logs.filter(!(
      col(Constants.PHINTS_KEY).getField(Phints.FLX_RLTD_MV_ID).isNotNull ||
      col(Constants.PHINTS_KEY).getField(Phints.FLX_RLTD_TLNT_ID).isNotNull ||
      col(Constants.PHINTS_KEY).getField(Phints.FLX_RLTD_TV_SRS_ID).isNotNull))
  }

  // Regex match is >40 word characters OR dash with characters on both ends
  def filterFandangoTestIDs(logs: DataFrame): DataFrame = {
    logs.filter(
      col(s"${Constants.PHINTS_KEY}.${Phints.FN_UID}").isNull ||
        col(s"${Constants.PHINTS_KEY}.${Phints.FN_UID}").rlike("\\w{40}|.+-.+"))
  }

  def getFlixsterAndFandango(logs: DataFrame, flxfanMetadata: DataFrame): DataFrame = {
    val flxfanColumns = flxfanMetadata.columns.map(x => first(x, ignoreNulls = true).alias(x))

    val fandango = flxfanMetadata
      .withColumn("temp_id", col("fandango_id"))
      .groupBy("temp_id")
      .agg(flxfanColumns.head, flxfanColumns.tail: _*)

    val flixster = flxfanMetadata
      .withColumn("temp_id", col("flixster_id"))
      .groupBy("temp_id")
      .agg(flxfanColumns.head, flxfanColumns.tail: _*)

    val allMoviesFlixster = logs.filter(col("phints").getField("FlxMvId").isNotNull)
    val allMoviesFandango = logs.filter(col("phints").getField("FlxMvId").isNull)
    val joinedFlixster = allMoviesFlixster.join(broadcast(flixster), col("phints").getField("FlxMvId").isNotNull && (col(s"${Constants.PHINTS_KEY}.${Phints.FLX_MV_ID}") === flixster("temp_id")), "left_outer")
    val joinedFandango = allMoviesFandango.join(broadcast(fandango), col("phints").getField("FnMvId").isNotNull && (col(s"${Constants.PHINTS_KEY}.${Phints.FN_MV_ID}") === fandango("temp_id")), "left_outer")

    joinedFlixster.union(joinedFandango).drop("temp_id")
  }

  def run(
    spark: SparkSession,
    statsd: StatsDClient,
    configs: Config,
    rundate: String,
    mmdbFileName: String,
    idMapFull: DataFrame,
    person: DataFrame,
    bkLogs: DataFrame,
    siteDataFull: DataFrame,
    ipblacklist: DataFrame,
    categoriesFull: DataFrame,
    flxfanMetadata: DataFrame,
    googleCampaignFull: DataFrame): Unit = {

    val tvSeriesMetaLoc: String = configs.getString("hdfs.arrow.meta.tvseries")

    val talentFull = person
      .select(col("id").alias("talent_id"), col("name").alias("talent_name"))
      .repartition(1)
    val talentMap = talentFull.rdd
      .keyBy(row => row.getDecimal(0))
      .mapValues(row => row.getString(1))
      .collectAsMap()

    // This function and the talentMap must be here in order to now slow down the job
    // I attempted to make PhintParser a class and pass the talentMap to it,
    // then use that class method as a UDF, but that increased the job run time by 2-3x
    def getPersonFcn(phints: Map[String, String]): String = {
      var person: String = null
      val flxTalent = Array(Phints.FLX_TLNT_ID, Phints.FLX_TLNT_ID_2)
      val fanTalent = Array(Phints.FN_MV_TALENT)

      try {
        var talent = PhintParser.getPhintValue(phints, flxTalent)

        if (talent != null) {
          person = talentMap.get(talent)
          if (person == null && phints.getOrElse(Phints.FLX_TLNT_NAME, null) != null) {
            val value = phints(Phints.FLX_TLNT_NAME)
            person = value.replaceAll("\\+", " ").trim().split(' ').map(_.capitalize).mkString(" ")
          }
        }
        talent = PhintParser.getPhintValue(phints, fanTalent)
        if (talent != null) {
          val value = talent
          val values = value.split("\\|")
          val name = values.lift(1)
          person = name.getOrElse("").replaceAll("\\+", " ").trim().split(' ').map(_.capitalize).mkString(" ")
        }

      } catch {
        case e: Exception =>
          ArrowLogger.log.warn(s"getPersonFcn unable to calculate person attribute from: $phints")
          ArrowLogger.log.warn(e)
      }

      person
    }

    // Establish UDFs
    val extractPhintsUDF = spark.udf.register("extractPhints", PhintParser.parse _)
    val getGenres = spark.udf.register("getGenres", PhintParser.getGenresFcn _)
    val getPerson = spark.udf.register("getPerson", getPersonFcn _)
    val getIsStatisticalID = spark.udf.register("getIsStatisticalID", Attributes.getIsStatisticalID _)
    val resolveArraySource = spark.udf.register("resolveArraySource", Attributes.resolveArraySourceFcn _)

    var logs = bkLogs.withColumn(Constants.TRANSACTION_DATE_KEY, col(s"${Constants.TIME_STATS_KEY}.${Constants.TIME_KEY}"))

    val inputCounter = spark.sparkContext.longAccumulator("BluekaiEnrich.Input")
    logs = spark.createDataFrame(logs.rdd.map(row => {
      inputCounter.add(1)
      row
    }), logs.schema)

    val siteDataDistinct = siteDataFull.distinct()

    var bkSite = siteRollup(logs, siteDataDistinct)

    val postSiteRollupFilterCounter = spark.sparkContext.longAccumulator("BluekaiEnrich.PostSiteRollupFilter")
    bkSite = spark.createDataFrame(bkSite.rdd.map(row => {
      postSiteRollupFilterCounter.add(1)
      row
    }), bkSite.schema)

    bkSite = filterRelatedEvents(bkSite)

    val postRelatedEventsFilterCounter = spark.sparkContext.longAccumulator("BluekaiEnrich.PostRelatedEventsFilter")
    bkSite = spark.createDataFrame(bkSite.rdd.map(row => {
      postRelatedEventsFilterCounter.add(1)
      row
    }), bkSite.schema)

    bkSite = filterFandangoTestIDs(bkSite)

    val postFandangoTestIDFilterCounter = spark.sparkContext.longAccumulator("BluekaiEnrich.PostFandangoTestIDFilter")
    bkSite = spark.createDataFrame(bkSite.rdd.map(row => {
      postFandangoTestIDFilterCounter.add(1)
      row
    }), bkSite.schema)

    // Google Campaign information
    // Reduced columns to only those necessary for join due to bloated nature of data
    val googleCampaign = googleCampaignFull.select(col("campaign"), col("campaign_id")).distinct()

    val bkCampaign = bkSite.withColumn("campaign_id", col("phints.cid"))
    val bkFiltered = bkCampaign.join(broadcast(googleCampaign), Seq("campaign_id"), "left_outer")

    // Add location IP field index is 8
    val withLocDF = GeoEnricher.enrich(spark, bkFiltered, 8, mmdbFileName, configs)
    // End Add Location
    val withLocAndIPBlacklist = withLocDF.join(ipblacklist, Seq(Constants.CLIENT_IP_KEY), "left_outer")

    val repartFlxfanMetadata = flxfanMetadata.repartition(1)
    val joinedFlixsterFandango = getFlixsterAndFandango(withLocAndIPBlacklist, repartFlxfanMetadata)

    // Dynamically build a schema based on the all cast fields in the joined flxfan data
    var modifiedTvSeriesSchema = SchemaBuilder.tvSeriesSchema

    for (column <- joinedFlixsterFandango.columns) {
      if (column.contains("cast_all")) {
        modifiedTvSeriesSchema = modifiedTvSeriesSchema.add(StructField(column, ArrayType(StringType)))
      }
    }

    // Read in Tv metadata with the cast fields
    var tvSeries = spark.read.schema(modifiedTvSeriesSchema).parquet(tvSeriesMetaLoc).repartition(1)

    // Rename case fields to start with series
    for (column <- tvSeries.columns) {
      if (column.contains("cast")) {
        tvSeries = tvSeries.withColumnRenamed(column, "series_" + column)
      }
    }

    tvSeries = tvSeries.withColumnRenamed("tv_talent", "series_talent").drop("tv_episode_id")

    // List of all columns besides Key inside a first function aliased back to the column name
    val tvFirstExpr = tvSeries.dtypes.filter(x => x._1 != "tv_series_id").map(x => first(col(x._1)).alias(x._1))

    tvSeries = tvSeries.groupBy("tv_series_id").agg(tvFirstExpr.head, tvFirstExpr.tail: _*)

    // Add in series data to flxfan data
    val addSeries = joinedFlixsterFandango.join(broadcast(tvSeries), col(s"${Constants.PHINTS_KEY}.${Phints.FLX_TV_SRS_ID}") === tvSeries("tv_series_id"), "left_outer")

    // Add columns with parsed phint values
    var logsEnriched = addSeries
      .withColumn("phintParser", extractPhintsUDF(col(Constants.PHINTS_KEY)))
      .withColumn(Constants.EVENT_TYPE_KEY, col("phintParser.eventType"))
      .withColumn(Constants.WEBSITE_SECTION_KEY, col("phintParser.website"))
      .withColumn(Constants.ARTICLE_TITLE_KEY, col("phintParser.articleTitle"))
      .withColumn(Constants.VIDEO_TITLE_KEY, col("phintParser.videoTitle"))
      .withColumn(Constants.TOPIC_KEY, col("phintParser.topic"))
      .withColumn(Constants.DC_SITE_PROPERTY_KEY, col("phintParser.dcSiteProperty"))
      .drop("phintParser")

    // Add additional columns
    logsEnriched = logsEnriched.withColumn(Constants.MOVIE_GENRES_KEY, getGenres(col("phints"), col("genres")))
      .withColumn(Constants.SERIES_GENRES_KEY, getGenres(lit(null), col("tv_series_genres")))
      .withColumn(Constants.PERSON_KEY, getPerson(col("phints")))
      .withColumn(Constants.FLIXSTER_USER_ID, col("phints.FlxUID"))
      .withColumn(Constants.IS_COOKIE_STATISTICAL, getIsStatisticalID(col("uid")))

    // Map dc_site_property to franchise_characters in Elastic to allow cross record characters
    // This allows DC Web Browse Events to remain on graphs with Movie properties that have Franchise Characters enrichments
    logsEnriched = logsEnriched
      .withColumn("temp_franchise", resolveArraySource(col("franchise_characters"), col(Constants.DC_SITE_PROPERTY_KEY)))
      .drop("franchise_characters")
      .withColumnRenamed("temp_franchise", "franchise_characters")

    val categoriesMap = categoriesFull.rdd
      .keyBy(row => row.getInt(0))
      .mapValues(row => row.getString(3).replaceAll(" --> ", "-->").split("-->"))
      .collect()
      .toMap

    // Add data in pairs or more
    // Maxmind needs to be done in a Sealizable manner
    // Switch to RDD focus for easier manipulation with multiple types and results
    val rows = logsEnriched.rdd.repartition(CommandLineArguments.numberOfPartitions.toInt)
      .map(row => Row.fromSeq(row.toSeq ++
        PhintParser.getProductPair(row.getAs[Map[String, String]]("phints"), row.getAs[String]("title"), row.getAs[String]("tv_series_title"), row.getAs[String]("siterollup"), row.getAs[String]("microsite")) ++
        PhintParser.getAidPair(row.getAs[String]("uid")) ++
        Attributes.getReleaseWindowPair(row.getAs[Map[String, String]]("time_stats").getOrElse("time", null), row.getAs[Timestamp]("theater_release_date")) ++
        PhintParser.getCategoryValues(categoriesMap, row.getAs[String]("categories"), row.getAs[Map[String, String]]("phints")) ++
        PhintParser.getTLDValues(row.getAs[String]("url"), row.getAs[Map[String, String]]("phints")) ++
        PhintParser.getFandangoIDs(row.getAs[Map[String, String]]("phints"))))

    // Convert back to DF using this schema
    val logsEnrichedSchema = StructType(logsEnriched.schema.fields ++ SchemaBuilder.bkLogsEnrichedPartialSchemaFields)

    val logsEnrichedFlat = spark.createDataFrame(rows, logsEnrichedSchema)
      .drop("tv_series_id", "tv_series_title", "tv_series_genres")

    val logsEnrichedColumns = logsEnrichedFlat.columns

    // Enrich flx/fan user data
    val emailMapFn = idMapFull
      .select(col(Constants.FANDANGO_USER_ID_SHA256), col(Constants.EMAIL_ADDRESS_KEY))
      .filter(col(Constants.FANDANGO_USER_ID_SHA256).isNotNull && col(Constants.EMAIL_ADDRESS_KEY).isNotNull)
      .distinct

    val emailMapFlx = idMapFull
      .select(col(Constants.FLIXSTER_USER_ID), col(Constants.EMAIL_ADDRESS_KEY))
      .filter(col(Constants.FLIXSTER_USER_ID).isNotNull && col(Constants.EMAIL_ADDRESS_KEY).isNotNull)
      .distinct

    val emailMapUID = idMapFull
      .select(col(Constants.UID_KEY), col(Constants.EMAIL_ADDRESS_KEY))
      .filter(col(Constants.UID_KEY).isNotNull && col(Constants.EMAIL_ADDRESS_KEY).isNotNull)
      .distinct

    val idMap = idMapFull
      .filter(col(Constants.UID_KEY).isNotNull)
      .select(Constants.UID_KEY, Constants.MAID_KEY, Constants.ADID_KEY, Constants.IDFA_KEY)
      .withColumnRenamed(Constants.MAID_KEY, s"${Constants.MAID_KEY}_temp")
      .withColumnRenamed(Constants.ADID_KEY, s"${Constants.ADID_KEY}_temp")
      .withColumnRenamed(Constants.IDFA_KEY, s"${Constants.IDFA_KEY}_temp")
      .distinct

    val flxUsersFull = logsEnrichedFlat
      .filter(col(Constants.FLIXSTER_USER_ID).isNotNull && col(Constants.FANDANGO_USER_ID_SHA256).isNull)
      .join(emailMapFlx, Seq(Constants.FLIXSTER_USER_ID), "left_outer")
    val flxUsersWithoutEmail = flxUsersFull
      .filter(col(Constants.EMAIL_ADDRESS_KEY).isNull)
      .drop(Constants.EMAIL_ADDRESS_KEY)
      .select(logsEnrichedColumns.head, logsEnrichedColumns.tail: _*)

    val fanUsersFull = logsEnrichedFlat
      .filter(col(Constants.FANDANGO_USER_ID_SHA256).isNotNull && col(Constants.FLIXSTER_USER_ID).isNull)
      .join(emailMapFn, Seq(Constants.FANDANGO_USER_ID_SHA256), "left_outer")
    val fanUsersWithoutEmail = fanUsersFull
      .filter(col(Constants.EMAIL_ADDRESS_KEY).isNull)
      .drop(Constants.EMAIL_ADDRESS_KEY)
      .select(logsEnrichedColumns.head, logsEnrichedColumns.tail: _*)

    val nonUsers = logsEnrichedFlat
      .filter(col(Constants.FANDANGO_USER_ID_SHA256).isNull && col(Constants.FLIXSTER_USER_ID).isNull)
      .union(flxUsersWithoutEmail)
      .union(fanUsersWithoutEmail)
      .join(emailMapUID, Seq(Constants.UID_KEY), "left_outer")
    val nonUsersColumns = nonUsers.columns

    val flxUsersWithEmail = flxUsersFull
      .filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull)
      .select(nonUsersColumns.head, nonUsersColumns.tail: _*)
    val fanUsersWithEmail = fanUsersFull
      .filter(col(Constants.EMAIL_ADDRESS_KEY).isNotNull)
      .select(nonUsersColumns.head, nonUsersColumns.tail: _*)

    // Bring all of the enriched flx/fan data together
    val enrichedUID = nonUsers
      .union(flxUsersWithEmail)
      .union(fanUsersWithEmail)
      .join(idMap, Seq(Constants.UID_KEY), "left_outer")
      .withColumn(
        Phints.IDFA,
        when(
          col(s"${Constants.PHINTS_KEY}.${Phints.IDFA}").isNotNull &&
            col(s"${Constants.PHINTS_KEY}.${Phints.IDFA}") =!= "00000000-0000-0000-0000-000000000000",
          col(s"${Constants.PHINTS_KEY}.${Phints.IDFA}"))
          .when(col(s"${Constants.IDFA_KEY}_temp").isNotNull, col(s"${Constants.IDFA_KEY}_temp")))
      .withColumn(
        Phints.ADID,
        when(
          col(s"${Constants.PHINTS_KEY}.${Phints.ADID}").isNotNull &&
            col(s"${Constants.PHINTS_KEY}.${Phints.ADID}") =!= "00000000-0000-0000-0000-000000000000",
          col(s"${Constants.PHINTS_KEY}.${Phints.ADID}"))
          .when(col(s"${Constants.ADID_KEY}_temp").isNotNull, col(s"${Constants.ADID_KEY}_temp")))
      .withColumn(
        Constants.MAID_KEY,
        when(col(Phints.IDFA).isNotNull, col(Phints.IDFA))
          .when(col(Phints.ADID).isNotNull, col(Phints.ADID))
          .when(col(s"${Constants.MAID_KEY}_temp").isNotNull, col(s"${Constants.MAID_KEY}_temp")))
      .withColumn(
        s"${Constants.MAID_KEY}_stitch",
        when(
          col(Constants.MAID_KEY).isNotNull &&
            col(s"${Constants.MAID_KEY}_temp").eqNullSafe(col(Constants.MAID_KEY)) &&
            !col(s"${Constants.MAID_KEY}").eqNullSafe(col(s"${Constants.PHINTS_KEY}.${Phints.IDFA}")) &&
            !col(s"${Constants.MAID_KEY}").eqNullSafe(col(s"${Constants.PHINTS_KEY}.${Phints.ADID}")),
          lit(true))
          .otherwise(lit(false)))
      .withColumn(
        s"${Constants.EMAIL_ADDRESS_KEY}_stitch",
        when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull, lit(true))
          .otherwise(lit(false)))
      .withColumn(s"${Constants.UID_KEY}_stitch", lit(false))
      .drop(s"${Constants.MAID_KEY}_temp", s"${Constants.IDFA_KEY}_temp", s"${Constants.ADID_KEY}_temp")

    // Rollup Records
    val columnExprs = enrichedUID.columns.map(x => first(x, ignoreNulls = true).alias(x))
    val sitesOfInterest = List(
      "Fandango",
      "Flixster",
      "DC Comics",
      "DC All Access",
      "Vertigo Comics")

    // Sites of Interest (high and low value)
    val highValue = col(Constants.SITEROLLUP_KEY).isin(sitesOfInterest: _*) && (col(Constants.PERSON_KEY).isNotNull || col(Constants.PRODUCT_TITLE_KEY).isNotNull)
    val lowValue = col(Constants.SITEROLLUP_KEY).isin(sitesOfInterest: _*) && col(Constants.PERSON_KEY).isNull && col(Constants.PRODUCT_TITLE_KEY).isNull

    val high = enrichedUID.filter(highValue)
    val hasHigh = high.select(concat_ws("~", col(Constants.UID_KEY), col(Constants.SITEROLLUP_KEY), col("time_stats.hour")).alias("highkey")).distinct
    val highKey = high.withColumn("key", concat_ws("~", col(Constants.UID_KEY), col(Constants.SITEROLLUP_KEY), col(Constants.PERSON_KEY), col(Constants.PRODUCT_TITLE_KEY), col("time_stats.hour")))
    val highFinal = highKey.sort(col("time_stats.time").desc).groupBy("key").agg(columnExprs.head, columnExprs.tail: _*).withColumn("value", lit("HIGH"))

    val low = enrichedUID.filter(lowValue)
    val lowKey = low.withColumn("key", concat_ws("~", col(Constants.UID_KEY), col(Constants.SITEROLLUP_KEY), col("time_stats.hour")))
    val hasLow = lowKey.select("key").distinct
    val filterLow = hasLow.join(hasHigh, hasLow("key") === hasHigh("highkey"), "left_outer").where(col("highkey").isNull).select(hasLow("key"))
    val lowFiltered = lowKey.join(filterLow, Seq("key"), "inner")
    val lowFinal = lowFiltered.sort(col("time_stats.time").desc).groupBy("key").agg(columnExprs.head, columnExprs.tail: _*).withColumn("value", lit("LOW"))

    // Media Campaigns (campaign and unknown)
    val camp = col(Constants.CAMPAIGN_ID_KEY).isNotNull && col(Constants.CAMPAIGN_NAME_KEY).isNotNull
    val unkValue = col(Constants.CAMPAIGN_ID_KEY).isNull && col(Constants.SITEROLLUP_KEY).isNotNull && !col(Constants.SITEROLLUP_KEY).isin(sitesOfInterest: _*)

    val toInt = udf[Int, String](x => if (x != null) x.toInt else 0)
    val campaign = enrichedUID.filter(camp).withColumn("value", lit("CAMPAIGN"))

    // Add key, create integer version of vidcomplete, set inclusion to false
    val campaignKey = campaign
      .withColumn("key", concat_ws("~", col(Constants.UID_KEY), col(Constants.SITEROLLUP_KEY), col(Constants.CAMPAIGN_ID_KEY)))
      .withColumn("vidComplete", toInt(col("phints.vidComplete")))
      .withColumn(Constants.ARROW_INCLUSION_KEY, lit(false))

    // Find max vidcomplete, join max back to original dataset
    val campaignWithMax = campaignKey
      .groupBy("key")
      .agg(max("vidComplete").alias("maxVidComplete"))
      .join(campaignKey, Seq("key"), "right_outer")

    // List of all columns besides Key inside a first function aliased back to the column name
    val campaignFirstExpr = campaignWithMax.dtypes.filter(x => x._1 != "key").map(x => first(col(x._1)).alias(x._1))

    // Find all records where the vidcomplete was max, and grab the first for each key, set inclusion to true
    val campaignOnlyMax = campaignWithMax
      .where("vidComplete = maxVidComplete")
      .groupBy("key")
      .agg(campaignFirstExpr.head, campaignFirstExpr.tail: _*)
      .withColumn(Constants.ARROW_INCLUSION_KEY, lit(true))

    // Union the included row back to the entire dataset
    // Each included row will also have a duplicate row with the inclusion set to false
    // With Maps in the dataset we cannot perform the dataframe.except function which would mitigate the duplicates
    val campaignFinal = campaignOnlyMax.union(campaignWithMax)
      .drop("key").drop("maxVidComplete").drop("vidComplete")

    // Add inclusion to unk records
    val unkRecs = enrichedUID
      .filter(unkValue).withColumn("value", lit("UNK"))
      .withColumn(Constants.ARROW_INCLUSION_KEY, lit(true))

    // Add inclusion to high/low value records
    val allArrowLogs = highFinal
      .union(lowFinal)
      .drop("key")
      .withColumn(Constants.ARROW_INCLUSION_KEY, lit(true))

    // This is the final result set for ARROW_INCLUSION = TRUE, but slightly bloated due to duplicates in campaign data
    var withCamp = campaignFinal
      .union(unkRecs)
      .union(allArrowLogs)

    // This count will be slightly bloated due to the duplicates in campaign data, but not worth doing a .count()
    val arrowInclusionCounter = spark.sparkContext.longAccumulator("BluekaiEnrich.ArrowInclusion")
    withCamp = spark.createDataFrame(withCamp.rdd.map(row => {
      arrowInclusionCounter.add(1)
      row
    }), withCamp.schema)

    // The rest of this is taking noValue/invalid records and putting them back in the data
    // I don't know why we care about these...
    val noValue = enrichedUID
      .filter(!camp && !unkValue)
      .withColumn("value", lit("NONE"))
      .withColumn(Constants.ARROW_INCLUSION_KEY, lit(false))

    val invalids = noValue
      .union(withCamp)
      .withColumn(
        Constants.VALID_EMAIL_ADDRESS_KEY,
        when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull, lit(true))
          .otherwise(lit(false)))
      .withColumn(
        Constants.VALID_MAID_KEY,
        when(col(Constants.MAID_KEY).isNotNull, lit(true))
          .otherwise(lit(false)))
      .withColumn(
        Constants.VALID_IP_KEY,
        when(col(Constants.MM_COUNTRY_CODE_KEY).eqNullSafe("US") && col(Constants.INVALID_CRITERIA_KEY).isNull, lit(true))
          .otherwise(lit(false)))
      .withColumn(
        Constants.NSR_READY_KEY,
        when(col(Constants.VALID_EMAIL_ADDRESS_KEY) || col(Constants.VALID_MAID_KEY) || col(Constants.VALID_IP_KEY), lit(true))
          .otherwise(lit(false)))
      .drop(Constants.INVALID_CRITERIA_KEY)

    // Filter expressions for statistical ids
    val statisticals = col(Constants.IS_COOKIE_STATISTICAL) &&
      ((col(Constants.EMAIL_ADDRESS_KEY).isNull && col(Constants.MAID_KEY).isNull) ||
        (!col(Constants.VALID_EMAIL_ADDRESS_KEY) && !col(Constants.VALID_MAID_KEY)))

    // Expression for null UID
    val UIDNull = when(
      col(Constants.IS_COOKIE_STATISTICAL),
      when(col(Constants.VALID_EMAIL_ADDRESS_KEY) || col(Constants.VALID_MAID_KEY) || col(Constants.VALID_IP_KEY), lit(null))
        .otherwise(col(Constants.UID_KEY)))
      .otherwise(col(Constants.UID_KEY))

    // Clear Invalid PII
    val clearIP = when(col(Constants.VALID_IP_KEY), col(Constants.CLIENT_IP_KEY)).otherwise(lit(null))

    // Build the final set
    val finalSet = invalids
      .filter(!statisticals)
      .withColumn(Constants.UID_KEY, UIDNull)
      .withColumn(Constants.CLIENT_IP_KEY, clearIP)
      .withColumn(
        Constants.WBCID_KEY,
        when(col(Constants.EMAIL_ADDRESS_KEY).isNotNull, sha2(sha2(lower(trim(col(Constants.EMAIL_ADDRESS_KEY))), 384), 256))
          .when(col(Constants.MAID_KEY).isNotNull, sha2(sha2(trim(col(Constants.MAID_KEY)), 384), 256))
          .when(col(Constants.UID_KEY).isNotNull, sha2(sha2(trim(col(Constants.UID_KEY)), 384), 256)))

    val rundate = CommandLineArguments.rundate
    val outputBase = configs.getString("hdfs.arrow.bluekai_enriched")
    val outputDir = s"$outputBase/date=${rundate.substring(0, 4)}-${rundate.substring(4, 6)}-${rundate.substring(6, 8)}"

    // TODO: Inject an output abstraction
    if (CommandLineArguments.numberOfOutputFiles.toInt > 0) {
      finalSet
        .repartition(CommandLineArguments.numberOfOutputFiles.toInt)
        .write
        .parquet(outputDir)
    } else {
      finalSet
        .write
        .parquet(outputDir)
    }

    statsd.gauge("BluekaiEnrich.Input", inputCounter.value)
    statsd.gauge("BluekaiEnrich.PostSiteRollupFilter", postSiteRollupFilterCounter.value)
    statsd.gauge("BluekaiEnrich.PostRelatedEventsFilter", postRelatedEventsFilterCounter.value)
    statsd.gauge("BluekaiEnrich.PostFandangoTestIDFilter", postFandangoTestIDFilterCounter.value)
    statsd.gauge("BluekaiEnrich.ArrowInclusionOutput", arrowInclusionCounter.value)
  }
}
