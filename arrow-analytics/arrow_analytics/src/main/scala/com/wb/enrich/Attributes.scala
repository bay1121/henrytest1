package com.wb.enrich

import java.math.BigInteger
import java.net.{InetAddress, URLDecoder}
import java.security.MessageDigest
import java.sql.Timestamp
import java.util.Locale

import com.google.common.net.InetAddresses
import com.wb.config.ArrowLogger
import com.wb.constants.{Constants, Phints}
import org.apache.commons.lang.StringEscapeUtils
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone, Days}

/**
  * Created by stephanieleighphifer on 2/23/17.
  */
object Attributes {
  val unk: String = null
  val DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"

  def getHashFcn(x:String) : String = {
    // Create md5 of the string
    val digest = MessageDigest.getInstance("MD5")
    val md5hash = digest.digest(x.getBytes).map("%02x".format(_)).mkString
    md5hash
  }

  def getValidIPFcn(x:String) : String = {
    var ip : String = null
    try {
      if (x != null && InetAddresses.isInetAddress(x)) {
        ip = InetAddress.getByName(x).getHostAddress
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("getValidIPFcn unable to determine client ip from: " + x)
        ArrowLogger.log.error(e.printStackTrace())
    }
    ip
  }

  def calculateHourWindowFcn(h:Int) : String = {
    val hourWindow = h match {
      case a if 0 until 4 contains a   => Constants.WINDOW_A
      case b if 4 until 8 contains b   => Constants.WINDOW_B
      case c if 8 until 12 contains c  => Constants.WINDOW_C
      case d if 12 until 16 contains d => Constants.WINDOW_D
      case e if 16 until 20 contains e => Constants.WINDOW_E
      case f if 20 until 24 contains f => Constants.WINDOW_F
      case _                           => null
    }
    hourWindow
  }

  def getValidTimeFcn(x:Long) : Map[String,String] = {
    var time_stats = Map[String,String]()
    try {
      val epochMillis = new DateTime(x).withZone(DateTimeZone.UTC)
      time_stats += "time" -> epochMillis.getMillis.toString
      time_stats += "hour" -> DateTimeFormat.forPattern("H").withZone(DateTimeZone.UTC).print(epochMillis)
      time_stats += "hour_window" -> calculateHourWindowFcn(DateTimeFormat.forPattern("H").withZone(DateTimeZone.UTC).print(epochMillis).toInt)
      time_stats += "month" -> DateTimeFormat.forPattern("M").withZone(DateTimeZone.UTC).print(epochMillis)
      time_stats += "day" -> DateTimeFormat.forPattern("d").withZone(DateTimeZone.UTC).print(epochMillis)
      time_stats += "year" -> DateTimeFormat.forPattern("Y").withZone(DateTimeZone.UTC).print(epochMillis)
      time_stats += "day_of_week" -> DateTimeFormat.forPattern("EEEE").withZone(DateTimeZone.UTC).print(epochMillis)
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("getValidTimeFcn unable to extract time information from: " + x)
        ArrowLogger.log.error(e.printStackTrace())
    }
      time_stats
  }

  //IP Validation
  def getValidTimeFromStringFcn(x:String, format:String) : Map[String,String] = {
    var time_stats = Map[String,String]()
    try {
      if (x != null) {
        val dateFormatted = DateTimeFormat.forPattern(format).withZone(DateTimeZone.UTC).parseDateTime(x)
        val millisToEpoch = dateFormatted.getMillis
        time_stats = getValidTimeFcn(millisToEpoch)
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("getValidTimeFcn unable to extract time information from: " + x)
        ArrowLogger.log.error(e)
    }
    time_stats
  }

  def getValidTimeFromStringDefaultFcn(x:String) : Map[String,String] = {
    getValidTimeFromStringFcn(x,DEFAULT_DATE_FORMAT)
  }

  def resolveProductTypeFcn(code:String) : String = {
    var productType = code match {
      case movie if "M" equals movie => Constants.MOVIE
      case tv if "T" equals tv => Constants.TV_SERIES
      case _ => null
    }

    productType
  }

  def cleanupValue(value:String, decode:Boolean = true) : String = {
    var out : String = value

    try {
      if (out != unk) {
        if (decode && out.contains("%") && !out.endsWith("%") && !out.startsWith("%")) {
          out = URLDecoder.decode(value, "UTF-8").replace("&apos;", "'")
        }

        out = StringEscapeUtils.unescapeHtml(out).trim

      }
    }  catch {
       case e: Exception =>
        ArrowLogger.log.warn("Attributes.cleanupValue unable to further clean value: " + value)
        ArrowLogger.log.warn(e.printStackTrace())
    }

    out
  }

  def getGenresFcn(genre:String,metaGenres:Seq[String]) : Seq[String] = {
    var gens = Seq[String]()
    try {
      if (metaGenres != null) {
        gens = metaGenres
      } else if (genre != null){
        gens = genre.split(";")
      }
      gens = normalizeGenres(gens)
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("Attributes.getGenres unable to calculate genres attribute from: " + genre + " " + metaGenres)
        ArrowLogger.log.warn(e.printStackTrace())
    }

    gens
  }

  def normalizeGenres(gens:Seq[String]) : Seq[String] = {
    if (gens != null) {
      gens.map(_.trim.split(Array('&','/','_'))).map(_.map(_.split(" ").map(_.capitalize).mkString(" ").trim).sorted.mkString("/").trim.replaceAll("/[/]+","")).distinct
    } else {
      gens
    }
  }

  def resolveSourceFcn(x:String,y:String) : String = {
    var out = x
    if (x == null || x.isEmpty) {
      out = y
    }
    out
  }

  def resolveArraySourceFcn(x:Seq[String],y:Seq[String]) : Seq[String] = {
    var out = x
    if (x == null || x.isEmpty) {
      out = y
    }
    out
  }

  def sha1Fcn(x:String): String =
  {
    if (!x.isEmpty)
    {
      try
      {
        var id = x.toUpperCase
        var digest = MessageDigest.getInstance("SHA-1");

        digest.reset();
        var data = digest.digest(id.getBytes("UTF-8"));

        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data)).toLowerCase(Locale.US);
      }
      catch
        {
          case e: Exception =>
            ArrowLogger.log.warn("Attributes.sha1Fcn unable to transform string to hash " + x)
            ArrowLogger.log.warn(e.printStackTrace())
        }
    }

    null
  }

  def getReleaseWindow(days:Integer) : String = {
    var window = unk
    try {
      if(days != null) {
        val ranges = Constants.theatrical_release_windows
        for (x <- ranges) {
          var name = x("name")
          var min = x("min")
          var max = x("max")

          if(((max == null) || (max != null && days < max.asInstanceOf[Int])) &&
            ((min == null) || (min != null && days >= min.asInstanceOf[Int]))) {
            window = name.asInstanceOf[String]
          }
        }
      }
    } catch {
      case e: Exception =>
      ArrowLogger.log.warn("getReleaseWindow unable to calculate release window attribute from: " + days)
      ArrowLogger.log.warn(e.printStackTrace())
    }

    window
  }

  def getReleaseWindowDays(x:String,mvTime:Timestamp) : Integer = {
    var days : Integer = null

    try {
      if(x != null && mvTime != null) {
        val epochMillis = x.toLong
        val eventtime = new DateTime(epochMillis)
        if (mvTime != null) {
          val releaseDate = new DateTime(mvTime.getTime)
          if (releaseDate != null) {
            var window = unk
            val delta = Days.daysBetween(releaseDate, eventtime)

            days = delta.getDays
          }
        }
      }
    } catch {
      case e: Exception =>
      ArrowLogger.log.warn("getReleaseWindowDays unable to calculate days attribute from: " + x + " " + mvTime)
      ArrowLogger.log.warn(e.printStackTrace())
    }

    days
  }

  def getReleaseWindowPair(x:String,mvTime:Timestamp) : Seq[Any] = {
    var pair = Seq[Any](null,null)
    try {
      val days = getReleaseWindowDays(x,mvTime)
      val window = getReleaseWindow(days)
      pair = Seq(window,days)
    } catch {
      case e: Exception =>
      ArrowLogger.log.warn("getReleaseWindowPair unable to calculate release window attributes from: " + x + " " + mvTime)
      ArrowLogger.log.warn(e.printStackTrace())
    }
    pair
  }

  def getIsStatisticalID(x:String) : Boolean = {
    var isStatisticalID = false

    if(x != null && x.matches("^.{5}[ADW6]ts99[eOY9].{5}$")){
      isStatisticalID = true
    }

    isStatisticalID
  }

  def getFnUIDHashFcn(x:String) : String = {
    var hash = unk
    if (x != null) {
      hash = x.toLowerCase
      if (hash.contains("-")) {
        hash = sha1Fcn(hash)
      }
    }
    hash
  }
}
