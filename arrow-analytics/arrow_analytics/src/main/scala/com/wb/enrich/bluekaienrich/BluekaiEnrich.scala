package com.wb.enrich.bluekaienrich

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SparkSession
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants
import com.wb.maxmind.MaxmindFileServer
import com.wb.model.SchemaBuilder
import com.wb.monitoring.Statsd
import com.wb.support.CSVReader
import com.wb.support.ParquetReader

object BluekaiEnrich {

  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }

    if (CommandLineArguments.rundate.isEmpty) {
      throw new RuntimeException("Required attribute HDFS rundate is not provided.")
    }
  }

  def main(args: Array[String]): Unit = {
    // this will set all the state within CommandLineArguments and validate
    parseAndValidateArgs(args)

    val configs = ConfigFactory.load()
    val categoryLoc = configs.getString("hdfs.static.bluekai_categories")
    val moviePersonLoc = configs.getString(Constants.FX_MOVIE_PERSON_LOC)
    val siteMappingLoc = configs.getString("hdfs.static.site_mapping")
    val googleCampaignLoc = configs.getString("hdfs.s3.google.campaign")
    val flxfanLoc = configs.getString("hdfs.arrow.meta.flxfan")
    val idListLoc = configs.getString("hdfs.arrow.idlist")
    val ipblacklistLoc = configs.getString("hdfs.arrow.ipblacklist")

    val statsd: StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port"))

    val rundate = CommandLineArguments.rundate
    val conf = new org.apache.spark.SparkConf()

    val geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate(rundate)
    val mmdbFilename = geoCityLocFile.substring(geoCityLocFile.lastIndexOf("/") + 1)
    conf.set("spark.files", geoCityLocFile)

    val spark = SparkSession
      .builder()
      .config(conf)
      .appName(name = "BluekaiEnrich")
      .getOrCreate()

    val parquetReader = new ParquetReader(spark)
    val csvReaderWithHeaders = new CSVReader(spark, hasHeader = true)
    val csvReaderWithoutHeaders = new CSVReader(spark, hasHeader = false)

    val bkLoc = configs.getString("hdfs.arrow.bluekai")
    val inputDir = s"$bkLoc/date=${rundate.substring(0, 4)}-${rundate.substring(4, 6)}-${rundate.substring(6, 8)}/*"

    BluekaiEnrichAnalytic.run(
      spark = spark,
      statsd = statsd,
      configs = configs,
      rundate = rundate,
      mmdbFileName = mmdbFilename,
      idMapFull = parquetReader.read(idListLoc, schema = None),
      person = parquetReader.read(moviePersonLoc, schema = None),
      bkLogs = parquetReader.read(inputDir, schema = None),
      siteDataFull = csvReaderWithoutHeaders.read(siteMappingLoc, Some(SchemaBuilder.siteSchema)),
      ipblacklist = parquetReader.read(ipblacklistLoc, schema = None),
      categoriesFull = csvReaderWithHeaders.read(categoryLoc, Some(SchemaBuilder.categorySchema), delimiter = "|"),
      flxfanMetadata = parquetReader.read(flxfanLoc, schema = None),
      googleCampaignFull = parquetReader.read(googleCampaignLoc, schema = None))
  }
}
