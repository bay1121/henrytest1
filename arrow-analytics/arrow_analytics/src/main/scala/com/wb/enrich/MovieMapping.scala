package com.wb.enrich

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.constants.Constants

/**
 * Created by sphifer on 1/9/2017.
 */
object MovieMapping {
  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {

    parseAndValidateArgs(args)

    val spark = SparkSession.
      builder().
      appName("MovieMapping").
      getOrCreate()
    val configs = ConfigFactory.load()
    val outputDir = configs.getString("hdfs.arrow.meta.movieid")
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val movieMappingLoc = configs.getString(Constants.FD_TO_FX_MAP_LOC)

    val internal = spark.read.parquet(movieMappingLoc)
    val mapped = internal.filter(col("is_mapped") === true)
    val mapping = mapped.select(
      internal("fan_title_id").alias("fandango_id"),
      internal("flix_title_id").alias("flixster_id")).distinct()

    //Write Data to HDFS
    if (numberOfOutputFiles > 0) {
      mapping.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      mapping.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }
  }
}
