package com.wb.enrich

import java.sql.Timestamp

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.{ Config, ConfigFactory }
import com.wb.config.{ ArrowLogger, CommandLineArguments, Util }
import com.wb.constants.Constants
import com.wb.model.SchemaBuilder
import com.wb.monitoring.Statsd
import org.apache.commons.lang.StringEscapeUtils
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{ DataFrame, Row, SaveMode, SparkSession }
import org.apache.spark.{ SparkConf, SparkContext }
import org.joda.time.DateTime
import org.kohsuke.args4j.{ CmdLineException, CmdLineParser }

import scala.collection.JavaConversions._
import scala.collection.Map
import scala.collection.mutable.ArrayBuffer

/*
Script to enrich bluekai log data with flxfan metadata

/usr/bin/spark-submit --master yarn-client --queue default --num-executors 20 --executor-memory 1G --executor-cores 2     --driver-memory 1G bluekai_enrich.py
*/

object FlxFanMetadata {

  val unk: String = null

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  private def setEmptyToNull(df: DataFrame): DataFrame = {
    val exprs = df.schema.map { f =>
      f.dataType match {
        case StringType => when(length(col(f.name)) === 0, lit(null: String).cast(StringType)).otherwise(col(f.name)).as(f.name)
        case ArrayType(StringType, true) => {
          when(size(col(f.name)) === 0, lit(null: String).cast(ArrayType(StringType))).otherwise(col(f.name)).as(f.name)
        }
        case _ => {
          col(f.name)
        }
      }
    }

    val newDF = df.select(exprs: _*)
    newDF
  }

  def getStaticIMDBAndFranchiseData(spark: SparkSession, configs: Config) = {
    val imdbKeywordLoc = configs.getString("hdfs.s3.imdb.imdb_keywords")
    val flxFranchiseLoc = configs.getString("hdfs.static.flixster_franchise")
    val flxIMDBLoc = configs.getString("hdfs.static.flixster_imdb_map")
    val catmanFranchiseLoc = configs.getString("hdfs.s3.imdb.imdb_catman_franchise_brand")

    val imdbTags = spark.read.
      format("com.databricks.spark.csv").
      option("header", "true").
      option("quote", "\"").
      option("parserLib", "univocity").
      schema(SchemaBuilder.imdbKeywordSchema).
      load(imdbKeywordLoc)

    val franchise = spark.read.
      format("com.databricks.spark.csv").
      option("header", "true").
      option("quote", "\"").
      schema(SchemaBuilder.franchiseSchema).
      load(flxFranchiseLoc).select("flx_movie_id", "property", "franchise", "brands", "characters")

    val flixsterImdbMapping = spark.read.
      format("com.databricks.spark.csv").
      option("header", "true").
      option("quote", "\"").
      schema(SchemaBuilder.flixsterIMDBSchema).
      load(flxIMDBLoc)

    val catmanFranchiseImdbMapping = spark.read.
      format("com.databricks.spark.csv").
      option("header", "true").
      option("quote", "\"").
      option("parserLib", "univocity").
      schema(SchemaBuilder.catmanFranchise).
      load(catmanFranchiseLoc)

    (flixsterImdbMapping, imdbTags, franchise, catmanFranchiseImdbMapping)
  }

  def pairs(str: Row): Array[(String, String)] = {
    var out: ArrayBuffer[(String, String)] = ArrayBuffer[(String, String)]()
    try {
      val key = str.getString(0)
      val values = str.getString(1)
      if (values != null) {
        for (value <- values.split(",")) {
          out.append((key, value.trim()))
        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("pairs unable to extract pair from: " + str)
        ArrowLogger.log.warn(e.printStackTrace())
    }
    out.toArray
  }

  def getRatingFcn(rating: String, isFlixster: Boolean): String = {
    val mpaaRatings = Map[String, String](
      Constants.RATING_G -> Constants.RATING_G,
      Constants.RATING_PG -> Constants.RATING_PG,
      "PG13" -> Constants.RATING_PG13,
      Constants.RATING_R -> Constants.RATING_R,
      "NC17" -> Constants.RATING_NC17,
      Constants.RATING_NR -> Constants.RATING_NR)

    var ratings = Map[String, String]()
    ratings += ("1" -> Constants.RATING_G)
    ratings += ("2" -> Constants.RATING_PG)
    ratings += ("3" -> Constants.RATING_PG13)
    ratings += ("4" -> Constants.RATING_R)
    ratings += ("5" -> Constants.RATING_NC17)
    ratings += ("6" -> Constants.RATING_UNRATED)
    ratings += ("7" -> Constants.RATING_UNRATED)

    var rate: String = null
    try {
      if (isFlixster) {
        rate = ratings(rating)
      } else {
        val unk: String = null
        val fanRating = rating.replaceAll("[\\s-_]", "")
        rate = mpaaRatings(fanRating)
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("getRatingFcn unable to calculate rating attribute from: " + rating + " " + isFlixster)
        ArrowLogger.log.warn(e)
    }
    rate
  }

  def resolveSource(x: Any, y: Any): Any = {
    if (x != null) {
      x
    } else {
      y
    }
  }

  def getReleaseYearFcn(source: Timestamp): String = {
    var year: String = null
    try {
      if (source != null) {
        year = new DateTime(source.getTime).getYear.toString
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("getReleaseYearFcn unable to calculate release year attribute from: " + source)
        ArrowLogger.log.warn(e.printStackTrace())
    }
    year
  }

  def cleanupTalentFcn(x: String): String = {
    var out = x
    try {
      if (x != null) {
        var decoded = StringEscapeUtils.unescapeHtml(x)
        if (decoded != null) {
          var talentNumberLoc = decoded.indexOf("#")
          if (talentNumberLoc > -1) {
            decoded = decoded.substring(0, talentNumberLoc)
          }
        }
        out = decoded
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("cleanupHTMLFcn unable to normalize talent name " + x)
        ArrowLogger.log.warn(e.printStackTrace())
    }

    out
  }

  def main(args: Array[String]): Unit = {

    parseAndValidateArgs(args)
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt

    val configs = ConfigFactory.load()
    val outputDir = configs.getString("hdfs.arrow.meta.flxfan")
    val flxMovieLoc = configs.getString(Constants.FX_MOVIE_LOC)
    val flxStudioLoc = configs.getString(Constants.FX_MOVIE_STUDIO_LOC)
    val flxCastLoc = configs.getString(Constants.FX_MOVIE_CAST_LOC)
    val flxPersonLoc = configs.getString(Constants.FX_MOVIE_PERSON_LOC)
    val flxMovieGenreLoc = configs.getString(Constants.FX_MOVIE_GENRE_LOC)
    val flxGenreLoc = configs.getString(Constants.FX_GENRE_LOC)
    val fnMovieLoc = configs.getString(Constants.FD_MOVIE_DETAIL_LOC)
    val movieMappingLoc = configs.getString("hdfs.arrow.meta.movieid")

    val statsd: StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port"))

    //Start spark session
    val spark = SparkSession.
      builder().
      appName("FlxFanMetadata").
      enableHiveSupport().
      getOrCreate()
    import spark.implicits._

    val cleanupTalent = spark.udf.register("cleanupHTML", cleanupTalentFcn _)
    val getReleaseYear = spark.udf.register("getReleaseYear", getReleaseYearFcn _)
    val getRating = spark.udf.register("getRating", getRatingFcn _)

    //Load Metadata Mapping
    val movie_id_mapping = spark.read.schema(SchemaBuilder.movie_mapping_schema).parquet(movieMappingLoc)

    //Load RS data from HDFS cache
    //Flixster
    val flixster = spark.read.parquet(flxMovieLoc)
    val studio = spark.read.parquet(flxStudioLoc)
    val cast = spark.read.parquet(flxCastLoc)
    val person_all = spark.read.parquet(flxPersonLoc)
    val genre = spark.read.parquet(flxMovieGenreLoc)
    val flixster_genre = spark.read.parquet(flxGenreLoc)

    //Fandango
    val fandango = spark.read.parquet(fnMovieLoc)

    //Join All Flixster data and extract fields of interest
    //Generate cast list

    val person = person_all.filter(col("name").isNotNull)
    val flixster_castmembers = flixster.join(cast, flixster("movie_id") === cast("movie_id")).
      join(person, cast("person_id") === person("id")).
      select(
        flixster("movie_id").alias("id"),
        cleanupTalent(person("name")).alias("person"),
        cast("sortkey").alias("sortkey"),
        cast("role").alias("role"))

    val directors = flixster_castmembers.filter(flixster_castmembers("role") === "DIR").sort(col("sortkey").asc)
    val director_set = directors.select(col("id"), col("person")).groupBy(col("id")).agg(first(col("person")).alias("director"))

    val top_cast = flixster_castmembers.filter(flixster_castmembers("role") === "ACT").sort(col("sortkey").asc)

    //movie talent attribute - top5 + director
    val movie_talent_set = flixster_castmembers.filter((flixster_castmembers("role") === "ACT") || (flixster_castmembers("role") === "DIR")).sort(col("sortkey"))
    val movie_talent_filtered = movie_talent_set.filter((col("sortkey") <= 5) || (col("role") === "DIR"))
    val movie_talent_distinct = movie_talent_filtered.distinct()
    val movie_talent_rdd = movie_talent_distinct.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val movie_talent = spark.createDataFrame(movie_talent_rdd, SchemaBuilder.top_cast_set)

    //top 1
    val top_cast_filtered = top_cast.filter(col("sortkey") <= 1)
    val top_cast_distinct = top_cast_filtered.distinct()
    val top_cast_rdd = top_cast_distinct.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val top_cast_set = spark.createDataFrame(top_cast_rdd, SchemaBuilder.top_cast_set)

    //top 3
    val top_cast_filtered_3 = top_cast.filter(col("sortkey") <= 3)
    val top_cast_distinct_3 = top_cast_filtered_3.distinct()
    val top_cast_rdd_3 = top_cast_distinct_3.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val top_cast_set_3 = spark.createDataFrame(top_cast_rdd_3, SchemaBuilder.top_cast_set)

    //top 5
    val top_cast_filtered_5 = top_cast.filter(col("sortkey") <= 5)
    val top_cast_distinct_5 = top_cast_filtered_5.distinct()
    val top_cast_rdd_5 = top_cast_distinct_5.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val top_cast_set_5 = spark.createDataFrame(top_cast_rdd_5, SchemaBuilder.top_cast_set)

    //top 10
    val top_cast_filtered_10 = top_cast.filter(col("sortkey") <= 10)
    val top_cast_distinct_10 = top_cast_filtered_10.distinct()
    val top_cast_rdd_10 = top_cast_distinct_10.rdd.map(r => Row(r.getDecimal(0), r.getString(1))).
      keyBy(x => x.getDecimal(0)).
      mapValues(x => x.getString(1)).
      groupByKey().map(y => Row(y._1, y._2.toArray.distinct))
    val top_cast_set_10 = spark.createDataFrame(top_cast_rdd_10, SchemaBuilder.top_cast_set)

    var cast_sets = top_cast_set.withColumnRenamed("cast", "cast_top_1").
      join(top_cast_set_3.withColumnRenamed("cast", "cast_top_3"), Seq("id"), "full_outer").
      join(top_cast_set_5.withColumnRenamed("cast", "cast_top_5"), Seq("id"), "full_outer").
      join(top_cast_set_10.withColumnRenamed("cast", "cast_top_10"), Seq("id"), "full_outer").
      join(movie_talent.withColumnRenamed("cast", Constants.MOVIE_TALENT_KEY), Seq("id"), "full_outer")

    //All cast
    val top_cast_distinct_all = flixster_castmembers.distinct
    val roles = top_cast_distinct_all.drop("sortkey").select("role").map(x => x.getString(0)).distinct.collectAsList()
    val top_cast_rdd_all = top_cast_distinct_all.groupBy("id", "role").agg(collect_set("person").alias("cast_set"))
    for (r <- roles) {
      cast_sets = cast_sets.join(top_cast_rdd_all.filter(col("role") === r).drop("role").withColumnRenamed("cast_set", s"cast_all_${r.toLowerCase}"), Seq("id"), "full_outer")
    }

    //Generate Genre Information
    val flixster_genre_join = flixster.join(genre, flixster("movie_id") === genre("movie_id")).
      join(flixster_genre, genre("genre_id") === flixster_genre("id")).
      select(flixster("movie_id").alias("id"), flixster_genre("name").alias("genre"))

    val flixster_genre_set = flixster_genre_join.groupBy(flixster_genre_join("id")).agg(collect_set("genre").alias("genres"))

    //Generate Studio Information
    val flixster_studio = flixster.
      join(studio, flixster("studio_id") === studio("id"), "outer").
      select(
        flixster("movie_id").alias("id"),
        flixster("title").alias("title_flx"),
        flixster("theater_release_date").alias("theater_release_date_flx"),
        flixster("box_office"),
        flixster("mpaa_rating"),
        studio("name").alias("studio_flx"),
        flixster("tomatometer"))

    val flixster_set = flixster_studio.
      join(cast_sets, Seq("id"), "full_outer").
      join(flixster_genre_set, Seq("id"), "full_outer").
      join(director_set, Seq("id"), "full_outer").
      withColumnRenamed("id", "flixster_id").
      withColumnRenamed("director", "director_flx")

    //Extract Fandango fields of interest
    val fandango_select = fandango.select(
      fandango("movie_detail_id").alias("fandango_id"),
      fandango("title").alias("title_fan"),
      fandango("date_released").alias("theater_release_date_fan"),
      fandango("rating").alias("rating_fan"),
      fandango("studio").alias("studio_fan"),
      fandango("directed_by").alias("director_fan"))

    //Join Fandango and Flixster to Movie ID Mapping
    val all_together_now = movie_id_mapping.
      join(fandango_select, Seq("fandango_id"), "outer").
      join(flixster_set, Seq("flixster_id"), "outer")

    //Extract fields of interest and format for output
    val flxfan_formatted = all_together_now.
      withColumn("title", when(col("title_flx").isNotNull, col("title_flx")).otherwise(col("title_fan"))).
      withColumn("theater_release_date", when(col("theater_release_date_flx").isNotNull, col("theater_release_date_flx")).otherwise(col("theater_release_date_fan"))).
      withColumn("release_year", getReleaseYear(col("theater_release_date"))).
      withColumn("rating", when(col("mpaa_rating").isNotNull, getRating(col("mpaa_rating").cast("String"), lit(true))).otherwise(getRating(col("rating_fan"), lit(false)))).
      withColumn("studio", when(col("studio_flx").isNotNull, col("studio_flx")).otherwise(col("studio_fan"))).
      withColumn("director", when(col("director_flx").isNotNull, col("director_flx")).otherwise(col("director_fan"))).
      drop(
        "title_flx", "title_fan",
        "theater_release_date_flx", "theater_release_date_fan",
        "mpaa_rating", "rating_fan",
        "studio_flx", "studio_fan",
        "director_flx", "director_fan")

    // Counters for datadog
    val imdbCatmanFranchiseBrandCounter = spark.sparkContext.longAccumulator("FlxFanMetadata.IMDBCatmanFranchiseBrand")
    val imdbKeywordsCounter = spark.sparkContext.longAccumulator("FlxFanMetadata.IMDBKeywords")
    val flxfanOutputCounter = spark.sparkContext.longAccumulator("FlxFanMetadata.Output")

    /*Get IMDB and Franchise Data from Flixster from the internally available Static Metadata*/
    val imdbAndFranchise = getStaticIMDBAndFranchiseData(spark, configs)
    var flixsterImdbMapping = imdbAndFranchise._1
    val imdbTags = imdbAndFranchise._2
    val franchiseData = imdbAndFranchise._3.cache()
    var catmanFranchiseImdbMapping = imdbAndFranchise._4

    // Set up Accumulators for IMDB counts
    flixsterImdbMapping = spark.createDataFrame(flixsterImdbMapping.rdd.map(row => {
      imdbKeywordsCounter.add(1)
      row
    }), flixsterImdbMapping.schema)

    catmanFranchiseImdbMapping = spark.createDataFrame(catmanFranchiseImdbMapping.rdd.map(row => {
      imdbCatmanFranchiseBrandCounter.add(1)
      row
    }), catmanFranchiseImdbMapping.schema)

    catmanFranchiseImdbMapping = catmanFranchiseImdbMapping.cache()

    /*Associate Flixster to IMDB Mapping*/
    val imdbTagList = imdbTags.flatMap(pairs).toDF("imdb_id", "tag").groupBy("imdb_id").agg(collect_set(col("tag")).alias("imdb_keywords"))
    val flixsterIdWithTags = flixsterImdbMapping.join(imdbTagList, Seq("imdb_id"), "left_outer").select("flx_movie_id", "imdb_id", "imdb_keywords").cache()
    val catmanFranchise = catmanFranchiseImdbMapping.select(col("imdb_title_cd"), col("catman_franchise_cd"), explode(split(col("catman_brand"), ","))).toDF("imdb_id", "catman_franchise_cd", "catman_brand")
    val catmanFranchiseList = catmanFranchise.
      groupBy("imdb_id").agg(
        collect_set(trim(col("catman_brand"))).alias("catman_brands"),
        first(trim(col("catman_brand"))).alias("catman_primary_brand"),
        first(col("catman_franchise_cd")).alias("catman_franchise"))
    val flixsterIdWithTagsAndCatmanFranchise = flixsterIdWithTags.join(catmanFranchiseList, Seq("imdb_id"), "left_outer").select("flx_movie_id", "imdb_id", "imdb_keywords", "catman_franchise", "catman_primary_brand", "catman_brands").cache()

    /*Go through the Flixster data and determine the Franchise, Property,Character and Brand information associated to each Flixster ID*/
    val brands = setEmptyToNull(franchiseData.select("flx_movie_id", "brands").flatMap(pairs).toDF("flx_movie_id", "brand"))
    val brandList = brands.groupBy("flx_movie_id").agg(collect_set(col("brand")).alias("franchise_brands"))
    val characters = setEmptyToNull(franchiseData.select("flx_movie_id", "characters").flatMap(pairs).toDF("flx_movie_id", "character"))
    val characterList = characters.groupBy("flx_movie_id").agg(collect_set(col("character")).alias("franchise_characters"))
    val franchiseAndPropertyList = setEmptyToNull(franchiseData.select("*")).groupBy("flx_movie_id").agg(collect_set(col("property")).alias("franchise_properties"))
    val flixsterFranchiseDetail = setEmptyToNull(brandList.
      join(characterList, Seq("flx_movie_id")).
      join(franchiseAndPropertyList, Seq("flx_movie_id")))

    /*Associate Movie to metadata from IMDB*/
    val movieMetaWithIMDB = flxfan_formatted.join(flixsterIdWithTagsAndCatmanFranchise, (flxfan_formatted("flixster_id") === flixsterIdWithTagsAndCatmanFranchise("flx_movie_id")), "left_outer").drop("flx_movie_id")

    /*Associate Flixster Franchise Details*/
    var movieMetaWithIMDBFranchise = movieMetaWithIMDB.join(flixsterFranchiseDetail, (movieMetaWithIMDB("flixster_id") === flixsterFranchiseDetail("flx_movie_id")), "left_outer").drop("flx_movie_id")

    // Output Accumulator
    movieMetaWithIMDBFranchise = spark.createDataFrame(movieMetaWithIMDBFranchise.rdd.map(row => {
      flxfanOutputCounter.add(1)
      row
    }), movieMetaWithIMDBFranchise.schema)

    //Write Data to HDFS
    if (numberOfOutputFiles > 0) {
      movieMetaWithIMDBFranchise.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      movieMetaWithIMDBFranchise.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }

    statsd.gauge("FlxFanMetadata.IMDBKeywords", imdbKeywordsCounter.value)
    statsd.gauge("FlxFanMetadata.IMDBCatmanFranchiseBrand", imdbCatmanFranchiseBrandCounter.value)
    statsd.gauge("FlxFanMetadata.Output", flxfanOutputCounter.value)
  }
}
