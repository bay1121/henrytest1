package com.wb.enrich

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.Constants
import org.apache.spark.sql.functions.{col, upper, when}
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._

/**
  * Created by Sanush on 10/25/17
  */
object FlxUsrEmailEnrich {
  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }
  
  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    parseAndValidateArgs(args)
    
    val spark = SparkSession.
      builder().
      appName("FxUsrEmailEnrich").
      enableHiveSupport.
      getOrCreate()
      
    val configs = ConfigFactory.load()
    val outputDir = configs.getString("hdfs.arrow.user_email")
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
      
    val flxUserLoc = configs.getString(Constants.FX_USER_USER_LOC)
    val flxEmailLoc = configs.getString(Constants.FX_USER_EMAIL_LOC)

    //Load RS data from HDFS cache
    //Flixster
    val flixUser = spark.read.parquet(flxUserLoc)
    val flixEmail = spark.read.parquet(flxEmailLoc)

    //Join flixter_id from user and email tables
    val fxUserEmail = flixEmail.join(flixUser,Seq("user_id")).
      select(col("country"),col("user_id").alias(Constants.FLIXSTER_USER_ID),col("email"))

    //Filter noisy emails.
    val filterUserEmail = fxUserEmail.
      filter(col("email").isNotNull &&
      col("email").rlike(".+@.+") &&
      !col("email").rlike(".+@(fandango|warnerbros|wb|dcentertainment|timewarner|flixster|_flixster)\\.com$") &&
      !col("email").rlike(".+facebook\\.com$") &&
      !col("email").rlike(".+@flixster-inc\\.+")).distinct()

    val formatCCD = filterUserEmail.
      select(when(upper(col("country")) === "ZR", "CG").
        when(upper(col("country")) === "UK", "GB").
        when(upper(col("country")) === "TP", "TL").
        when(upper(col("country")) === "YU", "CS").
        otherwise(upper(col("country"))).alias(Constants.MM_COUNTRY_CODE_KEY),
        col(Constants.FLIXSTER_USER_ID)).distinct()

    //Write Data to HDFS
    if (numberOfOutputFiles > 0) {
      formatCCD.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      formatCCD.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }
  }
}
