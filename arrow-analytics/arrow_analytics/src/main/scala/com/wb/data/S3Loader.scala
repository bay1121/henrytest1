package com.wb.data

import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, SaveMode, SparkSession}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._
import java.time.{LocalDate, ZoneId}
import java.time.format.DateTimeFormatter


/**
  * Created by sphifer on 1/4/2017.
  */


object LoadGoogleDCM {
  
  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
    
    if (CommandLineArguments.rundate.isEmpty) {
       throw new RuntimeException("Required attribute HDFS rundate is not provided.")
    }
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.
      builder().
      appName("S3Loader").
      getOrCreate()
   
    //Initialize Configuration
    parseAndValidateArgs(args)
    
    val configs = ConfigFactory.load()
    
    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
    val dirDateString = CommandLineArguments.dirDateString
    
    val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
    formatter.withZone(ZoneId.of("UTC"))
    val yesterday = LocalDate.parse(rundate, formatter).minusDays(1)
    val rundateMinusOne = formatter format yesterday

    val inputBase = configs.getString("googe.dcm.campaign.raw")
    var input = s"${inputBase}${rundateMinusOne}_${rundate}*.csv.gz"
    
    val outputDir = configs.getString("hdfs.s3.google.campaign")
     
    if(!dirDateString.isEmpty) {
      val googleCampaignLoc = configs.getString("google.dcm.campaign")

      input = s"$googleCampaignLoc/$dirDateString"
    }

    //Load Data
    //TODO Automate when s3 structure is solidifed
    val gc = spark.read.format("com.databricks.spark.csv").
      option("header", "true").
      load(input)

    val newColNames = gc.columns.map((c: String) => c.toLowerCase().replaceAll(" ", "_"))
    val gcUpdated = gc.toDF(newColNames: _*)
    val gcReduced = gcUpdated.distinct()

    //Store in HDFS

    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      gcReduced.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(outputDir)
    } else {
      gcReduced.write.mode(SaveMode.Overwrite).parquet(outputDir)
    }

  }
}
