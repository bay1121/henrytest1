package com.wb.data

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.seqAsJavaList

import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.SparkSession
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.config.CommandLineArguments
import com.wb.config.Util
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import net.snowflake.spark.snowflake.SnowflakeConnectorUtils

object LoadTables {

  def parseAndValidateArgs(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  def main(args: Array[String]): Unit = {
    //Start spark session
    val spark = SparkSession.
      builder().
      appName("DataLoader").
      getOrCreate()
    parseAndValidateArgs(args)
    val configs = ConfigFactory.load()
    val baseline = CommandLineArguments.baseline.toBoolean
    val rundate = baseline match {
      case true => ""
      case _ => CommandLineArguments.rundate.substring(0, 4) + "-" + CommandLineArguments.rundate.substring(4, 6) + "-" + CommandLineArguments.rundate.substring(6, 8)
    }
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt

    var outputDir = configs.getString("hdfs.rs.root")
    var tableConfig = "rs.tables"
    var userConfig = "rs.user"
    var dfClass = "com.databricks.spark.redshift"

    var options =
      Map(
        "url" -> configs.getString("jdbc.url"),
        "forward_spark_s3_credentials" -> "true")

    CommandLineArguments.dataWarehouse.toString() match {
      case "Snowflake" => {
        SnowflakeConnectorUtils.enablePushdownSession(spark)
        outputDir = configs.getString("hdfs.sf.root")
        tableConfig = "sf.tables"
        userConfig = "sf.user"
        dfClass = "net.snowflake.spark.snowflake"
        options = Map(
          "sfURL" -> (configs.getString("sf.account") + ".snowflakecomputing.com"),
          "sfAccount" -> configs.getString("sf.account"),
          "sfDatabase" -> configs.getString("sf.database"),
          "sfSchema" -> configs.getString("sf.schema"),
          "sfWarehouse" -> configs.getString("sf.datawarehouse"))
      }

      case _ => {

      }
    }

    var user = configs.getString(userConfig)
    var passwd = Util.getSecureCredential(userConfig)

    var tables = configs.getStringList(tableConfig).toList
    if (passwd == null) {
      throw new RuntimeException("Unable to obtain secure credential for Redshift/Snowflake user")
    }

    for (table <- tables) {
      val tableName = table.split('.').takeRight(2).mkString("/")
      /* Removing added table options */
      options = options - "dbtable" - "query"
      var hdfsDir = s"${outputDir}/$tableName/"
      var queryType = "dbtable"
      var queryStatement = table

      if (!baseline && !rundate.isEmpty) {
        try {
          val timeField = configs.getString(s"$table.timeField")
          queryType = "query"
          val start = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyy-MM-dd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(0, 0, 0, 0).getMillis)
          val end = new java.sql.Timestamp(DateTimeFormat.forPattern("yyyy-MM-dd").withZone(DateTimeZone.UTC).parseDateTime(rundate).withTime(23, 59, 59, 0).getMillis)
          queryStatement = "SELECT * FROM " + table + " WHERE " + timeField + " >='" + start + "' AND " + timeField + " <= '" + end + "'"
          hdfsDir = s"${hdfsDir}date=${rundate}"
        } catch {
          case e: Exception =>
            ArrowLogger.log.warn("Time field for table does not exist in configuration. Defaulting to full load. " + tableName)
            ArrowLogger.log.warn(e)
        }
      }

      CommandLineArguments.dataWarehouse.toString() match {
        case "Redshift" =>
          options = options + ("user" -> user, "password" -> passwd, "tempdir" -> (configs.getString("tempdir") + "/" + tableName), queryType -> queryStatement)
        case "Snowflake" =>
          options = options + ("sfUser" -> user, "sfPassword" -> passwd, queryType -> queryStatement,
            "s3MaxFileSize" -> configs.getString("sf.s3MaxFileSize"), "sfCompress" -> configs.getString("sf.compress"))
        case _ =>
      }
      val df = spark.read.format(dfClass).options(options).load()
      try {
        if (numberOfOutputFiles > 0) {
          df.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(hdfsDir)
        } else {
          df.write.mode(SaveMode.Overwrite).parquet(hdfsDir)
        }
      } catch {
        case e: Exception =>
          ArrowLogger.log.error(e)
          ArrowLogger.log.error("Unable to load table " + tableName + " - retrying")

          if (numberOfOutputFiles > 0) {
            df.coalesce(numberOfOutputFiles).write.mode(SaveMode.Overwrite).parquet(hdfsDir)
          } else {
            df.write.mode(SaveMode.Overwrite).parquet(hdfsDir)
          }
      }
    }
  }
}
