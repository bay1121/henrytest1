package com.wb.model

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.DataTypes

trait DataSource {
  val decimalType = DataTypes.createDecimalType(38, 0)
  val schema: StructType
  val rawSchema: StructType
  val arrowMapping: Map[String, String]
}