package com.wb.model

import com.wb.constants.Constants
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object DMADataSource extends DataSource {

  override val rawSchema: StructType = StructType(Array(
    StructField(Constants.MM_SOURCE_ZIPCODE, StringType),
    StructField(Constants.MM_SOURCE_DMA_ZONE, IntegerType),
    StructField(Constants.MM_SOURCE_DMA_NAME, StringType),
    StructField(Constants.MM_SOURCE_WB_DIVISION, StringType),
    StructField(Constants.MM_SOURCE_DMA_RANK, IntegerType)
  ))

  override val schema = StructType(Array(
    StructField(Constants.MM_SOURCE_ZIPCODE, StringType),
    StructField(Constants.MM_SOURCE_DMA_ZONE, IntegerType),
    StructField(Constants.MM_SOURCE_DMA_NAME, StringType),
    StructField(Constants.MM_SOURCE_WB_DIVISION, StringType),
    StructField(Constants.MM_SOURCE_DMA_RANK, IntegerType)
  ))

  override val arrowMapping: Map[String, String] = Map[String, String](
    Constants.MM_SOURCE_DMA_ZONE -> Constants.DMA_CODE_KEY,
    Constants.MM_SOURCE_ZIPCODE -> Constants.MM_ZIPCODE_KEY
  )
}
