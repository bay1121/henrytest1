package com.wb.model

import com.wb.constants.Constants
import org.apache.spark.sql.types.{ StructType, _ }

/**
 * Created by stephanieleighphifer on 1/23/17.
 */
object SchemaBuilder {
  /*
   Define output schema.
   Note: Must be in alphabetical order by key <- Only applies to Python
   */
  val decimalType = DataTypes.createDecimalType(38, 0)

  val bluekaiSchema = new StructType(Array(
    StructField("compositekey", StringType, true),
    StructField("record", StringType, true),
    StructField("op", MapType(StringType, StringType), true),
    StructField("uid", StringType, true),
    StructField("time", StringType, true),
    StructField("categories", StringType, true),
    StructField("site", StringType, true),
    StructField("url", StringType, true),
    StructField("client_ip", StringType, true)))

  val movie_mapping_schema = StructType(Array(
    StructField("fandango_id", decimalType),
    StructField("flixster_id", decimalType)))

  val flxfan_schema = StructType(Array(
    StructField("fandango_id", decimalType, true),
    StructField("flixster_id", decimalType, true),
    StructField("title", StringType, true),
    StructField("theater_release_date", TimestampType, true),
    StructField("rating", StringType, true),
    StructField("director", StringType, true),
    StructField("movie_talent", ArrayType(StringType, true), true),
    StructField("release_year", decimalType, true),
    StructField("box_office", decimalType, true),
    StructField("tomatometer", decimalType, true),
    StructField("fandango", StructType(Array(
      StructField("title", StringType, true),
      StructField("theater_release_date", TimestampType, true),
      StructField("rating", StringType, true),
      StructField("studio", StringType, true),
      StructField("director", StringType, true))), true),
    StructField("flixster", StructType(Array(
      StructField("title", StringType, true),
      StructField("theater_release_date", TimestampType, true),
      StructField("mpaa_rating", decimalType, true),
      StructField("studios", ArrayType(StringType, true), true),
      StructField("cast", ArrayType(MapType(StringType, StringType), true), true),
      StructField("genres", ArrayType(StringType, true), true),
      StructField("director", StringType, true))), true)))

  val cast_schema = StructType(Array(
    StructField("id", decimalType, true),
    StructField(
      "cast",
      ArrayType(MapType(StringType, StringType), true), true)))

  val top_cast_set = StructType(Array(
    StructField("id", decimalType, true),
    StructField("cast", ArrayType(StringType, true), true)))

  val confIdMappingSchema = StructType(Array(
    StructField("source", StringType, false),
    StructField("source_family", StringType, false),
    StructField("source_tag_type", StringType, false),
    StructField("confid", StringType, false)))

  val flixsterUserSchema = StructType(Array(
    StructField("flx_userid", StringType, false),
    StructField("flx_username", StringType, false)))

  val hashTagSchema = StructType(Array(
    StructField("wb_hashtag", StringType, false),
    StructField("wb_talent", StringType, false)))

  val imdbKeywordSchema = StructType(Array(
    StructField("imdb_id", StringType, false),
    StructField("tags", StringType, false)))

  val flixsterIMDBSchema = StructType(Array(
    StructField("flx_movie_id", StringType, false),
    StructField("imdb_id", StringType, false),
    StructField("eidr_id", StringType, false),
    StructField("title", StringType, false)))

  val franchiseSchema = StructType(Array(
    StructField("flx_movie_id", StringType, false),
    StructField("mpm_number", StringType, false),
    StructField("imdb_id", StringType, false),
    StructField("eidr_id", StringType, false),
    StructField("title_type", StringType, false),
    StructField("title", StringType, false),
    StructField("series_title", StringType, false),
    StructField("season_title", StringType, false),
    StructField("property", StringType, false),
    StructField("franchise", StringType, false),
    StructField("brands", StringType, false),
    StructField("steven_franchise", StringType, false),
    StructField("characters", StringType, false),
    StructField("association_titles", StringType, false),
    StructField("association_properties", StringType, false)))

  val tmzCelebritySchema = StructType(Array(
    StructField("celebrity_title", StringType, false),
    StructField("celebrity_slug", StringType, false),
    StructField("celebrity_guid", StringType, false)))

  val tmzCategorySchema = StructType(Array(
    StructField("category_title", StringType, false),
    StructField("category_slug", StringType, false),
    StructField("category_guid", StringType, false)))

  val genreSchema = StructType(Array(
    StructField("fx_genre_key", decimalType, false),
    StructField("id", decimalType, false),
    StructField("name", StringType, true),
    StructField("tags", StringType, true)))

  val movieGenreSchema = StructType(Array(
    StructField("fx_movie_genre_key", decimalType, false),
    StructField("movie_id", decimalType, false),
    StructField("genre_id", decimalType, true)))

  val catmanFranchise = StructType(Array(
    StructField("title_key", StringType, false),
    StructField("title_name", StringType, false),
    StructField("imdb_title_cd", StringType, false),
    StructField("catman_franchise_cd", StringType, false),
    StructField("catman_brand", StringType, false)))

  val domainFlixsterIDMapping = StructType(Array(
    StructField("kpa_domain_file", StringType, false),
    StructField("count", StringType, false),
    StructField("flx_movie_id_file", StringType, false)))

  val siteSchema = StructType(Array(
    StructField("site_id", StringType),
    StructField("sitecountry", StringType),
    StructField("sitename", StringType),
    StructField("microsite", StringType),
    StructField("siterollup", StringType)))

  val categorySchema = StructType(Array(
    StructField("id", IntegerType),
    StructField("node_name", StringType),
    StructField("parent_id", IntegerType),
    StructField("description", StringType)))

  val tvSeriesSchema = StructType(Array(
    StructField("tv_series_id", decimalType),
    StructField("tv_episode_id", decimalType),
    StructField("tv_series_title", StringType),
    StructField("tv_series_genres", ArrayType(StringType)),
    StructField("cast_top_1", ArrayType(StringType)),
    StructField("cast_top_3", ArrayType(StringType)),
    StructField("cast_top_5", ArrayType(StringType)),
    StructField("cast_top_10", ArrayType(StringType)),
    StructField("tv_talent", ArrayType(StringType))))

  val bkLogsEnrichedPartialSchemaFields = Array(
    StructField(Constants.PRODUCT_TYPE_KEY, StringType),
    StructField(Constants.PRODUCT_TITLE_KEY, StringType),
    StructField(Constants.MOVIE_TITLE_KEY, StringType),
    StructField(Constants.SERIES_TITLE_KEY, StringType),
    StructField(Constants.VIDEO_GAME_TITLE_KEY, StringType),
    StructField(Constants.COMIC_BOOK_TITLE_KEY, StringType),
    StructField(Constants.AID_TYPE_KEY, StringType),
    StructField(Constants.AID_KEY, StringType),
    StructField(Constants.RELEASE_WINDOW_KEY, StringType),
    StructField(Constants.RELEASE_WINDOW_DAYS_KEY, IntegerType),
    StructField(Constants.DEVICE_TYPE_KEY, StringType),
    StructField(Constants.CATEGORY_VALUES_KEY, MapType(StringType, StringType)),
    StructField(Constants.BROWSER_LANGUAGE_KEY, StringType),
    StructField(Constants.TLD, StringType),
    StructField(Constants.TLD_COUNTRY_CODE, StringType),
    StructField(Constants.FANDANGO_USER_ID, StringType),
    StructField(Constants.FANDANGO_USER_ID_SHA256, StringType))

  val locSchemaFields = Array(
    StructField(Constants.MM_SYSTEM_SUBDIVISION_NAME_KEY, StringType),
    StructField(Constants.MM_SYSTEM_SUBDIVISION_ISO_CODE_KEY, StringType),
    StructField(Constants.MM_COUNTRY_CODE_KEY, StringType),
    StructField(Constants.MM_COUNTRY_NAME_KEY, StringType),
    StructField(Constants.MM_CITY_KEY, StringType),
    StructField(Constants.MM_ZIPCODE_KEY, StringType),
    StructField(Constants.MM_CONTINENT_KEY, StringType),
    StructField(Constants.MM_GEOCODE_KEY, StringType),
    StructField(Constants.DMA_CODE_KEY, IntegerType),
    StructField(Constants.DMA_NAME_KEY, StringType))
}
