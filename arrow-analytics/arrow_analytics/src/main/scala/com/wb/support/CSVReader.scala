package com.wb.support

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}

class CSVReader(sparkSession: SparkSession, hasHeader: Boolean) extends SparkSqlReader {
  override def read(path: String, schema: Option[StructType]): DataFrame =
    schema match {
      case Some(s) =>
        sparkSession.read.format("csv")
          .option("header", hasHeader.toString)
          .schema(s)
          .load(path)
      case None =>
        throw new IllegalArgumentException("No Schema Provided!")
    }

  def read(path:String, schema: Option[StructType], delimiter:String): DataFrame =
    schema match {
      case Some(s) =>
        sparkSession.read.format("com.databricks.spark.csv")
          .option("header", hasHeader.toString)
          .option("delimiter", delimiter)
          .schema(s)
          .load(path)
      case None =>
        throw new IllegalArgumentException("No Schema Provided!")
    }
}
