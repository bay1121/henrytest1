package com.wb.support

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StructType

trait SparkSqlReader {
  def read(path: String, schema: Option[StructType] = None): DataFrame
}
