package com.wb.support

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}

class ParquetReader(sparkSession: SparkSession) extends SparkSqlReader {
  override def read(path: String, schema: Option[StructType]): DataFrame = {
    schema match {
      case Some(s) =>
        sparkSession.read.schema(s).parquet(path)
      case None =>
        sparkSession.read.parquet(path)
    }
  }
}
