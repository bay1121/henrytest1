package com.wb.support

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}

class JSONReader(sparkSession: SparkSession) extends SparkSqlReader {
  override def read(path: String, schema: Option[StructType]): DataFrame =
    schema match {
      case Some(s) =>
        sparkSession.read.format("json")
          .schema(s)
          .load(path)
      case None =>
        sparkSession.read.json(path)
    }

}
