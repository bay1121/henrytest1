package com.wb.config

object OutputToESCLIArgs {

  var childFields: Array[String] = _
  @org.kohsuke.args4j.Option(name = "-esChildFields", required = false, usage = "The only allowable columns that will be written to ES Children")
  def setEnvironment(value: String): Unit = {
    childFields = value.split(',')

    if (childFields.length == 0) {
      throw new IllegalArgumentException("Expected a comma separated list of columns to write to ES")
    }
  }

  var input: String = _
  @org.kohsuke.args4j.Option(name = "-input", required = true, usage = "Location of input files")
  def setInput(value: String): Unit = {
    if (value.isEmpty) {
      throw new IllegalArgumentException("Input can not be empty!")
    }

    input = value
  }

  @org.kohsuke.args4j.Option(name = "-rundate", required = true, usage = "Specific Hour to Process as YYYYMMddHH")
  var rundate: String = _

  @org.kohsuke.args4j.Option(name = "-esResourceName", required = true, usage = "ElasticSearch data index")
  var esResourceName: String = ""

  @org.kohsuke.args4j.Option(name = "-suggest", required = false, usage = "Flag to indicate suggester index")
  var suggest: String = "false"

  var esSuggestFields: Array[String] = _
  @org.kohsuke.args4j.Option(name = "-esSuggestFields", required = false, usage = "Declare which fields should be used with ElasticSearch suggest")
  def setEsFields(value: String): Unit = {
    esSuggestFields = value.split(',')

    if (esSuggestFields.length == 0) {
      throw new IllegalArgumentException("Expected a comma separated list of columns to write to ES")
    }
  }

  @org.kohsuke.args4j.Option(name = "-esSuggestResourceName", required = false, usage = "ElasticSearch suggester index")
  var esSuggestResourceName: String = ""

  @org.kohsuke.args4j.Option(name = "-esBatchSizeBytes", required = false, usage = "ElasticSearch batch size bytes")
  var esBatchSizeBytes: String = "4M"

  @org.kohsuke.args4j.Option(name = "-esBatchSizeEntries", required = false, usage = "ElasticSearch batch size entries index")
  var esBatchSizeEntries: String = "4000"

  @org.kohsuke.args4j.Option(name = "-esSuggestBatchSizeBytes", required = false, usage = "ElasticSearch batch size bytes")
  var esSuggestBatchSizeBytes: String = "4M"

  @org.kohsuke.args4j.Option(name = "-esSuggestBatchSizeEntries", required = false, usage = "ElasticSearch batch size entries index")
  var esSuggestBatchSizeEntries: String = "4000"

  @org.kohsuke.args4j.Option(name = "-esCompositeKey", required = false, usage = "ElasticSearch composite key used for upsert")
  var esCompositeKey: String = "default"

  @org.kohsuke.args4j.Option(name = "-numberOfOutputFiles", required = false, usage = "Number of output files for coalesce before write")
  var numberOfOutputFiles: String = "24"

  @org.kohsuke.args4j.Option(name = "-esParentField", required = false, usage = "Define which field to use as parent. Defaults to wbcid")
  var esParentField: String = "wbcid"

  @org.kohsuke.args4j.Option(name = "-bulkIndex", required = false, usage = "If set the index name will not have the rundate appended")
  var bulkIndex: Boolean = false
}