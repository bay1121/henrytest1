package com.wb.config

import java.io.File

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.security.alias.CredentialProviderFactory
import org.apache.spark.{SparkContext, SparkFiles}
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._
import scala.util.matching.Regex
import org.json4s._
import org.json4s.jackson.JsonMethods._

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest

/**
  * Created by stephanieleighphifer on 2/14/17.
  */
trait Util {
  
  val RS_USER_KEY = "rs.user"
  val ES_USER_KEY = "es.user"
  val AWS_SECRETS_NAME = "aws.secrets.name"
  val FILE_PATTERN : Regex = ".*(\\d{4}-\\d{2}-\\d{2}).*".r
  lazy val secrets = {
    val secret = getAwsSecrets(ConfigFactory.load().getString(AWS_SECRETS_NAME))
    
    implicit val formats = org.json4s.DefaultFormats
    parse(secret).extract[Map[String, String]]
  }
  

  def getSecureCredential(name:String) : String = {
    var passwd : String = null
    try {
      passwd = secrets(name)
    } catch{
      case e: Exception =>
        ArrowLogger.log.warn("Unable to obtain secure credential for: " +  name + " provider")
        ArrowLogger.log.warn(e.printStackTrace())
    }
    passwd
  }

  def getFileDate(fileName:String) : java.sql.Timestamp = {
    val FILE_PATTERN(dateStr) = fileName
    new java.sql.Timestamp(DateTimeFormat.forPattern("YYYY-MM-dd").withZone(DateTimeZone.UTC).parseDateTime(dateStr).getMillis)
  }

  def flattenNestedType(x:Seq[Seq[String]]) : Seq[String] = {
    x match {
      case x: Seq[Seq[String]] =>
        x.flatten.distinct
      case _ =>
        Array[String]()
    }
  }
  
  def getAwsSecrets(secretName: String): String 
 
}

object Util extends Util {
  
  def getAwsSecrets(secretName: String): String = {
    val client = AWSSecretsManagerClientBuilder.standard().build()
    val getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName)
    
    client.getSecretValue(getSecretValueRequest).getSecretString()
  }
}