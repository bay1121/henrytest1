package com.wb.config

import org.apache.log4j.Logger

/**
  * Created by natha on 2/11/2017.
  */
// TODO: Do not use a global logger... Grab the logger when you need it at the beginning of the class
private[wb] object ArrowLogger extends Serializable {
  @transient lazy val log = Logger.getLogger(getClass.getName)
}
