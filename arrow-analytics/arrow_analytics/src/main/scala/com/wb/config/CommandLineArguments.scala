package com.wb.config

object CommandLineArguments {

  val DEV_ENVIRONMENT = "dev"
  val PROD_ENVIRONMENT = "prod"
  var environment: String = _

  @org.kohsuke.args4j.Option(name = "-environment", required = false, usage = "The execution environment. This is needed to pull the correct properties.")
  def setEnvironment(value: String): Unit = {
    if (value != CommandLineArguments.DEV_ENVIRONMENT && value != CommandLineArguments.PROD_ENVIRONMENT) {
      val msg = "Invalid environment [" + value + "]\nExpected " +
        CommandLineArguments.DEV_ENVIRONMENT + " or " + CommandLineArguments.PROD_ENVIRONMENT
      throw new IllegalArgumentException(msg)
    } else {
      environment = value
    }
  }

  @org.kohsuke.args4j.Option(name = "-rundate", required = false, usage = "Specific Hour to Process as YYYYMMddHH")
  var rundate: String = ""

  @org.kohsuke.args4j.Option(name = "-historic", required = false, usage = "Flag to indicate load of historic transactional data")
  var historic: String = "false"

  @org.kohsuke.args4j.Option(name = "-suggest", required = false, usage = "Flag to indicate suggester index")
  var suggest: String = "false"

  @org.kohsuke.args4j.Option(name = "-dataWarehouse", required = false, usage = "Redshift/Snowflakes indicator")
  var dataWarehouse: String = "Redshift"

  @org.kohsuke.args4j.Option(name = "-esSuggestFields", required = false, usage = "Declare which fields should be used with ElasticSearch suggest")
  var esSuggestFields: String = "false"

  @org.kohsuke.args4j.Option(name = "-daysAgo", required = false, usage = "Pull records since x days ago")
  var daysAgo: String = "-1"

  @org.kohsuke.args4j.Option(name = "-outputDir", required = false, usage = "Specific Hour to Process as YYYYMMddHH")
  var outputDir: String = ""

  @org.kohsuke.args4j.Option(name = "-stableDir", required = false, usage = "Directory with stable data set for integration testing")
  var stableDir: String = ""

  @org.kohsuke.args4j.Option(name = "-version", required = false, usage = "Bluekai Processing Version")
  var version: String = ""

  @org.kohsuke.args4j.Option(name = "-input", required = false, usage = "Location of input files")
  var input: String = ""

  @org.kohsuke.args4j.Option(name = "-compareDir", required = false, usage = "Location of files to compare to input and extract differences")
  var compareDir: String = ""

  @org.kohsuke.args4j.Option(name = "-dirDateString", required = false, usage = "Specific Hour to Process as YYYYMMddHH")
  var dirDateString: String = ""

  @org.kohsuke.args4j.Option(name = "-startDate", required = false, usage = "Inclusive field to start filter query")
  var startDate: String = ""

  @org.kohsuke.args4j.Option(name = "-endDate", required = false, usage = "Inclusive field to end filter query")
  var endDate: String = ""

  @org.kohsuke.args4j.Option(name = "-dateField", required = false, usage = "Date field to user with start/end query")
  var dateField: String = ""

  @org.kohsuke.args4j.Option(name = "-esResourceName", required = false, usage = "ElasticSearch data index")
  var esResourceName: String = ""

  @org.kohsuke.args4j.Option(name = "-esSuggestResourceName", required = false, usage = "ElasticSearch suggester index")
  var esSuggestResourceName: String = ""

  @org.kohsuke.args4j.Option(name = "-esBatchSizeBytes", required = false, usage = "ElasticSearch batch size bytes")
  var esBatchSizeBytes: String = "4M"

  @org.kohsuke.args4j.Option(name = "-esBatchSizeEntries", required = false, usage = "ElasticSearch batch size entries index")
  var esBatchSizeEntries: String = "4000"

  @org.kohsuke.args4j.Option(name = "-esSuggestBatchSizeBytes", required = false, usage = "ElasticSearch batch size bytes")
  var esSuggestBatchSizeBytes: String = "4M"

  @org.kohsuke.args4j.Option(name = "-esSuggestBatchSizeEntries", required = false, usage = "ElasticSearch batch size entries index")
  var esSuggestBatchSizeEntries: String = "4000"

  @org.kohsuke.args4j.Option(name = "-esCompositeKey", required = false, usage = "ElasticSearch composite key used for upsert")
  var esCompositeKey: String = "default"

  @org.kohsuke.args4j.Option(name = "-numberOfOutputFiles", required = false, usage = "Number of output files for coalesce before write")
  var numberOfOutputFiles: String = "24"

  @org.kohsuke.args4j.Option(name = "-numberOfPartitions", required = false, usage = "Number of partitions for intermediate repartitions/coalesce calls")
  var numberOfPartitions: String = "24"

  @org.kohsuke.args4j.Option(name = "-rsRootDir", required = false, usage = "Specific Hour to Process as YYYYMMddHH")
  var rsRootDir: String = ""

  @org.kohsuke.args4j.Option(name = "-staticRootDir", required = false, usage = "Specific Hour to Process as YYYYMMddHH")
  var staticRootDir: String = ""

  @org.kohsuke.args4j.Option(name = "-mode", required = false, usage = "Flag to specify running mode: production or test")
  var mode: String = "production"

  @org.kohsuke.args4j.Option(name = "-conf", required = false, usage = "Configuration file")
  var confFile: String = ""

  @org.kohsuke.args4j.Option(name = "-esChildFields", required = false, usage = "Declare which fields to add to the child record as a comma-separated list")
  var esChildFields: String = ""

  @org.kohsuke.args4j.Option(name = "-esParentField", required = false, usage = "Define which field to use as parent. Defaults to wbcid")
  var esParentField: String = "wbcid"

  @org.kohsuke.args4j.Option(name = "-esParentFields", required = false, usage = "Declare which fields to add to the parent record as a comma-separated list")
  var esParentFields: String = ""

  @org.kohsuke.args4j.Option(name = "-baseline", required = false, usage = "Flag to indicate Baseline Load")
  var baseline: String = "false"
}
