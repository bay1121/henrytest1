package com.wb.constants

/**
 * Created by stephanieleighphifer on 1/25/17.
 */
object Constants {
  //Configuration Properties
  val DMA_ZIPCODE_LOCATION = "hdfs.static.dma_zipcodes"
  val GEOCITY_LOCATION = "hdfs.geocity"
  val FANDANGO_IP_ENRICH_START = "fandango.historical.ip.enrich.startdate"

  //Database unload locations
  val DIM_CONSUMER_LOC = "hdfs.sf.dim_consumer"
  val DIM_PRODUCT_LOC = "hdfs.sf.dim_product"
  val DIM_TITLE_LOC = "hdfs.sf.dim_title"
  val EIDR_FLX_MAPPING_LOC = "hdfs.sf.eidr_flx_mapping"
  val FACT_EVENT_LOC = "hdfs.sf.fact_event"
  val FACT_PLAYBACKS_LOC = "hdfs.sf.fact_playbacks"
  val FACT_REDEMPTIONS_LOC = "hdfs.sf.fact_redemptions"
  val FD_MOVIE_DETAIL_LOC = "hdfs.sf.fd_movie_detail"
  val FD_TBLCUSTOMER_LOC = "hdfs.sf.fd_tblcustomer"
  val FD_TBLTRANSACTIONCOMMITTED_LOC = "hdfs.sf.fd_tbltransactioncommitted"
  val FD_TBLTRANSACTION_ITEM_LOC = "hdfs.sf.fd_tbltransaction_item"
  val FD_TBLTRANSACTION_LOC = "hdfs.sf.fd_tbltransaction"
  val FD_TBLTRANSACTION_PERFORMANCEATTRIBUTE_LOC = "hdfs.sf.fd_tbltransaction_performanceattribute"
  val FD_TEST_EMAIL_LKP_LOC = "hdfs.sf.fd_test_email_lkp"
  val FD_TO_FX_MAP_LOC = "hdfs.sf.fd_to_fx_map"
  val FX_GENRE_LOC = "hdfs.sf.fx_genre"
  val FX_MOVIE_CAST_LOC = "hdfs.sf.fx_movie_cast"
  val FX_MOVIE_GENRE_LOC = "hdfs.sf.fx_movie_genre"
  val FX_MOVIE_LOC = "hdfs.sf.fx_movie"
  val FX_MOVIE_PERSON_LOC = "hdfs.sf.fx_movie_person"
  val FX_MOVIE_STUDIO_LOC = "hdfs.sf.fx_movie_studio"
  val FX_TV_CAST_LOC = "hdfs.sf.fx_tv_cast"
  val FX_TV_EPISODE_LOC = "hdfs.sf.fx_tv_episode"
  val FX_TV_SEASON_LOC = "hdfs.sf.fx_tv_season"
  val FX_TV_SERIES_LOC = "hdfs.sf.fx_tv_series"
  val FX_USER_EMAIL_LOC = "hdfs.sf.fx_user_email"
  val FX_USER_USER_LOC = "hdfs.sf.fx_user__user"

  //General Fields
  val TIME_STATS_KEY = "time_stats"
  val TIME_KEY = "time"
  val CLIENT_IP_KEY = "client_ip"
  val ARROW_INCLUSION_KEY = "arrow_inclusion"
  val NSR_READY_KEY = "nsr_ready"
  val UID_KEY = "uid"
  val SEEN_KEY = "seen"
  val FIRST_SEEN_KEY = "first_seen"
  val LAST_SEEN_KEY = "last_seen"
  val EVENT_COUNT_KEY = "event_count"
  val INVALID_CRITERIA_KEY = "invalid_criteria"
  val VALID_EMAIL_ADDRESS_KEY = "valid_email_address"
  val VALID_MAID_KEY = "valid_maid"
  val VALID_IP_KEY = "valid_client_ip"
  val Country_US = "US"
  val RECORD_DATE = "record_date"
  val MA_TRANSACTION_LOOKBACK_DAYS = "ma.transaction.lookback.days"
  val ATOM_TICKETS_LOOKBACK_DAYS = "atom.tickets.lookback.days"

  // Redshift/Snowflake fields
  val CIDW_LOAD_DTTM = "cidw_load_dttm"

  //Bluekai Fields
  val PHINTS_KEY = "phints"

  //Site Fields
  val CATEGORIES_KEY = "categories"
  val SITE_KEY = "site"
  val SITENAME_KEY = "sitename"
  val SITEID_KEY = "site_id"
  val SITEROLLUP_KEY = "siterollup"
  val FANDANGO_SITE = "Fandango"
  val FLIXSTER_SITE = "Flixster"
  val MOVIESANYWHERE_SITE = "Movies Anywhere"

  val CAMPAIGN_ID_KEY = "campaign_id"
  val CAMPAIGN_NAME_KEY = "campaign"

  //Ratings
  val RATING_G = "G"
  val RATING_PG = "PG"
  val RATING_PG13 = "PG-13"
  val RATING_R = "R"
  val RATING_NC17 = "NC-17"
  val RATING_UNRATED = "Unrated"
  val RATING_NR = "NR"

  //Hour Windows
  val WINDOW_A = "0-4"
  val WINDOW_B = "4-8"
  val WINDOW_C = "8-12"
  val WINDOW_D = "12-16"
  val WINDOW_E = "16-20"
  val WINDOW_F = "20-24"

  //Record Constants
  val FLIXSTER_SRC = "flixster"
  val FANDANGO_SRC = "fandango"
  val MOVIE_SEG = "movie"
  val TALENT_SEG = "talent"
  val SERIES_SEG = "series"

  val OP_KEY = "op"
  val COMPOSITE_KEY = "compositekey"
  val BROWSER_LANGUAGE_KEY = "browser_language"
  val FLIXSTER_USER_ID = "flixster_user_id"
  val FANDANGO_USER_ID = "fandango_user_id"
  val FANDANGO_USER_ID_SHA256 = "fandango_user_id_sha256"
  val MAID_KEY_HASH = "maid_hash"
  val MAID_KEY = "maid"
  val IDFA_KEY = "idfa"
  val ADID_KEY = "adid"
  val EMAIL_ADDRESS_KEY = "email_address"
  val TRANSACTION_DATE_KEY = "transaction_date"
  val WBCID_KEY = "wbcid"

  //Attribute Keys
  val MOVIE_TITLE_KEY = "movie_title"
  val VIDEO_GAME_TITLE_KEY = "video_game_title"
  val SERIES_TITLE_KEY = "series_title"
  val MOVIE_GENRES_KEY = "movie_genres"
  val SERIES_GENRES_KEY = "series_genres"
  val DC_SITE_PROPERTY_KEY = "dc_site_property"
  val ARTICLE_TITLE_KEY = "article_title"
  val TOPIC_KEY = "topic"
  val WEBSITE_SECTION_KEY = "website_section"
  val VIDEO_TITLE_KEY = "video_title"
  val COMIC_BOOK_TITLE_KEY = "comic_book_title"
  val EVENT_TYPE_KEY = "event_type"
  val PRODUCT_TYPE_KEY = "product_type"
  val PRODUCT_TITLE_KEY = "product_title"
  val AID_TYPE_KEY = "aid_type"
  val AID_KEY = "aid"
  val RELEASE_WINDOW_KEY = "release_window"
  val RELEASE_WINDOW_DAYS_KEY = "release_window_days"
  val PERSON_KEY = "person"
  val DEVICE_TYPE_KEY = "device_type"
  val CATEGORY_VALUES_KEY = "category_values"
  val GEO_VALUES_KEY = "geo"
  val IS_COOKIE_STATISTICAL = "is_cookie_statistical"
  val CATMAN_BRANDS_KEY = "catman_brands"
  val CATMAN_FRANCHISE_KEY = "catman_franchise"
  val CATMAN_PRIMARY_BRAND_KEY = "catman_primary_brand"
  val FRANCHISE_BRANDS_KEY = "franchise_brands"
  val FRANCHISE_CHARACTERS_KEY = "franchise_characters"
  val FRANCHISE_PROPERTIES_KEY = "franchise_properties"
  val IMDB_KEYWORDS_KEY = "imdb_keywords"
  val MOVIE_TALENT_KEY = "movie_talent"
  val TV_TALENT_KEY = "tv_talent"
  val RATING_KEY = "rating"
  val RELEASE_YEAR_KEY = "release_year"
  val ALL_CAST = "cast_all_act"
  val LIBRARY_COUNT = "library_count"
  val CONTENT_VIEWED_DATE = "content_viewed_date"
  val CONTENT_TYPE = "content_type"
  val CONTENT_VIEWED_PERCENT = "content_viewed_percent"
  val LIBRARY_RELEASE_COMPOSITION = "library_release_composition"
  val LIBRARY_CONTENT_COMPOSITION = "library_content_composition"
  val LIBRARY_GENRE_COMPOSITION = "library_genre_composition"

  //Supporting Attribute Values
  val attributePhintKeys = Map[String, Array[String]](
    MOVIE_TITLE_KEY -> Array(Phints.DC_MV_TITLE, Phints.VERTIGO_MV_TITLE, Phints.OO_MV_TITLE, Phints.WB_MV_TITLE),
    VIDEO_GAME_TITLE_KEY -> Array(Phints.DC_VID_GM_TITLE, Phints.VERTIGO_VID_GM_TITLE, Phints.WB_VID_GM_TITLE),
    SERIES_TITLE_KEY -> Array(Phints.DC_TV_SRS_TITLE, Phints.VERTIGO_TV_SRS_TITLE, Phints.WB_TV_SRS_TITLE),
    MOVIE_GENRES_KEY -> Array(Phints.FN_MV_GENRE, Phints.FLX_MV_GENRE),
    DC_SITE_PROPERTY_KEY -> Array(Phints.DC_CHAR, Phints.VERTIGO_CHAR, Phints.DC_PROPERTY),
    ARTICLE_TITLE_KEY -> Array(Phints.DC_ART_TITLE, Phints.VERTIGO_ART_TITLE),
    TOPIC_KEY -> Array(Phints.FLX_ART_TAG),
    WEBSITE_SECTION_KEY -> Array(Phints.FLX_SECTION, Phints.FLX_SITE_SEC, Phints.FN_PG_LVL_1, Phints.FN_PG_LVL_2, Phints.DC_SITE_SEC, Phints.VERTIGO_SITE_SEC, Phints.DC_CONTENT_TYPE, Phints.WBCOM_CNT_TYPE),
    VIDEO_TITLE_KEY -> Array(Phints.DC_VID_TITLE, Phints.VERTIGO_VID_TITLE),
    COMIC_BOOK_TITLE_KEY -> Array(Phints.DC_CMC_TITLE, Phints.VERTIGO_CMC_TITLE),
    MAID_KEY -> Array(Phints.ADID, Phints.IDFA))

  val websiteRollups = Map[String, Array[String]](
    "Person" -> Array("celebrity", "performer"),
    "Movie Showtimes" -> Array("times", "movie showtimes", "movie times"),
    "Movie" -> Array("movie", "movies"),
    "TV" -> Array("tv", "tv series"),
    "Video Game" -> Array("video game", "video games"),
    "Comic" -> Array("comic", "graphic_novel"),
    "Article" -> Array("article", "blog", "movienews", "news feed"),
    "Video" -> Array("video", "videos"),
    "Photo" -> Array("gallery", "photogallery"),
    "DVD" -> Array("dvd"),
    "Character" -> Array("character"),
    "Ticket Purchase" -> Array("purchase"))

  //Event Types
  val BROWSE = "Browse"
  val MEDIA_VIDEO_COMPLETE = "Media Video Complete"
  val VIEWED_TRAILER = "Viewed Trailer"
  val MEDIA_IMPRESSION = "Media Impression"
  val MEDIA_CLICK = "Media Click"
  val PRE_ORDER = "Pre-Order"

  val VIDEO_COMPLETE_MIN = 75

  val eventTypes = Map[String, String](
    "imp" -> MEDIA_IMPRESSION,
    "click" -> MEDIA_CLICK,
    "Buy Button Click (Home)" -> PRE_ORDER,
    "Buy Button Click" -> PRE_ORDER,
    "Trailer View" -> VIEWED_TRAILER)

  //TLD
  val TLD = "tld"
  val TLD_COUNTRY_CODE = "tld_country_code"

  //Maxmind Source Keys
  val MM_SOURCE_ZIPCODE = "zip_code"
  val MM_SOURCE_DMA_ZONE = "dma_zone"
  val MM_SOURCE_DMA_NAME = "dma_name"
  val MM_SOURCE_WB_DIVISION = "wb_division"
  val MM_SOURCE_DMA_RANK = "dma_rank"

  //Maxmind Keys
  val MM_SYSTEM_SUBDIVISION_NAME_KEY = "mm_subdivision_name"
  val MM_SYSTEM_SUBDIVISION_ISO_CODE_KEY = "mm_subdivision_iso_code"
  val MM_COUNTRY_CODE_KEY = "mm_country_code"
  val MM_COUNTRY_NAME_KEY = "mm_country_name"
  val MM_CITY_KEY = "mm_city"
  val MM_ZIPCODE_KEY = "mm_zipcode"
  val MM_CONTINENT_KEY = "mm_continent"
  val MM_GEOCODE_KEY = "mm_geocode"

  //DMA Keys
  val DMA_NAME_KEY = "dma_name"
  val DMA_CODE_KEY = "dma_code"

  //Device Type Keys
  val DESKTOP_KEY = "Desktop"
  val MOBILE_KEY = "Mobile"
  val ANDROID_KEY = "Android"
  val IOS_KEY = "iOS"

  val deviceTypes = Map[String, Array[Int]](
    MOBILE_KEY -> Array(179159, 158628, 158627, 384156, 204774),
    // Mobile, Android, iOS, Windows Phone 8 (WP8), WP7
    DESKTOP_KEY -> Array(179157, 158624, 158626, 160113,
      281155, 196823, 491866, 412462, 160113, 163191,
      163253, 163194, 163195, 372156, 163192, 163193,
      163196))

  //Release Window Values
  val PRE_RELEASE = "Pre-Release"
  val TICKET_PRE_SALES = "Ticket Pre-Sales"
  val THEATRICAL = "Theatrical Release"
  val DIGITAL_TNR = "Digital TNR"
  val TNR = "TNR"
  val CATALOG = "Catalog"

  //Movie Release Date
  val theatrical_release_windows = Array[Map[String, Any]](
    Map[String, Any](
      "name" -> CATALOG,
      "min" -> 270,
      "max" -> null),
    Map[String, Any](
      "name" -> TNR,
      "min" -> 105,
      "max" -> 270),
    Map[String, Any](
      "name" -> DIGITAL_TNR,
      "min" -> 90,
      "max" -> 105),
    Map[String, Any](
      "name" -> THEATRICAL,
      "min" -> -2,
      "max" -> 90),
    Map[String, Any](
      "name" -> TICKET_PRE_SALES,
      "min" -> -15,
      "max" -> -2),
    Map[String, Any](
      "name" -> PRE_RELEASE,
      "min" -> null,
      "max" -> -15))
  val PRE_OPENING = "Pre-Opening"
  val HOME_ENTERTAINMENT = "Home Entertainment"

  //Product Type Values
  val MOVIE = "Movie"
  val TV_SERIES = "TV Series"
  val VIDEO_GAME = "Video Game"
  val COMIC_BOOK = "Comic Book"

  val bkEnrichedSuggestFields = Map[String, String](
    ARTICLE_TITLE_KEY -> ARTICLE_TITLE_KEY,
    BROWSER_LANGUAGE_KEY -> BROWSER_LANGUAGE_KEY,
    CAMPAIGN_NAME_KEY -> CAMPAIGN_NAME_KEY,
    CATMAN_BRANDS_KEY -> CATMAN_BRANDS_KEY,
    CATMAN_FRANCHISE_KEY -> CATMAN_FRANCHISE_KEY,
    CATMAN_PRIMARY_BRAND_KEY -> CATMAN_PRIMARY_BRAND_KEY,
    COMIC_BOOK_TITLE_KEY -> COMIC_BOOK_TITLE_KEY,
    DC_SITE_PROPERTY_KEY -> DC_SITE_PROPERTY_KEY,
    DEVICE_TYPE_KEY -> DEVICE_TYPE_KEY,
    EVENT_TYPE_KEY -> EVENT_TYPE_KEY,
    FRANCHISE_BRANDS_KEY -> FRANCHISE_BRANDS_KEY,
    FRANCHISE_CHARACTERS_KEY -> FRANCHISE_CHARACTERS_KEY,
    FRANCHISE_PROPERTIES_KEY -> FRANCHISE_PROPERTIES_KEY,
    MOVIE_GENRES_KEY -> MOVIE_GENRES_KEY,
    SERIES_GENRES_KEY -> SERIES_GENRES_KEY,
    IMDB_KEYWORDS_KEY -> IMDB_KEYWORDS_KEY,
    MOVIE_TALENT_KEY -> MOVIE_TALENT_KEY,
    MOVIE_TITLE_KEY -> MOVIE_TITLE_KEY,
    RATING_KEY -> RATING_KEY,
    PERSON_KEY -> PERSON_KEY,
    ALL_CAST -> PERSON_KEY,
    PRODUCT_TITLE_KEY -> PRODUCT_TITLE_KEY,
    PRODUCT_TYPE_KEY -> PRODUCT_TYPE_KEY,
    RELEASE_WINDOW_KEY -> RELEASE_WINDOW_KEY,
    RELEASE_YEAR_KEY -> RELEASE_YEAR_KEY,
    SITEROLLUP_KEY -> SITEROLLUP_KEY,
    TOPIC_KEY -> TOPIC_KEY,
    SERIES_TITLE_KEY -> SERIES_TITLE_KEY,
    VIDEO_GAME_TITLE_KEY -> VIDEO_GAME_TITLE_KEY,
    VIDEO_TITLE_KEY -> VIDEO_TITLE_KEY,
    WEBSITE_SECTION_KEY -> WEBSITE_SECTION_KEY,
    DMA_NAME_KEY -> DMA_NAME_KEY,
    MM_ZIPCODE_KEY -> MM_ZIPCODE_KEY,
    MM_COUNTRY_CODE_KEY -> MM_COUNTRY_CODE_KEY)

  val arrowPhints = Array(
    s"$PHINTS_KEY.${Phints.FLX_ART_TAG}",
    s"$PHINTS_KEY.${Phints.BK_L}",
    s"$PHINTS_KEY.${Phints.BK_T}",
    s"$PHINTS_KEY.${Phints.CID}",
    s"$PHINTS_KEY.${Phints.DC_ART_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_CHAR}",
    s"$PHINTS_KEY.${Phints.DC_CMC_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_CONTENT_TYPE}",
    s"$PHINTS_KEY.${Phints.DC_MV_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_PROPERTY}",
    s"$PHINTS_KEY.${Phints.DC_SITE_SEC}",
    s"$PHINTS_KEY.${Phints.DC_TV_SRS_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_VID_GM_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_VID_TITLE}",
    s"$PHINTS_KEY.${Phints.EVENT}",
    s"$PHINTS_KEY.${Phints.FLX_MV_GENRE}",
    s"$PHINTS_KEY.${Phints.FLX_MV_ID}",
    s"$PHINTS_KEY.${Phints.FLX_RLTD_MV_ID}",
    s"$PHINTS_KEY.${Phints.FLX_RLTD_TLNT_ID}",
    s"$PHINTS_KEY.${Phints.FLX_RLTD_TV_SRS_ID}",
    s"$PHINTS_KEY.${Phints.FLX_SECTION}",
    s"$PHINTS_KEY.${Phints.FLX_SITE_SEC}",
    s"$PHINTS_KEY.${Phints.FLX_TLNT_NAME}",
    s"$PHINTS_KEY.${Phints.FLX_TLNT_ID}",
    s"$PHINTS_KEY.${Phints.FLX_TLNT_ID_2}",
    s"$PHINTS_KEY.${Phints.FLX_TV_SRS_ID}",
    s"$PHINTS_KEY.${Phints.FLX_UID}",
    s"$PHINTS_KEY.${Phints.FN_MV_GENRE}",
    s"$PHINTS_KEY.${Phints.FN_MV_ID}",
    s"$PHINTS_KEY.${Phints.FN_PG_LVL_1}",
    s"$PHINTS_KEY.${Phints.FN_PG_LVL_2}",
    s"$PHINTS_KEY.${Phints.FN_MV_TALENT}",
    s"$PHINTS_KEY.${Phints.FN_UID}",
    s"$PHINTS_KEY.${Phints.OO_MV_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_ART_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_CHAR}",
    s"$PHINTS_KEY.${Phints.VERTIGO_CMC_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_MV_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_SITE_SEC}",
    s"$PHINTS_KEY.${Phints.VERTIGO_TV_SRS_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_VID_GM_TITLE}",
    s"$PHINTS_KEY.${Phints.VERTIGO_VID_TITLE}",
    s"$PHINTS_KEY.${Phints.VID_COMPLETE}",
    s"$PHINTS_KEY.${Phints.WBCOM_CNT_TYPE}",
    s"$PHINTS_KEY.${Phints.WBCOM_TITLE}",
    s"$PHINTS_KEY.${Phints.DC_UID}")
}
