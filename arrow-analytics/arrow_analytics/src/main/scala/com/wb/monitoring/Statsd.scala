package com.wb.monitoring

import com.timgroup.statsd.{NoOpStatsDClient, NonBlockingStatsDClient, StatsDClient}

object Statsd {
  def getClient(prefix:String, host:String, port:Int):StatsDClient = {
    if (prefix.nonEmpty && host.nonEmpty && port > 0) {
      new NonBlockingStatsDClient(prefix, host, port)
    } else {
      new NoOpStatsDClient
    }
  }
}
