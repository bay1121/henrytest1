package com.wb.aws.security

import java.io.IOException
import java.net.URI
import java.util.Date
import java.util.logging.Logger
import java.lang.String
import org.apache.hadoop.conf.Configurable
import org.apache.hadoop.conf.Configuration
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.BasicSessionCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.InstanceProfileCredentialsProvider
import com.amazonaws.auth.PropertiesCredentials
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder
import com.amazonaws.services.securitytoken.model.Credentials
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest
import com.amazonaws.services.securitytoken.model.AssumeRoleResult
import com.amazonaws.services.securitytoken.model.GetSessionTokenRequest
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain

final class CustomAWSSecurityProvider(val uri:URI, val configuration:Configuration) extends AWSCredentialsProvider with Configurable {
  
    var creds:AWSCredentialsProvider = _
    var stsCreds:Credentials = _
    val bucket = uri.toString().replace("s3://", "").split("/")(0)
    
    val key = { 
      var temp = configuration.getPassword(s"fs.s3a.bucket.${bucket}.access.key")
      if (temp == null)
        temp = configuration.getPassword("fs.s3a.access.key")
      
      if (temp != null)
        new String(temp)
      else
        null        
    }

    val secret = { 
      var temp = configuration.getPassword(s"fs.s3a.bucket.${bucket}.secret.key")
      if (temp == null)
        temp = configuration.getPassword("fs.s3a.secret.key")
      
      if (temp != null)
        new String(temp)
      else
        null        
    }
        
    override def getCredentials(): AWSCredentials = {
        if (key != null) {
            if (stsCreds == null ||
                (stsCreds.getExpiration().getTime() - System.currentTimeMillis() < 60000)) {
               
              val builder = AWSSecurityTokenServiceClientBuilder.standard()
              val stsClient = builder.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(key, secret))).build()
              
              val getSessionTokenRequest = new GetSessionTokenRequest()
              getSessionTokenRequest.setDurationSeconds(3600);
              
              stsCreds = stsClient.getSessionToken(getSessionTokenRequest).getCredentials()
            }
            new BasicSessionCredentials(stsCreds.getAccessKeyId(),
                stsCreds.getSecretAccessKey(),
                stsCreds.getSessionToken())

        } else {
            //Extracting the credentials from EC2 metadata service
            if (creds == null) {
               creds = DefaultAWSCredentialsProviderChain.getInstance
            }
            creds.getCredentials();
        }    
    }
    
    override def refresh() {}

    override def setConf(conf:Configuration) {}

    override def getConf():Configuration = {
        return configuration
    }
  
}


//final class MyAWSCredentialsProviderWithUri implements AWSCredentialsProvider, Configurable {
//
//    private Configuration configuration;
//    private AWSCredentials credentials, iamUserCredentials;
//    private static final String role_arn =
//            "arn:aws:iam::123456789012:role/demo-role";
//
//    //Specifying the S3 bucket URI for the other account
//    private static final String bucket_URI = "s3://research-data/";
//
//    private URI uri;
//    private static InstanceProfileCredentialsProvider creds;
//    private static Credentials stsCredentials;
//
//    public MyAWSCredentialsProviderWithUri(URI uri, Configuration conf) {
//        this.configuration = conf;
//        this.uri = uri;
//    }
//
//    @Override
//    public AWSCredentials getCredentials() {
//        //Returning the credentials to EMRFS to make S3 API calls
//        if (uri.toString().startsWith(bucket_URI)) {
//            if (stsCredentials == null ||
//                    (stsCredentials.getExpiration().getTime() - System.currentTimeMillis() < 60000)) {
//                try {
//                    //Reading the credentials of the IAM users from Java properties file
//                    iamUserCredentials = new PropertiesCredentials
//                    (MyAWSCredentialsProviderWithUri.class.getResourceAsStream("Credentials.properties"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                AWSSecurityTokenServiceClient   stsClient = new
//                            AWSSecurityTokenServiceClient(iamUserCredentials);
//                //Assuming the role in the other account to obtain temporary credentials
//                AssumeRoleRequest assumeRequest = new AssumeRoleRequest()
//                .withRoleArn(role_arn)
//                .withDurationSeconds(3600)
//                .withRoleSessionName("demo");
//                AssumeRoleResult assumeResult = stsClient.assumeRole(assumeRequest);
//                stsCredentials = assumeResult.getCredentials();
//            }
//            BasicSessionCredentials temporaryCredentials =
//                    new BasicSessionCredentials(
//                            stsCredentials.getAccessKeyId(),
//                            stsCredentials.getSecretAccessKey(),
//                            stsCredentials.getSessionToken());
//            credentials = temporaryCredentials;
//
//        } else {
//            //Extracting the credentials from EC2 metadata service
//            Boolean refreshCredentialsAsync = true;
//            if (creds == null) {
//                creds = new InstanceProfileCredentialsProvider
//                    (refreshCredentialsAsync);
//            }
//            credentials = creds.getCredentials();
//        }
//        return credentials;
//    }
//
//    @Override
//    public void refresh() {}
//
//    @Override
//    public void setConf(Configuration conf) {
//    }
//
//    @Override
//    public Configuration getConf() {
//        return configuration;
//    }
//}