package com.wb.event

import com.timgroup.statsd.StatsDClient
import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments}
import com.wb.constants.{Constants, Phints}
import com.wb.enrich.Attributes
import com.wb.model.SchemaBuilder
import com.wb.monitoring.Statsd
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.joda.time.DateTimeZone
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._

object Bluekai {
  val unk : String = null

  def calculateHourWindowFcn(h:Int) : String = {
    val hourWindow = h match {
      case a if 0 until 4 contains a   => Constants.WINDOW_A
      case b if 4 until 8 contains b   => Constants.WINDOW_B
      case c if 8 until 12 contains c  => Constants.WINDOW_C
      case d if 12 until 16 contains d => Constants.WINDOW_D
      case e if 16 until 20 contains e => Constants.WINDOW_E
      case f if 20 until 24 contains f => Constants.WINDOW_F
      case _                           => null
    }
    hourWindow
  }

  //IP Validation
  def getValidTimeFcn(x:String) : Map[String,String] = {
    var time_stats = Map[String,String]()
    try {
      if (x != null) {
        val millisToEpoch = x.toLong * 1000
        time_stats = Attributes.getValidTimeFcn(millisToEpoch)
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.error("getValidTimeFcn unable to extract time information from: " + x)
        ArrowLogger.log.error(e.printStackTrace())
    }
    time_stats
  }

  //Extract site id
  def getValidSiteIDFcn(x:String) : String = {
    var site = None: Option[String]
    if(x != null) {
      val site_regex = "(?<=/site/)(\\d+)".r
      var site_id = site_regex.findFirstIn(x)
      if (site_id != null) {
        site = site_id
      }
    }
    site.orNull
  }

  //Split categories string
  def getCategoriesList(x:String) : String = {
    var categories = None: Option[String]
    try {
      if (x != null) {
        categories = Some(x.replaceAll(",", " "))
      }
    }
    catch {
      case e: Exception =>
        ArrowLogger.log.warn("getCategoriesList unable to parse " + x)
        ArrowLogger.log.warn(e.printStackTrace())
    }
    categories.orNull
  }


  //Extract phints from site as key value map
  def getPhintsList(site:String) : Map[String,String] = {
    var kv_phints = Map[String,String]()
    try {
      if (site != null) {
        val siteArray = site.substring(site.indexOf("?") + 1).split("&")
        for (a <- siteArray) {
          var itemArray = a.split("=")
          if (itemArray.length > 1) {
            if (itemArray(0) == "phint") {
              var value = Attributes.cleanupValue(itemArray(1))
              val valueArray = value.split("=")
              if (valueArray.length >= 2) {
                val key = valueArray(0)
                val value = valueArray.drop(0).drop(1).mkString("=").trim()
                kv_phints += key -> value
              } else if (itemArray.length  >= 3) {
                val key = itemArray(1)
                val value = itemArray(2)
                kv_phints += key -> value
              }
            } else {
              val key = itemArray(0)
              val value = Attributes.cleanupValue(itemArray(1))
              kv_phints += key -> value
            }
          }

        }
      }
    } catch {
      case e: Exception =>
        ArrowLogger.log.warn("getPhintsList unable to extract phints from: " + site)
        ArrowLogger.log.warn(e.printStackTrace())
    }

    kv_phints
  }

  def parseAndValidateArgs(args: Array[String]): Unit = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }
  }

  private def setEmptyToNull(df: DataFrame): DataFrame = {
    val exprs = df.schema.map { f =>
      f.dataType match {
        case StringType => when(length(col(f.name)) === 0, lit(null: String).cast(StringType)).otherwise(col(f.name)).as(f.name)
        case ArrayType(StringType,true) => {
          when(size(col(f.name)) === 0, lit(null:String).cast(ArrayType(StringType))).otherwise(col(f.name)).as(f.name)
        }
        case _ => {
          col(f.name)
        }
      }
    }

    val newDF = df.select(exprs: _*)
    newDF
  }

  def main(args: Array[String]): Unit = {
    // this will set all the state within CommandLineArguments and validate
    parseAndValidateArgs(args)

    val rundate = CommandLineArguments.rundate
    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt
  
    //Establish appropriate contexts
    //Start spark session
    val spark = SparkSession.
      builder().
      appName("Bluekai").
      getOrCreate()

    val configs = ConfigFactory.load()
    
    val version = configs.getString("arrow.version")
    
    val rawBkLocPrefix = configs.getString("wb.bluekai.prefix")
    val input = s"$rawBkLocPrefix${rundate}*"
    
    val bkLoc = configs.getString("hdfs.arrow.bluekai")
    val outputDir = s"$bkLoc/date=${rundate.substring(0,4)}-${rundate.substring(4,6)}-${rundate.substring(6,8)}"
    
    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis.toString

    val statsd:StatsDClient = Statsd.getClient(
      prefix = configs.getString("statsd.prefix"),
      host = configs.getString("statsd.host"),
      port = configs.getInt("statsd.port")
    )

    var op = Map[String,String]()
    op += "loadtime" -> now
    op += "version" -> version
    op += "sourcefile" -> input
    op += "updatedtime" -> now

    //Register UDFs
    val getCategories = spark.udf.register("getCategories", getCategoriesList _)
    val getPhints = spark.udf.register("getPhints",getPhintsList _)
    val getValidSiteID = spark.udf.register("getValidSiteID", getValidSiteIDFcn _)
    val getValidIP = spark.udf.register("getValidIP", Attributes.getValidIPFcn _)
    val getValidTime = spark.udf.register("getValidTime", getValidTimeFcn _)

    //Process Raw Logs
    var rawLogsAll = spark.read.
      format("com.databricks.spark.csv").
      option("header", "false").
      option("delimiter", "\t").
      load(input)

    val inputCounter = spark.sparkContext.longAccumulator("Bluekai.Input")
    rawLogsAll = spark.createDataFrame(rawLogsAll.rdd.map(row => {
      inputCounter.add(1)
      row
    }), rawLogsAll.schema)

    val rawLogs = rawLogsAll.distinct()

    val logsRDD = rawLogs.rdd.map(p => Row(Attributes.getHashFcn(p.mkString("\t")), p.mkString("\t"), op, p(0), p(1), p(2), p(3), p(4), p(5)))
    val bkLogs = setEmptyToNull(spark.createDataFrame(logsRDD,SchemaBuilder.bluekaiSchema))

    // Record any "get data events" which are useless to us as they don't have any valid phints that can be used for enrichment.
    // These records can be identified by the presence of "/getdata/" in the 4th field of the RAW bluekai log.
    val getDataCount = bkLogs.select("url").filter(url => url.toString.contains("/getdata/")).count()

    var logs = bkLogs.
      withColumn(Constants.CLIENT_IP_KEY, getValidIP(bkLogs(Constants.CLIENT_IP_KEY))).
      withColumn(Constants.SITEID_KEY, getValidSiteID(bkLogs(Constants.SITE_KEY))).
      withColumn(Constants.PHINTS_KEY, getPhints(bkLogs(Constants.SITE_KEY))).
      withColumn(Constants.TIME_STATS_KEY, getValidTime(bkLogs(Constants.TIME_KEY))).
      withColumn(Constants.CATEGORIES_KEY, getCategories(bkLogs(Constants.CATEGORIES_KEY))).
      drop(
        Constants.TIME_KEY,
        Constants.SITE_KEY
      )

    // Record any duplicate events (same time and flx/fan id)
    val duplicateFlxEvents = logs.withColumn("temp_flxid", col(Constants.PHINTS_KEY).getField(Phints.FLX_UID))
      .groupBy("time_stats.time", "temp_flxid").count().where("count > 1")
      .count()
    val duplicateFanEvents = logs.withColumn("temp_fnid", col(Constants.PHINTS_KEY).getField(Phints.FN_UID))
      .groupBy("time_stats.time", "temp_fnid").count().where("count > 1")
      .count()

    val outputCounter = spark.sparkContext.longAccumulator("Bluekai.Output")
    logs = spark.createDataFrame(logs.rdd.map(row => {
      outputCounter.add(1)
      row
    }), logs.schema)

    //Write Data to HDFS
    if(numberOfOutputFiles > 0) {
      logs.
        repartition(numberOfOutputFiles).
        write.
        parquet(outputDir)
    } else {
      logs.
        write.
        parquet(outputDir)
    }

    statsd.gauge("Bluekai.Input", inputCounter.value)
    statsd.gauge("Bluekai.GetDataEvents", getDataCount)
    statsd.gauge("Bluekai.DuplicateFlxEvents", duplicateFlxEvents)
    statsd.gauge("Bluekai.DuplicateFanEvents", duplicateFanEvents)
    statsd.gauge("Bluekai.Output", outputCounter.value)
  }
}
