package com.wb.egress
import com.typesafe.config.ConfigFactory
import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.client.CredentialsProvider
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.client.{RestClient, RestClientBuilder}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._


/**
  * Created by stephanieleighphifer on 3/30/17.
  */

object DeleteFromES {
  def initConfiguration(args: Array[String]) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }

    val inputDir = CommandLineArguments.input
    if (CommandLineArguments.input.isEmpty) {
      throw new RuntimeException("Required attribute  input is not provided.")
    }

    val esResourceName = CommandLineArguments.esResourceName
    if (CommandLineArguments.esResourceName.isEmpty) {
      throw new RuntimeException("Required attribute  esResourceName is not provided.")
    }
   
    (inputDir, esResourceName)
  }


  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    val configDetails = initConfiguration(args)
    val inputDir = configDetails._1
    val esResourceName = configDetails._2

    val spark = SparkSession.
      builder().
      appName("DeleteFromES").
      getOrCreate()
    import spark.implicits._

    val configs = ConfigFactory.load()
    val es_user = configs.getString("es.user")
    val es_passwd = Util.getSecureCredential(Util.ES_USER_KEY)
    val es_nodes = configs.getString("es.nodes")
    val es_port = configs.getString("es.port")

    if (es_passwd == null) {
      throw new RuntimeException("Unable to obtain secure credential for ElasticSearch user")
    }

    var keys = spark.read.parquet(inputDir).rdd.map(x => x.getAs[String](0)).collect()

    //AS REST CLIENT - Single Request

    var credentialsProvider : CredentialsProvider  = new BasicCredentialsProvider();
    credentialsProvider.setCredentials(AuthScope.ANY,
      new UsernamePasswordCredentials(es_user, es_passwd));

    var restClient : RestClient = RestClient.builder(new HttpHost(es_nodes, new Integer(es_port))).
      setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
        @Override
        def customizeHttpClient(httpClientBuilder:HttpAsyncClientBuilder ) : HttpAsyncClientBuilder = {
          return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        }
      }).
      build();

    for(t <- keys) {
      try {
        restClient.performRequest("DELETE",
          s"/$esResourceName/$t"
        )
      } catch {
        case e: Exception =>
          ArrowLogger.log.warn("DeleteFromES unable to remove record: " + t + " from " + esResourceName)
          ArrowLogger.log.warn(e.printStackTrace())
      }
    }

    restClient.close()
  }
}
