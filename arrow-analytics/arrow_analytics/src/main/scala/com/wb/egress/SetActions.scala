package com.wb.egress

import com.wb.config.{ArrowLogger, CommandLineArguments, Util}
import com.wb.constants.Constants
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.{SparkConf, SparkContext}
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}

import scala.collection.JavaConversions._

/**
  * Created by stephanieleighphifer on 3/28/17.
  */
object SetActions {
  val unk : String = null

  def initConfiguration(args: Array[String]): (String, Int, String, String) = {
    val parser = new CmdLineParser(CommandLineArguments)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        ArrowLogger.log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        System.exit(1)
    }

    var input = CommandLineArguments.input
    if (CommandLineArguments.input.isEmpty) {
      throw new RuntimeException("Required attribute input is not provided.")
    }

    var compareDir = CommandLineArguments.compareDir
    if (CommandLineArguments.compareDir.isEmpty) {
      throw new RuntimeException("Required attribute compareDir is not provided.")
    }

    var outputDir = CommandLineArguments.outputDir
    if (CommandLineArguments.outputDir.isEmpty) {
      //Default output location to current data set. New directories will be created for updated records and original data will be left in tact
      outputDir = input
    }

    val numberOfOutputFiles = CommandLineArguments.numberOfOutputFiles.toInt

    (outputDir, numberOfOutputFiles, input, compareDir)
  }

  def main(args: Array[String]): Unit = {
    //Initialize Configuration
    val configDetails = initConfiguration(args)
    val outputDir = configDetails._1
    val numberOfOutputFiles = configDetails._2
    val input = configDetails._3
    val compareDir = configDetails._4

    val spark = SparkSession.
      builder().
      appName("SetActions").
      getOrCreate()
    import spark.implicits._

    val newSet = spark.read.parquet(input)
    val oldSet = spark.read.parquet(compareDir)

    val newKeys = newSet.select(Constants.COMPOSITE_KEY)
    val oldKeys = oldSet.select(Constants.COMPOSITE_KEY)

    val addKeys = newKeys.except(oldKeys)
    val addRecords = newSet.join(addKeys, Seq(Constants.COMPOSITE_KEY), "inner")

    //Write new records to HDFS
    val hdfsNew = outputDir + "/new"
    if (numberOfOutputFiles > 0) {
      addRecords.coalesce(numberOfOutputFiles).write.parquet(hdfsNew)
    } else {
      addRecords.write.parquet(hdfsNew)
    }
  }
}
