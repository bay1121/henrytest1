package com.wb.egress

import com.wb.constants.Constants
import com.wb.enrich.Attributes
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.functions.{col, explode}
import org.apache.spark.sql.types._

import scala.collection.mutable.ListBuffer

object ESSuggesterAdapter {
  val log:Logger = Logger.getLogger(getClass)

  def createSuggesterDataFrames(sparkSession: SparkSession,
                                dataFrame: DataFrame,
                                whitelist: Array[String],
                                ingestTime: Long): Array[DataFrame] = {

    // TODO: Implement project wide suggest whitelist handling
    var colNames: Set[String] = Set.empty[String]
    whitelist match {
      case list: Array[String] => colNames = list.toSet
      case _ => colNames = Constants.bkEnrichedSuggestFields.keys.toSet
    }

    val keepColNames = colNames
      .toList
      .intersect(dataFrame.columns)
      .map(x => col(x))

    val suggests = dataFrame.select(keepColNames: _*)
    val st = suggests.dtypes

    val dataFrames = ListBuffer.empty[DataFrame]

    // For each suggest field, configure
    st.foreach { dftype =>
      val colName = dftype._1

      try {
        // Alias the colName - forced by the schema and in the hash
        val colAlias = Constants.bkEnrichedSuggestFields.getOrElse(colName, colName)
        val schema = StructType(Array(
          StructField("compositekey", StringType),
          StructField(colAlias, StringType),
          StructField("loaddate", LongType)
        ))

        var suggestersFiltered = suggests
        dftype._2 match {
          case "ArrayType(StringType,true)" =>
            suggestersFiltered = suggests
              .select(explode(col(colName)).alias("temp"))
              .withColumn(colName, col("temp").cast(StringType))
              .drop("temp")
          case "ArrayType(MapType(StringType,StringType,true),true)" => {
            // WARNING: Brittle. We are assuming the inner objects will have a key called "name"
            suggestersFiltered = suggests
              .select(explode(col(colName)).alias("temp"))
              .withColumn(colName, col("temp").getField("name"))
              .drop("temp")
          }
          case _ =>
            suggestersFiltered = suggests
              .select(col(colName).cast(StringType))
              .filter(col(colName).isNotNull)
        }

        val suggesters = suggestersFiltered.distinct
          .rdd
          .map(r => Row.fromSeq(Seq(Attributes.getHashFcn(colAlias + r(0)), r(0), ingestTime)))

        val suggestDataFrame = sparkSession.createDataFrame(suggesters, schema)
        val filteredDataFrame = suggestDataFrame.filter(col(colName) =!= "")

        dataFrames += filteredDataFrame

      } catch {
        case e: Exception =>
          log.warn(s"Unable to generate suggestions: ${dftype._1} - ${dftype._2}")
          log.warn(e)
      }
    }

    dataFrames.toArray
  }
}
