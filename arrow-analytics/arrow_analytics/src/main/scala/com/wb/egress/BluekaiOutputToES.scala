package com.wb.egress

import scala.collection.JavaConversions.seqAsJavaList

import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.lit
import org.elasticsearch.spark.sql.sparkDataFrameFunctions
import org.joda.time.DateTimeZone
import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.wb.config.OutputToESCLIArgs
import com.wb.config.Util
import com.wb.constants.Constants
import org.apache.spark.sql.types.DecimalType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.functions.col

case class ESWriteOptions(
  parentOptions: Map[String, String],
  childOptions: Map[String, String],
  suggestOptions: Map[String, String])

object BluekaiOutputToES {
  val log: Logger = Logger.getLogger(getClass)

  def parentFields: Array[String] = {
    Array[String](
      Constants.WBCID_KEY,
      Constants.UID_KEY,
      Constants.EMAIL_ADDRESS_KEY,
      Constants.MAID_KEY,
      Constants.MAID_KEY_HASH,
      Constants.CLIENT_IP_KEY)
  }

  def parse(args: Array[String]): Unit = {
    val parser = new CmdLineParser(OutputToESCLIArgs)
    try {
      parser.parseArgument(args.toList)
    } catch {
      case e:
        CmdLineException =>
        log.error(s"Error:${e.getMessage}\n Usage:\n")
        parser.printUsage(System.out)
        throw new RuntimeException("Failed to parse arguments")
    }

    val isSuggest = OutputToESCLIArgs.suggest.toBoolean
    val suggestResourceName = OutputToESCLIArgs.esSuggestResourceName
    if (isSuggest && suggestResourceName.isEmpty) {
      throw new RuntimeException("Required attribute esSuggestResourceName is not provided. " +
        "Either provide suggestion index name or set isSuggest to false")
    }
  }

  def getESWriteOptions(configs: Config): ESWriteOptions = {
    // Load Static Values from Properties file stored in HFDS
    val version = configs.getString("arrow.es.index.version")
    val esUser = configs.getString("es.user")
    val esNodes = configs.getString("es.nodes")
    val esPort = configs.getString("es.port")

    val esPasswd = Util.getSecureCredential(Util.ES_USER_KEY)

    if (esPasswd == null) {
      throw new RuntimeException("Unable to obtain secure credential for ElasticSearch user")
    }

    val rundate = OutputToESCLIArgs.rundate

    val esResourceNameTemplate = if (OutputToESCLIArgs.bulkIndex) {
      OutputToESCLIArgs.esResourceName
    } else {
      s"${OutputToESCLIArgs.esResourceName}-${rundate}"
    }
    val esResourceName = esResourceNameTemplate.replace("VERSION", version)

    val suggestResourceName = s"${OutputToESCLIArgs.esSuggestResourceName}".replace("VERSION", version)

    // Centralize the base elasticsearch options that will be used in all instances
    val baseEsOptions = Map[String, String](
      "es.nodes" -> esNodes,
      "es.port" -> esPort,
      "es.net.http.auth.user" -> esUser,
      "es.net.http.auth.pass" -> esPasswd,
      "es.net.ssl" -> "true",
      "es.net.ssl.cert.allow.self.signed" -> "true",
      "es.index.auto.create" -> "true",
      "es.resource" -> s"$esResourceName/_doc")

    var parentOptions = baseEsOptions
    parentOptions += "es.batch.size.bytes" -> OutputToESCLIArgs.esBatchSizeBytes
    parentOptions += "es.batch.size.entries" -> OutputToESCLIArgs.esBatchSizeEntries
    parentOptions += "es.mapping.id" -> OutputToESCLIArgs.esParentField

    var childOptions = baseEsOptions
    childOptions += "es.batch.size.bytes" -> OutputToESCLIArgs.esBatchSizeBytes
    childOptions += "es.batch.size.entries" -> OutputToESCLIArgs.esBatchSizeEntries
    childOptions += "es.mapping.join" -> "_arrowjoin"
    childOptions += "es.mapping.routing" -> OutputToESCLIArgs.esParentField
    childOptions += "es.mapping.id" -> Constants.COMPOSITE_KEY

    var suggestOptions = baseEsOptions
    suggestOptions += "es.resource" -> suggestResourceName
    suggestOptions += "es.batch.size.bytes" -> OutputToESCLIArgs.esSuggestBatchSizeBytes
    suggestOptions += "es.batch.size.entries" -> OutputToESCLIArgs.esSuggestBatchSizeEntries
    suggestOptions += "es.mapping.id" -> Constants.COMPOSITE_KEY

    ESWriteOptions(
      parentOptions = parentOptions,
      childOptions = childOptions,
      suggestOptions = suggestOptions)
  }

  /**
   * convert all decimal(38,0) cols to integer
   *  all the redshift integer columns were converted to decimal(38,0) in snowflake
   *  the elastic schema expects integers, this method typecasts the decimal to integer
   */
  def decimalToInteger(dfIn: DataFrame): DataFrame = {
    val selectCols = dfIn.dtypes.map(colType =>
      if (colType._2 == "DecimalType(38,0)")
        col(colType._1).cast(IntegerType)
      else
        col(colType._1))

    dfIn.select(selectCols: _*)
  }

  def main(args: Array[String]): Unit = {
    // reads in args and mutates OutputToESCLIArgs object
    parse(args)

    val spark = SparkSession.
      builder().
      appName("BluekaiOutputToES").
      getOrCreate()

    val configs = ConfigFactory.load()

    val eSWriteOptions: ESWriteOptions = BluekaiOutputToES.getESWriteOptions(configs)
    val now = new org.joda.time.DateTime().withZone(DateTimeZone.UTC).getMillis

    val inputBase = configs.getString(OutputToESCLIArgs.input)
    val rundate = OutputToESCLIArgs.rundate
    val inputDir = s"$inputBase/date=${rundate.substring(0, 4)}-${rundate.substring(4, 6)}-${rundate.substring(6, 8)}/*"

    val dateframe = spark.read.parquet(inputDir)
      .withColumn("ingest_time", lit(now))

    val filteredDataFrame = decimalToInteger(ESParentChildAdapter.filterOnInclusionAndRollup(dateframe))

    val parentFrame = ESParentChildAdapter.createParentsDataFrame(
      dataFrame = filteredDataFrame,
      groupByField = Constants.WBCID_KEY,
      parentFields = BluekaiOutputToES.parentFields)

    val childrenFrame = ESParentChildAdapter.createChildrenDataFrame(
      dataFrame = filteredDataFrame,
      childFields = OutputToESCLIArgs.childFields,
      parentField = Constants.WBCID_KEY,
      numberOfOutputField = OutputToESCLIArgs.numberOfOutputFiles.toInt)

    parentFrame.saveToEs(eSWriteOptions.parentOptions)
    childrenFrame.saveToEs(eSWriteOptions.childOptions)

    if (OutputToESCLIArgs.suggest.toBoolean) {
      val dataFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession = spark,
        dataFrame = childrenFrame,
        whitelist = OutputToESCLIArgs.esSuggestFields,
        ingestTime = now)

      dataFrames.foreach(df => df.saveToEs(eSWriteOptions.suggestOptions))
    }
  }
}