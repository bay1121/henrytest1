package com.wb.egress

import com.wb.constants.Constants
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, collect_set, lit, map}


object ESParentChildAdapter {
  val log: Logger = Logger.getLogger(getClass.getName)

  def filterOnInclusionAndRollup(df: DataFrame): DataFrame = {
    var out = df

    // Remove records from dataset for ES output
    if (out.columns.contains(Constants.ARROW_INCLUSION_KEY)) {
      out = out.filter(col(Constants.ARROW_INCLUSION_KEY) === true)
    }

    // Remove records from sites which have not been through QA
    if (out.columns.contains(Constants.SITEROLLUP_KEY)) {
      out = out.filter(col(Constants.SITEROLLUP_KEY).isNotNull)
    }

    out
  }

  def createParentsDataFrame(dataFrame: DataFrame,
                             groupByField: String,
                             parentFields: Array[String]): DataFrame = {

    var filteredDF = filterOnInclusionAndRollup(dataFrame)
    filteredDF = filteredDF.filter(col(groupByField).isNotNull)

    val aggregationFields = filteredDF.dtypes
      .filter(dtype => dtype._1 != groupByField && parentFields.contains(dtype._1))
      .map(x => collect_set(col(x._1)).alias(x._1))

    val parents: DataFrame = filteredDF
      .select(filteredDF.columns.intersect(parentFields).map(col): _*)
      .distinct
      .groupBy(groupByField)
      .agg(aggregationFields.head, aggregationFields.tail: _*)

    val es6Parents: DataFrame = parents.withColumn("_arrowjoin", lit("arrow_parent"))

    es6Parents
  }

  def createChildrenDataFrame(dataFrame: DataFrame,
                              childFields: Array[String],
                              parentField: String,
                              numberOfOutputField: Int): DataFrame = {

    val presentChildFields = childFields.intersect(dataFrame.columns).map(col)

    val selectedChildren = if (numberOfOutputField > 0) {
      dataFrame.select(presentChildFields: _*)
        .repartition(numberOfOutputField)
    } else {
      dataFrame.select(presentChildFields: _*)
    }

    val es6Children: DataFrame = selectedChildren.withColumn("_arrowjoin", map(lit("name"), lit("arrow_child"), lit("parent"), col(parentField)))

    es6Children
  }
}