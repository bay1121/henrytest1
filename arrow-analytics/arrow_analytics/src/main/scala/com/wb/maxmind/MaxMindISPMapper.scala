package com.wb.maxmind

import com.maxmind.geoip2.DatabaseReader
import com.maxmind.geoip2.model.IspResponse
import com.maxmind.geoip2.model.CityResponse
import com.google.common.net.InetAddresses
import com.maxmind.geoip2.exception.AddressNotFoundException
import java.net.InetAddress

import org.apache.commons.lang3.ObjectUtils

case class MaxMind(
  subdivisionName: String,
  subdivisionISOCode: String,
  countryCode: String,
  countryName: String,
  city: String,
  postalCode: String,
  continentName: String,
  geoCode: String) {

  def toSeq: Seq[String] = {
    Seq[String](
      subdivisionName,
      subdivisionISOCode,
      countryCode,
      countryName,
      city,
      postalCode,
      continentName,
      geoCode)
  }
}

object MaxMindISPMapper {
  val empty = MaxMind(null, null, null, null, null, null, null, null)

  def extractMaxMindFields(cityReader: DatabaseReader, ip: String): MaxMind = {

    if (ip != null && InetAddresses.isInetAddress(ip)) {
      try {
        val cityResponse = cityReader.city(InetAddress.getByName(ip))
        val someLat = Some(cityResponse.getLocation.getLatitude)
        val someLong = Some(cityResponse.getLocation.getLongitude)

        //maxmind defaults to this lat/long in US when the city cant be mapped
        if (someLat.contains(37.751) && someLong.contains(-97.822))
          MaxMind(
            null, null, cityResponse.getCountry.getIsoCode,
            cityResponse.getCountry.getName, null, null,
            cityResponse.getContinent.getName, null)
        else
          MaxMind(
            cityResponse.getMostSpecificSubdivision.getName,
            cityResponse.getMostSpecificSubdivision.getIsoCode,
            cityResponse.getCountry.getIsoCode,
            cityResponse.getCountry.getName,
            cityResponse.getCity.getName,
            cityResponse.getPostal.getCode,
            cityResponse.getContinent.getName,
            someLat.getOrElse("0.0") + ", " + someLong.getOrElse("0.0"))
      } catch {
        case ex: AddressNotFoundException => default
      }
    } else {
      default
    }
  }

  def default: MaxMind = {
    empty
  }
}