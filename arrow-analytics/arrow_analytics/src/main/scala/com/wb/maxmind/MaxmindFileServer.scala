package com.wb.maxmind

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.collection.Map

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path

import com.typesafe.config.ConfigFactory
import com.wb.constants.Constants

object MaxmindFileServer {

  private lazy val mmFiles = getMaxMindFilesAvailable

  /**
   * Get all available maxmind files
   */
  private def getMaxMindFilesAvailable: Map[String, String] = {
    val geoCityLoc = ConfigFactory.load.getString(Constants.GEOCITY_LOCATION)
    val path = new Path(geoCityLoc)

    val conf = new Configuration()
    val fileSystem = FileSystem.get(path.toUri, conf)

    val status = fileSystem.listStatus(path)
    val filesMap = status.map(stat => {
      val path = stat.getPath
      val mmdate = path.toString.split("/").last
      (mmdate, fileSystem.listStatus(path)(0).getPath.toString)
    }).toMap

    fileSystem.close
    filesMap
  }

  /**
   * Get the maxmind file to use for a given date
   */
  def getMaxMindFileForDate(rundate: String): String = {
    val date = LocalDate.parse(rundate, DateTimeFormatter.BASIC_ISO_DATE)

    //get file within last 10 days, otherwise failfast
    val fileDateOption = (0 until 10).map(x => {
      date.minusDays(x).format(DateTimeFormatter.BASIC_ISO_DATE)
    }).find(mmFiles.contains)

    if (fileDateOption.isEmpty)
      throw new RuntimeException(s"Maxmind file not found for $rundate")

    mmFiles(fileDateOption.get)
  }
}