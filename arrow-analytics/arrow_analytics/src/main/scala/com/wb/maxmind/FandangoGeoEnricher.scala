package com.wb.maxmind

import java.net.URI

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

import com.maxmind.geoip2.DatabaseReader
import com.typesafe.config.Config
import com.wb.config.ArrowLogger
import com.wb.constants.Constants
import com.wb.model.SchemaBuilder
import scala.collection.Map

object FandangoGeoEnricher extends GeoEnricher {

  /**
   * Enrigh the IP address with geo information based on the date associated with the IP. This involves
   * looking up the maxmind file corresponding to the date.
   */
  def enrich(spark: SparkSession, inputDF: DataFrame, configs: Config): DataFrame = {

    val withLocSchema = StructType(inputDF.schema.fields ++ SchemaBuilder.locSchemaFields)

    val dmaMap = DMAMapper.getDMAs(spark, configs)

    val withLocRDD = inputDF.rdd.map(row => (row.getAs[String](0), row.getAs[String](1))).groupByKey(200).mapPartitions(
      logRDDPartitions => {

        logRDDPartitions.flatMap(kv => {
          val date = kv._1

          if (date.toInt < configs.getInt(Constants.FANDANGO_IP_ENRICH_START)) {
            kv._2.map({ clientIP =>
              {
                val (mm: MaxMind, dmaSeq: Seq[Any]) = (MaxMindISPMapper.default, DMAMapper.dmaDefault)

                Row.fromSeq(Seq(date, clientIP) ++ mm.toSeq ++ dmaSeq)
              }
            })
          } else {
            val mmFile = MaxmindFileServer.getMaxMindFileForDate(date)
            ArrowLogger.log.info(s"$date, mmFile")

            val fileSystem = FileSystem.get(URI.create(mmFile), new Configuration())
            val inStream = fileSystem.open(new Path(mmFile))
            val cityReader = new DatabaseReader.Builder(inStream).build()

            val tmpList = kv._2.map({ clientIP =>

              val tmpMM = MaxMindISPMapper.extractMaxMindFields(cityReader, clientIP)

              val (mm, dmaSeq) = withDMA(tmpMM, dmaMap)

              Row.fromSeq(Seq(date, clientIP) ++ mm.toSeq ++ dmaSeq)
            }).toList

            inStream.close
            cityReader.close
            fileSystem.close
            tmpList.toIterable
          }
        })
      })

    spark.createDataFrame(withLocRDD, withLocSchema)
  }

  /**
   * US-only filter used for fandango purchase
   */
  override def withDMA(maxMind: MaxMind, dmaMap: Map[String, Seq[Any]]): (MaxMind, Seq[Any]) = {
    if (Constants.Country_US.equalsIgnoreCase(maxMind.countryCode)) {
      (maxMind, DMAMapper.getDMA(maxMind.postalCode, dmaMap))
    } else {
      (MaxMindISPMapper.default, DMAMapper.dmaDefault)
    }
  }
}
