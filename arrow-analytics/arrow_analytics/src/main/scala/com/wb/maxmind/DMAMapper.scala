package com.wb.maxmind

import java.io.File
import java.net.URI
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.collection.Map

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

import com.maxmind.geoip2.DatabaseReader
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.constants.Constants
import com.wb.model.DMADataSource
import com.wb.model.SchemaBuilder
import com.wb.support.CSVReader

object DMAMapper {

  /**
   * Enrich the input dataframe with Geo data, based on the IP address lookup using maxmind libraries
   */
  val dmaDefault = Seq[Serializable](null, null)

  def getDMAs(spark: SparkSession, configs: Config): Map[String, Seq[Any]] = {
    val csvReaderWithHeaders = new CSVReader(spark, hasHeader = true)
    val dmaFull = csvReaderWithHeaders.read(configs.getString(Constants.DMA_ZIPCODE_LOCATION), Some(DMADataSource.schema))

    dmaFull.rdd
      .keyBy(row => row.getString(0))
      .mapValues(row => Seq(row.getInt(1), row.getString(2)))
      .collectAsMap()
  }

  /**
   * Lookup DMA by postal code
   */
  def getDMA(postalCode: String, dmaMap: Map[String, Seq[Any]]): Seq[Any] = {
    if (postalCode != null) {
      dmaMap.getOrElse(postalCode, dmaDefault)
    } else {
      dmaDefault
    }
  }
}
