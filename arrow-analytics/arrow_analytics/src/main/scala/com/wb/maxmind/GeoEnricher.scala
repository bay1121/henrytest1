package com.wb.maxmind

import java.io.File
import java.net.URI
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.collection.Map

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

import com.maxmind.geoip2.DatabaseReader
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.wb.config.ArrowLogger
import com.wb.constants.Constants
import com.wb.model.DMADataSource
import com.wb.model.SchemaBuilder
import com.wb.support.CSVReader

trait GeoEnricher extends Serializable {

  /**
   * Enrich the input dataframe with Geo data, based on the IP address lookup using maxmind libraries
   */

  def enrich(spark: SparkSession, inputDF: DataFrame, ipfieldIndex: Integer,
    mmdbFileName: String, configs: Config): DataFrame = {

    val withLocSchema = StructType(inputDF.schema.fields ++ SchemaBuilder.locSchemaFields)

    val dmaMap = DMAMapper.getDMAs(spark, configs)

    val withLocRDD = inputDF.rdd.mapPartitions(
      logRDDPartitions => {
        val cityDatabase = new File(mmdbFileName)
        val cityReader = new DatabaseReader.Builder(cityDatabase).build()

        logRDDPartitions.map(record => {
          val rec = record.toSeq
          val clientIP = rec(ipfieldIndex).asInstanceOf[String]

          val tmpMM = MaxMindISPMapper.extractMaxMindFields(cityReader, clientIP)

          val (mm, dmaSeq) = withDMA(tmpMM, dmaMap)

          Row.fromSeq(rec ++ mm.toSeq ++ dmaSeq)
        })
      })

    spark.createDataFrame(withLocRDD, withLocSchema)
  }

  def withDMA(maxMind: MaxMind, dmaMap: Map[String, Seq[Any]]): (MaxMind, Seq[Any]) = {
    (maxMind, DMAMapper.getDMA(maxMind.postalCode, dmaMap))
  }
}

object GeoEnricher extends GeoEnricher
