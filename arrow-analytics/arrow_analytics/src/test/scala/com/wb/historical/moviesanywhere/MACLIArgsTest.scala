package com.wb.historical.moviesanywhere

import java.sql.Date

import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.kohsuke.args4j.{CmdLineException, CmdLineParser}
import org.scalatest.{Matchers, WordSpec}

class MACLIArgsTest extends WordSpec with Matchers {
  "MACLIArgs" should {
    
    "apply correct defaults" in {
      val parser = new CmdLineParser(MACLIArgs)

      
      MACLIArgs.skipTransactions shouldBe false
      MACLIArgs.skipConsumption shouldBe false
      MACLIArgs.runDate.getTime shouldBe 0
    }

    "forbid both skips" in {
      val parser = new CmdLineParser(MACLIArgs)

      a [CmdLineException] should be thrownBy
        parser.parseArgument("-skipTransactions", "-skipConsumption")
    }

    "validate runDate" in {
      val parser = new CmdLineParser(MACLIArgs)

      an [IllegalArgumentException] should be thrownBy
        parser.parseArgument("-rundate", "201801011")

      an [IllegalArgumentException] should be thrownBy
        parser.parseArgument("-rundate", "2018010")
    }

    "parse args successfully" in {
      val parser = new CmdLineParser(MACLIArgs)

      parser.parseArgument(
        "-rundate", "20180202",
        "-skipTransactions"
      )

      MACLIArgs.skipTransactions shouldBe true
      MACLIArgs.runDate.compareTo(new Date(
        DateTimeFormat
          .forPattern("yyyyMMdd")
          .withZone(DateTimeZone.UTC)
          .parseDateTime("20180202")
          .withTime(0, 0, 0, 0)
          .getMillis
      )) shouldBe 0
    }
  }
}
