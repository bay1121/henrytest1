package com.wb.historical

import java.sql.{Date, Timestamp}

import com.wb.constants.Constants
import com.wb.enrich.Attributes
import com.wb.model.{DMADataSource, DataSource}
import com.wb.support.{CSVReader, JSONReader}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DateType, StructField, StructType, TimestampType}
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import org.apache.spark.sql.expressions.UserDefinedFunction
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.ZoneId

import com.typesafe.config.{Config, ConfigFactory}
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.TimeZone

class AtomticketsActionTest extends WordSpec with Matchers with BeforeAndAfterAll {

  var sparkSession: SparkSession = _
  var getHash: UserDefinedFunction = _
  var configs: Config = _

  override def beforeAll(): Unit = {
    sparkSession = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    sparkSession.sparkContext.setLogLevel("WARN")
    getHash = sparkSession.udf.register("getHash", Attributes.getHashFcn _)

    configs = ConfigFactory.load()

  }

  override def afterAll(): Unit = sparkSession.stop()

  //we can move this function to Util
  def mapToArrowFields(dataFrame: DataFrame, fieldMapping: Map[String, String]): DataFrame = {
    val columns = dataFrame.columns
    var retFrame = dataFrame
    for (column <- fieldMapping.keySet) {
      if (columns.contains(column)) {
        retFrame = retFrame.withColumnRenamed(column, fieldMapping(column))
      }
    }

    retFrame
  }

  private[this] def createDataFrame(sparkSession: SparkSession, resourceLocation: String, source: DataSource): DataFrame = {
    val reader = new CSVReader(sparkSession, hasHeader = true)
    val loader = getClass.getClassLoader

    val df = reader.read(loader.getResource(resourceLocation).getPath, schema = Some(source.schema))
    mapToArrowFields(df, source.arrowMapping)
  }

  private[this] def convertToDate(date: String): Date = {
    new Date(
      LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
    )
  }

  "AtomTicketsAction" should {

    "load atom tickets correctly" in {
      val atomticketsFull = loadAtomtickets()

      atomticketsFull.count shouldBe 46

    }

    "generate atom tickets action correctly" in {
      val atomticketsFull = loadAtomtickets()
      val result = AtomticketsAction.generateActions(atomticketsFull)

      //result.printSchema
      //result.show(100, false)

      result.count shouldBe 17

    }

    "hash compositekey correctly" in {
      val atomticketsFull = loadAtomtickets()
      val atometicket_add_action = AtomticketsAction.generateActions(atomticketsFull)

      val result = AtomticketsAction.hashCompositeKeytoEvents(atometicket_add_action)

      //result.printSchema
      //result.show(100, false)

      result.count shouldBe 17

    }

    "validate run date correctly" in {

      val df = new SimpleDateFormat("yyyy-MM-dd")
      df.setTimeZone(TimeZone.getTimeZone("UTC"))

      val result = validateDate("20190122")

      df.format(result) shouldEqual "2019-01-22"

      val result1 = validateDate("")

      df.format(result1) shouldEqual "1970-01-01"

    }

    "capitalize setence correctly" in {
      val str1 = "Purchase"
      val str2 = "Here Is What We Really Want"
      val ret1 = capitalizeFirstLetters("PURCHASE")
      val ret2 = capitalizeFirstLetters("hEre is wHAt wE realLy want")

      ret1 shouldEqual str1
      ret2 shouldEqual str2

    }

  }

  def loadAtomtickets(): DataFrame = {
    val loader = getClass.getClassLoader

    val atomtickets = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("atomtickets/atomtickets.csv").getPath)
      .withColumn("DW_DATA_DT", to_timestamp(col("DW_DATA_DT"), "MM/dd/yyyy"))
      .withColumn("DW_LOAD_TS", to_timestamp(col("DW_LOAD_TS"), "MM/dd/yyyy HH:mm:ss"))

    //println("Atom Tickets Action, total: " + atomtickets.count)
    //atomtickets.printSchema()
    //atomtickets.show(50,false)

    atomtickets

  }


}