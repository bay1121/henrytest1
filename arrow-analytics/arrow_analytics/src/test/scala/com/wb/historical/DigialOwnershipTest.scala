package com.wb.historical

import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}

/**
  * Created by stephanieleighphifer on 3/3/17.
  */
class DigialOwnershipTest
  extends AsyncFlatSpec
    with Matchers
    with Eventually {

  "DigitalOwnership " should " require initial configuration values" in {
    eventually{
      a [IllegalArgumentException] should be thrownBy {
        DigitalOwnership.initConfiguration(Array())
      }
    }
  }

  "DigitalOwnership getTitleFormatFcn " should " return the appropriate title format based on digitalcopyformat and resolutionformat" in {
    val dcA = "BD 3D"
    val dcB = "BD Combo"
    val dcC = "BD Single"
    val dcD = "BD 3D Combo"
    val dcE = "DVD"
    val dcF = "UHD"
    val tfA = "Code Only Promotion HD"
    val tfB = "Multi-SKU HD"
    val tfC = "3rd Party Studio"
    val tfD = "SD"


    assert(DigitalOwnership.getTitleFormatFcn(dcA,null) == "BD")
    assert(DigitalOwnership.getTitleFormatFcn(dcB,null) == "BD")
    assert(DigitalOwnership.getTitleFormatFcn(dcC,null) == "BD")
    assert(DigitalOwnership.getTitleFormatFcn(dcD,null) == "BD")
    assert(DigitalOwnership.getTitleFormatFcn(dcE,null) == "DVD")
    assert(DigitalOwnership.getTitleFormatFcn(dcF,null) == "UHD")

    assert(DigitalOwnership.getTitleFormatFcn(tfA,null) == "Non-Disc HD")
    assert(DigitalOwnership.getTitleFormatFcn(tfB,null) == "Non-Disc HD")
    assert(DigitalOwnership.getTitleFormatFcn(tfC,null) == "Unknown")
    assert(DigitalOwnership.getTitleFormatFcn(null,tfD) == "Non-Disc SD")

    assert(DigitalOwnership.getTitleFormatFcn(null,null) == null)
  }
  //getTitleFormatFcn
  //getActivityTypeFcn

  "DigitalOwnership getActivityTypeFcn " should " return the activity type based on playback method" in {
    val pbA = "Flixster Download"
    val pbB = "Flixster Stream"

    assert(DigitalOwnership.getActivityTypeFcn(pbA) == "Download")
    assert(DigitalOwnership.getActivityTypeFcn(pbB) == "Stream")
    assert(DigitalOwnership.getActivityTypeFcn(null) == null)
  }
}