package com.wb.historical

import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}

import scala.reflect.io.Path
import scala.util.Try

/**
  * Created by stephanieleighphifer on 3/3/17.
  */
class IDMapperTest  extends AsyncFlatSpec
  with Matchers with Eventually with BeforeAndAfterAll {

  private val master = "local[1]"
  private val appName = "IDMapperTest"
  private val filePath: String = "target/testfile"

  override def beforeAll(): Unit = {
    val sparkConf = new SparkConf().setMaster(master).setAppName(appName).set("spark.driver.allowMultipleContexts", "true")
    val sc = new SparkContext(sparkConf)
  }

  override def afterAll(): Unit =  {
    Try(Path(filePath + "-1000").deleteRecursively)
  }
}