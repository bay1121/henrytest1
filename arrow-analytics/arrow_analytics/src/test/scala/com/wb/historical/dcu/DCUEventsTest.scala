package com.wb.historical.dcu

import java.sql.{Date, Timestamp}

import com.wb.enrich.Attributes
import com.wb.model.{DMADataSource, DataSource}
import com.wb.support.{CSVReader, JSONReader}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DateType, StructField, StructType, TimestampType}
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import org.apache.spark.sql.expressions.UserDefinedFunction
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.ZoneId
import java.util.{Calendar, TimeZone}

import com.typesafe.config.{Config, ConfigFactory}
import org.joda.time.{DateTime, DateTimeZone}
import com.wb.historical._

class DCUEventsTest extends WordSpec with Matchers with BeforeAndAfterAll {

  var sparkSession:SparkSession = _
  var getHash: UserDefinedFunction = _
  var configs: Config = _

  override def beforeAll(): Unit = {
    sparkSession = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    sparkSession.sparkContext.setLogLevel("WARN")
    getHash = sparkSession.udf.register("getHash", Attributes.getHashFcn _)

    configs = ConfigFactory.load()

  }

  override def afterAll(): Unit = sparkSession.stop()

  //we can move this function to Util
  def mapToArrowFields(dataFrame: DataFrame, fieldMapping: Map[String, String]): DataFrame = {
    val columns = dataFrame.columns
    var retFrame = dataFrame
    for (column <- fieldMapping.keySet) {
      if (columns.contains(column)) {
        retFrame = retFrame.withColumnRenamed(column, fieldMapping(column))
      }
    }

    retFrame
  }

  private[this] def createDataFrame(sparkSession: SparkSession, resourceLocation: String, source:DataSource): DataFrame = {
    val reader = new CSVReader(sparkSession, hasHeader=true)
    val loader = getClass.getClassLoader

    val df = reader.read(loader.getResource(resourceLocation).getPath, schema=Some(source.schema))
    mapToArrowFields(df, source.arrowMapping)
  }

  private[this] def convertToDate(date:String): Date = {
    new Date(
      LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
    )
  }

  "DCUEvents" should {
    "load dcu events correctly" in {
      val dcueventsFull = loadAllDCUEvents()

      dcueventsFull.count shouldBe 12

    }

    "load blukai logs correctly" in {
      val blklogsFull = loadAllBlklogs()

      blklogsFull.count shouldBe 8

    }

    "load dcu users correctly" in {
      val dcuusers = loadDCUUsers()

      dcuusers.count shouldBe 20
    }

    "load dcu user emails correctly" in {
      val dcuuseremails = loadDCUUserEmails()

      dcuuseremails.count shouldBe 20
    }

    "load dcu completed orders correctly" in {
      val dcuorders = loadDCUOrders()

      dcuorders.count shouldBe 11
    }

    "load dcu subscription status correctly" in {
      val dcusubscription = loadDCUSubscriptions()

      dcusubscription.count shouldBe 17

    }

    "load dcu merchant plans correctly" in {
      val dcumerchantplans = loadDCUMerchantPlans()

      dcumerchantplans.count shouldBe 36

    }

    "load dcu watched correctly" in {
      val dcuwatched = loadDCUWatched()

      dcuwatched.count shouldBe 9

    }

    "load dcu series correctly" in {
      val dcuseries = loadDCUSeries()

      dcuseries.count shouldBe 91

    }

    "load dcu episodes correctly" in {
      val dcuepisodes = loadDCUEpisodes()

      dcuepisodes.count shouldBe 97

    }

    "load product character list correctly" in {
      val dcuproduct_char = loadProductCharacters()

      dcuproduct_char.count shouldBe 10
    }

    "load site id list correctly" in {
      val blklogsFull = loadAllBlklogs()
      val siteids = "60276|60274|56137"
      val alist = siteids.split("\\|")
      //alist.foreach(println)

      blklogsFull.filter(col("site_id") isin (alist.toSeq: _*)).select("site_id").distinct.collect.map(row => row.getInt(0)).mkString("|") shouldBe siteids
    }

    "generate product name list correctly" in {
      val dcuorders = loadDCUOrders()
      val dcuproduct_name = DCUEvents.generateProductName(dcuorders)

      //dcuproduct_name.printSchema()
      //dcuproduct_name.show(100, false)

      dcuproduct_name.count shouldBe 10

    }

    "generate product category list correctly" in {
      val dcuorders = loadDCUOrders()
      val dcuproduct_cat = DCUEvents.generateProductCat(dcuorders)

      //dcuproduct_cat.printSchema()
      //dcuproduct_cat.show(100, false)

      dcuproduct_cat.count shouldBe 5

    }

    "dedup blukai logs correctly" in {
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      //blkdedup.printSchema()

      //blkdedup.show(100, false)

      blkdedup.count shouldBe 7

    }

    "dedup user subscription status correctly" in {
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)

      //dcusubscription_dedup.printSchema()
      //dcusubscription_dedup.show(100, false)

      dcusubscription_dedup.count shouldBe 5

    }

    "build user table including user name, email, merchant and subscription status correctly" in {
      val dcuusers = loadDCUUsers()
      val dcuuseremails = loadDCUUserEmails()
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)
      val dcumerchantplans = loadDCUMerchantPlans()

      val user_table = DCUEvents.buildUserTable(dcuusers, dcuuseremails, dcusubscription_dedup, dcumerchantplans)

      //user_table.printSchema()
      //user_table.orderBy("user_id")show(100, false)

      user_table.count shouldBe 20

    }

    "load id maps correctly while path not exist" in {
      val result = BuildMaps.loadIDMapsAll("\\path\\not\\exist", sparkSession)

      result.count shouldBe 0

    }

    "load cast maps correctly while no path provided" in {
      val result = BuildMaps.loadCastMapsAll("\\path\\not\\exist", sparkSession)

      result.count shouldBe 0

    }

    "build blukai id maps correctly" in {
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      val id_maps = BuildMaps.buildIdMaps(blkdedup)

      //id_maps.printSchema()

      //println("origin count is: " + id_maps.select("uid").count)
      //println("de-dup count is: " + id_maps.select("uid").distinct.count)

      //id_maps.show(500, false)

      //play around with compositekey
      //val id_maps_test = blkdedup.groupBy("uid")
      //  .agg(collect_set("phints.WBUID").alias("user_guids"), collect_set("compositekey").alias("compositekeys"))
      //  .select("uid", "user_guids", "compositekeys")
      //id_maps_test.printSchema()
      //id_maps_test.show(1000, false)
      //println("idmaps total: " +id_maps.count + ", with compositekey, totle: " +id_maps_test.count)

      id_maps.count shouldBe 7

    }

    "build cast data maps correctly" in {
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      val cast_maps = BuildMaps.buildCastMaps(blkdedup)

      //cast_maps.printSchema()

      //cast_maps.orderBy("bluekai_content_type").show(1000, false)

      //test WBSiteSec round 1
      //blkdedup.select("phints.WBSiteSec").distinct.show(100, false)

      //test WBSiteSec round 2
      //blkdedup.select("phints.WBSiteSec").
      //  filter(col("phints.WBCmcTitle").isNull &&  col("phints.WBMvTitle").isNull && col("phints.WBTvSrsTitle").isNull).distinct.show(100, false)

      //test WBSiteSec round 3
      //blkdedup.select("phints.WBSiteSec").
      //  filter(col("phints.WBCmcTitle").isNotNull ||  col("phints.WBMvTitle").isNotNull || col("phints.WBTvSrsTitle").isNotNull).distinct.show(100, false)

      //test WBSiteSec round 4
      //blkdedup.
      //  filter("phints.WBSiteSec == 'VideoSeriesDetail'").
      //  filter(col("phints.WBCmcTitle").isNull &&  col("phints.WBMvTitle").isNull && col("phints.WBTvSrsTitle").isNull).select("uid").show(100, false)

      cast_maps.count shouldBe 5
    }

    "Build idmaps correctly when adding to empty list" in {
      val idmap_existing = createEmptyDataframe(sparkSession)

      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      val id_maps = BuildMaps.buildIdMaps(blkdedup)

      val id_maps_new = BuildMaps.buildIdMaps(blkdedup, idmap_existing, 10000)

      id_maps.except(id_maps_new).count shouldBe 0

    }

    "Build castmaps correctly when adding to empty list" in {
      val castmap_existing = createEmptyDataframe(sparkSession)

      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      val cast_maps = BuildMaps.buildCastMaps(blkdedup)

      val cast_maps_new = BuildMaps.buildCastMaps(blkdedup, castmap_existing)

      cast_maps.except(cast_maps_new).count shouldBe 0

    }

    "filter dcu events with event name correctly" in {
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //dcuevents_filter.printSchema()
      //dcuevents_filter.show(100, false)

      dcuevents_filter.count shouldBe 11

    }

    //TO DO, not yet enabled.
    "dedup dcu events correctly" ignore {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val events_dedup = DCUEvents.removeEventsDuplicates(dcuevents_filter)

      assert(1 == 2)

    }

    "enrich events with blukai ids correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)

      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //events_addblkId.printSchema()

      //events_addblkId.show(1000, false)

      //println("DIFF is : " + events_addblkId.drop("bluekai_uid", "bluekai_compositekey").except(dcuevents_filter).count)

      //events_addblkId.drop("bluekai_uid", "bluekai_compositekey", "idfa", "adid", "maid", "website_section").except(dcuevents_filter).count shouldBe 0

      events_addblkId.count shouldBe 11

    }

    "enrich blukai logs with user information correctly" in {
      //build blukai
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)

      //build user table
      val dcuusers = loadDCUUsers()
      val dcuuseremails = loadDCUUserEmails()
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)
      val dcumerchantplans = loadDCUMerchantPlans()
      val user_table = DCUEvents.buildUserTable(dcuusers, dcuuseremails, dcusubscription_dedup, dcumerchantplans)

      val blukai_add_userinfo = DCUEvents.enrichUserInfotoBlukai(blkdedup, user_table)

      //blukai_add_userinfo.printSchema()

      //blukai_add_userinfo.show(200, false)

      //blukai_add_userinfo.drop("op", "time_stats", "phints", "user_name", "user_id", "user_guid", "date_joined",
      // "first_name", "last_name", "email", "premium_status", "premium_state", "merchant_type", "merchant_plan_id", "merchant_plan_name", "merchant_plan_price")
      // .except(blkdedup.drop("op", "time_stats", "phints")).count shouldBe 0

      blukai_add_userinfo.count shouldBe 5

    }

    "enrich events with cast info correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build cast
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)

      val events_add_cast = DCUEvents.enrichCasttoEvents(dcuevents_filter, cast_maps)

      //events_add_cast.printSchema()

      //events_add_cast.show(200, false)

      //println("DIFF is : " + events_add_cast.drop("bluekai_content_title", "bluekai_content_type", "bluekai_content_casts", "bluekai_content_keywords", "bluekai_content_urls").except(dcuevents_filter).count)
      //events_add_cast.select("cta_text", "event_name").distinct.collect().foreach(println)

      events_add_cast.drop("bluekai_content_title", "bluekai_content_type", "bluekai_content_casts", "bluekai_content_keywords", "bluekai_content_urls").except(dcuevents_filter).count shouldBe 0

      events_add_cast.count shouldBe 11

    }

    //currently we keep the existing events columns, just remove and rename them
    "consolidate events schema, remove and renamed specified columns correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val events_remove = columnsRemoveFromDataFrame(dcuevents_filter, DCUConstants.colsToRemove)

      //events_remove.printSchema()

      //events_remove.show(100, false)

      events_remove.count shouldBe 11

    }

    "consolidate events schema, add named columns correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val events_add = columnAddToDataFrame(dcuevents_filter, DCUConstants.colsToAdd)

      //events_add.printSchema()

      //events_add.show(100, false)

      events_add.count shouldBe 11

    }

    "enhance events with user information correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build user table
      val dcuusers = loadDCUUsers()
      val dcuuseremails = loadDCUUserEmails()
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)
      val dcumerchantplans = loadDCUMerchantPlans()
      val user_table = DCUEvents.buildUserTable(dcuusers, dcuuseremails, dcusubscription_dedup, dcumerchantplans)

      val events_add_userinfo = DCUEvents.enhanceUserInfotoEvents(dcuevents_filter, user_table)

      //events_add_userinfo.printSchema()

      //events_add_userinfo.show(500, false)

      //events_add_userinfo.drop("user_name", "user_guid", "date_joined", "first_name", "last_name", "email", "wbcid",
      //  "is_premium", "premium_status", "premium_state", "merchant_type", "merchant_plan_id", "merchant_plan_name", "merchant_plan_price")
      //  .except(dcuevents_filter).count shouldBe 0

      events_add_userinfo.count shouldBe 11

    }

    "enhance events with order and product information correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val dcuorders = loadDCUOrders()

      val dcuevents_add_productinfo = DCUEvents.enhanceProductInfotoEvents(dcuevents_filter, dcuorders)

      //dcuevents_add_productinfo.printSchema()

      //dcuevents_add_productinfo.show(500, false)

      //dcuevents_add_productinfo.drop("user_name", "user_guid", "date_joined", "first_name", "last_name", "email", "wbcid", "is_premium", "premium_status", "premium_state").except(dcuevents_filter).count shouldBe 0

      dcuevents_add_productinfo.count shouldBe 14

    }

    "enhance events with DMA related columns correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val dcuevents_add_dma = DCUEvents.enrichDMAtoEvents(sparkSession, dcuevents_filter, "", configs, true)

      //dcuevents_add_dma.printSchema()

      //dcuevents_add_dma.show(500, false)

      dcuevents_add_dma.count shouldBe 11

    }

    "enhance watched with series and episode information correctly" in {
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()

      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)

      //dcu_watched_add_more.printSchema()

      //dcu_watched_add_more.show(500, false)

      dcu_watched_add_more.count shouldBe 10

    }

    "enhance events with watched information correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()

      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)

      //val dcuevents_dedup = DCUEvents.removeEventDuplicates(dcuevents_filter)

      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(dcuevents_filter, dcu_watched_add_more)

      //dcuevents_add_watchedinfo.printSchema()

      //dcuevents_add_watchedinfo.show(500, false)

      dcuevents_add_watchedinfo.count shouldBe 11

    }

    "enhance events with comic books information correctly" in {
      val comic_name = "Batman (2011-)"
      val comic_genre = "Superheroes"

      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      val comicbooks = loadDCUComics()

      val result = DCUEvents.enhanceComicsInfotoEvents(dcuevents_filter, comicbooks)

      //result.printSchema()
      //result.show(500,false)

      result.count shouldBe 11

      val temp = result.filter(col("comics_series_title").isNotNull).limit(1)
      val comic_names = temp.select("comics_series_title").rdd.map(r => r(0)).collect()
      val comic_genres = temp.select("comics_genre").rdd.map(r => r(0)).collect()

      assert(comic_names.contains(comic_name))
      assert(comic_genres.contains(comic_genre))

    }

    "populate action field according to user activities correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)
      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //build cast
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)
      val events_add_cast = DCUEvents.enrichCasttoEvents(events_addblkId, cast_maps)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()
      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)
      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(events_add_cast, dcu_watched_add_more)

      val events_add_action = DCUEvents.populateUserActiontoEvents(dcuevents_add_watchedinfo)

      //events_add_action.printSchema()

      //events_add_action.show(500, false)

      events_add_action.count shouldBe 11

    }

    "match up with audience fields correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)
      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //build cast
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)
      val events_add_cast = DCUEvents.enrichCasttoEvents(events_addblkId, cast_maps)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()
      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)
      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(events_add_cast, dcu_watched_add_more)

      //build orders
      val dcuorders = loadDCUOrders()
      val dcuevents_add_productinfo = DCUEvents.enhanceProductInfotoEvents(dcuevents_add_watchedinfo, dcuorders)

      //build comics
      val comicbooks = loadDCUComics()
      val dcu_add_comics = DCUEvents.enhanceComicsInfotoEvents(dcuevents_add_productinfo, comicbooks)

      val events_add_action = DCUEvents.populateUserActiontoEvents(dcu_add_comics)

      val result = DCUEvents.columnsCosolidatedtoEvents(events_add_action)

      //println("DCU events, total: " + result.count)
      //result.printSchema()
      //result.show(500,false)

      result.count shouldBe 14

    }

    "perform validation correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)
      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //build cast
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)
      val events_add_cast = DCUEvents.enrichCasttoEvents(events_addblkId, cast_maps)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()
      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)
      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(events_add_cast, dcu_watched_add_more)

      //build orders
      val dcuorders = loadDCUOrders()
      val dcuevents_add_productinfo = DCUEvents.enhanceProductInfotoEvents(dcuevents_add_watchedinfo, dcuorders)

      //build comics
      val comicbooks = loadDCUComics()
      val dcu_add_comics = DCUEvents.enhanceComicsInfotoEvents(dcuevents_add_productinfo, comicbooks)

      val events_add_action = DCUEvents.populateUserActiontoEvents(dcu_add_comics)

      val events_consolidate = DCUEvents.columnsCosolidatedtoEvents(events_add_action)

      val dcuevents_rename_cols = mapToArrowFields(events_consolidate, DCUConstants.colsToRename)
      val dcuevents_remove_cols = columnsRemoveFromDataFrame(dcuevents_rename_cols, DCUConstants.colsToRemove)
      val dcuevents_add_cols = columnAddToDataFrame(dcuevents_remove_cols, DCUConstants.colsToAdd)

      val result = DCUEvents.validateFieldValuesToEvents(dcuevents_add_cols, sparkSession)

      //println("DCU events, total: " + result.count)
      //result.printSchema()
      //result.show(500,false)

      //result.select("user_id", "dcu_asset_name", "product_title", "event_type", "transaction_date", "segment_timestamp").orderBy("segment_timestamp").show(100, false)

      result.count shouldBe 14

      //result.filter(col("client_ip").isNotNull).count shouldBe 12
      //result.filter(col("comics_genre").isNotNull).count shouldBe 1

    }

    "Perform debup DCU events correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)
      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //build cast
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)
      val events_add_cast = DCUEvents.enrichCasttoEvents(events_addblkId, cast_maps)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()
      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)
      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(events_add_cast, dcu_watched_add_more)

      //build orders
      val dcuorders = loadDCUOrders()
      val dcuevents_add_productinfo = DCUEvents.enhanceProductInfotoEvents(dcuevents_add_watchedinfo, dcuorders)

      //build comics
      val comicbooks = loadDCUComics()
      val dcu_add_comics = DCUEvents.enhanceComicsInfotoEvents(dcuevents_add_productinfo, comicbooks)

      //build user table
      val dcuuseremails = loadDCUUserEmails()
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)
      val dcumerchantplans = loadDCUMerchantPlans()
      val user_table = DCUEvents.buildUserTable(dcuusers, dcuuseremails, dcusubscription_dedup, dcumerchantplans)
      val events_add_userinfo = DCUEvents.enhanceUserInfotoEvents(dcu_add_comics, user_table)

      val events_add_action = DCUEvents.populateUserActiontoEvents(events_add_userinfo)

      val events_consolidate = DCUEvents.columnsCosolidatedtoEvents(events_add_action)

      val dcuevents_rename_cols = mapToArrowFields(events_consolidate, DCUConstants.colsToRename)
      val dcuevents_remove_cols = columnsRemoveFromDataFrame(dcuevents_rename_cols, DCUConstants.colsToRemove)
      val dcuevents_add_cols = columnAddToDataFrame(dcuevents_remove_cols, DCUConstants.colsToAdd)

      val dcuevents_validated = DCUEvents.validateFieldValuesToEvents(dcuevents_add_cols, sparkSession)

      val result = DCUEvents.deDupPerAssetNametoEvents(dcuevents_validated)

      //println("DCU events, total: " + result.count)
      //result.printSchema()
      //result.show(500,false)

      //result.select("user_id", "dcu_asset_name", "product_title", "event_type", "transaction_date", "segment_timestamp").orderBy("segment_timestamp").show(100, false)


      result.count shouldBe 12

    }

    "Hash the composite key correctly" in {
      //build events
      val dcuevents = loadAllDCUEvents()
      val dcuevents_filter = DCUEvents.eventsFilterbyName(dcuevents)

      //build id map
      val blklogsFull = loadAllBlklogs()
      val blkdedup = DCUEvents.removeBlukaiDuplicates(blklogsFull)
      val id_maps = BuildMaps.buildIdMaps(blkdedup)
      val events_addblkId = DCUEvents.enrichBlkIdtoEvents(dcuevents_filter, id_maps)

      //build cast
      val cast_maps = BuildMaps.buildCastMaps(blkdedup)
      val events_add_cast = DCUEvents.enrichCasttoEvents(events_addblkId, cast_maps)

      //build watched
      val dcu_watched = loadDCUWatched()
      val dcu_series = loadDCUSeries()
      val dcu_episodes = loadDCUEpisodes()
      val dcuusers = loadDCUUsers()
      val dcu_watched_add_more = DCUEvents.enhanceMediaInfotoWatched(dcu_watched, dcuusers, dcu_series, dcu_episodes)
      val dcuevents_add_watchedinfo = DCUEvents.enhanceWatchedInfotoEvents(events_add_cast, dcu_watched_add_more)

      //build orders
      val dcuorders = loadDCUOrders()
      val dcuevents_add_productinfo = DCUEvents.enhanceProductInfotoEvents(dcuevents_add_watchedinfo, dcuorders)

      //build comics
      val comicbooks = loadDCUComics()
      val dcu_add_comics = DCUEvents.enhanceComicsInfotoEvents(dcuevents_add_productinfo, comicbooks)

      //build user table
      val dcuuseremails = loadDCUUserEmails()
      val dcusubscription = loadDCUSubscriptions()
      val dcusubscription_dedup = DCUEvents.removeSubscriptions(dcusubscription)
      val dcumerchantplans = loadDCUMerchantPlans()
      val user_table = DCUEvents.buildUserTable(dcuusers, dcuuseremails, dcusubscription_dedup, dcumerchantplans)
      val events_add_userinfo = DCUEvents.enhanceUserInfotoEvents(dcu_add_comics, user_table)

      val events_add_action = DCUEvents.populateUserActiontoEvents(events_add_userinfo)

      val events_consolidate = DCUEvents.columnsCosolidatedtoEvents(events_add_action)

      val dcuevents_rename_cols = mapToArrowFields(events_consolidate, DCUConstants.colsToRename)
      val dcuevents_remove_cols = columnsRemoveFromDataFrame(dcuevents_rename_cols, DCUConstants.colsToRemove)
      val dcuevents_add_cols = columnAddToDataFrame(dcuevents_remove_cols, DCUConstants.colsToAdd)

      val dcuevents_validated = DCUEvents.validateFieldValuesToEvents(dcuevents_add_cols, sparkSession)

      val dcuevents_dedup = DCUEvents.deDupPerAssetNametoEvents(dcuevents_validated)

      val result = DCUEvents.hashCompositeKeytoEvents(dcuevents_dedup, sparkSession)

      result.count shouldBe 11

      //result.select("compositekey", "user_id", "dcu_asset_name", "product_title", "event_type", "transaction_date", "segment_timestamp").orderBy("segment_timestamp").show(100, false)

      assert(result.count == result.select("compositekey").distinct.count)


    }

    "process maps correctly" ignore {


    }

    "process time status field correctly" ignore {
      val getValidTime = sparkSession.udf.register("getValidTime", Attributes.getValidTimeFromStringDefaultFcn _)
      val result = Attributes.getValidTimeFromStringFcn("2019-01-23", "yyyy-MM-dd")
      //println(result)
      //for ((k,v) <- result) println("key: %s, value: %s\n", k, v)

    }

    "date string is parsed correctly" ignore {
      val start_date = getDateStr(subtractDays(new java.sql.Date(Calendar.getInstance().getTime().getTime()), 60), "yyyyMMdd")
      println(start_date)

      val start_date1 = getDateStr(subtractDays(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime().getTime()), 1), "yyyyMMdd")
      println(start_date1)

      val add_one_day = getDateStr(addDays(getDate(start_date), 1), "yyyyMMdd")

      //println(add_one_day)

      //println(add_one_day > start_date)

      val dayLimit = new java.sql.Timestamp(new DateTime(DateTimeZone.UTC).minusDays(60).getMillis)

      //println(dayLimit)

    }

    "save events output correctly" ignore {

    }

    "save bluekai output correctly" ignore {

    }

    "Replace String with values in the Map correctly" in {
      val source = "browse VideoSeriesDetail,VideoHub,MovieDetail,Premium,ComicIssueDetail,ComicSeriesDetail,HomeHub,ComicHub"
      val replace = Map("VideoSeriesDetail" -> "", "MovieDetail" -> "", "ComicIssueDetail" -> "", "ComicSeriesDetail" -> "", "ArticleDetail" -> "", "TopicDetail" -> "", "Hub" -> "", "," -> " ")

      val result = replaceStrFromMap(source, replace)

      assert(result == "browse  Video  Premium   Home Comic")

    }

  }

  def loadAllDCUEvents(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcueventsFull = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("timestampFormat", "MM/dd/yyyy HH:mm:ss")
      .load(loader.getResource("dcu/dcu_events_new.csv").getPath)

    //println("DCU events, total: " + dcueventsFull.count)
    //dcueventsFull.printSchema()
    //dcueventsFull.show(500,false)

    dcueventsFull
  }

  def loadAllBlklogs(): DataFrame = {

    val loader = getClass.getClassLoader

    val blklogsFull = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/blk.csv").getPath)


    //convert back to map for phints and time_stats
    val convermapFN = sparkSession.udf.register("convertomapFcn", convertToMap _)

    val blk0 = blklogsFull
                .withColumn("phints", typedLit(convermapFN(col("phintsmap"))))
                .withColumn("time_stats", typedLit(convermapFN(col("timemap"))))
                .withColumn("op", typedLit(convermapFN(col("opmap"))))
                .drop("phintsmap")
                .drop("timemap")
                .drop("opmap")
        .withColumn("seen", typedLit(new java.sql.Timestamp(new DateTime(DateTimeZone.UTC).minusDays(0).getMillis)))


    //blk0.select("phints.WBUID").distinct.show(10, false)
    //blk0.select("time_stats.time").distinct.show(10, false)

    //println("Blukai logs, total: " + blk0.count)
    //blk0.printSchema()
    //blk0.show(500,false)

    blk0
  }

  def loadDCUUsers(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcuusers = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_users.csv").getPath)

    //println("DCU users, total: " + dcuusers.count)
    //dcuusers.printSchema()
    //dcuusers.show(50,false)

    dcuusers
  }

  def loadDCUUserEmails(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcuuseremails = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_user_emails.csv").getPath)

    //println("DCU user emails, total: " + dcuuseremails.count)
    //dcuuseremails.printSchema()
    //dcuuseremails.show(50,false)

    dcuuseremails
  }

  def loadDCUOrders(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcuorders = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_shop_order_new.csv").getPath)
      .withColumn("order_date", col("SEGMENT_TIMESTAMP").cast(DateType))

    //println("DCU orders, total: " + dcuorders.count)
    //dcuorders.printSchema()
    //dcuorders.show(50,false)

    dcuorders
  }

  def loadDCUComics(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcucomics = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_comics.csv").getPath)

    //println("DCU orders, total: " + dcucomics.count)
    //dcucomics.printSchema()
    //dcucomics.show(50,false)

    dcucomics
  }

  def loadDCUSubscriptions(): DataFrame = {

    val loader = getClass.getClassLoader

    val dcusubscriptions = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_subscription_status.csv").getPath)

    //println("DCU subscription status, total: " + dcusubscriptions.count)
    //dcusubscriptions.printSchema()
    //dcusubscriptions.show(50,false)

    dcusubscriptions
  }

  def loadDCUMerchantPlans(): DataFrame = {
    val loader = getClass.getClassLoader

    val dcumerchantplans = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_product_view.csv").getPath)

    //println("DCU merchant plans, total: " + dcumerchantplans.count)
    //dcumerchantplans.printSchema()
    //dcumerchantplans.show(50,false)

    dcumerchantplans

  }

  def loadDCUSeries(): DataFrame = {
    val loader = getClass.getClassLoader

    val dcuseries = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_series.csv").getPath)

    //println("DCU series, total: " + dcuseries.count)
    //dcuseries.printSchema()
    //dcuseries.show(50,false)

    dcuseries

  }

  def loadDCUEpisodes(): DataFrame = {
    val loader = getClass.getClassLoader

    val dcuepisodes = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_episodes.csv").getPath)

    //println("DCU episodes, total: " + dcuepisodes.count)
    //dcuepisodes.printSchema()
    //dcuepisodes.show(50,false)

    dcuepisodes

  }

  def loadDCUWatched(): DataFrame = {
    val loader = getClass.getClassLoader

    val dcuwatched = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_watched_new.csv").getPath)
      .withColumn("watched_date", col("VIEW_DATE").cast(DateType))

    //println("DCU watched, total: " + dcuwatched.count)
    //dcuwatched.printSchema()
    //dcuwatched.show(50,false)

    dcuwatched

  }

  def loadProductCharacters(): DataFrame = {
    val loader = getClass.getClassLoader

    val dcuproductchar = sparkSession.read.format("csv")
      .option("delimiter", ",")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(loader.getResource("dcu/dcu_product_character.csv").getPath)

    //println("DCU product characters, total: " + dcuproductchar.count)
    //dcuproductchar.printSchema()
    //dcuproductchar.show(50,false)

    dcuproductchar

  }

}
