package com.wb.historical.moviesanywhere

import java.math.BigDecimal
import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Calendar
import java.util.TimeZone

import scala.collection.mutable

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DateType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.TimestampType
import org.joda.time.format.DateTimeFormat
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.WordSpec

import com.timgroup.statsd.NoOpStatsDClient
import com.wb.constants.Constants
import com.wb.enrich.Attributes
import com.wb.historical.moviesanywhere.schema.ConsumptionDataSource
import com.wb.historical.moviesanywhere.schema.EidrDataSource
import com.wb.historical.moviesanywhere.schema.EidrFlixsterMapping
import com.wb.historical.moviesanywhere.schema.FlxFanMetadataSource
import com.wb.historical.moviesanywhere.schema.MAConstants
import com.wb.historical.moviesanywhere.schema.MAConstants.DataFields
import com.wb.historical.moviesanywhere.schema.TransactionsDataSource
import com.wb.historical.moviesanywhere.schema.UserContentDataSource
import com.wb.historical.moviesanywhere.schema.UserGenreDataSource
import com.wb.historical.moviesanywhere.schema.UserReleaseStatusDataSource
import com.wb.historical.moviesanywhere.schema.UsersDataSource
import com.wb.model.DMADataSource
import com.wb.model.DataSource
import com.wb.support.CSVReader
import com.wb.support.JSONReader

class MoviesAnywhereTest extends WordSpec with Matchers with BeforeAndAfterAll {
  var sparkSession: SparkSession = _
  var getHash: UserDefinedFunction = _

  override def beforeAll(): Unit = {
    sparkSession = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    sparkSession.sparkContext.setLogLevel("WARN")
    getHash = sparkSession.udf.register("getHash", Attributes.getHashFcn _)

  }

  override def afterAll(): Unit = sparkSession.stop()

  private[this] def createDataFrame(sparkSession: SparkSession, resourceLocation: String, source: DataSource): DataFrame = {
    val reader = new CSVReader(sparkSession, hasHeader = true)
    val loader = getClass.getClassLoader

    val df = reader.read(loader.getResource(resourceLocation).getPath, schema = Some(source.schema))
    MoviesAnywhere.mapToArrowFields(df, source.arrowMapping)
  }

  private[this] def convertToDate(date: String): Date = {
    new Date(
      LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyyMMdd")).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli())
  }

  "MoviesAnywhere" should {
    "process transactions data" in {
      val usersDF = createDataFrame(sparkSession, "moviesanywhere/users.csv", UsersDataSource)
      val currentUsers = MoviesAnywhere.filterAndDropOnCurrent(usersDF)

      val eidrDF = createDataFrame(sparkSession, "moviesanywhere/eidr.csv", EidrDataSource)
      val dmaDF = createDataFrame(sparkSession, "moviesanywhere/dma.csv", DMADataSource)

      var userGenreDF = createDataFrame(sparkSession, "moviesanywhere/usergenre.csv", UserGenreDataSource)
      val currentUserGenre = MoviesAnywhere.filterAndDropOnCurrent(userGenreDF)

      var userContentDF = createDataFrame(sparkSession, "moviesanywhere/usercontent.csv", UserContentDataSource)
      val currentUserContent = MoviesAnywhere.filterAndDropOnCurrent(userContentDF)

      var userReleaseStatusDF = createDataFrame(sparkSession, "moviesanywhere/userreleasestatus.csv", UserReleaseStatusDataSource)
      val currentUserRelease = MoviesAnywhere.filterAndDropOnCurrent(userReleaseStatusDF)

      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)
      val currentTransactions = MoviesAnywhere.filterAndDropOnCurrent(transactionsDF)
      val currentTransactionsCompositeKey = MoviesAnywhere.addCompositeKey(getHash, currentTransactions, DataFields.TRANSACTION_ID)

      val jsonReader = new JSONReader(sparkSession)
      val loader = getClass.getClassLoader
      val flixsterMetaData = MoviesAnywhere.mapToArrowFields(
        jsonReader.read(loader.getResource("moviesanywhere/flxFanMetadata.jsonl").getPath, Some(FlxFanMetadataSource.schema)),
        FlxFanMetadataSource.arrowMapping)
      val normalizeGenresUDF = sparkSession.udf.register("normalizeGenres", Attributes.normalizeGenres _)
      val improvedFlixsterMetadata = flixsterMetaData.withColumn("flixsterGenres", normalizeGenresUDF(col("flixsterGenres")))

      val eidrFlixsterMapping = createDataFrame(sparkSession, "moviesanywhere/eidrFlixsterMapping.csv", EidrFlixsterMapping)

      val result = MoviesAnywhere.processMoviesAnywhereData(
        dataToBeProcessed = currentTransactionsCompositeKey,
        users = currentUsers,
        eidrGenre = eidrDF,
        dma = dmaDF,
        userGenre = currentUserGenre,
        userContent = currentUserContent,
        userReleaseStatus = currentUserRelease,
        flxFanMetadata = improvedFlixsterMetadata,
        eidrFlixsterMapping = eidrFlixsterMapping,
        statsd = new NoOpStatsDClient)

      // Ensure fields are renamed
      for (column <- MAConstants.fieldMapping.keySet) {
        if (transactionsDF.columns.contains(column) && !MAConstants.fieldMapping(column).equals(column)) {
          result.columns should contain(MAConstants.fieldMapping(column))
          result.columns should not contain column
        }
      }

      val resultArray = result.collect()

      // Ensure we have 1 record with the correct # of cols and there is pertinent data from each DF we joined
      resultArray.length shouldBe 1
      resultArray.head.length shouldBe 51
      resultArray.head.getAs[String](Constants.EVENT_TYPE_KEY) shouldBe "Free Movie"
      resultArray.head.getAs[String](Constants.DEVICE_TYPE_KEY) shouldBe "Mobile Phone"
      resultArray.head.getAs[String](Constants.MOVIE_TITLE_KEY) shouldBe "Analyze That"
      resultArray.head.getAs[String](Constants.EMAIL_ADDRESS_KEY) shouldBe "john.doe@gmail.com"
      val expectedGenresFromFlixster = Array("Family/Kids", "Fantasy/Science Fiction", "Animation", "Musical/Performing Arts")
      resultArray.head.getAs[Array[String]](Constants.MOVIE_GENRES_KEY) shouldBe expectedGenresFromFlixster
      resultArray.head.getAs[Array[String]]("cast_top_1") shouldBe Array("Dom DeLuise")
      resultArray.head.getAs[Integer](Constants.DMA_CODE_KEY) shouldBe 803
      resultArray.head.getAs[String](Constants.DMA_NAME_KEY) shouldBe "Los Angeles, CA"
      resultArray.head.getAs[String](Constants.WBCID_KEY) shouldBe
        "b2f311a547212fba3602eb086ec7afe40913036c04e093ea11440d56048851fc"

      resultArray.head.getAs[String](Constants.COMPOSITE_KEY) shouldBe
        Attributes.getHashFcn("1")

      val genreComposition = resultArray.head.getAs[Map[String, String]](Constants.LIBRARY_GENRE_COMPOSITION)
      genreComposition shouldBe Map[String, Any]("Animation" -> "38", "Thriller" -> "6")

      val contentComposition = resultArray.head.getAs[Map[String, String]](Constants.LIBRARY_CONTENT_COMPOSITION)
      contentComposition shouldBe Map[String, Any]("Redemption" -> "17", "Free Movie" -> "21", "Est" -> "62")

      resultArray.head.getAs[String](Constants.SITEROLLUP_KEY) should be(Constants.MOVIESANYWHERE_SITE)
      resultArray.head.getAs[String](Constants.CONTENT_TYPE) should be("Feature")
    }

    "process consumption data" in {
      val usersDF = createDataFrame(sparkSession, "moviesanywhere/users.csv", UsersDataSource)
      val currentUsers = MoviesAnywhere.filterAndDropOnCurrent(usersDF)

      val eidrDF = createDataFrame(sparkSession, "moviesanywhere/eidr.csv", EidrDataSource)
      val csvReader = new CSVReader(sparkSession, hasHeader = true)
      val loader = getClass.getClassLoader
      val dmaDF = csvReader.read(loader.getResource("moviesanywhere/dma.csv").getPath, Some(DMADataSource.schema))

      var userGenreDF = createDataFrame(sparkSession, "moviesanywhere/usergenre.csv", UserGenreDataSource)
      val currentUserGenre = MoviesAnywhere.filterAndDropOnCurrent(userGenreDF)

      var userContentDF = createDataFrame(sparkSession, "moviesanywhere/usercontent.csv", UserContentDataSource)
      val currentUserContent = MoviesAnywhere.filterAndDropOnCurrent(userContentDF)

      var userReleaseStatusDF = createDataFrame(sparkSession, "moviesanywhere/userreleasestatus.csv", UserReleaseStatusDataSource)
      val currentUserRelease = MoviesAnywhere.filterAndDropOnCurrent(userReleaseStatusDF)

      val consumptionDF = createDataFrame(sparkSession, "moviesanywhere/consumption.csv", ConsumptionDataSource)
      val currentConsumption = MoviesAnywhere.filterAndDropOnCurrent(consumptionDF)
      val currentConsumptionCompositeKey = MoviesAnywhere.addCompositeKey(getHash, currentConsumption, DataFields.VIEWING_ID)

      val jsonReader = new JSONReader(sparkSession)
      val flixsterMetaData = MoviesAnywhere.mapToArrowFields(
        jsonReader.read(loader.getResource("moviesanywhere/flxFanMetadata.jsonl").getPath, Some(FlxFanMetadataSource.schema)),
        FlxFanMetadataSource.arrowMapping)
      val normalizeGenresUDF = sparkSession.udf.register("normalizeGenres", Attributes.normalizeGenres _)
      val improvedFlixsterMetadata = flixsterMetaData.withColumn("flixsterGenres", normalizeGenresUDF(col("flixsterGenres")))

      val eidrFlixsterMapping = createDataFrame(sparkSession, "moviesanywhere/eidrFlixsterMapping.csv", EidrFlixsterMapping)

      val result = MoviesAnywhere.processMoviesAnywhereData(
        dataToBeProcessed = currentConsumptionCompositeKey,
        users = currentUsers,
        eidrGenre = eidrDF,
        dma = dmaDF,
        userGenre = currentUserGenre,
        userContent = currentUserContent,
        userReleaseStatus = currentUserRelease,
        flxFanMetadata = improvedFlixsterMetadata,
        eidrFlixsterMapping = eidrFlixsterMapping,
        statsd = new NoOpStatsDClient)
      // Ensure fields are renamed
      for (column <- MAConstants.fieldMapping.keySet) {
        if (consumptionDF.columns.contains(column) && !MAConstants.fieldMapping(column).equals(column)) {
          result.columns should contain(MAConstants.fieldMapping(column))
          result.columns should not contain column
        }
      }

      // Ensure there are no decimal types
      //result.dtypes.map(_._2).foreach(x => x.contains("DecimalType") shouldBe false)
      val resultArray = result.collect()

      // Ensure we have 1 record with the correct # of cols and there is pertinent data from each DF we joined
      resultArray.length shouldBe 1
      resultArray.head.length shouldBe 53
      resultArray.head.getAs[Timestamp](Constants.CONTENT_VIEWED_DATE).toString shouldBe "2017-04-27 03:54:20.0"
      resultArray.head.getAs[String](Constants.MOVIE_TITLE_KEY) shouldBe "Die Hard"
      resultArray.head.getAs[String](Constants.DEVICE_TYPE_KEY) shouldBe "Test Replace Stuff"
      resultArray.head.getAs[String](Constants.EMAIL_ADDRESS_KEY) shouldBe "jane.doe@gmail.com"
      resultArray.head.getAs[Array[String]](Constants.MOVIE_GENRES_KEY) shouldBe Array("Action", "Suspense")

      resultArray.head.getAs[BigDecimal](Constants.DMA_CODE_KEY) shouldBe new BigDecimal(803)
      resultArray.head.getAs[String](Constants.DMA_NAME_KEY) shouldBe "Los Angeles, CA"
      resultArray.head.getAs[String](Constants.COMPOSITE_KEY) shouldBe
        Attributes.getHashFcn("3")

      val genreComposition = resultArray.head.getAs[Map[String, String]](Constants.LIBRARY_GENRE_COMPOSITION)
      genreComposition shouldBe Map[String, Any]("Animation" -> "12", "Thriller" -> "16")

      val contentComposition = resultArray.head.getAs[Map[String, String]](Constants.LIBRARY_CONTENT_COMPOSITION)
      contentComposition shouldBe Map[String, Any]("Redemption" -> "17", "Free Movie" -> "21")

      resultArray.head.getAs[String](Constants.SITEROLLUP_KEY) should be(Constants.MOVIESANYWHERE_SITE)
      resultArray.head.getAs[String](Constants.CONTENT_TYPE) should be("Feature")

      result
    }

    "not filter when runDate is default" in {
      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)

      val result = MoviesAnywhere.filterOnRundate(
        df = transactionsDF,
        runDate = new Date(0),
        dateField = MAConstants.DataFields.RETAILER_TRANSACTION_DT)

      // Ensure we have 3 records, since the records are tagged with the runDate
      result.count() shouldBe 3
    }

    "subtract dates give correct date" in {
      val format = new SimpleDateFormat("yyyyMMdd");
      val date = format.parse("20171114");

      val result = MoviesAnywhere.subtractDays(convertToDate("20171118"), 4)
      result shouldBe date
    }

    "fiter greater than date should return correct number of trans" in {
      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)

      val result = MoviesAnywhere.filterGreaterThanDate(
        df = transactionsDF,
        runDate = MoviesAnywhere.subtractDays(convertToDate("20171118"), 4),
        dateField = MAConstants.DataFields.LAST_MODIFIED_DT)

      // Ensure we have 0 records, since the record is tagged with a different date
      result.count() shouldBe 2
    }

    "filter OUT transaction data not matching runDate" in {
      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)

      val result = MoviesAnywhere.filterOnRundate(
        df = transactionsDF,
        runDate = convertToDate("20180101"),
        dateField = MAConstants.DataFields.RETAILER_TRANSACTION_DT)

      // Ensure we have 0 records, since the record is tagged with a different date
      result.count() shouldBe 0
    }

    "filter FOR transaction data matching runDate" in {
      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)

      val result = MoviesAnywhere.filterOnRundate(
        df = transactionsDF,
        runDate = convertToDate("20171113"),
        dateField = MAConstants.DataFields.RETAILER_TRANSACTION_DT)

      // Ensure we have 1 record, since the record is tagged with the runDate
      result.count() shouldBe 1
    }

    "filter OUT consumption data not matching runDate" in {
      val consumptionDF = createDataFrame(sparkSession, "moviesanywhere/consumption.csv", ConsumptionDataSource)

      val result = MoviesAnywhere.filterOnRundate(
        df = consumptionDF,
        runDate = convertToDate("20170429"),
        dateField = MAConstants.DataFields.VIEWING_TS)

      // Ensure we have 0 records, since the record is tagged with a different date
      result.count() shouldBe 0
    }

    "filter FOR consumption data matching runDate" in {
      val consumptionDF = createDataFrame(sparkSession, "moviesanywhere/consumption.csv", ConsumptionDataSource)

      val result = MoviesAnywhere.filterOnRundate(
        df = consumptionDF,
        runDate = convertToDate("20170427"),
        dateField = MAConstants.DataFields.VIEWING_TS)

      // Ensure we have 2 record, since the records are tagged with the runDate
      result.count() shouldBe 2
    }

    "get a directory date from runDate" in {
      val date = new java.sql.Date(
        DateTimeFormat
          .forPattern("yyyyMMdd")
          .withZoneUTC()
          .parseDateTime("20180101")
          .withTime(0, 0, 0, 0)
          .getMillis)
      MoviesAnywhere.getDirectoryDate(date) shouldBe "2018-01-01"
    }

    "get today's directory date from an empty runDate" in {
      val dirDateFormat = new SimpleDateFormat("yyyy-MM-dd")
      dirDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

      val now = Calendar.getInstance().getTime
      val expectedResult = dirDateFormat.format(now)
      MoviesAnywhere.getDirectoryDate(new Date(0)) shouldBe expectedResult
    }

    "filter and drop on DW Current" in {
      val transactionsDF = createDataFrame(sparkSession, "moviesanywhere/transactions.csv", TransactionsDataSource)
      val df = MoviesAnywhere.filterAndDropOnCurrent(transactionsDF)
      df.count() shouldBe 2
      df.columns should not contain DataFields.DW_CURRENT_FLAG
      df.collect().foreach(row => row.getAs[String](DataFields.MA_USER_ID) should not be "11111111-1111-1111-1111-111111111111")
    }

    "add release window when theater_release_date is valid" in {
      val schema = List(
        StructField("test_date", DateType, true),
        StructField("theater_release_date", TimestampType, true))

      val data = Seq(
        Row(Date.valueOf("2016-09-30"), Timestamp.valueOf("2015-12-02 03:04:00")))

      val df = sparkSession.createDataFrame(
        sparkSession.sparkContext.parallelize(data),
        StructType(schema))

      val result = MoviesAnywhere.addReleaseWindow(sparkSession, df, "test_date")
      result.head.getAs[Integer]("release_window_days") shouldBe 302
      result.head.getAs[String]("release_window") shouldBe "Catalog"
    }

    "set release window to null when theater_release_date is null" in {
      val schema = List(
        StructField("test_date", DateType, true),
        StructField("theater_release_date", TimestampType, true))

      val data = Seq(
        Row(Date.valueOf("2016-09-30"), null))

      val df = sparkSession.createDataFrame(
        sparkSession.sparkContext.parallelize(data),
        StructType(schema))

      val result = MoviesAnywhere.addReleaseWindow(sparkSession, df, "test_date")
      result.head.getAs[Integer]("release_window_days") shouldBe null
      result.head.getAs[String]("release_window") shouldBe null
    }
  }
}
