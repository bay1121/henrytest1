package com.wb.enrich.bluekaienrich

import com.wb.config.CommandLineArguments
import com.wb.support.JSONReader
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

class BluekaiEnrichAnalyticTest extends WordSpec with Matchers with BeforeAndAfterAll {
  var sparkSession:SparkSession = _

  override def beforeAll(): Unit = {
    sparkSession = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    sparkSession.sparkContext.setLogLevel("WARN")
  }

  override def afterAll(): Unit = sparkSession.stop()

  private[this] def createDataFrame(sparkSession: SparkSession, resourceLocation:String):DataFrame = {
    val reader = new JSONReader(sparkSession)
    val loader = getClass.getClassLoader

    reader.read(loader.getResource(resourceLocation).getPath, schema=None)
  }

  "BluekaiEnrichAnalytic" should {
    "left-outer join site data based on site_id field" in {
      val bkLogs = createDataFrame(sparkSession, "bluekaienrich/abbreviatedBluekaiLog.jsonl")
      val siteData = createDataFrame(sparkSession, "bluekaienrich/mockedSiteData.jsonl")

      val resultDF = BluekaiEnrichAnalytic.siteRollup(bkLogs, siteData)
      resultDF.count() shouldBe 8
      resultDF.where("siterollup == 'site111'").count() shouldBe 4
      resultDF.where("siterollup == 'site222'").count() shouldBe 4
    }

    "not filter records with null siteRollup if not in production mode" in {
      val bkLogs = createDataFrame(sparkSession, "bluekaienrich/abbreviatedBluekaiLog.jsonl")
      val siteData = createDataFrame(sparkSession, "bluekaienrich/mockedSiteData.jsonl")
      CommandLineArguments.mode = "dev"

      val resultDF = BluekaiEnrichAnalytic.siteRollup(bkLogs, siteData)
      resultDF.count() shouldBe 9
      resultDF.where(col("siterollup").isNull).count() shouldBe 1
    }

    "filter out unrelated events" in  {
      val bkLogs = createDataFrame(sparkSession, "bluekaienrich/abbreviatedBluekaiLog.jsonl")

      val resultDF = BluekaiEnrichAnalytic.filterRelatedEvents(bkLogs)
      resultDF.count() shouldBe 6
      resultDF.where("site_id == '111'").count() shouldBe 1
    }

    "filter out fandango test ids" in  {
      val bkLogs = createDataFrame(sparkSession, "bluekaienrich/abbreviatedBluekaiLog.jsonl")

      val resultDF = BluekaiEnrichAnalytic.filterFandangoTestIDs(bkLogs)
      resultDF.count() shouldBe 8
    }
  }
}
