package com.wb.enrich.tvseries

import org.apache.spark.sql.SparkSession

trait SparkSessionSetup {
  def withSparkSession(testMethod: (SparkSession) => Any) {
    val spark = SparkSession.builder()
        .appName("Spark Test")
        .master("local")
        .config("spark.ui.showConsoleProgress", value = false)  // prevent stderr progress bar
        .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    try {
      testMethod(spark)
    }
    finally spark.stop()
  }
}
