package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object CastSchema {
  val decimalType = DataTypes.createDecimalType(38, 0)

  val schema = StructType(
    Array(
      StructField("fx_tv_cast_key", LongType),
      StructField("id", decimalType),
      StructField("tv_episode_id", decimalType),
      StructField("movie_person_id", decimalType),
      StructField("role", StringType),
      StructField("deleted_ind", decimalType),
      StructField("source", StringType),
      StructField("sortkey", decimalType),
      StructField("last_update_date", TimestampType),
      StructField("tv_series_id", decimalType),
      StructField("cidw_dml_flag", StringType),
      StructField("cidw_etlload_id", decimalType),
      StructField("cidw_isrowcurrentind", BooleanType),
      StructField("cidw_rowhash", StringType),
      StructField("cidw_roweffectivestartdttm", TimestampType),
      StructField("cidw_roweffectiveenddttm", TimestampType),
      StructField("cidw_load_dttm", TimestampType)))
}
