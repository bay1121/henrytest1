package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object GenreSchema {
  val schema = StructType(
    Array(
      StructField("fx_genre_key",LongType),
      StructField("id",IntegerType),
      StructField("name",StringType),
      StructField("tags",StringType),
      StructField("cidw_dml_flag",StringType),
      StructField("cidw_etlload_id",LongType),
      StructField("cidw_isrowcurrentind",BooleanType),
      StructField("cidw_rowhash",StringType),
      StructField("cidw_roweffectivestartdttm",TimestampType),
      StructField("cidw_roweffectiveenddttm",TimestampType),
      StructField("cidw_load_dttm",TimestampType)
    )
  )
}
