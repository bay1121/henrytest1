package com.wb.enrich.tvseries

import com.wb.enrich.tvseries.schema._
import com.wb.support.CSVReader
import org.scalatest.concurrent.Eventually
import org.scalatest.{Matchers, WordSpec}

/**
  * Created by stephanieleighphifer on 2/21/17.
  */
class TVSeriesMetadataAnalyticTest
  extends WordSpec
    with Matchers
    with SparkSessionSetup
    with Eventually {

  "TVSeriesMetadataAnalytic" should {
    "create the expected output file" in withSparkSession { (sparkSession) =>
      val reader = new CSVReader(sparkSession, hasHeader = true)
      val loader = getClass.getClassLoader

      val analytic = new TVSeriesMetadataAnalytic(
        spark = sparkSession,
        flx = reader.read(loader.getResource("tvseries/series.csv").getPath, Some(SeriesSchema.schema)),
        flxGenre = reader.read(loader.getResource("tvseries/genre.csv").getPath, Some(GenreSchema.schema)),
        flxCast = reader.read(loader.getResource("tvseries/cast.csv").getPath, Some(CastSchema.schema)),
        flxPersonAll = reader.read(loader.getResource("tvseries/person.csv").getPath, Some(PersonSchema.schema)),
        flxSeason = reader.read(loader.getResource("tvseries/season.csv").getPath, Some(SeasonSchema.schema)),
        flxEpisode = reader.read(loader.getResource("tvseries/episode.csv").getPath, Some(EpisodeSchema.schema))
      )

      val resultDataFrame = analytic.run()
      resultDataFrame.count() shouldBe 5
    }
  }
}