package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object SeriesSchema {
  val decimalType = DataTypes.createDecimalType(38, 0)

  val schema = StructType(
    Array(
      StructField("fx_tv_series_key", LongType),
      StructField("id", decimalType),
      StructField("tv_network_id", decimalType),
      StructField("title", StringType),
      StructField("description", StringType),
      StructField("genre_id", decimalType),
      StructField("main_image", decimalType),
      StructField("status", StringType),
      StructField("creation_date", TimestampType),
      StructField("last_update_date", TimestampType),
      StructField("version", decimalType),
      StructField("start_year", decimalType),
      StructField("cidw_dml_flag", StringType),
      StructField("cidw_etlload_id", decimalType),
      StructField("cidw_isrowcurrentind", BooleanType),
      StructField("cidw_rowhash", StringType),
      StructField("cidw_roweffectivestartdttm", TimestampType),
      StructField("cidw_roweffectiveenddttm", TimestampType),
      StructField("cidw_load_dttm", TimestampType)))
}