package com.wb.enrich.tvseries

import org.apache.spark.{SparkConf, SparkContext}

trait SparkContextSetup {
  def withSparkContext(testMethod: (SparkContext) => Any) {
    val conf = new SparkConf()
      .setMaster("local")
      .setAppName("Spark test")
    val sparkContext = new SparkContext(conf)
    sparkContext.setLogLevel("WARN")
    try {
      testMethod(sparkContext)
    }
    finally sparkContext.stop()
  }
}
