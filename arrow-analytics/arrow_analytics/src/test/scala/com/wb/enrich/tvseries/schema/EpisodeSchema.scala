package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object EpisodeSchema {
  val decimalType = DataTypes.createDecimalType(38, 0)

  val schema = StructType(
    Array(
      StructField("fx_tv_episode_key", LongType),
      StructField("id", decimalType),
      StructField("tv_season_id", decimalType),
      StructField("episode_number", decimalType),
      StructField("title", StringType),
      StructField("synopsis", StringType),
      StructField("air_date", TimestampType),
      StructField("duration", decimalType),
      StructField("vchip_rating", StringType),
      StructField("main_image", decimalType),
      StructField("status", StringType),
      StructField("creation_date", TimestampType),
      StructField("last_update_date", TimestampType),
      StructField("version", decimalType),
      StructField("active_cast_source", StringType),
      StructField("synopsis_id", decimalType),
      StructField("main_trailer", decimalType),
      StructField("cidw_dml_flag", StringType),
      StructField("cidw_etlload_id", decimalType),
      StructField("cidw_isrowcurrentind", BooleanType),
      StructField("cidw_rowhash", StringType),
      StructField("cidw_roweffectivestartdttm", TimestampType),
      StructField("cidw_roweffectiveenddttm", TimestampType),
      StructField("cidw_load_dttm", TimestampType)))
}
