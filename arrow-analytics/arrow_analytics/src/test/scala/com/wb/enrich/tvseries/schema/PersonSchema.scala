package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object PersonSchema {
  val schema = StructType(
    Array(
      StructField("fx_movie_person_key",LongType),
      StructField("id",IntegerType),
      StructField("version",IntegerType),
      StructField("name",StringType),
      StructField("gender",StringType),
      StructField("primary_role",StringType),
      StructField("bio_id",IntegerType),
      StructField("bio",StringType),
      StructField("bio_long",StringType),
      StructField("bio_wikipedia",IntegerType),
      StructField("creation_date",TimestampType),
      StructField("last_update_date",TimestampType),
      StructField("dob",DateType),
      StructField("pob",StringType),
      StructField("dod",DateType),
      StructField("deleted_ind",IntegerType),
      StructField("public_ind",IntegerType),
      StructField("main_rtactor_image_id",IntegerType),
      StructField("cidw_dml_flag",StringType),
      StructField("cidw_etlload_id",LongType),
      StructField("cidw_isrowcurrentind",BooleanType),
      StructField("cidw_rowhash",StringType),
      StructField("cidw_roweffectivestartdttm",TimestampType),
      StructField("cidw_roweffectiveenddttm",TimestampType),
      StructField("cidw_load_dttm",TimestampType)
    )
  )
}
