package com.wb.enrich

import com.wb.constants.{Constants, Phints}
import com.wb.enrich.bluekaienrich.BluekaiEnrich
import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, Matchers}

class PhintParserTest
  extends AsyncFlatSpec
    with Matchers
    with Eventually {

  "BluekaiEnrich " should " require initial configuration values" in {
    eventually{
      a [RuntimeException] should be thrownBy {
        BluekaiEnrich.parseAndValidateArgs(Array())
      }
    }
  }

  "BluekaiEnrich.getPhintValueByKeyFcn " should " return the appropriate phint value when provided a key" in {
    val videoTitle="Test Video Title"
    val test = Map[String,String](
      Phints.VERTIGO_VID_TITLE -> videoTitle
    )

    assert(PhintParser.getPhintValueByKeyFcn(test,Constants.VIDEO_TITLE_KEY) == videoTitle)
  }

  "BluekaiEnrich.getPhintValue " should " return a phint value from a provided array of key values decoded " in  {
    val keys = Constants.attributePhintKeys.get(Constants.MOVIE_TITLE_KEY).orNull
    val title = "Test Movie's Title"

    val test = Map[String,String](
      Phints.OO_MV_TITLE -> title
    )

    assert(PhintParser.getPhintValue(null,keys) == null)
    assert(PhintParser.getPhintValue(test,keys) == title)
  }

  "BluekaiEnrich.getPhintValue " should " return a phint value from a provided array of key values " in  {
    val keys = Constants.attributePhintKeys.get(Constants.MOVIE_TITLE_KEY).orNull
    val title = "Test++Movie +Title&apos;"
    val test = Map[String,String](
      Phints.OO_MV_TITLE -> title
    )

    assert(PhintParser.getPhintValue(null,keys,false) == null)
    assert(PhintParser.getPhintValue(test,keys,false) == title)
  }

  "BluekaiEnrich.getProductPair " should  " return product title and type values " in {
    val movieTitle = "Fabuleux destin Amélie Poulain, Le"
    val seriesTitle = "Fabuleux destin Am&#233;lie Poulain, Le"
    val titleResolved = "Fabuleux destin Amélie Poulain, Le"
    val videoGameTitle = "NORTHLANDERS VOL. 2: THE CROSS + THE HAMMER"
    val comicBookTitle = "CB Title"
    var microsite = "Video Game Microsite Title"
    var siteRollup = "Game Microsite"

    val movieOut =  Seq(Constants.MOVIE,titleResolved,titleResolved,null,null,null)
    val seriesOut =  Seq(Constants.TV_SERIES,titleResolved,null,titleResolved,null,null)
    val videoGameOut =  Seq(Constants.VIDEO_GAME,videoGameTitle, null,null,videoGameTitle,null)
    val videoGameOutMicrosite =  Seq(Constants.VIDEO_GAME,microsite, null,null,microsite,null)
    val comicBookOut =  Seq(Constants.COMIC_BOOK,comicBookTitle,null,null,null,comicBookTitle)

    val moviePhints = Map[String,String](
      Phints.VERTIGO_MV_TITLE -> movieTitle
    )
    val seriesPhints = Map[String,String](
      Phints.VERTIGO_TV_SRS_TITLE -> seriesTitle
    )
    val videoGamePhints = Map[String,String](
      Phints.VERTIGO_VID_GM_TITLE -> videoGameTitle
    )
    val comicBookPhints = Map[String,String](
      Phints.DC_CMC_TITLE -> comicBookTitle
    )

    assert(PhintParser.getProductPair(moviePhints,titleResolved,null,null,null) == movieOut)
    assert(PhintParser.getProductPair(moviePhints,null,null,null,null) == movieOut)
    assert(PhintParser.getProductPair(seriesPhints,null,seriesTitle,null,null) == seriesOut)
    assert(PhintParser.getProductPair(seriesPhints,null,null,null,null) == seriesOut)
    assert(PhintParser.getProductPair(videoGamePhints,null,null,null,null) == videoGameOut)
    assert(PhintParser.getProductPair(videoGamePhints,null,null,siteRollup,microsite) == videoGameOut)
    assert(PhintParser.getProductPair(null,null,null,siteRollup,microsite) == videoGameOutMicrosite)
    assert(PhintParser.getProductPair(comicBookPhints,null,null,null,null) == comicBookOut)
  }

  "BluekaiEnrich.getVideoGameTitleFcn " should " return the video title or microsite value " in {
    var vgTitle = "Video Game Title VG"
    var dcTitle = "Video Game Title DC"
    var wbTitle = "Video Game Title WB"
    var microsite = "Video Game Microsite Title"
    var siteRollup = "Game Microsite"
    var phintsDC = Map[String,String](
      Phints.DC_VID_GM_TITLE -> dcTitle
    )

    var phintsVG = Map[String,String](
      Phints.VERTIGO_VID_GM_TITLE -> vgTitle
    )

    var phintsWB = Map[String,String](
      Phints.WBCOM_CNT_TYPE -> "Video Game",
      Phints.WBCOM_TITLE -> wbTitle
    )

    var phintsOther = Map[String,String] (
      Phints.FLX_MV_ID -> "12345"
    )

    assert(PhintParser.getVideoGameTitleFcn(phintsWB,siteRollup,microsite) == wbTitle)
    assert(PhintParser.getVideoGameTitleFcn(phintsWB,null,null) == wbTitle)
    assert(PhintParser.getVideoGameTitleFcn(phintsOther,siteRollup,microsite) == microsite)
    assert(PhintParser.getVideoGameTitleFcn(phintsVG,null,null) == vgTitle)
    assert(PhintParser.getVideoGameTitleFcn(phintsVG,siteRollup,null) == vgTitle)
    assert(PhintParser.getVideoGameTitleFcn(phintsDC,null,null) == dcTitle)
    assert(PhintParser.getVideoGameTitleFcn(null,null,microsite) == null)
    assert(PhintParser.getVideoGameTitleFcn(null,siteRollup,microsite) == microsite)
  }

  "BluekaiEnrich.getSeriesTitleFcn " should " return the series title " in {
    var seriesTitle = "Series Title"
    var seriesTitlePhint = "Phint Series Title"
    var phints = Map[String,String](
      Phints.DC_TV_SRS_TITLE -> seriesTitlePhint
    )

    var wbcomPhints = Map[String,String](
      Phints.WBCOM_CNT_TYPE -> "TV Series",
      Phints.WBCOM_TITLE -> seriesTitlePhint
    )

    assert(PhintParser.getSeriesTitleFcn(phints,seriesTitle) == seriesTitle)
    assert(PhintParser.getSeriesTitleFcn(phints,null) == seriesTitlePhint)
    assert(PhintParser.getSeriesTitleFcn(null,seriesTitle) == seriesTitle)
    assert(PhintParser.getSeriesTitleFcn(wbcomPhints,null) == seriesTitlePhint)
  }

  "BluekaiEnrich.getTitleFcn " should " return the movie title " in {
    var movieTitle = "Movie Title"
    var movieTitlePhint = "Phint Movie Title"
    var phints = Map[String,String](
      Phints.OO_MV_TITLE -> movieTitlePhint
    )

    var wbcomPhints = Map[String,String](
      Phints.WBCOM_CNT_TYPE -> "Movies",
      Phints.WBCOM_TITLE -> movieTitlePhint
    )

    assert(PhintParser.getTitleFcn(phints,movieTitle) == movieTitle)
    assert(PhintParser.getTitleFcn(phints,null) == movieTitlePhint)
    assert(PhintParser.getTitleFcn(null,movieTitle) == movieTitle)
    assert(PhintParser.getTitleFcn(wbcomPhints,null) == movieTitlePhint)
  }

  "BluekaiEnrich.getPhintValueByKeyAndFormatFcn " should " return the appropriate phint value when provided a key as Array of Strings" in {
    val topic ="Test, Video,Title"
    val topicSingle = "Test"
    val output = Seq[String](
      "Test",
      "Video",
      "Title"
    )

    val outputSingle = Seq[String](
      topicSingle
    )

    val test = Map[String,String](
      Phints.FLX_ART_TAG -> topic
    )

    val testSingle = Map[String,String](
      Phints.FLX_ART_TAG -> topicSingle
    )

    assert(PhintParser.getPhintValueByKeyAndFormatFcn(test,Constants.TOPIC_KEY) == output)
    assert(PhintParser.getPhintValueByKeyAndFormatFcn(testSingle,Constants.TOPIC_KEY) == outputSingle)
    assert(PhintParser.getPhintValueByKeyAndFormatFcn(Map[String,String](),Constants.TOPIC_KEY) == Seq())
  }

  "BluekaiEnrich.getEventTypeFcn " should " return the right event type based on phint values" in  {
    val impEvent = Map[String,String](
      Phints.EVENT -> "imp"
    )
    val clickEvent = Map[String,String](
      Phints.EVENT -> "click"
    )
    val bbcHomeEvent = Map[String,String](
      Phints.EVENT -> "Buy Button Click (Home)"
    )
    val bbcEvent = Map[String,String](
      Phints.EVENT -> "Buy Button Click"
    )
    val tvEvent = Map[String,String](
      Phints.EVENT -> "Trailer View"
    )

    val vidEvent = Map[String,String](
      Phints.VID_COMPLETE -> "75"
    )

    val cidEvent = Map[String,String](
      Phints.VID_COMPLETE -> "50",
      Phints.CID -> "12345"
    )

    val trailerEvent = Map[String,String](
      Phints.FN_PG_LVL_1 -> "video-player"
    )

    //standard event type
    assert(PhintParser.getEventTypeFcn(impEvent) == Constants.MEDIA_IMPRESSION)
    assert(PhintParser.getEventTypeFcn(clickEvent) == Constants.MEDIA_CLICK)
    assert(PhintParser.getEventTypeFcn(bbcHomeEvent) == Constants.PRE_ORDER)
    assert(PhintParser.getEventTypeFcn(bbcEvent) == Constants.PRE_ORDER)
    assert(PhintParser.getEventTypeFcn(tvEvent) == Constants.VIEWED_TRAILER)

    //vid complete
    assert(PhintParser.getEventTypeFcn(vidEvent) == Constants.MEDIA_VIDEO_COMPLETE)
    assert(PhintParser.getEventTypeFcn(cidEvent) == Constants.MEDIA_IMPRESSION)

    //trailer view
    assert(PhintParser.getEventTypeFcn(trailerEvent) == Constants.VIEWED_TRAILER)
  }

  "BluekaiEnrich.getWebsiteSectionFcn " should " return the website section calculated by phint " in {
    val phints = Map[String,String](
      Phints.FLX_SITE_SEC -> "video games"
    )

    val phintsVid = Map[String,String](
      Phints.DC_SITE_SEC -> "video"
    )

    val phintsIgnoreCase = Map[String,String](
      Phints.FLX_SITE_SEC -> "MoViE TIMEs"
    )

    val phintsPgLvl = Map[String,String](
      Phints.FN_PG_LVL_2 -> "movie show | times"
    )

    val outputIgnoreCase = "Movie Showtimes"

    assert(PhintParser.getWebsiteSectionFcn(phints) == "Video Game")
    assert(PhintParser.getWebsiteSectionFcn(phintsVid) == "Video")
    assert(PhintParser.getWebsiteSectionFcn(phintsIgnoreCase) == outputIgnoreCase)
    assert(PhintParser.getWebsiteSectionFcn(phintsPgLvl) == outputIgnoreCase)
    assert(PhintParser.getWebsiteSectionFcn(null) == null)
  }

  "BluekaiEnrich.getArticleTitleFcn " should " return the article title if phint exists" in  {
    val articleTitle = "NORTHLANDERS VOL. 2: THE CROSS + THE HAMMER"
    val vertArticleTitle = "NORTHLANDERS VOL. 2: THE CROSS + THE HAMMER"
    val rtTitle = "RottenTomatoes News"
    val rtJunk = " << More things that are in the title"
    val article = Map[String,String] (
      "FlxSiteSec" -> "Article",
      "__bk_t" -> articleTitle
    )

    val articleWithTitle = Map[String,String] (
      "vertigoArtTitle" -> vertArticleTitle,
      "__bk_t" -> articleTitle
    )

    val articleWithExtra = Map[String,String] (
      "vertigoArtTitle" ->  s"$rtTitle$rtJunk"
    )

    val notArticle = Map[String,String] (
      "FlxSiteSec" -> "Movie"
    )

    assert(PhintParser.getArticleTitleFcn(article) == articleTitle)
    assert(PhintParser.getArticleTitleFcn(articleWithTitle) == vertArticleTitle)
    assert(PhintParser.getArticleTitleFcn(articleWithExtra) == rtTitle)
    assert(PhintParser.getArticleTitleFcn(notArticle) == null)
    assert(PhintParser.getArticleTitleFcn(null) == null)
  }

  "BluekaiEnrich.getGenresFcn " should " return flixster genres or formatted genres array " in {
    assert(PhintParser.getGenresFcn(Map("FnMvGenre" -> "documentary,drama"),null) == Seq("Documentary","Drama"))
    assert(PhintParser.getGenresFcn(Map("FnMvGenre" -> "Action/adventure,adventure/action"),null) == Seq("Action/Adventure"))
    assert(PhintParser.getGenresFcn(Map("FnMvGenre" -> "documentary++music/performing+arts"),null) == Seq("Documentary", "Music/Performing Arts"))
    assert(PhintParser.getGenresFcn(Map("FnMvGenre" -> "documentary imax"),null) == Seq("Documentary","Imax"))

    assert(PhintParser.getGenresFcn(Map("FlxMvGenre" -> "Documentary,Drama,Gay"),null) == Seq("Documentary","Drama","Gay"))
    assert(PhintParser.getGenresFcn(Map("FlxMvGenre" -> "Documentary,+Television,+Special+Interest"),null) == Seq("Documentary", "Television", "Special Interest"))
    assert(PhintParser.getGenresFcn(Map("FlxMvGenre" -> "Documentary, Television, Special Interest"),null) == Seq("Documentary","Television","Special Interest"))

    assert(PhintParser.getGenresFcn(Map("FlxMvGenre" -> "Documentary, Television, Special Interest"),Seq("Comedy")) == Seq("Comedy"))
    assert(PhintParser.getGenresFcn(Map("FlxMvGenre" -> "Action & Adventure, Drama, Science Fiction & Fantasy"),null) == Seq("Action/Adventure", "Drama", "Fantasy/Science Fiction"))
  }


  "BluekaiEnrich.getFandangoIDs " should " calculate the hash of the provided user id or simply return hash" in {
    var hashed = "nodasheshere123"
    var notHashed = "dashes-123-so-many-dashes"
    var phintsHashed = Map[String,String](Phints.FN_UID -> hashed)
    var phintsNotHashed = Map[String,String](Phints.FN_UID -> notHashed)

    assert(PhintParser.getFandangoIDs(phintsHashed) == Seq(null,hashed))
    assert(PhintParser.getFandangoIDs(phintsNotHashed) == Seq(notHashed,"60bb2f0d127142ba8e7c350c9f359807475e73d3"))
  }

  "BluekaiEnrich.getTLDFcn " should " calculate the host suffix and country code if applicable " in {
    var url = "https://google.co.uk"
    var bkl = "https://google.co.es"
    var com = "https://google.com"
    var abc = "https://google.abc"

    var phints = Map[String,String](
      Phints.BK_L -> bkl
    )

    assert(PhintParser.getTLDValues(url,null) == Seq("uk","UK"))
    assert(PhintParser.getTLDValues(url,phints) == Seq("uk","UK"))
    assert(PhintParser.getTLDValues(null,phints) == Seq("es","ES"))
    assert(PhintParser.getTLDValues(com,null) == Seq("com","US"))
    assert(PhintParser.getTLDValues(com,phints) == Seq("com","US"))
    assert(PhintParser.getTLDValues(abc,phints) == Seq("abc",null))
  }

  "BluekaiEnrich.getAidPair " should " return the appropriate aid pair by type" in  {
    val aid = "bluekai"
    val bkPair = Seq(Phints.AID_BK_VAL,aid)
    val otherPair = Seq(Phints.AID_OTHER_VAL,null)

    assert(PhintParser.getAidPair(aid) == bkPair)
    assert(PhintParser.getAidPair(null) == otherPair)
  }
}
