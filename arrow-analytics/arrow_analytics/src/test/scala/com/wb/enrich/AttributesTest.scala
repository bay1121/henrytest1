package com.wb.enrich

import java.sql.Timestamp

import com.wb.constants.Constants
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, BeforeAndAfterAll, Matchers}

import scala.reflect.io.Path
import scala.util.Try

/**
  * Created by stephanieleighphifer on 2/21/17.
  */
class AttributesTest extends AsyncFlatSpec
  with Matchers with Eventually with BeforeAndAfterAll {

  private val master = "local[1]"
  private val appName = "AttributesTest"
  private val filePath: String = "target/testfile"

  override def beforeAll(): Unit = {
    val sparkConf = new SparkConf().setMaster(master).setAppName(appName).set("spark.driver.allowMultipleContexts", "true")
    val sc = new SparkContext(sparkConf)
  }

  override def afterAll(): Unit =  {
    Try(Path(filePath + "-1000").deleteRecursively)
  }

  "Attributes sha1Fcn " should " provide an input string" in {
    val input = "abcd-efd-12f-123jklm"
    val output = "771ab7cfb750f0af458b5490ff96c07bab106b97"
    val test = Attributes.sha1Fcn(input)

    assert(test == output)
  }

  "Attributes calculateHourWindowFcn " should " calculate hour window based on hour int and return null otherwise" in {
    assert(Attributes.calculateHourWindowFcn(0) == Constants.WINDOW_A)
    assert(Attributes.calculateHourWindowFcn(4) == Constants.WINDOW_B)
    assert(Attributes.calculateHourWindowFcn(8) == Constants.WINDOW_C)
    assert(Attributes.calculateHourWindowFcn(12) == Constants.WINDOW_D)
    assert(Attributes.calculateHourWindowFcn(16) == Constants.WINDOW_E)
    assert(Attributes.calculateHourWindowFcn(20) == Constants.WINDOW_F)
    assert(Attributes.calculateHourWindowFcn(69) == null)
    assert(Attributes.calculateHourWindowFcn(-69) == null)
  }

  //BluekaiCanonicalization.getHashFcn()
  "Attributes getHashFcn " should " provide a hash of the entire record" in {
    val input = "i6wHz4sH991+zU8Z\t1481323302\t142416,158621,158628,179159,618463,633817,672783,672784,672792\t/site/37549?ret=json&phint=FlxTheaterID%3D585855569&phint=FlxSection%3DTheater&phint=FlxUID%3D&phint=Asset%3DFlixster+Android+App&phint=FlxTheaterName%3DThe+Grand+16+-+Slidell&phint=appVersion%3D8.2.5&bkrid=1493515417&r=1860681&adid=531b6c93-54b0-4751-8e80-909c01a42ce2\t\t50.86.48.201wpxRMwsH991iSrlX\t1481323301\t142416,158621,158628,179159,618463,633817,639882,639884,639885,639887,639893,675713,686762\t/site/37335?ret=json&phint=FnPgLvl1%3Dmovie&phint=GglAdID%3D93c06387-64bf-4626-9eba-c5c3a92de28c&phint=FnMvGenre%3Dcomedy&phint=FnUID%3D2281f14cXXXXXXXXXXXXXXXXXXXXXXXf51fe1ef&phint=FnMvId%3D188530&phint=FnMvTitle%3Dalmostchristmas&phint=FnPgLvl2%3Dmovie+%7C+showtimes&phint=appVersion%3D1.0&bkrid=146XXXX387&r=8126666&adid=93c06387-64bf-4626-9eba-c5c3a92de28c\t107.77.197.109"
    val output = "5e5ce064b8f8beecfd7d1ffdfc6e0876"
    val test = Attributes.getHashFcn(input)

    assert(test == output)
  }

  "Attributes getValidTimeFcn " should " parse a date string and provide related date points" in {
    val input : Long = 1501148196000L
    val output = Map[String, String](
      "year" -> "2017",
      "hour" -> "9",
      "time" -> "1501148196000",
      "hour_window" -> Constants.WINDOW_C,
      "day" -> "27",
      "month" -> "7",
      "day_of_week" -> "Thursday"
    )

    assert(Attributes.getValidTimeFcn(input) == output)
  }

  "Attributes getValidTimeFromStringDefaultFcn " should " parse a date string and provide related date points" in {
    val input = "2016-12-26 19:23:39"
    val dateFormatted = DateTimeFormat.forPattern(Attributes.DEFAULT_DATE_FORMAT).withZone(DateTimeZone.UTC).parseDateTime(input)
    val millisToEpoch = dateFormatted.getMillis.toString
    val output = Map[String, String](
      "year" -> "2016",
      "hour" -> "19",
      "time" -> millisToEpoch,
      "hour_window" -> Constants.WINDOW_E,
      "day" -> "26",
      "month" -> "12",
      "day_of_week" -> "Monday"
    )

    val inputBad = "ABCDEFG"
    val outputBad = Map[String,String]()

    assert(Attributes.getValidTimeFromStringDefaultFcn(input) == output)
    assert(Attributes.getValidTimeFromStringDefaultFcn(inputBad) == outputBad)
  }

  "Attributes resolveProductTypeFcn " should " output the produc type based on input" in {
    var movie = "M"
    var tv = "T"
    var other = "S"

    assert(Attributes.resolveProductTypeFcn(movie) == Constants.MOVIE)
    assert(Attributes.resolveProductTypeFcn(tv) == Constants.TV_SERIES)
    assert(Attributes.resolveProductTypeFcn(other) == null)
  }

//  Attributes.getGenresFcn()
  "Attributes getGenresFcn " should " output an array of genres based on semicolon separator" in {
    var genres = "Comedy;Romantic;Science Fiction;Musical & Theater"
    var genresArrayMeta = Seq[String]("Romantic", "Science Fiction", "Musical & Theater")
    var genresOut = Seq[String]("Comedy", "Romantic", "Science Fiction", "Musical/Theater")
    var genresMetaOut = Seq[String]("Romantic", "Science Fiction", "Musical/Theater")

    assert(Attributes.getGenresFcn(genres,null) == genresOut)
    assert(Attributes.getGenresFcn(null,genresArrayMeta) == genresMetaOut)
    assert(Attributes.getGenresFcn(genres,genresArrayMeta) == genresMetaOut)
  }

  "Attributes normalizeGenres " should " normalize genres and return nomalized array" in {
    var genresArrayMeta = Seq[String]("Romantic", "Science Fiction", "Musical & Theater", "Action/Adventure","Adventure & Action")
    var genresMetaOut = Seq[String]("Romantic", "Science Fiction", "Musical/Theater", "Action/Adventure")

    assert(Attributes.normalizeGenres(genresArrayMeta) == genresMetaOut)
  }

  "Attributes resolveSource " should " return leading value it available" in {
    var a = "test1"
    var b = "test2"

    assert(Attributes.resolveSourceFcn(a,b) == a)
    assert(Attributes.resolveSourceFcn(null,b) == b)
    assert(Attributes.resolveSourceFcn(a,null) == a)
    assert(Attributes.resolveSourceFcn(null,null) == null)
  }

  "Attributes resolveArraySource " should " return leading value it available" in {
    var a = Seq[String]("a","b","c")
    var b = Seq[String]("c","d","e")

    assert(Attributes.resolveArraySourceFcn(a,b) == a)
    assert(Attributes.resolveArraySourceFcn(null,b) == b)
    assert(Attributes.resolveArraySourceFcn(a,null) == a)
    assert(Attributes.resolveArraySourceFcn(null,null) == null)
  }

  "Attributes.getReleaseWindowDays " should " calculate release window days" in {
    var today = "1488560691000" //2017-03-03T17:04:51.000Z

    val oldDate = "2017-02-03 00:00:00.000"
    val newDate = "2017-04-04 00:00:00.000"

    var older : Timestamp = Timestamp.valueOf(oldDate)
    var newer : Timestamp = Timestamp.valueOf(newDate)

    assert(Attributes.getReleaseWindowDays(today,older) == 28)
    assert(Attributes.getReleaseWindowDays(today,newer) == -31)
    assert(Attributes.getReleaseWindowDays(today,null) == null)

  }

  "Attributes.getReleaseWindow " should " calculate release window" in {
    assert(Attributes.getReleaseWindow(28) == Constants.THEATRICAL)
    assert(Attributes.getReleaseWindow(-31) == Constants.PRE_RELEASE)
    assert(Attributes.getReleaseWindow(null) == null)

  }

  "Attributes.getReleaseWindowPair " should " calculate release window" in {
    var today = "1488560691000"
    val oldDate = "2017-02-03 00:00:00.000"
    val newDate = "2017-04-04 00:00:00.000"

    var older : Timestamp = Timestamp.valueOf(oldDate)
    var newer : Timestamp = Timestamp.valueOf(newDate)

    assert(Attributes.getReleaseWindowPair(today,older) == Seq(Constants.THEATRICAL, 28))
    assert(Attributes.getReleaseWindowPair(today,newer) == Seq(Constants.PRE_RELEASE, -31))
    assert(Attributes.getReleaseWindowPair(today,null) == Seq(null,null))

  }

  "Attributes.getIsStatisticalID " should " identify statistical IDs" in {
    assert(!Attributes.getIsStatisticalID(null))
    assert(Attributes.getIsStatisticalID("1v2jnAts99ellndo"))
    assert(!Attributes.getIsStatisticalID("1v2jntss99llndo19"))
    assert(!Attributes.getIsStatisticalID("1v2jnAts99llndo"))
  }

  "Attributes getHashFcn " should " provide an input string" in {
    val input = "abcd-efd-12f-123jklm"
    val output = "771ab7cfb750f0af458b5490ff96c07bab106b97"

    assert(Attributes.getFnUIDHashFcn(input) == output)
    assert(Attributes.getFnUIDHashFcn(output) == output)
  }

  "Attributes.cleanupValue " should " cleanup URL encoding and HTML value" in {
    val test = "3%"
    val testN = "%n"
    val test2 = "3%25"
    val articleTitle = "NORTHLANDERS VOL. 2: THE CROSS + THE HAMMER"
    val movieTitle = "Fabuleux%20destin%20Am%26%23233%3Blie%20Poulain%2C%20Le"
    val seriesTitle = "Fabuleux destin Am&#233;lie Poulain, Le"
    val titleResolved = "Fabuleux destin Amélie Poulain, Le"
    val plus = "Jekyll + Hyde"
    val plusEncoded = "Jekyll%20%2B%20Hyde"
    
    assert(Attributes.cleanupValue(null) == null)
    assert(Attributes.cleanupValue(test) == test)
    assert(Attributes.cleanupValue(test2) == test)
    assert(Attributes.cleanupValue(testN) == testN)
    assert(Attributes.cleanupValue(movieTitle) == titleResolved)
    assert(Attributes.cleanupValue(seriesTitle) == titleResolved)
    assert(Attributes.cleanupValue(plus, false) == "Jekyll + Hyde")
    assert(Attributes.cleanupValue(plusEncoded, true) == "Jekyll + Hyde")
  }
}