package com.wb.enrich

import java.sql.Timestamp

import com.wb.constants.Constants
import org.joda.time.DateTime
import org.scalatest._
import org.scalatest.concurrent.Eventually

/**
  * Created by stephanieleighphifer on 2/14/17.
  */

class FlxFanMetadataTest
  extends AsyncFlatSpec
    with Matchers
    with Eventually {

  "FlxFanMetadata resolveSource " should " return leading value it available" in {
    var a = "test1"
    var b = "test2"
    assert(FlxFanMetadata.resolveSource(a,b) == a)
    assert(FlxFanMetadata.resolveSource(null,b) == b)
    assert(FlxFanMetadata.resolveSource(a,null) == a)
    assert(FlxFanMetadata.resolveSource(null,null) == null)

  }

  "FlxFanMetadata getRatingFcn " should " calculate rating leaning towards flixster data" in {
    assert(FlxFanMetadata.getRatingFcn("1",true) == Constants.RATING_G)
    assert(FlxFanMetadata.getRatingFcn("GP",false) == null)
    assert(FlxFanMetadata.getRatingFcn("6",true) == Constants.RATING_UNRATED)
    assert(FlxFanMetadata.getRatingFcn(null,true) == null)
    assert(FlxFanMetadata.getRatingFcn(null,false) == null)
    assert(FlxFanMetadata.getRatingFcn("Test",true) == null)
    assert(FlxFanMetadata.getRatingFcn("NC-17",false) == Constants.RATING_NC17)
    assert(FlxFanMetadata.getRatingFcn("PG 13",false) == Constants.RATING_PG13)
    assert(FlxFanMetadata.getRatingFcn("PG-13",false) == Constants.RATING_PG13)
    assert(FlxFanMetadata.getRatingFcn("PG_13",false) == Constants.RATING_PG13)
    assert(FlxFanMetadata.getRatingFcn("PG13",false) == Constants.RATING_PG13)
  }

  "FlxFanMetadata getReleaseYearFcn " should "calculate the release year from flixtser if available, fandango otherwise from timestamp" in {
    val flx = "2011-06-24 18:48:05.123456"
    val fan = "2013-09-25 18:48:05.123456"

    var flxTS : Timestamp = Timestamp.valueOf(flx)
    var fanTS : Timestamp = Timestamp.valueOf(fan)

    var flxYear = new DateTime(flxTS.getTime).getYear.toString
    var fanYear = new DateTime(fanTS.getTime).getYear.toString

    assert(FlxFanMetadata.getReleaseYearFcn(flxTS) == flxYear)
    assert(FlxFanMetadata.getReleaseYearFcn(fanTS) == fanYear)

  }
}

