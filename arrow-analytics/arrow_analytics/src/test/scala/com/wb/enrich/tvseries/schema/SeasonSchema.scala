package com.wb.enrich.tvseries.schema

import org.apache.spark.sql.types._

object SeasonSchema {
  val schema = StructType(
    Array(
      StructField("fx_tv_season_key",LongType),
      StructField("id",IntegerType),
      StructField("tv_series_id",IntegerType),
      StructField("season_number",IntegerType),
      StructField("name",StringType),
      StructField("description",StringType),
      StructField("main_image",IntegerType),
      StructField("status",StringType),
      StructField("bundle_ind",IntegerType),
      StructField("start_date",TimestampType),
      StructField("end_date",TimestampType),
      StructField("start_year",IntegerType),
      StructField("end_year",IntegerType),
      StructField("main_trailer",IntegerType),
      StructField("cidw_dml_flag",StringType),
      StructField("cidw_etlload_id",LongType),
      StructField("cidw_isrowcurrentind",BooleanType),
      StructField("cidw_rowhash",StringType),
      StructField("cidw_roweffectivestartdttm",TimestampType),
      StructField("cidw_roweffectiveenddttm",TimestampType),
      StructField("cidw_load_dttm",TimestampType)
    )
  )
}
