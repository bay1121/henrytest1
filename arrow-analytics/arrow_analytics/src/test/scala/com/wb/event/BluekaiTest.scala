package com.wb.event

import com.wb.constants.Constants
import org.scalatest.concurrent.Eventually
import org.scalatest.{AsyncFlatSpec, Matchers}

/**
  * Created by freddy.ayuso-henson on 10/10/17.
  */
class BluekaiTest
  extends AsyncFlatSpec
    with Matchers
    with Eventually {

  "Bluekai calculateHourWindowFcn " should " calculate hour window based on hour int and return null otherwise" in {
    assert(Bluekai.calculateHourWindowFcn(0) == Constants.WINDOW_A)
    assert(Bluekai.calculateHourWindowFcn(4) == Constants.WINDOW_B)
    assert(Bluekai.calculateHourWindowFcn(8) == Constants.WINDOW_C)
    assert(Bluekai.calculateHourWindowFcn(12) == Constants.WINDOW_D)
    assert(Bluekai.calculateHourWindowFcn(16) == Constants.WINDOW_E)
    assert(Bluekai.calculateHourWindowFcn(20) == Constants.WINDOW_F)
    assert(Bluekai.calculateHourWindowFcn(69) == null)
    assert(Bluekai.calculateHourWindowFcn(-69) == null)
  }

  "Bluekai getValidTimeFcn " should " parse a date string and provide related date points" in {
    val input: String = "1501148196"
    val output = Map[String, String](
      "year" -> "2017",
      "hour" -> "9",
      "time" -> "1501148196000",
      "hour_window" -> Constants.WINDOW_C,
      "day" -> "27",
      "month" -> "7",
      "day_of_week" -> "Thursday"
    )

    assert(Bluekai.getValidTimeFcn(input) == output)
  }

  "Bluekai getValidSiteIDFcn " should " parse a click-through URL and return the site ID" in {
    val input: String = "/site/36845?phint=appVersion%3D8.2&idfa=C586A3AE-EAEF-4E60-AFA2-9D876701D418&phint=FnPgLvl1%3Dpurchase&phint=FnMvGenre%3Daction%2Fadventure%20scifi%2Ffantasy"
    val output: String = "36845"

    assert(Bluekai.getValidSiteIDFcn(input) == output)
  }

  "Bluekai getPhintsList " should " parse a click-through URL and return phints (k-v pairs)" in {
    val input: String = "/site/36845?" +
      "phint=appVersion%3D8.2&" +
      "idfa=C586A3AE-EAEF-4E60-AFA2-9D876701D418&" +
      "phint=audience=++audience_id&" +
      "phint=FnPgLvl1%3Dpurchase&" +
      "phint=FnMvGenre%3Daction%2Fadventure%20scifi%2Ffantasy&" +
      "phint=WBTvSrsTitle%3DDC%26apos%3Bs+Legends&" +
      "phint=WBMvTitle%3DFabuleux%20destin%20Am%26%23233%3Blie%20Poulain%2C%20LeTest+Title++Movie%26apos%3Bs&" +
      "phint=__bk_t%3DNFL%20Star%20Popped%20for%20DUI%20at%20McD%26apos%3Bs%20%7C%20TMZ.com&" +
      "phint=vertigoCmcTitle%3DNORTHLANDERS%20VOL.%202%3A%20THE%20CROSS%20%2B%20THE%20HAMMER"

    val output = Map[String, String](
      "appVersion" -> "8.2",
      "idfa" -> "C586A3AE-EAEF-4E60-AFA2-9D876701D418",
      "audience" -> "++audience_id",
      "FnPgLvl1" -> "purchase",
      "FnMvGenre" -> "action/adventure scifi/fantasy",
      "WBTvSrsTitle" -> "DC's Legends",
      "WBMvTitle" -> "Fabuleux destin Amélie Poulain, LeTest Title  Movie's",
      "__bk_t" -> "NFL Star Popped for DUI at McD's | TMZ.com",
      "vertigoCmcTitle" -> "NORTHLANDERS VOL. 2: THE CROSS + THE HAMMER"
    )

    assert(Bluekai.getPhintsList(input) == output)
  }

  "Bluekai getCategoriesList " should " parse a comma-separated list of categories" in {
    val input: String = "123456,789123,012345"
    val output = "123456 789123 012345"

    assert(Bluekai.getCategoriesList(input) == output)
  }
}
