package com.wb.egress

import com.wb.constants.Constants
import com.wb.support.JSONReader
import com.wb.enrich.tvseries.SparkSessionSetup
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.collection.mutable

class BluekaiOutputToESTest extends WordSpec
  with Matchers
  with SparkSessionSetup
  with Eventually
  with BeforeAndAfterAll {

  "ESParentChildAdapter " should {
    val childFields = Array("event_type", "wbcid")
    val numOutputFiles = 1

    "limit the child data frame columns to the fields specified" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithInclusionWithRollup.json")
      val exptectedEventType = "Movie Ticket Purchase"

      val resultDF = ESParentChildAdapter.createChildrenDataFrame(dataFrame, childFields, Constants.WBCID_KEY, numOutputFiles)
      resultDF.columns.length shouldBe 3
      resultDF.columns shouldEqual childFields ++ Array("_arrowjoin")
      resultDF.count() shouldBe 1
      resultDF.take(1)(0).getString(0) shouldEqual exptectedEventType
    }

    "limit the parent dataFrame columns to the ones expected" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseParent.json")
      val parents = ESParentChildAdapter.createParentsDataFrame(dataFrame, Constants.WBCID_KEY, BluekaiOutputToES.parentFields)
      parents.columns.length shouldBe BluekaiOutputToES.parentFields.length + 1 // _arrowjoin
      parents.columns.sorted shouldEqual (BluekaiOutputToES.parentFields ++ Array("_arrowjoin")).sorted
    }

    "aggregate the parent data frame properly" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchasesParent.json")
      val resultDF = ESParentChildAdapter.createParentsDataFrame(dataFrame, Constants.WBCID_KEY, BluekaiOutputToES.parentFields)
      val row = resultDF.take(1)(0)

      // wbcid
      row.getString(0) shouldEqual "wbcid12345678"

      // client ip
      row.get(1).asInstanceOf[mutable.WrappedArray[String]].length shouldBe 2
      row.get(1).asInstanceOf[mutable.WrappedArray[String]](0) shouldEqual "2.2.2.2"
      row.get(1).asInstanceOf[mutable.WrappedArray[String]](1) shouldEqual "1.1.1.1"

      // Note: email address will actually be an Array with one good and one null value, but
      // the null gets removed some point during the write

      // maid
      row.get(3).asInstanceOf[mutable.WrappedArray[String]].length shouldBe 2
      row.get(3).asInstanceOf[mutable.WrappedArray[String]](0) shouldEqual "maid456"
      row.get(3).asInstanceOf[mutable.WrappedArray[String]](1) shouldEqual "maid123"
    }

    "filter out records when arrow_inclusion is false" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithoutInclusionWithRollup.json")
      ESParentChildAdapter.filterOnInclusionAndRollup(dataFrame).count() shouldBe 0
    }

    "filter out records with null siterollup" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithlusionWithoutRollup.json")
      ESParentChildAdapter.filterOnInclusionAndRollup(dataFrame).count() shouldBe 0
    }
  }

  "ESSuggesterAdapter" should {
    val expectedColumns = Array("release_year", "product_type", "release_window", "product_title",
      "mm_country_code", "rating", "event_type", "movie_title", "website_section", "siterollup")

    "create the correct number of data frames" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithInclusionWithRollup.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        expectedColumns,
        ingestTime = System.currentTimeMillis()
      )
      resultFrames.length shouldBe expectedColumns.length
    }

    "not create records when fields are empty or null" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchasesWithEmptyProductTitle.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        expectedColumns,
        ingestTime = System.currentTimeMillis()
      )

      val maybeFrame = resultFrames.find(frame => frame.take(1)(0).schema(1).name == "product_title")
      maybeFrame.isEmpty shouldBe false
      val productTitleFrame = maybeFrame.get
      productTitleFrame.count() shouldBe 1
    }

    "create the correct records when there are multiple entries in a field" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchases.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        expectedColumns,
        ingestTime = System.currentTimeMillis()
      )

      val maybeFrame = resultFrames.find(frame => frame.take(1)(0).schema(1).name == "product_title")
      maybeFrame.isEmpty shouldBe false
      val productTitleFrame = maybeFrame.get
      productTitleFrame.count() shouldBe 2

      productTitleFrame.filter(productTitleFrame("product_title")==="Spiderman").count() shouldBe 1
      productTitleFrame.filter(productTitleFrame("product_title")==="Black Panther 3D").count() shouldBe 1

      val compositeKey1 = productTitleFrame.take(2)(0).getString(0)
      val compositeKey2 = productTitleFrame.take(2)(1).getString(0)
      compositeKey1.equalsIgnoreCase(compositeKey2) shouldBe false
    }

    "include the correct columns" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithInclusionWithRollup.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        expectedColumns,
        ingestTime = System.currentTimeMillis()
      )

      resultFrames.foreach { frame =>
        frame.count() shouldBe 1

        val dynamicColumn = frame.columns(1)
        expectedColumns.contains(dynamicColumn)
      }
    }

    "not create multiple records for the same metadata column name and value pair " in withSparkSession { (sparkSession) =>
      // in this data set, there should be 2 identical movie titles records, and one unique
      val dataFrame = createDataFrame(sparkSession, "fandango/purchasesWithSameTitle.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        expectedColumns,
        ingestTime = System.currentTimeMillis()
      )

      var movieTitleColFound = false

      resultFrames.foreach { frame =>
        val dynamicColumn = frame.columns(1)
        if (dynamicColumn == "movie_title") {
          frame.count() shouldBe 2
          movieTitleColFound = true
        }
      }

      movieTitleColFound shouldBe true
    }

    "default to BKSuggest fields when presented with a null whitelist" in withSparkSession { (sparkSession) =>
      val dataFrame = createDataFrame(sparkSession, "fandango/purchaseWithInclusionWithRollup.json")
      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        null,
        ingestTime = System.currentTimeMillis()
      )
      resultFrames.length shouldBe expectedColumns.length
    }

    "handle Array(Map[String,String]) type suggesters" in withSparkSession { (sparkSession) =>
      val data = Seq(
        Row(
          Array(
            Map[String,String]("name" -> "Horror", "value" -> "10"),
            Map[String,String]("name" -> "Comedy", "value" -> "90")
          )
        )
      )

      val schema = List(StructField("composition", ArrayType(MapType(StringType,StringType,true),true), true))

      val dataFrame = sparkSession.createDataFrame(
        sparkSession.sparkContext.parallelize(data),
        StructType(schema)
      )

      val resultFrames = ESSuggesterAdapter.createSuggesterDataFrames(
        sparkSession,
        dataFrame,
        Array("composition"),
        ingestTime = System.currentTimeMillis()
      )
      resultFrames.length shouldBe 1
      resultFrames.head.columns.contains("composition") shouldBe true
      resultFrames.head.count() shouldBe 2
    }
  }

  private[this] def createDataFrame(sparkSession: SparkSession, resourceLocation: String): DataFrame = {
    val reader = new JSONReader(sparkSession)
    val loader = getClass.getClassLoader
    reader.read(loader.getResource(resourceLocation).getPath, schema=None)
  }
}