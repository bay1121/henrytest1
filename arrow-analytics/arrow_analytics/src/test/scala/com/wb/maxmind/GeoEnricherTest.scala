package com.wb.maxmind

import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.RawLocalFileSystem
import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToStringShouldWrapper

import com.maxmind.geoip2.DatabaseReader
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row
import org.apache.spark.sql.{ DataFrame, SparkSession }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }
import com.typesafe.config.ConfigFactory

class GeoEnricherTest extends FlatSpec with Matchers with BeforeAndAfterAll {
  var spark: SparkSession = _
  val config = ConfigFactory.load

  override def beforeAll(): Unit = {
    spark = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
  }

  override def afterAll(): Unit = spark.stop()

  "GeoEnricher" should "pass enrich tests" in {
    val spark = this.spark

    import spark.implicits._

    val inDF = Seq(
      (8, "2.125.160.216"),
      (64, "216.160.83.59"), //only US IP in this set
      (71, "89.160.20.112"), //has null postal code
      (75, "202.196.224.1"), //has postal code not found in DMA
      (-27, "invalid")).toDF("number", "ip")

    val outDs = GeoEnricher.enrich(spark, inDF, 1, MaxmindFileServer.getMaxMindFileForDate("20190126").replace("file:", ""), config).collect().map(_.toSeq)

    assert(Array(
      Array(8, "2.125.160.216", "West Berkshire", "WBK", "GB", "United Kingdom", "Boxford", "OX1", "Europe", "51.75, -1.25", 112, "London"),
      Array(64, "216.160.83.59", "Washington", "WA", "US", "United States", "Milton", "98354", "North America", "47.2513, -122.3149", 133, "Sanjan"),
      Array(71, "89.160.20.112", "Östergötland County", "E", "SE", "Sweden", "Linköping", null, "Europe", "58.4167, 15.6167", null, null),
      Array(75, "202.196.224.1", null, null, "PH", "Philippines", null, "34021", "Asia", "13.0, 122.0", null, null),
      Array(-27, "invalid", null, null, null, null, null, null, null, null, null, null)) === outDs)
  }
}
