package com.wb.maxmind

import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.RawLocalFileSystem
import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToStringShouldWrapper

import com.maxmind.geoip2.DatabaseReader
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row
import org.apache.spark.sql.{ DataFrame, SparkSession }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }
import com.typesafe.config.ConfigFactory

class DMAMapperTest extends FlatSpec with Matchers with BeforeAndAfterAll {
  var spark: SparkSession = _
  val config = ConfigFactory.load

  override def beforeAll(): Unit = {
    spark = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
  }

  override def afterAll(): Unit = spark.stop()

  "DMAMapper" should "pass DMA tests" in {
    val dmas = DMAMapper.getDMAs(spark, config)

    assert(Map(
      "OX1" -> Seq(112, "London"),
      "98354" -> Seq(133, "Sanjan")) === dmas)

    assert(Seq(null, null) === DMAMapper.getDMA("ox", dmas))
    assert(Seq(null, null) === DMAMapper.getDMA(null, dmas))
    assert(Seq(112, "London") === DMAMapper.getDMA("OX1", dmas))
  }
}
