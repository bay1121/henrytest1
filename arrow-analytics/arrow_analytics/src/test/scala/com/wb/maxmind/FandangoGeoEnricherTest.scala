package com.wb.maxmind

import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.RawLocalFileSystem
import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToStringShouldWrapper

import com.maxmind.geoip2.DatabaseReader
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row
import org.apache.spark.sql.{ DataFrame, SparkSession }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }
import com.typesafe.config.ConfigFactory

class FandangoGeoEnricherTest extends FlatSpec with Matchers with BeforeAndAfterAll {
  var spark: SparkSession = _
  val config = ConfigFactory.load

  override def beforeAll(): Unit = {
    spark = SparkSession.builder()
      .appName("Spark Test")
      .master("local")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
  }

  override def afterAll(): Unit = spark.stop()

  "FandangoGeoEnricher" should "enrich only IPs for USA" in {
    val spark = this.spark

    import spark.implicits._

    val inDF = Seq(
      (8, "2.125.160.216"),
      (64, "216.160.83.59"), //only US IP in this set
      (71, "89.160.20.112"), //has null postal code
      (75, "202.196.224.1"), //has postal code not found in DMA
      (-27, "invalid")).toDF("number", "ip")

    val outDs = FandangoGeoEnricher.enrich(spark, inDF, 1, MaxmindFileServer.getMaxMindFileForDate("20190126").replace("file:", ""), config).collect().map(_.toSeq)

    assert(Array(
      Array(8, "2.125.160.216", null, null, null, null, null, null, null, null, null, null),
      Array(64, "216.160.83.59", "Washington", "WA", "US", "United States", "Milton", "98354", "North America", "47.2513, -122.3149", 133, "Sanjan"),
      Array(71, "89.160.20.112", null, null, null, null, null, null, null, null, null, null),
      Array(75, "202.196.224.1", null, null, null, null, null, null, null, null, null, null),
      Array(-27, "invalid", null, null, null, null, null, null, null, null, null, null)) === outDs)
  }

  "FandangoGeoEnricher" should "enrich only IPs for USA based on date" in {
    val spark = this.spark

    import spark.implicits._

    val inDF = Seq(
      ("20190126", "2.125.160.216"),
      ("20190126", "216.160.83.59"), // US IP in this set
      ("20190127", "216.160.83.59"), // US IP in this set
      ("20190109", "216.160.83.59"), // US IP in this set
      ("20190128", "216.160.83.59"), // US IP in this set
      ("20190127", "89.160.20.112"), //has null postal code
      ("20190128", "202.196.224.1"), //has postal code not found in DMA
      ("20190126", "invalid")).toDF("number", "ip")

    val outDs = FandangoGeoEnricher.enrich(spark, inDF, config).collect().map(_.toSeq)

    assert(Array(
      Array("20190126", "2.125.160.216", null, null, null, null, null, null, null, null, null, null),
      Array("20190126", "216.160.83.59", "Washington", "WA", "US", "United States", "Milton", "98354", "North America", "47.2513, -122.3149", 133, "Sanjan"),
      Array("20190127", "216.160.83.59", "California", "CA", "US", "United States", "Pasadena", "98354", "North America", "47.2513, -122.3149", 133, "Sanjan"),
      Array("20190109", "216.160.83.59", null, null, null, null, null, null, null, null, null, null),
      Array("20190128", "216.160.83.59", "Texas", "TX", "US", "United States", "Dallas", "98354", "North America", "47.2513, -122.3149", 133, "Sanjan"),
      Array("20190127", "89.160.20.112", null, null, null, null, null, null, null, null, null, null),
      Array("20190128", "202.196.224.1", null, null, null, null, null, null, null, null, null, null),
      Array("20190126", "invalid", null, null, null, null, null, null, null, null, null, null)).sortWith((x, y) => x(0).toString + x(1).toString < y(0).toString + y(1).toString)
      === outDs.sortWith((x, y) => x(0).toString + x(1).toString < y(0).toString + y(1).toString))
  }

}
