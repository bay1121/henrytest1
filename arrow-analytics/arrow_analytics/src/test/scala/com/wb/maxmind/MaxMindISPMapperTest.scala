package com.wb.maxmind

import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.RawLocalFileSystem
import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToStringShouldWrapper

import com.maxmind.geoip2.DatabaseReader

class MaxMinsISPMapperTest extends FlatSpec {

  "MaxmindISPMapper" should "return the correct sequence" in {
    val mm = MaxMind("subDN", "subDC", "cC", "cN", "c", "pC", "contN", "gC")

    assert(Seq("subDN", "subDC", "cC", "cN", "c", "pC", "contN", "gC") === mm.toSeq)
  }

  "MaxmindISPMapper" should "return the right enrichment values" in {
    // uses a test/dummy maxmind file
    val geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate("20190124")
    val cityDatabase = new RawLocalFileSystem().pathToFile(new Path(geoCityLocFile))
    val cityReader = new DatabaseReader.Builder(cityDatabase).build()

    assert(Seq("West Berkshire", "WBK", "GB", "United Kingdom", "Boxford", "OX1", "Europe", "51.75, -1.25") ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "2.125.160.216").toSeq)

    assert(Seq(null, null, null, null, null, null, null, null) ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "abc.A").toSeq)

    assert(Seq(null, null, null, null, null, null, null, null) ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "8.8.8.8").toSeq)

    assert(Seq(null, null, null, null, null, null, null, null) ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, null).toSeq)
  }

  "MaxmindISPMapper" should "return the null city/zip for default lat long" in {
    // uses a test/dummy maxmind file
    val geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate("20190310")
    val cityDatabase = new RawLocalFileSystem().pathToFile(new Path(geoCityLocFile))
    val cityReader = new DatabaseReader.Builder(cityDatabase).build()

    assert(Seq("West Berkshire", "WBK", "GB", "United Kingdom", "Boxford", "OX1", "Europe", "51.75, -1.25") ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "2.125.160.216").toSeq)

    assert(Seq(null, null, null, null, null, null, null, null) ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "8.8.8.8").toSeq)

    assert(Seq(null, null, "US", "United States", null, null, "North America", null) ===
      MaxMindISPMapper.extractMaxMindFields(cityReader, "216.160.83.59").toSeq)
  }
}
