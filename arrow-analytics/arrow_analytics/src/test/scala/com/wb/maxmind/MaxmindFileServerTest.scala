package com.wb.maxmind

import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToStringShouldWrapper
import org.scalatest.Matchers.equal

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path

class MaxmindFileServerTest extends FlatSpec {

  "MaxmindFileServer" should "fetch correct maxmind file given a date" in {
    val rundate = "20190101"

    val cwd = System.getProperty("user.dir")
    val fileLoc = s"file:${cwd}/target/test-classes/maxmind/City"

    var geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate(rundate)
    geoCityLocFile should equal(s"${fileLoc}/${rundate}/GeoIP2-City.txt")

    geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate("20190104")
    geoCityLocFile should equal(s"${fileLoc}/${rundate}/GeoIP2-City.txt")

    geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate("20190109")
    geoCityLocFile should equal(s"${fileLoc}/20190106/GeoIP2-City.test.txt")

    geoCityLocFile = MaxmindFileServer.getMaxMindFileForDate("20190115")
    geoCityLocFile should equal(s"${fileLoc}/20190106/GeoIP2-City.test.txt")
  }

  "MaxmindFileServer" should "throw exception when file not available in previous 10 days" in {

    var thrown = intercept[Exception] {
      MaxmindFileServer.getMaxMindFileForDate("20191231")
    }
    assert(thrown.getMessage === "Maxmind file not found for 20191231")

    thrown = intercept[Exception] {
      MaxmindFileServer.getMaxMindFileForDate("20190116")
    }
    assert(thrown.getMessage === "Maxmind file not found for 20190116")

  }
}
