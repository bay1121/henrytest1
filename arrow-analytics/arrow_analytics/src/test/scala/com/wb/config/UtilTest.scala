package com.wb.config

import org.apache.spark.{SparkConf, SparkContext}
import org.json4s._
import org.scalatest._
import org.scalatest.concurrent.Eventually

import scala.reflect.io.Path
import scala.util.Try
import com.typesafe.config.{Config, ConfigFactory}
import com.amazonaws.services.secretsmanager.model.AWSSecretsManagerException

class UtilTest extends FlatSpec {

  "Util.getSecureCredential" should " return null where aws secrets cant be accessed" in {
    assert(Util.getSecureCredential("test") == null)
  }
  
  "Util.getSecureCredential" should " return 'secretValue' after mock" in {
      object myutil extends Util {
        def getAwsSecrets(secretName: String): String = "{\"secretKey\" : \"secreValue\"}"
      }
    
      assert(myutil.getSecureCredential("secretKey") == "secreValue")
  }
  
}
