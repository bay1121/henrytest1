package com.wb.data

import org.scalatest._
import org.scalatest.concurrent.Eventually

/**
  * Created by stephanieleighphifer on 2/14/17.
  */

class S3LoaderTest
  extends AsyncFlatSpec
    with Matchers
    with Eventually {

  "S3 Loader " should " require initial configuration values" in {
    
    eventually{
      a [RuntimeException] should be thrownBy {
        LoadGoogleDCM.parseAndValidateArgs(Array())
      }
    }
  }
}
