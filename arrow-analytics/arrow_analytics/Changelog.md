# Arrow_Analytics Changelog

### 1.4.x
- Conversion to parent-child

### 1.5.0
- Migrate HDFS to S3
- Automate and simplify build/deploy
- Configure environment handling
- Added Scoverage
- Added DataDog monitoring
- Refactor BKenrich pipeline
