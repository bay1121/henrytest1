from __future__ import print_function
from airflow.operators import BashOperator
from airflow.models import DAG
from datetime import datetime, timedelta
from common import properties
from emr import helper

import os

dag = DAG(
    'arrow_maxmind_pull_dag',
    default_args=helper.get_default_dag_args(properties),
    max_active_runs=1,
    catchup=False,
    schedule_interval='0 1 * * Wed,Thu'
)

download_task = BashOperator(
    task_id='download_maxmind_file',
    bash_command="/opt/airflow/dags/wb-ci-arrow/scripts/getMaxMind.sh {{ ds_nodash }} ",
    dag=dag
)
