from os import path

email = 'arrowsupport@warnerbros.com'
emr_log_location = 's3://cmdt-log/elasticmapreduce/bdd/arrow'
emr_tag_name_prefix =  'priv-bane-emr-bdd-arrow-'
emr_bootstrap_location = ['s3://cmdt-software/bdd/emrfs/configure_emrfs_lib.sh',
                          's3://cmdt-software/bdd/arrow/arrow_analytics/1.7.25/bootstrap.sh']
jar = 'arrow_analytics-v1.7.25-jar-with-dependencies.jar'

airflow_aws_connection_id = 'bdd-region'

owner = 'arrow-analytic'
run_as = 'airflow'

#this is on the emr machines , bootstrap copies files to this location
emr_artifacts_location = '/home/hadoop' 
jar = '{}/{}'.format(emr_artifacts_location, jar)
application_conf = '{}/application.conf'.format(emr_artifacts_location)
reference_conf = '{}/reference.conf'.format(emr_artifacts_location)

spark_confs = [        
    'spark.hadoop.fs.s3.enableServerSideEncryption=true',
    'spark.hadoop.fs.s3a.enableServerSideEncryption=true',
    'spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256',
    # --files makes files available on executor classpath and not on driver/masterNode in emr
    # if deploy mode is NOT cluster we need the application.conf to be read on the driver, 
    #spark.driver.extraClassPath messes up other classpaths as it is already set on EMR, 
    #so /home/hadoop is added to the spark.drive.extraClassPath in the bootstrap script
    'spark.driver.maxResultSize=4g',
    'spark.sql.shuffle.partitions=1024',
    'spark.sql.parquet.compression.codec=gzip',    
    'spark.hadoop.fs.s3.customAWSCredentialsProvider=com.wb.aws.security.CustomAWSSecurityProvider',
]

files = [application_conf, reference_conf]

run_date = '{{ ds_nodash }}'

def get_tags(name):
    return [{
               'Key': 'Name',
               'Value': emr_tag_name_prefix + name,
           },
           {
               'Key': 'Project',
               'Value': 'Arrow'
           },
           {
               'Key': 'owner',
               'Value': email
           },
           {
               'Key': 'Financial Owner',
               'Value': 'Lawrence Smith',
           },
           {
               'Key': 'Application Name',
               'Value': 'Arrow - Audience Record Repository',
           },
           {
               'Key': 'TS3 Number',
               'Value': 'TS3.40680',
           },
           {
               'Key': 'Cost Center',
               'Value': 'C0100763'
           }]