from datetime import datetime, timedelta
import airflow
from airflow import DAG
from airflow.contrib.operators.emr_create_job_flow_operator \
    import EmrCreateJobFlowOperator

# from airflow.contrib.operators.emr_add_steps_operator \
#     import EmrAddStepsOperator
# from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.operators.wb_airflow_plugin import EmrAddStepsOperator
from airflow.operators.wb_airflow_plugin import EmrStepSensor

from airflow.contrib.operators.emr_terminate_job_flow_operator \
    import EmrTerminateJobFlowOperator
from airflow.operators.bash_operator import BashOperator
import helper
    
""" This class is a helper to create a EMR job flow """

class EMR(object):
    
    def __init__(self, job_name, schedule, jar, aws_connection_id  = 'aws_default'):
        self.job_name = job_name
        self.schedule = schedule
        self.jar = jar
        self.aws_connection_id = aws_connection_id
        self.spark_steps = []
        
    def add_step(self, name, class_name, files = [], spark_conf = [], job_args = {}):
                            
        args = ['spark-submit',  '--class', class_name]
        
        for conf in spark_conf:
            args.append('--conf')
            args.append(conf)
         
        if files:
            args.append('--files')
            args.append(','.join(files))
    
        args.append(self.jar)
 
        for key in job_args:
            args.append(key)
            # support boolean params
            if job_args[key]:
                args.append(job_args[key])
        
        step = {
            'Name': name,
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': args
            }
        }
           
        self.spark_steps.append(step)
        

    def get_emr_dag(self, properties):
        
        run_as = properties.run_as
         
        dag = DAG(
            self.job_name,
            default_args=helper.get_default_dag_args(properties),
            max_active_runs=1,
            catchup=False,
            schedule_interval = self.schedule
        )
         
        cluster_creator = EmrCreateJobFlowOperator(
            task_id='create_cluster_job_flow',
            run_as_user = run_as,
            job_flow_overrides=helper.get_job_flow_overrides(properties, self.job_name),
            aws_conn_id=self.aws_connection_id,
            emr_conn_id='emr_default',
            dag=dag
        )
         
        step_adder = EmrAddStepsOperator(
            task_id='add_steps',
            run_as_user = run_as,
            job_flow_id="{{ task_instance.xcom_pull('create_cluster_job_flow', key='return_value') }}",
            aws_conn_id=self.aws_connection_id,
            steps=self.spark_steps,
            dag=dag
        ) 
        
        chain = cluster_creator >> step_adder
        
        for i in range(len(self.spark_steps)):         
            step_checker = EmrStepSensor(
                task_id='watch_{}'.format(str(self.spark_steps[i]['Name'])),
                run_as_user = run_as,
                job_flow_id="{{ task_instance.xcom_pull('create_cluster_job_flow', key='return_value') }}",
                #4 braces to escape python string formatting
                step_id="{{{{ task_instance.xcom_pull('add_steps', key='return_value')[{}] }}}}".format(str(i)),
                aws_conn_id=self.aws_connection_id,
                dag=dag
            )
            chain = chain >> step_checker
          
        cluster_remover = EmrTerminateJobFlowOperator(
            task_id='remove_cluster',
            run_as_user = run_as,
            job_flow_id="{{ task_instance.xcom_pull('create_cluster_job_flow', key='return_value') }}",
            aws_conn_id=self.aws_connection_id,
            dag=dag
        )
                
        chain >> cluster_remover
        
        return dag
