from datetime import datetime

def get_default_dag_args(properties):
    return  {
            'owner': properties.owner,
            'depends_on_past': False,
            'email': [properties.email],
            'email_on_failure': True,
            'email_on_retry': True,
            'retries': 0,
            'start_date': datetime(2019, 1, 20), #some randome date because airflow needs it
        }


def get_bootstrap_actions(properties):
    return [ 
            { 
                "Name": "Bootstrap_{}".format(idx),
                "ScriptBootstrapAction": { 
                    "Args": [],
                    "Path": bootstrapfile
                }
            } for idx, bootstrapfile in enumerate(properties.emr_bootstrap_location)
        ]

def get_job_flow_overrides(properties, name):  
    
    job_flow_base = {
        'Name': name,
        'JobFlowRole': 'bane-bdd-ec2-arrow-analytic-role',
        'ServiceRole': 'emr-service-bdd-restricted-role',
    }

    instance_base = {
        'Ec2KeyName': 'bane-emr-bdd-key',
        'Ec2SubnetId': 'subnet-a0beb4c4',
    }
    
    emr_tags = properties.get_tags(name) 
    
    job_flow_overrides = {
        'Applications' : [{'Name':'Spark'}, {'Name':'Ganglia'}],
        'LogUri': properties.emr_log_location,
        'ReleaseLabel': 'emr-5.20.0',
        'VisibleToAllUsers' : True,
        'BootstrapActions' : get_bootstrap_actions(properties),
        'Tags': emr_tags,
        
        #the following along with jobflowbase and instance base can be a airflow connection
        'Instances' : {
            'InstanceGroups': [
                {
                    'Name': 'Master nodes',
                    'Market': 'ON_DEMAND',
                    'InstanceRole': 'MASTER',
                    'InstanceType': 'm5.2xlarge',
                    'InstanceCount': 1
                },
                {
                    'Name': 'Slave nodes',
                    'Market': 'SPOT',
                    'InstanceRole': 'CORE',
                    'InstanceType': 'm5.4xlarge',
                    'InstanceCount': 10,
                    'EbsConfiguration': {
                    'EbsBlockDeviceConfigs': [
                            {
                                # default is 32G and bluekai enrich runs out of disk, ebs is cheap bumping to 300
                                'VolumeSpecification': {
                                    'VolumeType': 'gp2',
                                    'SizeInGB': 300
                                }
                            },
                        ],
                    },
                }
            ],
        },
        
        'Configurations': [
            {
                "Classification": "spark",
                "Properties": {
                    "maximizeResourceAllocation": "true"
                }
            },
            {
                #emr sets the extraClassPath in spark-defaults.conf where it adds a bunch of aws jars
                #it is almost impossible to override from spark-submit (we have to know all aws jars)
                #so we append out classpath to HADOOP_CONF_DIR here (it makes it to the classpath)

                "Classification":"spark-env", 
                "Configurations":[
                    {
                        "Classification":"export", 
                        "Properties":{
                            "HADOOP_CONF_DIR":"/etc/hadoop/conf:/home/hadoop"
                        }
                    }
                ]
            }
        ]
    }
    
    job_flow_overrides.get('Instances').update(instance_base)
    job_flow_overrides.update(job_flow_base)
    
    return job_flow_overrides
