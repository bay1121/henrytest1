#!/bin/bash

ROOT_LOC=s3://cmdt-software/bdd/arrow

BASE_LOC=$ROOT_LOC/arrow_analytics
BASE_LOC_VERSION=$BASE_LOC/1.7.25
JAR=arrow_analytics-v1.7.25-jar-with-dependencies.jar

aws s3 cp ${BASE_LOC_VERSION}/application.conf /home/hadoop/application.conf
aws s3 cp ${BASE_LOC_VERSION}/reference.conf /home/hadoop/reference.conf
aws s3 cp ${BASE_LOC_VERSION}/${JAR} /home/hadoop/${JAR}

