#!/usr/bin/env bash

set -e

trap 'printf "\n\nINSTALL FAILED !!!!!!\n"' INT TERM QUIT

BASE_DIR=`dirname "$0"`

BASE_LOC=s3://dev-cmdt-software/bdd/emrfs
JAR=arrow_analytics-v1.7.25-jar-with-dependencies.jar
EMRFS_JAR=emrfs-auth-v1.7.25.jar
SECURITY_CLASS=com/wb/aws/security/CustomAWSSecurityProvider.class

pushd $BASE_DIR/../../

# This is a crazy way to do this - did not want to do a separate git project
# Common modules have to be in their own git, TODO later
jar -xf $JAR $SECURITY_CLASS
jar -cf $EMRFS_JAR $SECURITY_CLASS

popd

aws s3 cp $BASE_DIR/../../$EMRFS_JAR ${BASE_LOC}/${EMRFS_JAR} --sse
aws s3 cp $BASE_DIR/s3/configure_emrfs_lib.sh ${BASE_LOC}/configure_emrfs_lib.sh --sse

trap - INT TERM QUIT

printf "\n\nINSTALL SUCCCESSFUL emrfs-v1.7.25\n"
