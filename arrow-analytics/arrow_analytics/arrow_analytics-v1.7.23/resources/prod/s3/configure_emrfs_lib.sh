#!/bin/bash

# This config script is installed as an indepenent part of the arrow project

BASE_LOC=s3://cmdt-software/bdd/emrfs
EMRFS_LIB=/usr/share/aws/emr/emrfs/auxlib

sudo mkdir -p $EMRFS_LIB
sudo aws s3 cp $BASE_LOC/emrfs-auth-v1.7.23.jar $EMRFS_LIB
