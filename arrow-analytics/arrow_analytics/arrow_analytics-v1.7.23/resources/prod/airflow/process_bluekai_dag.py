from airflow.models import DAG
from emr.emr import EMR
from common import properties
from common.properties import files, spark_confs, jar, run_date, airflow_aws_connection_id

job_name = 'arrow_bluekai'
schedule = "0 6 * * *"

bk_enrich_es_args = {'-rundate':run_date, '-numberOfOutputFiles':'-1',
    '-suggest':'true', 
    '-input':'hdfs.arrow.bluekai_enriched', 
    '-esResourceName':'bluekai-enriched-vVERSION',
    '-esSuggestResourceName':'arrow-suggest-vVERSION/suggest',     
    '-esChildFields':('product_title,movie_title,series_title,video_game_title,comic_book_title,product_type,movie_talent,rating,release_window,release_year,'
                      'movie_genres,series_genres,campaign,person,browser_language,website_section,event_type,siterollup,device_type,topic,article_title,'
                      'video_title,dc_site_property,imdb_keywords,franchise_characters,catman_primary_brand,catman_franchise,catman_brands,mm_zipcode,dma_name,'
                      'box_office,theater_release_date,performance_dm,tickets,tomatometer,release_window_days,mm_country_code,transaction_date,person,'
                      'cast_top_1,cast_top_3,cast_top_5,cast_top_10,series_cast_top_1,series_cast_top_3,series_cast_top_5,series_cast_top_10,time_stats,'
                      'compositekey,maid_stitch,email_address_stitch,uid_stitch,fandango_id,flixster_id,client_ip,email_address,maid,uid,wbcid')
    }

dag_helper =  EMR(job_name, schedule, jar, airflow_aws_connection_id)

dag_helper.add_step('canonicalize_bluekai', 'com.wb.event.Bluekai', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfOutputFiles':'128'})
dag_helper.add_step('id_map', 'com.wb.historical.IDMapper', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfPartitions':'128'})
dag_helper.add_step('frequency_counter', 'com.wb.historical.FrequencyCounter', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfPartitions':'128'})
dag_helper.add_step('id_list', 'com.wb.historical.IDList', files, spark_confs, job_args = {'-numberOfPartitions':'128'})
dag_helper.add_step('ip_blacklist', 'com.wb.historical.IPBlacklist', files, spark_confs, job_args = {'-numberOfPartitions':'128'})
dag_helper.add_step('bk_enrich', 'com.wb.enrich.bluekaienrich.BluekaiEnrich', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfPartitions':'256', '-numberOfOutputFiles':'256'})
dag_helper.add_step('load_to_es', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = bk_enrich_es_args)

dag = dag_helper.get_emr_dag(properties)
