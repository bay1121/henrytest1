from airflow.models import DAG
from emr.emr import EMR
from common import properties
from common.properties import files, spark_confs, jar, run_date, airflow_aws_connection_id

job_name = 'arrow_historical'
schedule = "0 8 * * *"

es_args = {'-rundate':run_date, '-numberOfOutputFiles':'-1',
    '-suggest':'true', 
    '-input':'hdfs.arrow.historical.purchase.movie', 
    '-esResourceName':'arrow-historic-vVERSION-purchase',
    '-esSuggestResourceName':'arrow-suggest-vVERSION/suggest',     
    '-esChildFields':('product_title,movie_title,series_title,video_game_title,comic_book_title,product_type,movie_talent,rating,release_window,release_year,'
                      'movie_genres,series_genres,campaign,person,browser_language,website_section,event_type,siterollup,device_type,topic,article_title,'
                      'video_title,dc_site_property,imdb_keywords,franchise_characters,catman_primary_brand,catman_franchise,catman_brands,mm_zipcode,dma_name,'
                      'box_office,theater_release_date,performance_dm,tickets,tomatometer,release_window_days,mm_country_code,transaction_date,person,'
                      'cast_top_1,cast_top_3,cast_top_5,cast_top_10,series_cast_top_1,series_cast_top_3,series_cast_top_5,series_cast_top_10,time_stats,'
                      'compositekey,maid_stitch,email_address_stitch,uid_stitch,fandango_id,flixster_id,client_ip,email_address,maid,uid,wbcid')
    }

rating_movie_es_update = {'-input':'hdfs.arrow.historical.rating.movie',  '-esResourceName':'arrow-historic-vVERSION-rating'}
rating_tv_es_update = {'-input':'hdfs.arrow.historical.rating.tv',  '-esResourceName':'arrow-historic-vVERSION-rating'}

ma_es_fields = {
    '-esChildFields':('wbcid,ma_user_id,event_type,transaction_date,maid,device_type,dma_code,mm_country_code,mm_zipcode,email_address,library_count,'
                   'movie_title,movie_genres,content_viewed_percent,content_type,content_viewed_date,library_release_composition,'
                   'library_content_composition,library_genre_composition,siterollup,box_office,tomatometer,cast_top_1,cast_top_3,cast_top_5,'
                   'cast_top_10,movie_talent,theater_release_date,release_year,rating,studio,director,imdb_keywords,catman_franchise,catman_primary_brand,'
                   'catman_brands,franchise_characters,release_window_days,release_window,compositekey'),
    '-esSuggestFields':('content_type,mm_country_code,device_type,event_type,movie_genres,movie_title,transaction_date,mm_zipcode,'
                     'siterollup,cast_top_1,cast_top_3,cast_top_5,cast_top_10,movie_talent,'
                     'theater_release_date,release_year,rating,studio,director,imdb_keywords,catman_franchise,catman_primary_brand,catman_brands,'
                     'franchise_characters,release_window')
    }
ma_transactions_es_update = {'-input':'hdfs.arrow.historical.ma.transactions',  '-esResourceName':'arrow-ma-vVERSION'}
ma_consumption_es_update = {'-input':'hdfs.arrow.historical.ma.consumption',  '-esResourceName':'arrow-ma-vVERSION'}


dag_helper =  EMR(job_name, schedule, jar, airflow_aws_connection_id)

dag_helper.add_step('fandango_purchase', 'com.wb.historical.FandangoPurchase', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfOutputFiles':'16'})
dag_helper.add_step('load_to_es_purchase', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

dag_helper.add_step('rating', 'com.wb.historical.Rating', files, spark_confs, job_args = {'-rundate':run_date, '-startDate':'20090801', '-numberOfOutputFiles':'16'})

es_args.update(rating_movie_es_update)
dag_helper.add_step('load_to_es_rating_movie', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

es_args.update(rating_tv_es_update)
dag_helper.add_step('load_to_es_rating_tv', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

dag_helper.add_step('movies_anywhere', 'com.wb.historical.moviesanywhere.MoviesAnywhere', files, spark_confs, job_args = {'-rundate':run_date})

es_args.update(ma_es_fields)
es_args.update({'-bulkIndex': None})
es_args.update(ma_transactions_es_update)
dag_helper.add_step('load_to_es_ma_transactions', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

es_args.update(ma_consumption_es_update)
dag_helper.add_step('load_to_es_ma_consumption', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

dag = dag_helper.get_emr_dag(properties)
