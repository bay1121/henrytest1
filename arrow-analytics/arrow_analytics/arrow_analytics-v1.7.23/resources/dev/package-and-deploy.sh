#!/usr/bin/env bash

set -e

trap 'printf "\n\nINSTALL FAILED !!!!!!\n"' INT TERM QUIT

BASE_DIR=`dirname "$0"`

BASE_LOC=s3://dev-cmdt-software/bdd/arrow/arrow_analytics
BASE_LOC_VERSION=$BASE_LOC/1.7.23

JAR=arrow_analytics-v1.7.23-jar-with-dependencies.jar

aws s3 cp $BASE_DIR/../../$JAR ${BASE_LOC_VERSION}/${JAR} --sse
aws s3 cp $BASE_DIR/s3/application.conf ${BASE_LOC_VERSION}/application.conf --sse
aws s3 cp $BASE_DIR/s3/reference.conf ${BASE_LOC_VERSION}/reference.conf --sse
aws s3 cp $BASE_DIR/s3/bootstrap.sh ${BASE_LOC_VERSION}/bootstrap.sh --sse

pushd $BASE_DIR/airflow

zip -r arrow_airflow_dags.zip *

popd

mv $BASE_DIR/airflow/arrow_airflow_dags.zip .

echo "scp the arrow_airflow_dags.zip to the airflow dag location"

trap - INT TERM QUIT

printf "\n\nINSTALL SUCCCESSFUL arrow_analytics-v1.7.23\n"
