from airflow.models import DAG
from emr.emr import EMR
from common import properties
from common.properties import files, spark_confs, jar, run_date, airflow_aws_connection_id

job_name = 'arrow_historical_others'
schedule = "0 7 * * *"

es_args = {'-rundate':run_date, '-numberOfOutputFiles':'-1',
    '-suggest':'true',
    '-input':'hdfs.arrow.historical.dcu.dcuevents',
    '-esResourceName':'arrow-dcu-vVERSION',
    '-esSuggestResourceName':'arrow-suggest-vVERSION/suggest',
    '-esChildFields':('wbcid,app_name,device_manufacture,device_model,device_name,device_type,ingest_time,event_name,load_time,dcu_asset_name,dcu_asset_type,'
                      'cta_text,campaign,video_position,video_total_length,video_language,product_review,transaction_date,mm_subdivision_name,mm_subdivision_iso_code,mm_country_code,'
                      'mm_country_name,mm_city,mm_zipcode,mm_continent,dma_code,dma_name,uid,website_section,maid,date_joined,email_address,premium_status,premium_state,merchant_type,'
                      'merchant_plan_id,merchant_plan_name,product_type,product_title,watched_site,series_title,tv_rating,mpaa_rating,network,series_start_year,series_end_year,video_title,time_stats,'
                      'event_type,comics_genre,comic_book_title,movie_title,article_title,rating,release_year,custom_movie_person,custom_tv_person,talent,sitename,siterollup,is_cookie_statistical,compositekey'),
    '-esSuggestFields':('device_type,dma_name,dcu_asset_name,dcu_asset_type,product_type,product_title,premium_status,premium_state,comics_genre,series_title,'
                        'tv_rating,rating,event_type,comic_book_title,movie_title,article_title,mm_country_code,mm_zipcode,custom_movie_person,custom_tv_person,talent,sitename,siterollup'),
    '-bulkIndex': None
    }


atom_es_fields = {
    '-esChildFields':('wbcid,maid_hash,event_type,movie_title,production_id,tickets,engages,transaction_date,load_time,time_stats,sitename,siterollup,compositekey'),
    '-esSuggestFields':('event_type,movie_title,sitename,siterollup')
    }

atom_tickets_es_update = {'-input':'hdfs.arrow.historical.atomtickets.action',  '-esResourceName':'arrow-atom-tickets-vVERSION'}


dag_helper =  EMR(job_name, schedule, jar, airflow_aws_connection_id)

dag_helper.add_step('dcu_build_maps', 'com.wb.historical.dcu.BuildMaps', files, spark_confs, job_args = {'-rundate':run_date, '-daysAgo':'180', '-numberOfPartitions':'256'})
dag_helper.add_step('dcu_events', 'com.wb.historical.dcu.DCUEvents', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfPartitions':'256', '-numberOfOutputFiles':'256'})
dag_helper.add_step('load_to_es_dcu', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

es_args.update(atom_es_fields)
es_args.update(atom_tickets_es_update)
dag_helper.add_step('atom_tickets_actions', 'com.wb.historical.AtomticketsAction', files, spark_confs, job_args = {'-rundate':run_date, '-numberOfPartitions':'256'})
dag_helper.add_step('load_to_es_atom_tickets', 'com.wb.egress.BluekaiOutputToES', files, spark_confs, job_args = es_args)

dag = dag_helper.get_emr_dag(properties)
