rm -r ~/max

echo "mkdir ~/max && cd ~/max"
mkdir ~/max && cd ~/max

today=$1

# gets this file
echo "pulling file"

echo 'wget https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz&license_key=81zISQIkXVCL'
wget "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz&license_key=81zISQIkXVCL" -O GeoIP2-City.tar.gz

echo 'wget "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz.md5&license_key=81zISQIkXVCL"'
wget "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz.md5&license_key=81zISQIkXVCL" -O GeoIP2-City.tar.gz.md5

#Checksum not working as .md5 file doesn't have file name in it.
#Add file name(GeoIP2-City.tar.gz) beside checksum AFTER TWO SPACES
echo "  GeoIP2-City.tar.gz" >> GeoIP2-City.tar.gz.md5

#Checksum compare
md5sum -c GeoIP2-City.tar.gz.md5

echo unzip GeoIP2-City.tar.gz
tar -xvzf GeoIP2-City.tar.gz

#extracting the date when the file was updated
city_dt="$(ls -d */| grep GeoIP2-City| cut -f2 -d "_")"
echo $city_dt

echo 'Copy the city MMdb file'

aws s3 cp --sse AES256 --region us-west-2 GeoIP2-City_$city_dt/GeoIP2-City.mmdb ${arrow.base}/metadata/maxmind/City/"${city_dt::-1}"/GeoIP2-City_${city_dt::-1}.mmdb --sse

echo 'Copy the City MMdb file to latest'

aws s3 cp --sse AES256 --region us-west-2 GeoIP2-City_$city_dt/GeoIP2-City.mmdb ${arrow.base}/metadata/maxmind/City/latest/GeoIP2-City.mmdb --sse

echo 'Archive the full folder for later lookups'

aws s3 cp --recursive --sse AES256 --region us-west-2 GeoIP2-City_$city_dt ${arrow.base}/metadata/maxmind/City/GeoIP2-City_${city_dt::-1}/
rm -r ~/max
