#!/bin/bash

# This config script is installed as an indepenent part of the arrow project

BASE_LOC=${s3.emrfs.base.loc}
EMRFS_LIB=/usr/share/aws/emr/emrfs/auxlib

sudo mkdir -p $EMRFS_LIB
sudo aws s3 cp $BASE_LOC/emrfs-auth-v${project.version}.jar $EMRFS_LIB
