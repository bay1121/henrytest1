#!/bin/bash

ROOT_LOC=${s3.base.loc}

BASE_LOC=$ROOT_LOC/${project.name}
BASE_LOC_VERSION=$BASE_LOC/${project.version}
JAR=${assembly.final.name}.jar

aws s3 cp ${BASE_LOC_VERSION}/application.conf /home/hadoop/application.conf
aws s3 cp ${BASE_LOC_VERSION}/reference.conf /home/hadoop/reference.conf
aws s3 cp ${BASE_LOC_VERSION}/${JAR} /home/hadoop/${JAR}

