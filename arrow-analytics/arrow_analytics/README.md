TEST 113
# Arrow Analytics
## Building, Testing, and Deploying

### Unit and Integration Tests
All tests are located in `src/main/test`. Unit tests are written in the typical way where they do not include running an actual spark session, and only test specific methods and classes in isolation. Integration tests are used to ensure end-to-end correctness within the spark environment. A good reference for creating integration tests can be found in `src/main/test/scala/enrich/tvseries/` and `src/main/test/scala/historical/moviesanywhere/` 

### Build and Run Tests ###
```
mvn clean test
```

### Code Coverage ###
```
mvn clean scoverage:report
```

This will create the file `arrow-analytics/prod/arrow_analytics/target/site/scoverage/index.html` that you can throw into firefox. If you just want to see a basic overall amount, then you can just run:

```
mvn clean scoverage:check
```


### Build And Deploy ###

* "mvn clean package" results in a arrow*.tgz in the target directory
* Untar the tgz and execute "sh arrow_analytics-v<version>/ENV/package-and-deploy.sh"
  * The ENV is one of {dev, prod} for dev and prod environments respectively
  * The deploy script copies the jar and configuration files to a s3 location (make sure you have aws-credentials set-up for the s3 location)
  * The deploy script creates a arrow_airflow_dags.zip 
* Copy this arrow_airflow_dags.zip to the "airflow server - dags location(/opt/airflow/dags/wb-ci-arrow/")
* The build has been parameterized heavily as part of the airflow changes, all environment specific paramenters are now present in deploy/<ENV>.config.properties, these get into the airflow properties, refereence.conf files through maven resource filtering. The arrow_analytic application properties are now abstracted into application.conf and get resolved by the typesafe library from a reference.conf

* All of the above steps should be run from Jenkins for CI-CD. The Dev team should have write access to the s3 location for the project artifacts, the EMR cluster has read access to the s3 location for project artifacts, the bootstrap.sh script loads the project artifacts to each machine in the emr cluster

#### Properties

 * They are located in "deploy", Properties specify to ENV are located in
   * dev.config.properties
   * prod.config.properties
 * Maven filtering is used for property substitution.
 * The properties are rendered as a "application.config" and "reference.config" (these are automatically loaded from classpath by the typesafe.config utility.
 * The application.config and reference.config files are made available on the spark classpath by adding the /home/hadoop location to EMR classpath. This is specified in the EMR - Classification - Configuration (spark-env) as "HADOOP\_CONF\_DIR":"/etc/hadoop/conf:/home/hadoop". This is part of deploy/airflow/emr/helper.py . This can be verified on the EMR GUI where the json configuration would be

 ``` json
 [{"classification":"spark","properties":{"maximizeResourceAllocation":"true"}},{"configurations":[{"classification":"export","properties":{"HADOOP_CONF_DIR":"/etc/hadoop/conf:/home/hadoop"}}],"classification":"spark-env","properties":{}}]
 ```

### Manual testing of the spark Jobs ###
* From the AWS console, view the emr clusters, filter the arrow cluster, click on a cluster to view the details, click on the "steps" tab to view the individual spark steps, expand the step that has to be manually tested and copy the spark-submit command that is shown on the screen. SSH to the master node, the jar file is the /home/hadoop location, scp a new jar if there is one, submit the copied spark-submit command

* Another way is clone a cluster that has already run, edit the steps to just include the step that has to be tested and run that step. In this case the step just starts running without any user control.

* Create a EMR cluster, ssh to masternode, scp the required artifacts, have fun.

## S3 ##

You may want to do some ad-hoc exploration of S3 data. Given that most of our source data is compressed with parquet, and requires authentication to access, here are the steps to read it into Spark SQL:
```
spark-shell ./arrow_analytics-vVERSION-jar-with-dependencies.jar --conf spark.hadoop.fs.s3a.security.credential.provider.path=jceks://hdfs/arrow/hadoop/resources/arrow.jceks --conf spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256 --conf spark.hadoop.fs.s3a.enableServerSideEncryption=true
```

Within the `spark-shell`:
```
val hadoopConfiguration = spark.sparkContext.hadoopConfiguration
hadoopConfiguration.set("fs.s3a.enableServerSideEncryption", "true")
hadoopConfiguration.set("fs.s3a.server-side-encryption-algorithm", "AES256")
val user = spark.read.parquet("s3a://path/to/your/parquet/dir")
``` 

## Data Model

The analytics utilize a series of sql-like Dataframes that are based off parquet files. It is often useful to understand the inputs and outputs of any given analytic. Here is one method to accomplish that, using `BluekaiEnrich` as an example:

1. First, let's find the corresponding submitScript located in `scheduled-jobs/oozie-jobs/processBluekai/submitBluekaiEnrich.sh` That will tell us the input and output locations.
2. For input, I will use `s3://cmdt-datatree/processed/arrow/output/prod/bluekai/date=2018-09-09/part-00107-250d38d1-f819-40fd-8ea7-92631eb64581-c000.gz.parquet` as an example
3. I will go into a spark shell and run the commands to initialize S3 reading (as outlined in the S3 section above)
4. Then I grab the schema to understand the Data Model like this:
```
scala> spark.read.parquet("s3a://cmdt-datatree/processed/arrow/output/prod/bluekai/date=2018-09-09/part-00107-250d38d1-f819-40fd-8ea7-92631eb64581-c000.gz.parquet").printSchema
root
 |-- compositekey: string (nullable = true)
 |-- record: string (nullable = true)
 |-- op: map (nullable = true)
 |    |-- key: string
 |    |-- value: string (valueContainsNull = true)
 |-- uid: string (nullable = true)
 |-- categories: string (nullable = true)
 |-- url: string (nullable = true)
 |-- client_ip: string (nullable = true)
 |-- site_id: string (nullable = true)
 |-- phints: map (nullable = true)
 |    |-- key: string
 |    |-- value: string (valueContainsNull = true)
 |-- time_stats: map (nullable = true)
 |    |-- key: string
 |    |-- value: string (valueContainsNull = true)

```

_NOTE:_ The same process can be used to obtain the output schema, as well as getting the data model for the other analytics


## DCU events

### Building the map
The maps will be built before running the DCU events. It takes the maps from the date before the rundate as input and apply new mappings generated from bluekai of rundate, then output as maps of rundate. Such as,
If we want to generate maps from previous date of rundate:

```
spark-submit --class com.wb.historical.dcu.BuildMaps --conf spark.hadoop.fs.s3.enableServerSideEncryption=true --conf spark.hadoop.fs.s3a.enableServerSideEncryption=true
--conf spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256 --conf spark.driver.maxResultSize=4g --conf spark.sql.shuffle.partitions=1024
--conf spark.sql.parquet.compression.codec=gzip --conf spark.hadoop.fs.s3.customAWSCredentialsProvider=com.wb.aws.security.CustomAWSSecurityProvider
--files /home/hadoop/application.conf,/home/hadoop/reference.conf /home/hadoop/arrow_analytics-v1.7.25-jar-with-dependencies.jar
-numberOfPartitions 256 -daysAgo 180 -rundate 20190717

```

Or if we want to generate maps from start date till the rundate:

```
spark-submit --class com.wb.historical.dcu.BuildMaps --conf spark.hadoop.fs.s3.enableServerSideEncryption=true --conf spark.hadoop.fs.s3a.enableServerSideEncryption=true
--conf spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256 --conf spark.driver.maxResultSize=4g --conf spark.sql.shuffle.partitions=1024
--conf spark.sql.parquet.compression.codec=gzip --conf spark.hadoop.fs.s3.customAWSCredentialsProvider=com.wb.aws.security.CustomAWSSecurityProvider
--files /home/hadoop/application.conf,/home/hadoop/reference.conf /home/hadoop/arrow_analytics-v1.7.25-jar-with-dependencies.jar
-numberOfPartitions 256 -daysAgo 180 -rundate 20190717 -startDate 20190601

```

### Running DCU events
Normal day to day running command will include rundate, and dateField as optional which means which date of maps to use to apply to the DCU events, by default it uses rundate.
When back filling, the programm will bypass loading of bluekai data, so don't include a rundate, and make dateField required (and choose the latest date available of maps for more joins), such as:
```
spark-submit --class com.wb.historical.dcu.DCUEvents --conf spark.hadoop.fs.s3.enableServerSideEncryption=true --conf spark.hadoop.fs.s3a.enableServerSideEncryption=true
--conf spark.hadoop.fs.s3a.server-side-encryption-algorithm=AES256 --conf spark.driver.maxResultSize=4g --conf spark.sql.shuffle.partitions=1024 --conf spark.sql.parquet.compression.codec=gzip
--conf spark.hadoop.fs.s3.customAWSCredentialsProvider=com.wb.aws.security.CustomAWSSecurityProvider --files /home/hadoop/application.conf,/home/hadoop/reference.conf
/home/hadoop/arrow_analytics-v1.7.25-jar-with-dependencies.jar -numberOfOutputFiles 256 -numberOfPartitions 256 -dateField 20190717

```

### Issue when pushing all DCU events to AWS ES (such as when back filling)
The backfill of DCU can result huge numbers, such as ~100 million records by 07/17/2019, and the number will grow in the future when a backfill is needed again. Push to our existing ES wont be an issue so far,
however there might be issues when pushing to AWS Elasticsearch service that we are planning to move over. Records will be just disregarded when bulk pool size has reached limit. We can either increase the number
of nodes or use larger instance type. If we can't increase the AWS ES's capacity, then we can also use these 2 options of ES to prevent data loss:

```
childOptions += "es.batch.write.retry.wait" -> OutputToESCLIArgs.esBatchWriteRetryWait
childOptions += "es.batch.write.retry.count" -> OutputToESCLIArgs.esBatchWriteRetryCount

```

More information can be found in the test brach here:
https://bitbucket.org/wbcidata/arrow-analytics/src/henry-connect-aws-es/


## Atomtickets
Atomtickets will load all the actions for each run, and then filter the events only newer than the 'atom.tickets.lookback.days' specified in the application.conf, so when backfilling, we can adjust this to a proper number.
By default is 10 days.
